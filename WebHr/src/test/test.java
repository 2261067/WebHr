package test;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;


public class test {

	public static String downLoadHtml(String url) {
		// TODO Auto-generated method stub
		BasicCookieStore cookieStore = new BasicCookieStore();
		BasicClientCookie cookie = new BasicClientCookie("userid", "abc");
		cookie.setDomain("localhost");
		cookie.setPath("/");
		cookieStore.addCookie(cookie);
		BasicClientCookie cookie2 = new BasicClientCookie("userpassword", "123");
		cookie2.setDomain("localhost");
		cookie2.setPath("/");
		cookieStore.addCookie(cookie2);
		DefaultHttpClient client = new DefaultHttpClient();
		client.setCookieStore(cookieStore);
		HttpGet httpGet = new HttpGet(url);
		try {
//			HttpResponse response = MyHttp.getResponse(client, httpGet);
			HttpResponse response = client.execute(httpGet);
			byte[] bytes = MyHttp.getBytes(response);
			String html = new String(bytes, "utf-8");
			return html;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// log.error("生成html失败");
		} finally {
			httpGet.releaseConnection();
		}

		return null;
	}

	public static void main(String[] args) {
		try {
			Thread.sleep(10000);
			int i = 0;
			while (i < 1000) {
				String urlpreffix = "http://localhost:8080/WebHr/inbox/save.do?";
				String urlParams = 
						"receive_name=wq&message=werwer345345";
				System.out.println(downLoadHtml(urlpreffix + urlParams));
				Thread.sleep(100);
				i++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
