package com.proj.module.searchEngine;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;












import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LogByteSizeMergePolicy;
import org.apache.lucene.index.LogMergePolicy;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.proj.utils.ConfigProperties;

public class DefaultLuceneIndex extends AbstractLuceneIndex{
	LuceneDocumentTool luceneDocument=new LuceneDocumentTool();
	static  ConfigProperties config =LuceneConfig.config;
	public DefaultLuceneIndex(Directory defaultDir,Analyzer defaultAnalyzer){
		
		try {
			// 索引文件的保存位置
			 dir = defaultDir;
			// 分析器(分词器)
			 analyzer = defaultAnalyzer;
			// 配置类
			iwc = new IndexWriterConfig(Version.LATEST, analyzer);
			iwc.setMergePolicy(optimizeIndex());
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);// 创建模式 OpenMode.CREATE_OR_APPEND 添加模式
			//如果有锁，先释放
			//if(IndexWriter.isLocked(dir))
			try{
            IndexWriter.unlock(dir);
            File lockFile=new File(LuceneConfig.getDefaultIndexPath()+"/write.lock");

            if(lockFile.exists())
            	lockFile.delete();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			writer = new IndexWriter(dir, iwc);
			writer.commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 优化索引，返回优化策略
	 * 
	 * @return
	 */
	private static LogMergePolicy optimizeIndex() {
		LogMergePolicy mergePolicy = new LogByteSizeMergePolicy();

		// 设置segment添加文档(Document)时的合并频率
		// 值较小,建立索引的速度就较慢
		// 值较大,建立索引的速度就较快,>10适合批量建立索引
		// 达到50个文件时就和合并
		mergePolicy.setMergeFactor(50);

		// 设置segment最大合并文档(Document)数
		// 值较小有利于追加索引的速度
		// 值较大,适合批量建立索引和更快的搜索
		mergePolicy.setMaxMergeDocs(5000);

		// 启用复合式索引文件格式,合并多个segment
		
		return mergePolicy;
	}
	
	
	
	/* 加入一篇文档到lucene索引 ，文档的形式为键值对，比如("title","如何使用搜索引擎?")
	 * @see com.proj.module.searchEngine.IDocumentIndex#addDocumentIndex(java.util.HashMap)
	 */
	@Override
	public boolean addDocumentIndex(HashMap<String, String> document) throws Exception{
		if(document==null)return true;
		Document doc=luceneDocument.getDefaultDocument(document);
		
		writer.addDocument(doc);
		writer.commit();
		// TODO Auto-generated method stub
		return true;
	}
	public boolean addDocumentIndex(Document document) throws Exception{
		if(document==null)return true;
		
		
		writer.addDocument(document);
		// TODO Auto-generated method stub
		writer.commit();
		return true;
	}
	@Override
	public boolean deleteDocumentIndex(String documentId) {
		// TODO Auto-generated method stub
		Term term=new Term("id",documentId);
		try {
			this.writer.deleteDocuments(term);
			this.writer.commit();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}  
		return false;
	}

	@Override
	public boolean updateDocumentIndex(HashMap<String, String> document) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Analyzer getLuceneAnalyser() throws Exception {
		// TODO Auto-generated method stub
		//Analyzer analyzer = LuceneContext.getContextInstance().getAnalyzer();
		return null;
		
	}

	@Override
	public Directory getIndexDir() throws Exception {
		// TODO Auto-generated method stub

		
		return null;
	}
	public void close() throws IOException {
		// TODO Auto-generated method stub
		this.writer.commit();
		this.writer.close();
		
	}

	@Override
	public boolean deleteDocumentIndex(HashMap<String, String> conditions) throws Exception{
		// TODO Auto-generated method stub
		BooleanQuery condition=new  BooleanQuery();
		if(condition instanceof BooleanQuery){
			Iterator iter = conditions.entrySet().iterator();
			while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			Object key = entry.getKey();
			Object val = entry.getValue();
			TermQuery tQuery=new TermQuery(new Term((String)key, (String)val));
		((BooleanQuery) condition).add(tQuery,org.apache.lucene.search.BooleanClause.Occur.MUST);
		}
	   }

		this.writer.deleteDocuments(condition);  
	         //这两句一定要执行  
		  this.writer.commit();;  
		  
		return true;
	}

}
