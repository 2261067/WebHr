package com.proj.module.searchEngine;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.proj.utils.ConfigProperties;

public class DefaultLuceneSearcher implements ISearcher {
	
	private IndexSearcher searcher = null;
	
	private Query query = null;
	private Analyzer defaultAnalyzer = null;
	private String field = "contents";
	//默认搜索并计算得分的字段(一般为每个文档的标题和内容字段"contents","title")
	private String[] defaultFields = {"contents","title"};
	int defaultPageSize=10;
	// public final static String INDEX_STORE_PATH = "g:/luceneTest/index"; //
	Thread timerSeacherThread;
	ConfigProperties config = LuceneConfig.config;
	QueryParser defaultParser;

	/**该类为一个定时器，每隔一段时间重新打开lucene索引类，用于实时搜索
	 * @author ZTW
	 *
	 */
	class SearcherTimer implements Runnable {
		//重新打开索引的时间间隔
		long timeInterval = 300000;
		Date lastChangeIndexTime = new Date();

		public void run() {
			while (!Thread.interrupted()) {
				Date now = new Date();
				synchronized (this) {
					long nowLong = now.getTime();
					long lastLong = lastChangeIndexTime.getTime();
					long minus = nowLong - lastLong;
					if (minus > timeInterval) {
						IndexReader reader;
						try {
							reader = DirectoryReader.open(LuceneContext
									.getContextInstance().getIndexDir());
							searcher = new IndexSearcher(reader);
							System.out.print("索引重新打开！");
							lastChangeIndexTime = new Date();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

	}

	public DefaultLuceneSearcher(Analyzer analyzer, QueryParser parser) {
		try {
			IndexReader reader = DirectoryReader.open(LuceneContext
					.getContextInstance().getIndexDir());
			searcher = new IndexSearcher(reader);

			//parser非线程安全
			if (parser == null)
				parser = new MultiFieldQueryParser(defaultFields, analyzer);
			if (defaultAnalyzer == null)
				this.defaultAnalyzer = new StandardAnalyzer();
			this.defaultParser = parser;
			this.defaultAnalyzer = analyzer;
			SearcherTimer st = new SearcherTimer();
		    timerSeacherThread=new Thread(st);
			timerSeacherThread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	public TopDocs search(String keyword, int page) {
		return search( keyword,  page ,defaultPageSize);
	}
	//
	/**根据keyword搜索默认的域defaultFields，返回结果
	 * @param keyword
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public TopDocs search(String keyword, int page,int pageSize) {

		try {
			// 为了防止重新打开索引时出现线程安全和引用问题，这里用局部变量
			IndexSearcher tempSearcher = searcher;
			//
			synchronized (defaultParser) {
				query = defaultParser.parse(keyword);
			}
			int start = (page - 1) * pageSize;
			int hm = start + pageSize;
            TopScoreDocCollector res = TopScoreDocCollector.create(hm, false);
            searcher.search(query, res);
 
            
            
            int rowCount = res.getTotalHits();
            int pages = (rowCount - 1) / pageSize + 1; //计算总页数
            TopDocs results = res.topDocs(start, pageSize);
			

			// Date end = new Date();
			// System.out.println("妫?储瀹屾垚锛岀敤鏃? + (end.getTime() - start.getTime()) + "姣");
			return results;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//该方法加入一些域强制匹配限制
	/**该方法根据一些域查询的限制返回结果，比如限制"title"域必须包含"搜索"关键词，比如限制"author"必须为"张XX"
	 * fields[]  conditionQueries[]  flags[]为一一对应的，方法实现里暂时没用flags，都设置flag为must。
	 * @param keyword
	 * @param fields
	 * @param conditionQueries
	 * @param flags
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public TopDocs search(String keyword, String fields[],String conditionQueries[],String flags[],int page,int pageSize) {
		try {
			// 为了防止重新打开索引时出现线程安全和引用问题，这里用局部变量
			IndexSearcher tempSearcher = searcher;
			//
			synchronized (defaultParser) {
				query = defaultParser.parse(keyword);
			}
			BooleanQuery condition=new  BooleanQuery();
			if(condition instanceof BooleanQuery){
				for(int i=0;i<fields.length;i++){
					if(fields[i]!=null&&conditionQueries[i]!=null){
						
					TermQuery tQuery=new TermQuery(new Term(fields[i], conditionQueries[i]));tQuery.setBoost(0);
				((BooleanQuery) condition).add(tQuery,org.apache.lucene.search.BooleanClause.Occur.MUST);
				
					}
				}
			}
			Filter filter=new QueryWrapperFilter(condition);
			int start = (page - 1) * pageSize;
			int hm = start + pageSize;
            TopScoreDocCollector res = TopScoreDocCollector.create(hm, false);
            searcher.search(query,filter, res);
 
            
            
            int rowCount = res.getTotalHits();
            int pages = (rowCount - 1) / pageSize + 1; //计算总页数
            TopDocs results = res.topDocs(start, pageSize);
			

			// Date end = new Date();
			// System.out.println("妫?储瀹屾垚锛岀敤鏃? + (end.getTime() - start.getTime()) + "姣");
			return results;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	// 鎵撳嵃缁撴灉
	public void printResult(TopDocs results) {
		ScoreDoc[] h = results.scoreDocs;
		if (h.length == 0) {
			System.out.println("瀵逛笉璧凤紝娌℃湁鎵惧埌鎮ㄨ鐨勭粨鏋溿?");
		} else {
			for (int i = 0; i < h.length; i++) {
				try {
					Document doc = searcher.doc(h[i].doc);
					// System.out.print("杩欐槸绗? + i + "涓绱㈠埌鐨勭粨鏋滐紝鏂囦欢鍚嶄负锛?);
					System.out.println(doc.get("contents"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("--------------------------");
	}

	/**将搜索结果转换为hashmap返回
	 * @param results
	 * @return
	 */
	public List returnResult(TopDocs results) {

		List<Map> resultList = new LinkedList<Map>();
		if (results == null || results.scoreDocs == null)
			return resultList;
		ScoreDoc[] h = results.scoreDocs;
		if (h.length == 0) {

		} else {
			for (int i = 0; i < h.length; i++) {
				try {
					Document doc = searcher.doc(h[i].doc);
					List<IndexableField> fields = doc.getFields();
					Map<String, String> fieldMap = new HashMap<String, String>();
					for (IndexableField f : fields) {
						fieldMap.put(f.name(), f.stringValue());
					}

					resultList.add(fieldMap);
				} catch (Exception e) {
					return resultList;
				}

			}
		}

		return resultList;
	}

	@Override
	public List<Object> searchByQueryString(String query, int page) {
		TopDocs midResults = search(query, page);

		return returnResult(midResults);
	}
	
	@Override
	public List<Object> searchByQueryString(String query, int page, int pageSize) {
		TopDocs midResults = search(query, page,pageSize);

		return returnResult(midResults);
	}
	@Override
	public List<Object> searchByQueryString(String query, String fields[],String conditionQueries[],String flags[],int page, int pageSize) {
		TopDocs midResults = search( query,  fields, conditionQueries, flags, page,  pageSize);

		return returnResult(midResults);
	}



	@Override
	public void close() {
		// TODO Auto-generated method stub
		timerSeacherThread.interrupt();
	}
}