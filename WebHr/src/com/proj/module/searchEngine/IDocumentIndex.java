package com.proj.module.searchEngine;

import java.util.HashMap;

public interface IDocumentIndex {
	
	public boolean addDocumentIndex(HashMap<String,String> document) throws Exception;
	public boolean deleteDocumentIndex(String documentId);
	public boolean deleteDocumentIndex(HashMap<String,String> conditions) throws Exception;
	public boolean updateDocumentIndex(HashMap<String,String> document);
}
