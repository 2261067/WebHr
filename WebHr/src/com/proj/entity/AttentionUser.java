package com.proj.entity;

// Generated 2015-1-27 20:09:00 by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AttentionUser generated by hbm2java
 */
@Entity
@Table(name = "attention_user", catalog = "webhr")
public class AttentionUser implements java.io.Serializable {

	private Long id;
//	关注者
	private String userId;
//	被关注者
	private String attentionUserId;
	private Date createTime;

	public AttentionUser() {
	}

	public AttentionUser(String userId, String attentionUserId) {
		this.userId = userId;
		this.attentionUserId = attentionUserId;
	}

	public AttentionUser(String userId, String attentionUserId, Date createTime) {
		this.userId = userId;
		this.attentionUserId = attentionUserId;
		this.createTime = createTime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "user_id", nullable = false, length = 45)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "attention_user_id", nullable = false, length = 45)
	public String getAttentionUserId() {
		return this.attentionUserId;
	}

	public void setAttentionUserId(String attentionUserId) {
		this.attentionUserId = attentionUserId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", length = 19)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
