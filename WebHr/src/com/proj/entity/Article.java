package com.proj.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "article", catalog = "webhr")
public class Article implements java.io.Serializable {

	private String articleId;
	private String userId;
	private String articleName;
	private String articleContext;
	private Integer commentNum;
	private Integer important;
	private Date createTime;
	private Date lastModifyTime;

	public Article() {
		super();
	}

	public Article(String articleId, String userId, String articleName,
			Integer commentNum, Date createTime, Date lastModifyTime) {
		super();
		this.articleId = articleId;
		this.userId = userId;
		this.articleName = articleName;
		this.commentNum = commentNum;
		this.createTime = createTime;
		this.lastModifyTime = lastModifyTime;
	}

	public Article(String articleId, String userId, String articleName,
			String articleContext, Integer commentNum, Integer important,
			Date createTime, Date lastModifyTime) {
		super();
		this.articleId = articleId;
		this.userId = userId;
		this.articleName = articleName;
		this.articleContext = articleContext;
		this.commentNum = commentNum;
		this.important = important;
		this.createTime = createTime;
		this.lastModifyTime = lastModifyTime;
	}

	@Id
	@Column(name = "article_id", unique = true, nullable = false, length = 50)
	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	@Column(name = "user_id", length = 50)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "article_name", length = 100)
	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	@Column(name = "article_context", length = 20000)
	public String getArticleContext() {
		return articleContext;
	}

	public void setArticleContext(String articleContext) {
		this.articleContext = articleContext;
	}

	@Column(name = "comment_num")
	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	@Column(name = "important")
	public Integer getImportant() {
		return important;
	}

	public void setImportant(Integer important) {
		this.important = important;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "last_modify_time")
	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

}
