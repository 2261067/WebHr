package com.proj.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "admin_info", catalog = "webhr")
public class AdminInfo implements java.io.Serializable {
	
	private String adminId;
	private String email;
	private String password;
	private Date createTime;
	
	
	public AdminInfo() {
		super();
	}


	public AdminInfo(String adminId, String email, String password,
			Date createTime) {
		super();
		this.adminId = adminId;
		this.email = email;
		this.password = password;
		this.createTime = createTime;
	}

	@Id
	@Column(name = "admin_id", unique = true, nullable = false, length = 50)
	public String getAdminId() {
		return adminId;
	}


	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", nullable = false, length = 19)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
