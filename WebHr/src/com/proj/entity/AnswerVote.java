package com.proj.entity;

// Generated 2015-1-21 19:51:44 by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AnswerVote generated by hbm2java
 */
@Entity
@Table(name = "answer_vote", catalog = "webhr")
public class AnswerVote implements java.io.Serializable {

	private Long id;
	private String answerId;
	private String userId;
	private Byte voteType;
	private Date createTime;

	public AnswerVote() {
	}

	public AnswerVote(String answerId, String userId) {
		this.answerId = answerId;
		this.userId = userId;
	}

	public AnswerVote(String answerId, String userId, Byte voteType,
			Date createTime) {
		this.answerId = answerId;
		this.userId = userId;
		this.voteType = voteType;
		this.createTime = createTime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "answer_id", nullable = false, length = 45)
	public String getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	@Column(name = "user_id", nullable = false, length = 45)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "vote_type")
	public Byte getVoteType() {
		return this.voteType;
	}

	public void setVoteType(Byte voteType) {
		this.voteType = voteType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", length = 19)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
