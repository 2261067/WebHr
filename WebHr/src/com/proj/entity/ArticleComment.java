package com.proj.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "article_comment", catalog = "webhr")
public class ArticleComment implements java.io.Serializable {

	private String commentId;
	private String articleId;
	private String userId;
	private String commentContext;
	private Date createTime;

	public ArticleComment() {
		super();
	}

	public ArticleComment(String commentId, String articleId, String userId,
			String commentContext, Date createTime) {
		super();
		this.commentId = commentId;
		this.articleId = articleId;
		this.userId = userId;
		this.commentContext = commentContext;
		this.createTime = createTime;
	}

	@Id
	@Column(name = "comment_id", unique = true, nullable = false, length = 50)
	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	@Column(name = "article_id", length = 50)
	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	@Column(name = "user_id", length = 50)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "comment_context", length = 1000)
	public String getCommentContext() {
		return commentContext;
	}

	public void setCommentContext(String commentContext) {
		this.commentContext = commentContext;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
