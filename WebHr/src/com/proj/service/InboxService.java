package com.proj.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.annotation.CometInbox;
import com.proj.aspect.comet.ReturnType;
import com.proj.dao.InboxDao;
import com.proj.dao.UserInfoDao;
import com.proj.entity.Inbox;
import com.proj.entity.UserInfo;
import com.proj.utils.ConstantUtil;
import com.proj.utils.StringUtils;

@Service("inboxService")
public class InboxService {
	@Autowired
	private InboxDao inboxDao;

	@Autowired
	private UserInfoDao userInfoDao;
	
	public int getUserUnreadInboxNum(String userId) {
		return inboxDao.getUserUnreadInboxNum(userId);
	}

	public List<Inbox> getUserInboxMessage(String userId) {
		return inboxDao.getUserInboxMessage(userId);
	}

	public List<Inbox> getLatestUserInboxMessage(String userId){
		return inboxDao.getLatestUserInboxMessage(userId);
	}

	public List<Inbox> getUserUndeleteInteractionInboxMessage(String userId,
			String interacId) throws Exception {
		
		return inboxDao.getUserUndeleteInteractionMessage(userId, interacId);
	}

	public List<Inbox> deleteUserInteractionInboxMessage(String userId,
			String interacId) {
		List<Inbox> inboxs = inboxDao.getUserInteractionMessage(userId,
				interacId);
		// 检查deleteuser字段，如果存在则删除，若不存在，则加入该字段，不删除
		if (inboxs != null && !inboxs.isEmpty()) {
			for (Inbox inbox : inboxs) {
				if (StringUtils.isBlank(inbox.getDeleteUser())) {
					inbox.setDeleteUser(userId);
					inboxDao.update(inbox);
				} else if (!userId.equals(inbox.getDeleteUser())) {
					inboxDao.delete(inbox);
				}
			}
		}
		return inboxs;
	}

	// update私信状态为已读，仅针对别人发送过来的私信
	@CometInbox(returnType = ReturnType.INBOX)
	public List<Inbox> updateUserInteractionInboxMessageStatus(String userId,
			String interacId) {
		List<Inbox> inboxs = inboxDao.getUserUndeleteInteractionMessage(userId,
				interacId);
		if (inboxs != null && !inboxs.isEmpty()) {
			for (Inbox inbox : inboxs) {
				if (inbox.getUserId().equals(userId)) {
					if (inbox.getIsRead() != 1) {
						inbox.setIsRead((byte) 1);
						inboxDao.update(inbox);
					}
				}
			}
		}
		return inboxs;
	}

	@CometInbox(returnType = ReturnType.INBOX)
	public Inbox save(Inbox inbox) {
		inboxDao.save(inbox);
		return inbox;
	}
	
	public List<InboxAndFromUser> getInboxAndFromUser(List<Inbox> inboxs,String cuurentUserId){
		if(inboxs == null || inboxs.isEmpty())
			return null;
		List<InboxAndFromUser> inboxAndFromUsers = new LinkedList<InboxService.InboxAndFromUser>();
		for (Inbox inbox : inboxs) {
			UserInfo userInfo = null;
			if(!inbox.getFromId().equals(cuurentUserId)){
				userInfo = userInfoDao.isExistUserId(inbox.getFromId());
			}else {
				userInfo = userInfoDao.isExistUserId(inbox.getUserId());
			}
			 
			if(userInfo == null){
				userInfo = new UserInfo();
				userInfo.setUserId(ConstantUtil.SYSTEM_ID);
				userInfo.setNickName(ConstantUtil.SYSTEM_ID);
			}
			InboxAndFromUser inboxAndFromUser = new InboxAndFromUser(inbox, userInfo);
			inboxAndFromUsers.add(inboxAndFromUser);
		}
		return inboxAndFromUsers;
		
	}
	
	public class InboxAndFromUser{
		Inbox inbox;
		UserInfo fromUser;
		
		public InboxAndFromUser(Inbox inbox, UserInfo fromUser) {
			this.inbox = inbox;
			this.fromUser = fromUser;
		}
	}
}
