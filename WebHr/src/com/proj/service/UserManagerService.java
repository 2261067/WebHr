package com.proj.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.AdminInfoDao;
import com.proj.dao.ThirdPartLandingDao;
import com.proj.dao.UserInfoDao;
import com.proj.entity.AdminInfo;
import com.proj.entity.Role;
import com.proj.entity.ThirdPartLanding;
import com.proj.entity.UserInfo;
import com.proj.utils.ConstantUtil;
import com.proj.utils.EncrypAES;
import com.proj.utils.StringUtils;

@Service
public class UserManagerService {
	@Autowired
	private UserInfoDao userInfoDao;

	@Autowired
	private AdminInfoDao adminInfoDao;

	@Autowired
	private ThirdPartLandingDao thirdPartLandingdao;

	/**
	 * 查询普通用户账户是否存在
	 */
	public UserInfo userLogin(String userAccount, String passWord) {
		passWord=EncrypAES.getInstance().EncrytorToString(passWord);
		UserInfo current = null;
		current = userInfoDao.getUserByEmailAndPass(userAccount,
				passWord);
		if (current== null)
			current = userInfoDao.getUserByUidAndPass(userAccount, passWord);		
		return current;
	}

	/**
	 * 查询管理员账户是否存在
	 */
	public AdminInfo adminLogin(String adminAccount, String passWord) {
		passWord = EncrypAES.getInstance().EncrytorToString(passWord);
		AdminInfo current = null;
		AdminInfo adminid = adminInfoDao.getAdminByAidAndPass(adminAccount,
				passWord);
		AdminInfo email = adminInfoDao.getAdminByEmailAndPass(adminAccount,
				passWord);
		if (adminid != null)
			current = adminid;
		if (email != null)
			current = email;
		return current;
	}

	public Role userPermission(String userName) {
		return userInfoDao.getUserRoleByUidAndPass(userName);
	}

	public void saveUserInfo(UserInfo u) {
		userInfoDao.save(u);
	}

	public void saveAdminInfo(AdminInfo a) {
		adminInfoDao.save(a);
	}

	/*
	 * 根据uid返回用户信息
	 */
	public UserInfo peopleInfo(String uid) {
		UserInfo user = userInfoDao.get(uid);
		return user;
	}

	/*
	 * 查询用户注册或填写账户时的nickname是否存在
	 */
	public boolean isExistUserNickname(String nickName) {
		UserInfo user = userInfoDao.isExistUserNick(nickName);
		if (user == null)
			return false;
		return true;
	}

	/*
	 * 查询用户注册或填写账户时的uid是否存在
	 */
	public boolean isExistUserId(String uid) {
		UserInfo user = userInfoDao.isExistUserId(uid);
		if (user == null)
			return false;
		return true;
	}

	/*
	 * 查询用户注册或填写账户时的nick是否存在
	 */
	public UserInfo isExistUserNick(String nickName) {
		UserInfo user = userInfoDao.isExistUserNick(nickName);
		return user;
	}

	/*
	 * 查询管理员注册或填写账户时的uid是否存在
	 */
	public boolean isExistAdminId(String aid) {
		AdminInfo admin = adminInfoDao.isExistAdminId(aid);
		if (admin == null)
			return false;
		return true;
	}

	/*
	 * 查询用户注册或填写账户时的email是否存在
	 */
	public boolean isExistEmail(String email) {
		UserInfo user = userInfoDao.isExistEmail(email);
		if (user == null)
			return false;
		return true;
	}

	/*
	 * 查询管理员注册或填写账户时的email是否存在
	 */
	public boolean isExistAdminEmail(String email) {
		AdminInfo admin = adminInfoDao.isExistEmail(email);
		if (admin == null)
			return false;
		return true;
	}

	/*
	 * 忘记密码找回密码
	 */
	public String findpwd(String email) {
		UserInfo user = userInfoDao.findpwd(email);
		if (user == null)
			return "";

		return EncrypAES.getInstance().DecryptorToString(user.getPassword());
	}

	/*
	 * 修改密码
	 */
	public boolean updatePwd(UserInfo user, String pwd) {
		pwd = EncrypAES.getInstance().EncrytorToString(pwd);
		if (userInfoDao.updatePwd(user, pwd) == null)
			return false;
		return true;
	}

	/*
	 * 更新用户信息
	 */
	public boolean updateUserInfo(UserInfo user) {
		if (userInfoDao.updateUserInfo(user) == null)
			return false;
		return true;
	}

	// like查询相似用户
	public List<UserInfo> getSimilarUserInfos(String uid) {
		return userInfoDao.getSimilarUserInfos(uid);
	}

	public UserInfo getUserById(String userId) {
		return userInfoDao.get(userId);
	}

	public UserInfo getUserByEmail(String email) {
		return userInfoDao.isExistEmail(email);
	}

	/*
	 * 保存第三方登录信息
	 */
	public void saveThirdPartLanding(ThirdPartLanding tpl) {
		thirdPartLandingdao.saveThirdPartLanding(tpl);
	}

	/*
	 * 查询第三方账号是否登录过
	 */
	public boolean isExistqqpid(String pid) {
		ThirdPartLanding tpl = new ThirdPartLanding();
		tpl = thirdPartLandingdao.isExistqqpid(pid);
		if (tpl == null)
			return false;
		return true;
	}

	/*
	 * qq登录账户管理
	 */
	public UserInfo qqloginUserInfo(String pid, String qqname) {
		ThirdPartLanding tpl = thirdPartLandingdao.isExistqqpid(pid);
		UserInfo qquser = new UserInfo();
		if (tpl == null) {
			Date now = new Date();
			qquser.setUserId(qqRegister(qqname));
			qquser.setPhoto(ConstantUtil.RELATIVE_PATH + "default.jpg");
			qquser.setCreateTime(StringUtils.dateFormatTransform(now,
					ConstantUtil.DATE_FORMAT));
			ThirdPartLanding tplnew = new ThirdPartLanding();
			tplnew.setPid(pid);
			tplnew.setUser_id(qqRegister(qqname));
			thirdPartLandingdao.save(tplnew);
			saveUserInfo(qquser);
		} else {
			qquser = peopleInfo(tpl.getUser_id());
		}
		return qquser;
	}

	/*
	 * 为qq登录用户注册账号
	 */
	public String qqRegister(String qqname) {
		int count = 0;
		while (true) {
			if (!isExistUserId(qqname)) {
				return qqname;
			} else {
				qqname += (count++);
			}
		}
	}

	public List<UserInfo> getAllUserInfos() {
		return userInfoDao.getAllUserInfos();
	}

	public long getAllUserNum() {
		return userInfoDao.getAllUserNums();
	}

	// 获取所有用户列表
	public List<UserInfo> getAllUserInfo(int page, int pageSize) {
		return userInfoDao.getAllUserInfo(page, pageSize);
	}

	/*
	 * 管理员删除用户
	 */
	public boolean deleteUserById(String userId) {
		return userInfoDao.deleteById(userId);
	}
}
