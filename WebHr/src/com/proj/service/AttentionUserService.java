package com.proj.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.AttentionUserDao;
import com.proj.entity.AttentionUser;
import com.proj.entity.UserInfo;

@Service
public class AttentionUserService {
	@Autowired
	private AttentionUserDao attentionUserDao;

	public class AttentionUserAndUserInfo {
		AttentionUserAndUserInfo(AttentionUser attentionUser, UserInfo userInfo) {
			this.attentionUser = attentionUser;
			this.userInfo = userInfo;
		}
		AttentionUser attentionUser;
		UserInfo userInfo;
	}

	public AttentionUser getAttentionUser(String userId, String attentionUserId) {
		return attentionUserDao.getAttentionUser(userId, attentionUserId);
	}

	public boolean isAttention(String userId, String attentionUserId) {
		if (attentionUserDao.getAttentionUser(userId, attentionUserId) != null) {
			return true;
		}
		return false;
	}

	public void saveAttentionUser(String userId, String attentionUserId) {
		AttentionUser attentionUser = new AttentionUser();
		attentionUser.setUserId(userId);
		attentionUser.setAttentionUserId(attentionUserId);
		attentionUser.setCreateTime(new Date());
		attentionUserDao.save(attentionUser);
	}

	public void deleteAttentionUser(AttentionUser attentionUser) {
		attentionUserDao.delete(attentionUser);
	}

	// 获取粉丝数
	public long getFollowUserNum(String attentionUserId) {
		return attentionUserDao.getFollowUserNum(attentionUserId);
	}

	// 获取该用户关注的人数目
	public long getAttentionUserNum(String userId) {
		return attentionUserDao.getAttentionUserNum(userId);
	}

	// 用户关注人物列表
	public List<Object> getAttentionUserList(String userId, int page) {
		List<Object> resultList = attentionUserDao.getAttentionUserList(userId,page, 5);
		List<Object> returnList = new LinkedList<Object>();
		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AttentionUserAndUserInfo((AttentionUser) temp[0],
					(UserInfo) temp[1]));
		}
		return returnList;
	}

	// 用户粉丝列表
	public List<Object> getFollowUserList(String attentionUserId,
			int page) {
		List<Object> resultList = attentionUserDao.getFollowUserList(attentionUserId, page, 5);
		List<Object> returnList = new LinkedList<Object>();
		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AttentionUserAndUserInfo((AttentionUser) temp[0],
					(UserInfo) temp[1]));
		}
		return returnList;
	}
}
