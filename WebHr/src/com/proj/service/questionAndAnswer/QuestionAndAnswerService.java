package com.proj.service.questionAndAnswer;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.annotation.CometInbox;
import com.proj.annotation.IndexField;
import com.proj.aspect.comet.ReturnType;
import com.proj.dao.AnswerCommentDao;
import com.proj.dao.AnswerDao;
import com.proj.dao.AnswerVoteDao;
import com.proj.dao.AttentionQuestionDao;
import com.proj.dao.QuestionDao;
import com.proj.dao.QuestionInviteDao;
import com.proj.dao.QuestionTopicDao;
import com.proj.entity.Answer;
import com.proj.entity.AnswerComment;
import com.proj.entity.AnswerVote;
import com.proj.entity.AttentionQuestion;
import com.proj.entity.Question;
import com.proj.entity.QuestionInvite;
import com.proj.entity.QuestionTopic;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.resultData.ResultNo;
import com.proj.utils.GUIDGenerator;

@Service
public class QuestionAndAnswerService {
	@Autowired
	private AnswerDao answerDao;
	@Autowired
	private AttentionQuestionDao attentionQuestionDao;
	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private QuestionTopicDao questionTopicDao;
	@Autowired
	private AnswerVoteDao answerVoteDao;
	@Autowired
	private AnswerCommentDao answerCommentDao;
	@Autowired
	private QuestionInviteDao questionInviteDao;

	public static final int pageSize = 10;

	public class AnswerAndUser {
		AnswerAndUser(Answer answer, UserInfo userInfo) {
			this.answer = answer;
			this.user = userInfo;
		}

		public Answer answer;
		public UserInfo user;

	}

	public class AnswerAndQuestion {
		AnswerAndQuestion(Answer answer, Question question) {
			this.answer = answer;
			this.question = question;
		}

		Answer answer;
		Question question;

	}

	@IndexField
	public Question saveQuestion(Question question, List<Topic> topics) {

		questionDao.save(question);
		for (Topic topic : topics) {
			QuestionTopic questionTopic = new QuestionTopic();
			questionTopic.setQuestionId(question.getQuestionId());
			questionTopic.setTopicName(topic.getTopicName());
			questionTopic.setTopicId(topic.getTopicId());
			questionTopicDao.save(questionTopic);
		}
		AttentionQuestion attentionQuestion = new AttentionQuestion();
		attentionQuestion.setQuestion(question.getQuestionId());
		attentionQuestion.setUserId(question.getUserId());
		attentionQuestion.setCreateTime(new Date());
		attentionQuestion.setId(GUIDGenerator.getGUID());
		attentionQuestionDao.save(attentionQuestion);
		return question;

	}

	public List<Object> getQuestionAnswer(String questionId, int page) {

		List<Object> resultList = answerDao.getQuestionAnswerOrderByVote(
				questionId, page, pageSize);
		List<Object> returnList = new LinkedList<Object>();

		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AnswerAndUser((Answer) temp[0],
					(UserInfo) temp[1]));

		}

		return returnList;

	}

	public List<AnswerAndUser> getQuestionAnswerAndUserInfoOrderByVote(
			String questionId,int page,int pageSize) {

		List<Object> resultList = answerDao
				.getQuestionAnswerOrderByVote(questionId,page,pageSize);
		List<AnswerAndUser> returnList = new LinkedList<AnswerAndUser>();

		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			Answer answer=(Answer) temp[0];
			answer.setCommentNum((int) answerDao.getAnswerCommentCount(answer.getAnswerId()));
			returnList.add(new AnswerAndUser(answer,
					 SerializationUtils.clone((UserInfo)temp[1])));

		}
		return returnList;
	}

	public List<AnswerAndUser> getQuestionAnswerAndUserInfoOrderByTime(
			String questionId, int page , int pageSize, String sort) {

		List<Object> resultList = answerDao.getQuestionAnswerOrderByTime(
				questionId, page,pageSize,sort);
		List<AnswerAndUser> returnList = new LinkedList<AnswerAndUser>();

		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AnswerAndUser((Answer) temp[0],
					SerializationUtils.clone((UserInfo)temp[1])));

		}
		return returnList;
	}

	@CometInbox(returnType = ReturnType.ANSWER)
	@IndexField
	public Answer saveAnswer(UserInfo user, String answerContent,
			String questionId, String focus,String anonymous) {
		int autoFocus = Integer.parseInt(focus);

		int ifAnonymous = Integer.parseInt(anonymous);
		
		Answer newAnswer = new Answer();
		newAnswer.setAnswerContext(answerContent);
		newAnswer.setCommentNum(0);

		newAnswer.setQuestion(questionId);
		newAnswer.setVoteCount(0);
		newAnswer.setUserId(user.getUserId());
		newAnswer.setIfAnonymous(ifAnonymous);
		newAnswer.setAnswerId(GUIDGenerator.getGUID());
		answerDao.save(newAnswer);
		Question sourceQuestion=questionDao.get(questionId);
		if(sourceQuestion!=null){
			
			if(sourceQuestion.getAnswerTime()==null||sourceQuestion.getAnswerTime()==0){
				long answerCount=answerDao.getQuestionAnswerCount(questionId);
				sourceQuestion.setAnswerTime((int)answerCount+1);
				questionDao.saveOrUpdate(sourceQuestion);
			}
			else{
				sourceQuestion.setAnswerTime(sourceQuestion.getAnswerTime()+1);
			}
			
		}
		if (autoFocus == 1) {
			if (!isAttention(user.getUserId(), questionId)) {
				saveAttentionQuestion(user.getUserId(),questionId);
			}
		}

		// 更新回答问题时间
		questionDao.updateQuestionLastMotify(questionId);
		return newAnswer;

	}

	public Question getQuestionById(String id) {
		return questionDao.get(id);
	}

	public List<AttentionQuestion> getAttentionQuestionUsers(String questionId) {
		return attentionQuestionDao.getQuestionAttentionUser(questionId);
	}

	public List<QuestionTopic> getQuestionTopics(String questionId) {
		return questionTopicDao.getQuestionTopics(questionId);
	}

	public boolean isAttention(String userId, String questionId) {

		AttentionQuestion attentionQuestion = attentionQuestionDao
				.getQuestioAttention(userId, questionId);
		if (attentionQuestion == null) {
			return false;
		}

		return true;
	}

	public AttentionQuestion getAttentionQuestion(String userId,
			String questionId) {
		return attentionQuestionDao.getQuestioAttention(userId, questionId);
	}

	public void deleteAttentionQuestion(AttentionQuestion attentionQuestion) {
		attentionQuestionDao.delete(attentionQuestion);
	}

	public void saveAttentionQuestion(String userId, String questionId) {
		AttentionQuestion attentionQuestion = new AttentionQuestion();
		attentionQuestion.setId(GUIDGenerator.getGUID());
		attentionQuestion.setUserId(userId);
		attentionQuestion.setQuestion(questionId);
		attentionQuestion.setCreateTime(new Date());
		
		Question sourceQuestion=questionDao.get(questionId);
		if(sourceQuestion.getAttentionTime()==null||sourceQuestion.getAttentionTime()==0){
			Long attentionCount=attentionQuestionDao.getQuestionAttentionCount(questionId);
			if(attentionCount==null)attentionCount=0L;
			sourceQuestion.setAttentionTime((int) attentionCount.intValue()+1);
		}
		else{
			sourceQuestion.setAttentionTime(sourceQuestion.getAttentionTime()+1);
		}
		
		attentionQuestionDao.save(attentionQuestion);
		questionDao.saveOrUpdate(sourceQuestion);
	}

	// public int updateAnswerVoteNum(String answerId,byte changeNum){
	// return answerDao.updateAnswerVoteNum(answerId, changeNum);
	// }

	public void saveAnswerVote(String answerId, String userId, byte changeNum) {
		AnswerVote answerVote = new AnswerVote();
		answerVote.setAnswerId(answerId);
		answerVote.setUserId(userId);
		answerVote.setVoteType(changeNum);
		answerVoteDao.save(answerVote);
		answerDao.updateAnswerVoteNum(answerId, changeNum);
	}

	public AnswerVote getAnswerVote(String userId, String answerId) {
		return answerVoteDao.getAnswerVote(answerId, userId);
	}

	public void saveAnswerComment(String userId, String answerId,
			String questionId, String comment) {
		AnswerComment answerComment = new AnswerComment();
		answerComment.setAnswerId(answerId);
		answerComment.setUserId(userId);
		answerComment.setQuestionId(questionId);
		answerComment.setCommentContex(comment);
		answerComment.setCreateTime(new Date());
		answerCommentDao.save(answerComment);
	}

	public List<AnswerComment> getAnswerComments(String answerId) {
		return answerCommentDao.getAnswerComment(answerId);
	}

	public List<Object> getAnswerCommentsAndUsers(String answerId) {
		List<Object> commentsAndUserList = answerCommentDao
				.getAnswerCommentAndUser(answerId);
		List<Object> returnList = new LinkedList<Object>();

		for (Object o : commentsAndUserList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AnswerCommentAndUser((AnswerComment) temp[0],
					(UserInfo) temp[1]));

		}

		return returnList;
	}

	class AnswerCommentAndUser {
		public AnswerCommentAndUser(AnswerComment comment, UserInfo user) {
			this.comment = comment;
			this.user = user;
		}

		private AnswerComment comment;
		private UserInfo user;
	}

	@CometInbox(returnType = ReturnType.QUESTION_INVITE)
	public QuestionInvite saveQuestionInvite(String userId, String questionId,
			String inviteUserId) {
		QuestionInvite questionInvite = new QuestionInvite();
		questionInvite.setUserId(userId);
		questionInvite.setQuestionId(questionId);
		questionInvite.setInviteUserId(inviteUserId);
		questionInvite.setCreateTime(new Date());
		questionInviteDao.save(questionInvite);

		return questionInvite;
	}

	public QuestionInvite getQuestionInvite(String questionId,
			String inviteUserId) {
		return questionInviteDao.getQuestionInvite(inviteUserId, questionId);
	}

	public Object getQuestionAndUserInfo(String questionId) {
		return questionDao.getQuestionAndUserInfo(questionId);
	}

	public Long getQuestionAttentionCount(String questionId) {
		return attentionQuestionDao.getQuestionAttentionCount(questionId);
	}

	/**
	 * 获取所有用户回复问题列表,按时间排序
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Object> getUserAnswers(UserInfo curUser, int page) {
		List<Object> resultList = answerDao.getUserAnswers(curUser.getUserId(),
				page, 5);
		List<Object> returnList = new LinkedList<Object>();

		for (Object o : resultList) {
			Object[] temp = (Object[]) o;
			returnList.add(new AnswerAndQuestion((Answer) temp[0],
					(Question) temp[1]));

		}
		return returnList;
	}
//	用户提问数
	public Long getUserQuestionCount(String userId){
		return questionDao.getUserQuestionCount(userId);
	}
//	用户回答问题数
	public long getUserAnswerCount(String userId){
		return answerDao.getUserAnswerCount(userId);
	}
//	用户答案被赞次数
	public long getUserAnswerVotedCount(String userId){
		return answerDao.getUserAnswerVotedCount(userId);
	}
	
//	问题答案数
	public long getQuestionAnswerCount(String questionId){
		return answerDao.getQuestionAnswerCount(questionId);
	}
}
