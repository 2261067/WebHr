package com.proj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.ArticleDao;
import com.proj.dao.ArticleCommentDao;
import com.proj.dao.UserInfoDao;
import com.proj.entity.Article;
import com.proj.entity.ArticleComment;
import com.proj.entity.UserInfo;
import com.proj.utils.GUIDGenerator;

@Service
public class ArticleService {

	@Autowired
	private ArticleDao articleDao;

	@Autowired
	private UserInfoDao userInfoDao;

	@Autowired
	private ArticleCommentDao articleCommentDao;

	public Article saveArticle(Article article) {
		articleDao.save(article);
		return article;
	}

	public Article updateArticle(Article article) {
		articleDao.update(article);
		return article;
	}

	class ArticleData {
		Article article;
		UserInfo user;

		public ArticleData(Article article, UserInfo userInfo) {
			this.article = article;
			this.user = userInfo;
		}
	}

	public List<Object> getArticleData(int page, Integer size) {
		List<Object> resultList = new ArrayList<Object>(10);
		List<Article> articles = articleDao.getArticlesByPage(page, size);
		for (Article article : articles) {
			UserInfo userInfo = userInfoDao.get(article.getUserId());
			resultList.add(new ArticleData(article, userInfo));
		}
		return resultList;
	}

	public long getAllArticleCount() {
		return articleDao.getAllArticleCount();
	}

	public Article getArticleById(String articleId) {
		return articleDao.get(articleId);
	}

	/*
	 * 管理员删除专栏
	 */
	public boolean deleteArticleById(String articleId) {
		return articleDao.deleteById(articleId);
	}

	// 获取所有专栏列表
	public List<Article> getAllArticles(int page, int pageSize) {
		return articleDao.getAllArticles(page, pageSize);
	}

	/*
	 * 专栏评论
	 * */
	
	class ArticleCommentAndUser {
		private ArticleComment comment;
		private UserInfo user;
		public ArticleCommentAndUser(ArticleComment comment, UserInfo user) {
			this.comment = comment;
			this.user = user;
		}
		
	}
	
	public void saveArticleComment(String userId, String articleId,String comment) {
		ArticleComment articleComment = new ArticleComment();
		articleComment.setCommentId(GUIDGenerator.getGUID());
		articleComment.setArticleId(articleId);
		articleComment.setUserId(userId);
		articleComment.setCommentContext(comment);
		articleComment.setCreateTime(new Date());
		
		Article article = articleDao.getArticleById(articleId);
		article.setCommentNum(article.getCommentNum()+1);
		
		articleCommentDao.save(articleComment);
		articleDao.update(article);
	}

	public List<ArticleComment> getArticleComments(String articleId) {
		return articleCommentDao.getArticleComment(articleId);
	}

	public List<Object> getArticleCommentsAndUsers(String articleId) {
		List<Object> commentsAndUserList = articleCommentDao
				.getArticleCommentAndUser(articleId);
		List<Object> returnList = new LinkedList<Object>();

		for (Object o : commentsAndUserList) {
			Object[] temp = (Object[]) o;
			returnList.add(new ArticleCommentAndUser((ArticleComment) temp[0],
					(UserInfo) temp[1]));
		}
		return returnList;
	}
	
}
