package com.proj.service.topic;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.proj.dao.*;
import com.proj.entity.Answer;
import com.proj.entity.AttentionTopic;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService.AnswerAndQuestion;
import com.proj.utils.GUIDGenerator;
@Service

public class TopicShowService {
@Autowired
TopicDao topicDao;
@Autowired
AttentionTopicDao attentionTopicDao;
@Autowired
QuestionTopicDao questionTopicDao;

public class AttentionTopicAndTopic {
	AttentionTopicAndTopic(AttentionTopic attentionTopic, Topic topic) {
		this.attentionTopic = attentionTopic;
		this.topic = topic;
	}

	public AttentionTopic attentionTopic;
	public Topic topic;

}

public List<Map> getHotTopics(int page,int pageSize){
	List<Map> resultList=new LinkedList<>();
	List<Topic> topicList=topicDao.getHotTopicByPage(page,pageSize);
	for(Topic t:topicList){
		if(t==null)continue;
		Map<String,Object> topicData=getTopicData( t);
		resultList.add(topicData);
	}
	
	return resultList;
}

public List<Map> getFocusTopics(int page,String userId){
	List<Map> resultList=new LinkedList<>();
	List<Topic> topicList=topicDao.findFocusTopic(userId);
	for(Topic t:topicList){
		if(t==null)continue;
		Map<String,Object> topicData=getTopicData( t);
		resultList.add(topicData);
	}
	
	return resultList;
}

public long getFocusTopicsCount(String userId){
	
	long count=topicDao.getFocusTopicCount(userId);

	return count;
}


public long findAllTopicCount(){
	return topicDao.findAllTopicCount();
}

/*
 * 返回用户关注话题
 * */
public List<Object> findAttentionTopic(String userId, int page){
	List<Object> resultList = attentionTopicDao.findAttentionTopic(userId, page, 10);
	List<Object> returnList = new LinkedList<Object>();
	for (Object o : resultList) {
		Object[] temp = (Object[]) o;
		returnList.add(new AttentionTopicAndTopic((AttentionTopic) temp[0],
				(Topic) temp[1]));
	}
	return returnList;
}
/*
 * 返回用户关注话题总数
 * */
public long getAttentionCountByuid(String userId){
	return attentionTopicDao.getAttentionCountByuid(userId);
}

public Map getTopicData(Topic t){
	Map<String,Object> topicData=new HashMap<String,Object>();
	long questionCount=questionTopicDao.getQuestionCountByTopic(t.getTopicId());
	long attentionCount=attentionTopicDao.getAttentionCountByTopic(t.getTopicId());
	topicData.put("topic", t);
	topicData.put("topicQuestionCount", questionCount);
	topicData.put("topicAttentionCount", attentionCount);
	return topicData;
}

public Map<String,Object> getTopicData(String userId,String topicId){
	Map<String,Object> topicData=new HashMap<String,Object>();
	if(topicId==null) return topicData;
	Topic t=topicDao.get(topicId);
	topicData=getTopicData(t);
	
	int focus=attentionTopicDao.getIfAttentionByTopic(userId,topicId);
	topicData.put("focus",focus);
	return topicData;
	
}
//此函数涉及到的前后台逻辑不严密，需要修改
public String focusOrCancelFocusTopic(String userId,String topicId){
	
	List<AttentionTopic> at=attentionTopicDao.getIfAttentionByTopicAndUser(userId, topicId);
	if(at!=null&&at.size()==1){
		//有关注记录的情况
		attentionTopicDao.delete(at.get(0));
		return "cancel";
	}
	else if(at==null||at.size()==0){
		//无关注记录的情况
		AttentionTopic newAttention=new AttentionTopic();
		newAttention.setId(GUIDGenerator.getGUID());
		newAttention.setTopicId(topicId);
		newAttention.setUserId(userId);
		newAttention.setCreateTime(new Date());
		attentionTopicDao.save(newAttention);
		return "add";
	}
	return "error";
}
}
