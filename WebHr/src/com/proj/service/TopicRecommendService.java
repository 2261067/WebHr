package com.proj.service;

import java.util.ArrayList;
import java.util.List;

import javassist.expr.NewArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.TopicRecommendDao;
import com.proj.entity.UserInfo;

@Service
public class TopicRecommendService {
	private static final int limitNum = 12;
	@Autowired
	private TopicRecommendDao topicRecommendDao;

	public List<Object> getTopicRecommendAnswerPeople(String topic) {
		List<Object> peoples = topicRecommendDao.getTopicRecommendAnswerPeople(
				topic, limitNum);
		List<Object> results = new ArrayList<Object>(20);;
		if (peoples != null && !peoples.isEmpty()) {
			for (Object people : peoples) {
				Object[] temp = (Object[]) people;
				results.add(new TopicRecommendPeople((UserInfo) temp[0],
						(String) temp[1], (Long) temp[2]));
			}
		}
		return results;
	}

	class TopicRecommendPeople {
		UserInfo user;
		String topic;
		long voteNum;

		public TopicRecommendPeople(UserInfo user, String topic, Long voteNum) {
			this.user = user;
			this.topic = topic;
			this.voteNum = voteNum;
		}
	}
}
