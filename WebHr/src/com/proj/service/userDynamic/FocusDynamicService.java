package com.proj.service.userDynamic;


import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.AnswerDao;
import com.proj.dao.AttentionQuestionDao;
import com.proj.dao.QuestionDao;
import com.proj.dao.TopicDao;
import com.proj.dao.UserInfoDao;
import com.proj.dao.VUserDynamicDao;
import com.proj.entity.AttentionQuestion;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.entity.VUserDynamic;
import com.proj.service.UserQuestionService;
import com.proj.session.SessionHelper;

@Service
public class FocusDynamicService {
	
	@Autowired
	UserQuestionService uqs;
	//该类封装了最终返回给上层的形式
	public class DynamicData{
		public VUserDynamic getUserDynamicData() {
			return userDynamicData;
		}
		public void setUserDynamicData(VUserDynamic userDynamicData) {
			this.userDynamicData = userDynamicData;
		}
		public UserInfo getDataUserInfo() {
			return dataUserInfo;
		}
		public void setDataUserInfo(UserInfo dataUserInfo) {
			this.dataUserInfo = dataUserInfo;
		}
		VUserDynamic userDynamicData;
		UserInfo dataUserInfo;
		List<Topic> dataTopics;
		Question sourceQuestion;
		Integer ifAttention=0;
		long answerNum=0;
		public long getAnswerNum() {
			return answerNum;
		}
		public void setAnswerNum(long answerNum) {
			this.answerNum = answerNum;
		}
		public long getAttentionNum() {
			return attentionNum;
		}
		public void setAttentionNum(long attentionNum) {
			this.attentionNum = attentionNum;
		}
		long attentionNum=0;
		public Integer getIfAttention() {
			return ifAttention;
		}
		public void setIfAttention(Integer ifAttention) {
			this.ifAttention = ifAttention;
		}
		public Question getSourceQuestion() {
			return sourceQuestion;
		}
		public void setSourceQuestion(Question sourceQuestion) {
			this.sourceQuestion = sourceQuestion;
		}
		public List<Topic> getDataTopics() {
			return dataTopics;
		}
		public void setDataTopics(List<Topic> dataTopics) {
			this.dataTopics = dataTopics;
		}
		
	}
	
	
	
	//设置数据类型的优先级，比如设置用户关注的问题为最高优先级，去重和展示时使用(用户可能关注了某个问题，或者关注的某个话题下有同一个问题，那么以直接关注的问题为准，其他重复的去除)
    public static final HashMap<String, Integer> FOCUS_DYNAMIC_DATA_PRIORITY=new HashMap<String, Integer>();
    {
    	FOCUS_DYNAMIC_DATA_PRIORITY.put("focus_question_answer", 1);
    	FOCUS_DYNAMIC_DATA_PRIORITY.put("focus_user_question", 2);
    	FOCUS_DYNAMIC_DATA_PRIORITY.put("focus_user_answer", 3);
    	FOCUS_DYNAMIC_DATA_PRIORITY.put("focus_topic_question", 4);
    }
	@Autowired
	private VUserDynamicDao vUserDynamicDao;
	@Autowired
	private UserInfoDao userInfoDao;
	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private TopicDao topicDao;
	@Autowired
	private AttentionQuestionDao attentionQuestionDao;
	@Autowired
	private AnswerDao answerDao;
	public List<DynamicData> getFocusDynamicByPage(int currentPage,int pageSize,String uid){
		//有重复数据，需要去重
		List<VUserDynamic> middleResultsDynamics=vUserDynamicDao.getDynamicByPage(currentPage, pageSize, uid);
		List<DynamicData> resultsDynamics=this.deleteRepeatData(middleResultsDynamics);
		return resultsDynamics;
		
	}
	
	
	/**
	 * @param currentPage
	 * @param pageSize
	 * @param uid
	 * @param sesstionVDynamicData这个为sesstion中此次连续请求中上一次的剩余列表数据，用于动态连续翻页数据生成
	 * @return
	 * @throws Exception 
	 */
	public List<DynamicData> getFocusDynamicByPage(int currentPage,int pageSize,String uid,VUserDynamicDataLists sessionVDynamicData) throws Exception{
		if(currentPage!=1&&sessionVDynamicData==null)throw new Exception("动态数据有误，会话中不存在动态历史记录！");
		//有重复数据，需要去重
		List<VUserDynamic> middleResultsDynamics=this.getMiddleResultsDynamics(currentPage, pageSize, uid,sessionVDynamicData);
		List<DynamicData> resultsDynamics=deleteRepeatData(middleResultsDynamics);
		return resultsDynamics;
		
	}
	public List<DynamicData> deleteRepeatData(List<VUserDynamic> middleResultsDynamics){
		List<DynamicData> resultsDynamics=new LinkedList<DynamicData>();
		//按优先级去重
		if(middleResultsDynamics==null)
			return resultsDynamics;
		Iterator<VUserDynamic> iter=middleResultsDynamics.iterator();
		VUserDynamic beforeDynamic=null;
		//去重
		while(iter.hasNext()){
			VUserDynamic temp=iter.next();
			if(beforeDynamic!=null){
				
				
				if(beforeDynamic.getId().getDataId().equals(temp.getId().getDataId())){
					if(FOCUS_DYNAMIC_DATA_PRIORITY.get(beforeDynamic.getId().getType())
							>FOCUS_DYNAMIC_DATA_PRIORITY.get(temp.getId().getType())){
						continue;
						
						
					}//if
					
					else{
						beforeDynamic=temp;
						continue;
					}
					
				} 
				else{
					DynamicData newData=new DynamicData();
					newData.setUserDynamicData(beforeDynamic);
					UserInfo newUser=userInfoDao.isExistUserId(beforeDynamic.getUserId());
					newData.setDataUserInfo(newUser);
					newData.setDataTopics(getDynamicDataTopic(beforeDynamic));
					
//					获取问题的回复数
					newData.setAnswerNum(answerDao.getQuestionAnswerCount(beforeDynamic.getQuestionId()));
//					获取问题的关注数
					newData.setAttentionNum(attentionQuestionDao.getQuestionAttentionCount(beforeDynamic.getQuestionId()));
					
					newData.setSourceQuestion(questionDao.get(beforeDynamic.getQuestionId()));
					AttentionQuestion attentionQuestion = 
							attentionQuestionDao.getQuestioAttention(beforeDynamic.getOriginUserId(), beforeDynamic.getQuestionId());
					if (attentionQuestion != null) {
						newData.setIfAttention(1);
						}
					resultsDynamics.add(newData);
					
				}
				
				
			}
			beforeDynamic=temp;
		}
		//if(resultsDynamics.size()==0&&beforeDynamic!=null)resultsDynamics.add(beforeDynamic);
		return resultsDynamics;
	}
	
	public List<Topic> getDynamicDataTopic(VUserDynamic dynamicData){
		List<Topic> resultTopics=null;
		if(dynamicData==null)return resultTopics;
		String dataTypeString=dynamicData.getId().getType();
		int dataType=FOCUS_DYNAMIC_DATA_PRIORITY.get(dataTypeString);
		switch (dataType) {
		case 1:
			resultTopics= topicDao.getAnswerTopic(dynamicData.getId().getDataId());
			break;
		case 2:
			resultTopics= topicDao.getQuestionTopic(dynamicData.getId().getDataId());
			break;
		case 3:
			resultTopics= topicDao.getAnswerTopic(dynamicData.getId().getDataId());
			break;
		case 4:
			resultTopics= topicDao.getQuestionTopic(dynamicData.getId().getDataId());
			break;
		default:
			
			break;
		}
		return resultTopics;
	}
	
	
	public class VUserDynamicDataLists{
		public VUserDynamicDataLists(String uid,int defaultPageSize){
			this.userId=uid;
			this.defaultPageSize=defaultPageSize;
		}
		public List<VUserDynamic> getFocusQuestionAnswer() {
			//首先检查容器中是否还有元素，若没有，依次取下一页的元素
			if(ifEmptyList(focusQuestionAnswer))
			{
				List<VUserDynamic> temp=vUserDynamicDao.getFocusQuetionAnswer(this.focusQuestionAnswerPage, defaultPageSize, userId);
				//
				if(ifEmptyList(temp)){
				//没有下一页的数据了，此时应去掉该类列表数据	
					return null;
				}
				focusQuestionAnswer.addAll(temp);
				focusQuestionAnswerPage++;
			}
			return focusQuestionAnswer;
		}
		public void setFocusQuestionAnswer(List<VUserDynamic> focusQuestionAnswer) {
			this.focusQuestionAnswer = focusQuestionAnswer;
		}
		public List<VUserDynamic> getFocusUserQuestion() {
			//首先检查容器中是否还有元素，若没有，依次取下一页的元素
			if(ifEmptyList(focusUserQuestion))
			{
				List<VUserDynamic> temp=vUserDynamicDao.getFocusUserQuetion(this.focusUserQuestionPage, defaultPageSize, userId);
				
				if(ifEmptyList(temp)){
				//没有下一页的数据了，此时应去掉该类列表数据
					return null;
				}
				focusUserQuestion.addAll(temp);
				focusUserQuestionPage++;
			}
			return focusUserQuestion;
		}
		public void setFocusUserQuestion(List<VUserDynamic> focusUserQuestion) {
			this.focusUserQuestion = focusUserQuestion;
		}
		public List<VUserDynamic> getFocusUserAnswer() {
			//首先检查容器中是否还有元素，若没有，依次取下一页的元素
			if(ifEmptyList(focusUserAnswer))
			{
				List<VUserDynamic> temp=vUserDynamicDao.getFocusUserAnswer(this.focusUserAnswerPage, defaultPageSize, userId);
				
				if(ifEmptyList(temp)){
				//没有下一页的数据了，此时应去掉该类列表数据
					return null;
				}
				focusUserAnswer.addAll(temp);
				focusUserAnswerPage++;
			}
			
			return focusUserAnswer;
		}
		public void setFocusUserAnswer(List<VUserDynamic> focusUserAnswer) {
			this.focusUserAnswer = focusUserAnswer;
		}
		public List<VUserDynamic> getFocusTopicQuestion() {
			//首先检查容器中是否还有元素，若没有，依次取下一页的元素
			if(ifEmptyList(focusTopicQuestion))
			{
				List<VUserDynamic> temp=vUserDynamicDao.getFocusTopicQuetion(this.focusTopicQuestionPage, defaultPageSize, userId);
				
				if(ifEmptyList(temp)){
				//没有下一页的数据了，此时应去掉该类列表数据
					return null;
				}
				focusTopicQuestion.addAll(temp);
				focusTopicQuestionPage++;
			}
			return focusTopicQuestion;
		}
		public void setFocusTopicQuestion(List<VUserDynamic> focusTopicQuestion) {
			this.focusTopicQuestion = focusTopicQuestion;
		}
		public int getFocusQuestionAnswerPage() {
			return focusQuestionAnswerPage;
		}
		public void setFocusQuestionAnswerPage(int focusQuestionAnswerPage) {
			this.focusQuestionAnswerPage = focusQuestionAnswerPage;
		}
		public int getFocusUserQuestionPage() {
			return focusUserQuestionPage;
		}
		public void setFocusUserQuestionPage(int focusUserQuestionPage) {
			this.focusUserQuestionPage = focusUserQuestionPage;
		}
		public int getFocusUserAnswerPage() {
			return focusUserAnswerPage;
		}
		public void setFocusUserAnswerPage(int focusUserAnswerPage) {
			this.focusUserAnswerPage = focusUserAnswerPage;
		}
		public int getFocusTopicQuestionPage() {
			return focusTopicQuestionPage;
		}
		public void setFocusTopicQuestionPage(int focusTopicQuestionPage) {
			this.focusTopicQuestionPage = focusTopicQuestionPage;
		}
		public void deleteUserDynamic(VUserDynamic data) {
			if(focusQuestionAnswer.size()>0&&focusQuestionAnswer.get(0).getId().getDataId().equals(data.getId().getDataId()))focusQuestionAnswer.remove(0);
			if(focusUserQuestion.size()>0&&focusUserQuestion.get(0).getId().getDataId().equals(data.getId().getDataId()))focusUserQuestion.remove(0);
			if(focusUserAnswer.size()>0&&focusUserAnswer.get(0).getId().getDataId().equals(data.getId().getDataId()))focusUserAnswer.remove(0);
			if(focusTopicQuestion.size()>0&&focusTopicQuestion.get(0).getId().getDataId().equals(data.getId().getDataId()))focusTopicQuestion.remove(0);
		}
		List<VUserDynamic> focusQuestionAnswer=new LinkedList<>();
		List<VUserDynamic> focusUserQuestion=new LinkedList<>();
		List<VUserDynamic> focusUserAnswer=new LinkedList<>();
		List<VUserDynamic> focusTopicQuestion=new LinkedList<>();
		int focusQuestionAnswerPage=1;
		int focusUserQuestionPage=1;
		int focusUserAnswerPage=1;
		int focusTopicQuestionPage=1;
		//默认页大小
		int defaultPageSize=15;
		//用于记录客户端上次访问的时间，用于清理session中的数据
		Date lastResustTime;
		String userId;
	}
	
	/**从数据库或缓存获取动态中间结果，之后需要根据优先级做去重操作
	 * @param currentPage
	 * @param pageSize
	 * @param uid
	 * @return
	 */
	public List<VUserDynamic> getMiddleResultsDynamics(int currentPage,int pageSize,String uid,VUserDynamicDataLists sessionVDynamicDataLists){
		List<VUserDynamic> reuslt=new LinkedList<VUserDynamic>();
		//if(currentPage==1)sessionVDynamicData=new VUserDynamicDataLists(uid,pageSize);
		for(int i=0;i<pageSize;i++){
			//首先检查几类容器中是否还有元素，若没有，依次取下一页的元素
			List<VUserDynamic> focusQuestionAnswer=sessionVDynamicDataLists.getFocusQuestionAnswer();
			List<VUserDynamic> focusUserQuestion=sessionVDynamicDataLists.getFocusUserQuestion();
			List<VUserDynamic> focusUserAnswer=sessionVDynamicDataLists.getFocusUserAnswer();
			List<VUserDynamic> focusTopicQuestion=sessionVDynamicDataLists.getFocusTopicQuestion();
			//用于放置时间最晚的记录
			VUserDynamic udTemp=null;
			if(!ifEmptyList(focusQuestionAnswer))udTemp=focusQuestionAnswer.get(0);
			if(!ifEmptyList(focusUserQuestion)){
				
				VUserDynamic fuq=focusUserQuestion.get(0);
				if(udTemp==null)udTemp=fuq;
				udTemp=fuq.getCreateTime().after(udTemp.getCreateTime())?fuq:udTemp;
				}
			if(!ifEmptyList(focusUserAnswer)){
				VUserDynamic fuq=focusUserAnswer.get(0);
				if(udTemp==null)udTemp=fuq;
				udTemp=fuq.getCreateTime().after(udTemp.getCreateTime())?fuq:udTemp;
				}
			if(!ifEmptyList(focusTopicQuestion)){
				VUserDynamic fuq=focusTopicQuestion.get(0);
				if(udTemp==null)udTemp=fuq;
				udTemp=fuq.getCreateTime().after(udTemp.getCreateTime())?fuq:udTemp;
				}
			if(udTemp==null)break;
			reuslt.add(udTemp);
			sessionVDynamicDataLists.deleteUserDynamic(udTemp);
			
		}
		return reuslt;
	//return vUserDynamicDao.getDynamicByPage(currentPage, pageSize, uid);
	}
	public boolean ifEmptyList(List list){
		if(list==null||list.size()==0)return true;
		return false;
	}
	public VUserDynamicDataLists newVUserDynamicDataLists(String uid,int pageSize){
		return new VUserDynamicDataLists(uid,pageSize);
	}
}
