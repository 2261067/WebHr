package com.proj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.TopicDao;
import com.proj.entity.Topic;

@Service
public class TopicService {
	@Autowired
	private TopicDao topicDao;
	
	public Topic getTopicByName(String topicName){
		return topicDao.findTopicByName(topicName);
	}
	
	public Topic getTopicById(String topicId){
		return topicDao.findTopicById(topicId);
	}
	
	public List<Topic> getAllTopics(){
		return topicDao.findAllTopic();
	}
	public List<Topic> findSimilarTopic(String topicName){
		return topicDao.findSimilarTopic(topicName);
	}
	
	public List<Topic> findSimilarTopicByPage(String topicName,int page,int pageSize){
		return topicDao.findSimilarTopic(topicName,page,pageSize);
	}
	
	public List<Topic> findAllTopicByPage(int page,int pageSize){
		return topicDao.findAllTopicByPage(page, pageSize);
	}
	
	public List<Topic> findAllTopicByPageAndTimeDesc(int page,int pageSize){
		return topicDao.findAllTopicByPageAndTimeDesc(page, pageSize);
	}
	
	public long findAllTopicCount(){
		return topicDao.findAllTopicCount();
	}
	public void savaTopic(Topic t){
		topicDao.save(t);
	}
	public boolean deleteTopicById (String topicId){
		return topicDao.deleteById( topicId);
	}
	public void updateTopic(Topic t){
		topicDao.update(t);
	}
}
