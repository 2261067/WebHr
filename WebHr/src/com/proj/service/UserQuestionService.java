package com.proj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

import com.proj.annotation.DeleteIndex;
import com.proj.dao.*;
import com.proj.entity.*;
import com.proj.utils.DateCalUtil;

/**
 * @author ZTW 该类用于首页问题展示，主要包括问题名和相关用户
 * 
 */
@Service
public class UserQuestionService {
	@Autowired
	QuestionDao questionDao;
	@Autowired
	private AnswerDao answerDao;
	@Autowired
	private UserInfoDao userInfoDao;
	@Autowired
	private AttentionQuestionDao attentionQuestionDao;
	public static int pageSize = 5;

	public static int getPageSize() {
		return pageSize;
	}

	public static void setPageSize(int pageSize) {
		UserQuestionService.pageSize = pageSize;
	}

	// 封装了首页问题的数据类
	public class QuestionData {
		public UserInfo getQuestionUser() {
			return questionUser;
		}

		public void setQuestionUser(UserInfo questionUser) {
			this.questionUser = questionUser;
		}

		public String getLastModifyTime() {
			return lastModifyTime;
		}

		public void setLastModifyTime(String lastModifyTime) {
			this.lastModifyTime = lastModifyTime;
		}

		public Question getQuestion() {
			return question;
		}

		public void setQuestion(Question question) {
			this.question = question;
		}

		public UserInfo getLatestAnswerUser() {
			return latestAnswerUser;
		}

		public void setLatestAnswerUser(UserInfo latestAnswerUser) {
			this.latestAnswerUser = latestAnswerUser;
		}

		public List<UserInfo> getTopContributeUsers() {
			return topContributeUsers;
		}

		public void setTopContributeUsers(List<UserInfo> topContributeUsers) {
			this.topContributeUsers = topContributeUsers;
		}

		public long getAttentionNum() {
			return attentionNum;
		}

		public void setAttentionNum(long attentionNum) {
			this.attentionNum = attentionNum;
		}

		public long getAnswerNum() {
			return answerNum;
		}

		public void setAnswerNum(long answerNum) {
			this.answerNum = answerNum;
		}

		private long attentionNum;
		private long answerNum;
		Question question;
		UserInfo latestAnswerUser;
		UserInfo questionUser;
		String lastModifyTime;
		List<UserInfo> topContributeUsers;

	}

	/**
	 * 根据用户关注获取问题列表
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Question> getQuestionByFollow(UserInfo curUser, int page,int pageSize) {

		return questionDao.getQuestionByFollow(curUser.getUserId(), page, pageSize);

	}
	/**
	 * 根据被邀请的问题列表
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Question> getQuestionByInvite(UserInfo curUser, int page,int pageSize) {

		return questionDao.getQuestionByInvite(curUser.getUserId(), page, pageSize);

	}
	/**
	 * 获取所有问题列表,按时间排序
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Question> getQuestions(UserInfo curUser, int page,int pageSize) {

		return questionDao.getQuestions(curUser.getUserId(), page, pageSize);

	}
	//获取所有问题列表
	public List<Question> getAllQuestions(int page,int pageSize) {
		return questionDao.getAllQuestions( page, pageSize);
	}
	/**
	 * 获取用户问题列表,按时间排序
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Question> getUserQuestions(UserInfo curUser, int page) {

		return questionDao.getUserQuestions(curUser.getUserId(), page, pageSize);

	}
	

	public List<QuestionData> getFocusQuestionAndRelativeUsers(UserInfo curUser,
			int page,int pageSize) {
		
		List<Question> middleResult=getQuestionByFollow( curUser,  page, pageSize);
		List<QuestionData> result=new LinkedList<QuestionData>();
		for(Question q:middleResult){
			QuestionData qData=this.getQestionData(q);
			if(qData!=null)result.add(qData);
		}
		return result;
	}
	public List<QuestionData> getInvitedQuestion(UserInfo curUser,
			int page,int pageSize) {
		
		List<Question> middleResult=getQuestionByInvite( curUser,  page, pageSize);
		List<QuestionData> result=new LinkedList<QuestionData>();
		for(Question q:middleResult){
			QuestionData qData=this.getQestionData(q);
			if(qData!=null)result.add(qData);
		}
		return result;
	}	
	public List<QuestionData> getQuestionAndRelativeUsers(UserInfo curUser,
			int page,int pageSize) {

		List<Question> questions = getQuestions(curUser, page,pageSize);
		// Map<String,Object> resultMap=new HashMap<String,Object>();
		List<QuestionData> resultData = new LinkedList<QuestionData>();
		for (Question q : questions) {
			QuestionData questionData = getQestionData(q);
			List<UserInfo> userInfos = getContributeUser(q);
			questionData.setTopContributeUsers(userInfos);
			
			resultData.add(questionData);
		}
		return resultData;

	}
	
	//获取问题的最后答复及修改时间
	
	public QuestionData getQestionData(Question q){
		QuestionData questionData = new QuestionData();
		Answer latestAnswer = answerDao.getLatestAnswer(q.getQuestionId());
		//根据最后的回复记录和问题时间确定问题最后修改时间
		Date latestModifyTime=new Date();
		
		if (latestAnswer != null) {
			UserInfo latestUser = userInfoDao.isExistUserId(latestAnswer
					.getUserId());
			questionData.setLatestAnswerUser(latestUser);
			latestModifyTime=latestAnswer.getCreateTime();
		}
		else{
			latestModifyTime=q.getCreateTime();
		}
		
		if(latestModifyTime!=null){
			questionData.setLastModifyTime(DateCalUtil.diffNow(latestModifyTime));
			
		}
		questionData.setQuestionUser(userInfoDao.isExistUserId(q.getUserId()));
		questionData.setQuestion(q);
//		获取问题的回复数
		questionData.setAnswerNum(answerDao.getQuestionAnswerCount(q.getQuestionId()));
//		获取问题的关注数
		questionData.setAttentionNum(attentionQuestionDao.getQuestionAttentionCount(q.getQuestionId()));
		return questionData;
	}
//获取贡献最高的用户
	public List<UserInfo> getContributeUser(Question q){
		List<Answer> contributeAnswers = answerDao
				.getTopContributeAnswers(q.getQuestionId());
		List<UserInfo> contributeUsers = new LinkedList<UserInfo>();
		for (Answer contributeAns : contributeAnswers) {
			UserInfo user = userInfoDao.isExistUserId(contributeAns
					.getUserId());
			if (null != user)
				contributeUsers.add(user);
		}

		return contributeUsers;
	}
	
	public List<QuestionData> getTopicQuestionDatas(String topicId,int page,int pageSize){
		List<QuestionData> resultList = new ArrayList<UserQuestionService.QuestionData>();
		List<Question> topicQuestions = questionDao.getTopicQuestions(topicId, page, pageSize);
		for (Question question : topicQuestions) {
			QuestionData questionData = getQestionData(question);
			resultList.add(questionData);
		}
		return resultList;
	}
	
	/**
	 * 根据问题id查询问题
	 * 
	 * 
	 * @param id
	 * @return
	 */
	public Question getQuestionById(String id) {

		return questionDao.get(id);

	}

	public long getQuestionPages() {
		long questioCount = questionDao.getQuestionCount();
		long page = questioCount / pageSize;
		if (questioCount % pageSize > 0)
			page++;

		return page;

	}
	
	
	public long getAllQuestionCount() {
		long questioCount = questionDao.getQuestionCount();
		return questioCount;
	}
	
	@DeleteIndex
	public boolean deleteQuestionById (String questionId){
		
		return questionDao.deleteById(questionId);
	}
}
