package com.proj.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.dao.QuestionDao;
import com.proj.entity.*;
import com.proj.service.UserQuestionService.QuestionData;

/**用来产生首页的热门数据，包括问题，专栏，活动等
 * @author ZTW
 *
 */
@Service
public class HotDataService {
	@Autowired
	QuestionDao qd;
	@Autowired
	UserQuestionService uqs;
	double hotQuestionPer=0.5;
	double hotArticlePer=0.25;
	double hotActivityPer=0.25;
	public List<QuestionData> getHotQuestions(int num){
		List<Question> hotQuestions=qd.getHotQuestions(0,num);
		List<QuestionData> hotQuestionDatas=new LinkedList<QuestionData>();
		for(Question q:hotQuestions){
			hotQuestionDatas.add(uqs.getQestionData(q));
		}
		return hotQuestionDatas;
	}
	public List<Object> getHotArticles(int num){
		return null;
	}
	public List<Object> getHotActivities(int num){
		return null;
	}
}
