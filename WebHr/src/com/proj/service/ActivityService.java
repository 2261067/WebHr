package com.proj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proj.annotation.CometInbox;
import com.proj.annotation.IndexField;
import com.proj.aspect.comet.ReturnType;
import com.proj.dao.ActivityDao;
import com.proj.dao.ActivitySignDao;
import com.proj.dao.UserInfoDao;
import com.proj.entity.Activity;
import com.proj.entity.ActivitySign;
import com.proj.entity.UserInfo;

@Service
public class ActivityService {

	@Autowired
	private ActivityDao activityDao;

	@Autowired
	private UserInfoDao userInfoDao;

	@Autowired
	private ActivitySignDao activitySignDao;

	public static final int pageSize = 7;

	@CometInbox(returnType = ReturnType.SAVE_ACTIVITY)
	@IndexField
	public Activity saveActvity(Activity activity) {
		activityDao.save(activity);
		return activity;
	}

	@CometInbox(returnType = ReturnType.UPDATE_ACTIVITY)
	@IndexField
	public Activity updateActivity(Activity activity) {
		activityDao.update(activity);
		return activity;
	}

	public List<Object> getActivityData(int page, Integer size) {
		List<Object> resultList = new ArrayList<Object>(10);
		if (size == null) {
			size = pageSize;
		}
		List<Activity> activities = activityDao.getActivitiesByPage(page, size);
		for (Activity activity : activities) {
			ActivitySign lastSignUser = activitySignDao.getLastestSign(activity
					.getActivityId());
			UserInfo userInfo = userInfoDao.get(activity.getUserId());
			long signUserCount = activitySignDao.getActivitySignCount(activity
					.getActivityId());
			resultList.add(new ActivityData(activity, userInfo, lastSignUser,
					signUserCount));
		}
		return resultList;
	}

	class ActivityData {
		Activity activity;
		UserInfo user;
		ActivitySign lastSignUser;
		long signUserCount;

		public ActivityData(Activity activity, UserInfo userInfo,
				ActivitySign lastSignUser, long signUserCount) {
			this.activity = activity;
			this.user = userInfo;
			this.lastSignUser = lastSignUser;
			this.signUserCount = signUserCount;
		}
	}

	public long getAllActivityCount() {
		return activityDao.getAllActivityCount();
	}

	public long getActivitySignCount(String activityId) {
		return activitySignDao.getActivitySignCount(activityId);
	}

	public Activity getActivityById(String activityId) {
		return activityDao.get(activityId);
	}

	public boolean isSign(String userId, String activityId) {
		if (activitySignDao.getActivitySign(userId, activityId) == null) {
			return false;
		}

		return true;
	}

	public ActivitySign getActivitySign(String userId, String activityId) {
		return activitySignDao.getActivitySign(userId, activityId);
	}

	public void deleteActivitySign(ActivitySign activitySign) {
		activitySignDao.delete(activitySign);
	}

	public void saveActivitySign(String userId, String activityId) {
		ActivitySign activitySign = new ActivitySign();
		activitySign.setUserId(userId);
		activitySign.setActivityId(activityId);
		activitySign.setCreateTime(new Date());
		activitySignDao.save(activitySign);
	}

	public static final int showSignUserNum = 100;

	public List<UserInfo> getLastestActivitySignUser(String activityId) {
		return activitySignDao.getActivitySignUser(activityId, showSignUserNum);
	}

	public List<ActivitySign> getAllActivitySigns(String activityId) {
		return activitySignDao.getAllActivitySigns(activityId);
	}

	/**
	 * 获取用户活动列表,按时间排序
	 * 
	 * @param curUser
	 * @param page
	 * @return
	 */
	public List<Activity> getUseActivity(UserInfo curUser, int page) {

		return activityDao.getUserActivity(curUser.getUserId(), page, 5);

	}

	/*
	 * 管理员删除活动
	 */
	public boolean deleteActivityById(String activityId) {
		return activityDao.deleteById(activityId);
	}

	// 获取所有活动列表
	public List<Activity> getAllActivities(int page, int pageSize) {
		return activityDao.getAllActivities(page, pageSize);
	}
}
