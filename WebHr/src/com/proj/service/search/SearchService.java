package com.proj.service.search;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.log.Log;
import com.proj.annotation.IndexField;
import com.proj.dao.ActivityDao;
import com.proj.dao.AnswerDao;
import com.proj.dao.BaseDao;
import com.proj.dao.QuestionDao;
import com.proj.dao.UserInfoDao;
import com.proj.entity.*;
import com.proj.module.searchEngine.SearchStrategy;
import com.proj.utils.SpringContextUtil;
@Service
public class SearchService {
	//用于反射寻找查询的中间结果所属的类，然后找出entity
	public  final Class[] indexClassList={Question.class,Answer.class,Activity.class,};
    public final int SEARCH_TYPE_QUESTION=1;
    public final int SEARCH_TYPE_ANSWER=2;
    public final int SEARCH_TYPE_ACTIVITY=3;
    public final int SEARCH_TYPE_USER=4;
	public SearchStrategy ss=SearchStrategy.getSearchStrategyInstanc();
	@Autowired
	QuestionDao qd;
	@Autowired
	AnswerDao ad;
	@Autowired
	ActivityDao activityDao;
	@Autowired 
	UserInfoDao userInfoDao;
    public  final HashMap<Class, BaseDao> ENTITY_DAO=new HashMap<Class, BaseDao>();
    
    
 
	public List<Question> searchQuestion(String query,int page){
		List<Question> qList=new LinkedList<Question>();
		List<Object> midResult=ss.searchByQueryString(query, page);
		for(Object o:midResult){
			Question question=getQuestion(o);
			if(question!=null)qList.add(question);
		}
		return qList;
	}
	
	/**该方法由上层指定搜索的类别，如问题，活动等，只搜索指定类别的数据,搜索来源于搜索引擎
	 * @param query
	 * @param page
	 * @param pageSize
	 * @param type    1:问题   2:回答   3:活动 4:用户 5:
	 * @return
	 */
	public List<Object> searchEntity(String query,int page,int pageSize,int type){
		List<Object> result=new LinkedList<Object>();
		Class targetClass=null;
		for(Class c:indexClassList){
			IndexField classAnnotation = (IndexField) c.getAnnotation(IndexField.class);
			if(classAnnotation==null)continue;
			if(classAnnotation.type()==type)
			{  targetClass=c;
			}
		}
		if(targetClass==null)return null;
		
		IndexField targetAnnotation = (IndexField) targetClass.getAnnotation(IndexField.class);
		
		String[] fields=new String[1];
		String[] conditionQueries=new String[1];
		//flags 为 MUST(等效与and),SHOULD(等效与or)等
		String[] flags={"MUST"};
		fields[0]=targetAnnotation.fieldName();
		conditionQueries[0]=targetAnnotation.fieldValue();
		List<Object> midResult=ss.searchByQueryString(query, fields, conditionQueries, flags, page,  pageSize);
		for(Object o:midResult){
			Object dataEntity=getDataEntity(o);
			if(dataEntity==null)continue;
			result.add(dataEntity);
		}
		return result;
	}
	
	public List<UserInfo> searchUser(String query,int page,int pageSize){
		return userInfoDao.searchUserInfos(query, page, pageSize);
	}
	
	public Question getQuestion(Object o){
		String id=getQuestionId(o);
		
		Question question=qd.get(id);
		return question;
	}
	
	
	
	
	
	
	public List<Object> searchAllData(String query,int page,int pageSize){
		List<Object> midResult=ss.searchByQueryString(query, page,pageSize);
		List<Object> result=new LinkedList<Object>();
		for(Object o:midResult){
			Object dataEntity=getDataEntity(o);
			if(dataEntity==null)continue;
			result.add(dataEntity);
		}
		return result;
	}
	/**获取搜索返回的中间结果的对应entity实体
	 * @param o
	 * @return
	 */
	public Object getDataEntity(Object o){
		try{
		String dataTableName=(String) ((Map)o).get("table");
		if(dataTableName==null)return null;
		//根据table字段判断返回的数据类型，并从数据库中查询
		
	for(Class c:indexClassList){
		IndexField classAnnotation = (IndexField) c.getAnnotation(IndexField.class);
		if(classAnnotation==null)continue;
		if(classAnnotation.fieldName().equals("table")
				&&classAnnotation.fieldValue().equals(dataTableName))
		{   String dataId=(String) ((Map)o).get("id");
		//此处变为小写有问题，应该是首字母小写
			String daoName=c.getSimpleName().toLowerCase()+"Dao";
		    BaseDao daoObject=(BaseDao)SpringContextUtil.getApplicationContext().getBean(daoName);
			return daoObject.get(dataId);
		}
	}
	
		}
		catch(Exception e){
			return null;
		}
		return null;
	
	}
	public String getQuestionId(Object o){
		String tableName=(String) ((Map)o).get("table");
		String id="";
		if(tableName==null)return null;
		if(tableName.equals("answer")){String ansId=(String) ((Map)o).get("id");
		Answer answer=ad.get(ansId);
		if(answer!=null)return answer.getQuestion();
		}
		else if(tableName.equals("question")){id=(String) ((Map)o).get("id");}
		return id;
		
		
	}
}
