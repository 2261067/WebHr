package com.proj.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.AttentionUserService;
import com.proj.service.UserManagerService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.session.SessionHelper;
import com.proj.utils.JsonUtils;
@Controller
public class PeopleShowController {
	@Autowired
	private UserManagerService userManagerService;
	@Autowired
	private AttentionUserService attentionUserService;
	@Autowired
	private QuestionAndAnswerService qas;
	
	@RequestMapping("/ajax/PeopleShow.do")
	@ResponseBody
	public String getUserShowInfo(@RequestParam("uid")String uid,HttpServletRequest request){
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String,Object>>();
		UserInfo userInfo = userManagerService.getUserById(uid);
		if(userInfo!=null){
			UserInfo currentUserInfo = SessionHelper.getSessionAttribute(request, SessionHelper.SESSION_USER, UserInfo.class);
			if(currentUserInfo== null){
				result.setError(ResultNo.NEED_LOGIN);
				return JsonUtils.toJson(userInfo);
			}
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("user", userInfo);
			map.put("isAttentionUser",attentionUserService.isAttention(currentUserInfo.getUserId(), uid));
			map.put("questionCount", qas.getUserQuestionCount(uid));
			map.put("answerCount",qas.getUserAnswerCount(uid));
			map.put("votedCount",qas.getUserAnswerVotedCount(uid));
			result.setRsm(map);
		}
		
		return JsonUtils.toJson(result);
	}
}
