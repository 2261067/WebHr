package com.proj.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.proj.dao.TopicDao;
import com.proj.entity.Topic;
import com.proj.resultData.BaseResult;
import com.proj.service.TopicService;

@Controller
@RequestMapping("/search/ajax/find_topic.do")
public class AjaxFindTopicController {

	@Autowired
	private TopicService topicService;
	
	@RequestMapping()
	@ResponseBody
	public String findTopic(@RequestParam("topic_title") String topicName,HttpServletResponse response){
		Topic topic = topicService.getTopicByName(topicName);
		BaseResult<Topic> result = new BaseResult<Topic>();
		if(topic == null){
			result.setErrno(0);
			result.setErr("没有此话题");
		}else{
			result.setRsm(topic);
		}
		
		Gson gson = new Gson();
		
		response.setContentType("application/json");

		return gson.toJson(result);
	}
	
}
