package com.proj.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.resultData.BaseResult;
import com.proj.service.ActivityService;
import com.proj.service.TopicService;
import com.proj.service.UserManagerService;
import com.proj.service.UserQuestionService;
import com.proj.utils.ConstantUtil;
import com.proj.utils.EncrypAES;
import com.proj.utils.GUIDGenerator;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;
import com.proj.entity.Activity;
import com.proj.entity.AdminInfo;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;

@Controller
@RequestMapping()
public class AdminManageController {

	private static final Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	private TopicService ts;

	@Autowired
	private UserQuestionService uqs;

	@Autowired
	private ActivityService as;

	@Autowired
	private UserManagerService ums;

	/*
	 * 获取话题总数
	 */
	@RequestMapping(value = "/adminmanage/gettopiccount.do", method = RequestMethod.GET)
	public @ResponseBody String findAllTopicCount() {
		BaseResult<String> result = new BaseResult<String>();
		long topicCount = ts.findAllTopicCount();
		result.setRsm(Long.toString(topicCount));
		return JsonUtils.toJson(result);
	}

	/*
	 * 获取话题信息
	 */
	@RequestMapping(value = "/adminmanage/topicinfo.do", method = RequestMethod.GET)
	public @ResponseBody String topicInfo(
			@RequestParam("topicid") String topicid) {
		BaseResult<Topic> result = new BaseResult<Topic>();
		Topic t = ts.getTopicById(topicid);
		result.setRsm(t);
		return JsonUtils.toJson(result);
	}

	/*
	 * 根据页面大小和页数返回话题
	 */
	@RequestMapping(value = "/adminmanage/{page}/{pagesize}/gettopic.do", method = RequestMethod.GET)
	public @ResponseBody String adminGetTopic(@PathVariable String page,
			@PathVariable String pagesize) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Topic> topic = ts.findAllTopicByPageAndTimeDesc(
				Integer.parseInt(page), Integer.parseInt(pagesize));
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Topic", topic);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 查找话题是否已经存在ByName
	 */
	@RequestMapping(value = "/adminmanage/{topicname}/topicnamecheck.do", method = RequestMethod.GET)
	public @ResponseBody String topicNameCheck(@PathVariable String topicname) {
		BaseResult<Topic> result = new BaseResult<Topic>();
		Topic t = ts.getTopicByName(topicname);
		if (t != null) {
			result.setErr("话题已存在！");
		}
		result.setRsm(t);
		return JsonUtils.toJson(result);
	}

	/*
	 * 判断话题能否修改
	 */
	@RequestMapping(value = "/adminmanage/{topicid}/{topicname}/topicupdatecheck.do", method = RequestMethod.GET)
	public @ResponseBody String topicIdCheck(@PathVariable String topicid,
			@PathVariable String topicname) {
		BaseResult<Topic> result = new BaseResult<Topic>();
		Topic t = ts.getTopicById(topicid);
		Topic topic = ts.getTopicByName(topicname);
		if ((topic != null && !topic.getTopicName().equals(t.getTopicName()))
				|| StringUtils.isBlank(topicname)) {
			result.setErr("话题已存在！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 删除话题
	 */
	@RequestMapping(value = "/adminmanage/deletetopic.do", method = RequestMethod.GET)
	public @ResponseBody String deleteTopic(
			@ModelAttribute("currentAdmin") AdminInfo curAdmin, ModelMap m,
			@RequestParam("topicid") String topicid) throws Exception {
		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (!ts.deleteTopicById(topicid)) {
			result.setErr("删除话题失败！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 添加话题
	 */
	@RequestMapping(value = "/adminmanage/addtopic.do")
	public String addTopic(HttpServletRequest req, ModelMap m,
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			@RequestParam("topicname") String topicname,
			@RequestParam("topicdesc") String topicdesc) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}
		boolean isSuccess = true;

		if (ts.getTopicByName(topicname) != null
				|| StringUtils.isBlank(topicname)
				|| StringUtils.isBlank(topicdesc)) {
			isSuccess = false;
		}

		if (isSuccess) {
			Topic t = new Topic();
			Date now = new Date();
			t.setTopicId(GUIDGenerator.getGUID());
			t.setTopicName(topicname);
			t.setTopicDesc(topicdesc);
			t.setTopicImage("/picture/topic/default.jpg");
			t.setCreateTime(StringUtils.dateFormatTransform(now,
					ConstantUtil.DATE_FORMAT));
			ts.savaTopic(t);

			LOG.info("admin添加话题成功！");
		} else {
			LOG.info("admin添加话题失败！");
		}
		return "redirect:/redirect.do?page=adminTopicAdd";
	}

	/*
	 * 更新话题
	 */
	@RequestMapping(value = "/adminmanage/updatetopic.do")
	public String updateTopic(HttpServletRequest req, ModelMap m,
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			@RequestParam("topicid") String topicid,
			@RequestParam("topicname") String topicname,
			@RequestParam("topicdesc") String topicdesc) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}
		boolean isSuccess = true;
		Topic t = ts.getTopicById(topicid);
		Topic topic = ts.getTopicByName(topicname);
		if ((topic != null && !topic.getTopicName().equals(t.getTopicName()))
				|| StringUtils.isBlank(topicname)
				|| StringUtils.isBlank(topicdesc)) {
			isSuccess = false;
		}

		if (isSuccess) {
			t.setTopicName(topicname);
			t.setTopicDesc(topicdesc);
			ts.updateTopic(t);
			LOG.info("admin修改话题成功！");
		} else {
			LOG.info("admin修改话题失败！");
		}
		return "redirect:/redirect.do?page=adminTopicUpdate&topicid="
				+ t.getTopicId();
	}

	/*
	 * 获取问题总数
	 */
	@RequestMapping(value = "/adminmanage/getquestioncount.do", method = RequestMethod.GET)
	public @ResponseBody String findAllQuestionCount() {
		BaseResult<String> result = new BaseResult<String>();
		long questionCount = uqs.getAllQuestionCount();
		result.setRsm(Long.toString(questionCount));
		return JsonUtils.toJson(result);
	}

	/*
	 * 根据页面大小和页数返回问题
	 */
	@RequestMapping(value = "/adminmanage/{page}/{pagesize}/getquestion.do", method = RequestMethod.GET)
	public @ResponseBody String adminGetQuestion(@PathVariable String page,
			@PathVariable String pagesize) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Question> question = uqs.getAllQuestions(Integer.parseInt(page),
				Integer.parseInt(pagesize));
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Question", question);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 删除问题
	 */
	@RequestMapping(value = "/adminmanage/deletequestion.do", method = RequestMethod.GET)
	public @ResponseBody String deleteQuestion(
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			HttpServletRequest req, ModelMap m,
			@RequestParam("questionid") String questionid) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (!uqs.deleteQuestionById(questionid)) {
			result.setErr("删除问题失败！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 获取活动总数
	 */
	@RequestMapping(value = "/adminmanage/getactivitycount.do", method = RequestMethod.GET)
	public @ResponseBody String findAllActivityCount() {
		BaseResult<String> result = new BaseResult<String>();
		long activityCount = as.getAllActivityCount();
		result.setRsm(Long.toString(activityCount));
		return JsonUtils.toJson(result);
	}

	/*
	 * 根据页面大小和页数返回活动
	 */
	@RequestMapping(value = "/adminmanage/{page}/{pagesize}/getactivity.do", method = RequestMethod.GET)
	public @ResponseBody String adminGetActivity(@PathVariable String page,
			@PathVariable String pagesize) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Activity> activity = as.getAllActivities(Integer.parseInt(page),
				Integer.parseInt(pagesize));
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Activity", activity);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 删除活动
	 */
	@RequestMapping(value = "/adminmanage/deleteactivity.do", method = RequestMethod.GET)
	public @ResponseBody String deleteActivity(
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			HttpServletRequest req, ModelMap m,
			@RequestParam("activityid") String activityid) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (!as.deleteActivityById(activityid)) {
			result.setErr("删除活动失败！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 获取用户总数
	 */
	@RequestMapping(value = "/adminmanage/getusercount.do", method = RequestMethod.GET)
	public @ResponseBody String findAllUserCount() {
		BaseResult<String> result = new BaseResult<String>();
		long userCount = ums.getAllUserNum();
		result.setRsm(Long.toString(userCount));
		return JsonUtils.toJson(result);
	}

	/*
	 * 获取用户信息
	 */
	@RequestMapping(value = "/adminmanage/userinfo.do", method = RequestMethod.GET)
	public @ResponseBody String userInfo(@RequestParam("userid") String userid) {
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		UserInfo user = ums.getUserById(userid);
		user.setPassword(EncrypAES.getInstance().DecryptorToString(
				user.getPassword()));
		result.setRsm(user);
		return JsonUtils.toJson(result);
	}

	/*
	 * 根据页面大小和页数返回用户信息
	 */
	@RequestMapping(value = "/adminmanage/{page}/{pagesize}/getuser.do", method = RequestMethod.GET)
	public @ResponseBody String adminGetUser(@PathVariable String page,
			@PathVariable String pagesize) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<UserInfo> user = ums.getAllUserInfo(Integer.parseInt(page),
				Integer.parseInt(pagesize));
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("User", user);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 删除用户
	 */
	@RequestMapping(value = "/adminmanage/deleteuser.do", method = RequestMethod.GET)
	public @ResponseBody String deleteUser(
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			HttpServletRequest req, ModelMap m,
			@RequestParam("userid") String userid) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (!ums.deleteUserById(userid)) {
			result.setErr("删除用户失败！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 判断用户邮箱能否修改
	 */
	@RequestMapping(value = "/adminmanage/{userid}/{email}/userupdatecheck.do", method = RequestMethod.GET)
	public @ResponseBody String userCheck(@PathVariable String userid,
			@PathVariable String email) {
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		
		UserInfo user = ums.getUserById(userid);
		UserInfo u = ums.getUserByEmail(email);
		
		if  (u != null && !user.getEmail().equals(u.getEmail())
				|| StringUtils.isBlank(email)) {
			result.setErr("邮箱已存在！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 更新用户信息
	 */
	@RequestMapping(value = "/adminmanage/updateuser.do")
	public String updateUser(HttpServletRequest req, ModelMap m,
			@ModelAttribute("currentAdmin") AdminInfo curAdmin,
			@RequestParam("userid") String userid,
			@RequestParam("email") String email,
			@RequestParam("password") String password) throws Exception {

		if (curAdmin == null) {
			throw new Exception("登录失效，请重新登录！");
		}

		boolean isSuccess = true;

		UserInfo user = ums.getUserById(userid);
		UserInfo u = ums.getUserByEmail(email);
		
		if  (u != null && !user.getEmail().equals(u.getEmail())
				|| StringUtils.isBlank(email)) {
			isSuccess = false;
		}

		if (isSuccess) {
			user.setEmail(email);
			user.setPassword(EncrypAES.getInstance().EncrytorToString(password));
			ums.updateUserInfo(user);
			LOG.info("admin修改用户信息成功！");
		} else {
			LOG.info("admin修改用户信息失败！");
		}
		return "redirect:/redirect.do?page=adminUserUpdate&userid="
				+ user.getUserId();
	}

}
