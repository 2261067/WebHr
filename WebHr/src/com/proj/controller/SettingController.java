package com.proj.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.service.UserManagerService;
import com.proj.session.SessionHelper;
import com.proj.utils.ConfigProperties;
import com.proj.utils.ConstantUtil;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
@RequestMapping()
@SessionAttributes("currentUser")
public class SettingController extends BaseController {

	public static final int RESULT_STATUS_SUCCESS = 1;
	private static final Logger LOG = Logger.getLogger(SettingController.class);

	@Autowired
	private UserManagerService ums;

	@RequestMapping("/setting.do")
	public String settingProfilePage() {
		return "setting/profile";
	}

	/*
	 * 检查nickname(昵称)是否能够保存修改
	 */
	@RequestMapping(value = "setting/{nickname}/nicknamecheck.do", method = RequestMethod.GET)
	public @ResponseBody String nicknamecheck(HttpServletRequest req,
			HttpServletResponse rep, @PathVariable String nickname) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (user.getNickName().equals(nickname)) {
			return JsonUtils.toJson(result);
		}
		if (ums.isExistUserNickname(nickname)) {
			result.setErr("昵称已存在！");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 修改密码：判断用户输入密码是否正确
	 */
	@RequestMapping(value = "setting/{oldpwd}/oldpwdcheck.do", method = RequestMethod.GET)
	public @ResponseBody String oldpwdcheck(HttpServletRequest req,
			HttpServletResponse rep, @PathVariable String oldpwd) {

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);

		UserInfo isExits = ums.userLogin(user.getUserId(), oldpwd);
		if (isExits == null || StringUtils.isBlank(oldpwd)) {
			result.setErr("密码错误！");
			return JsonUtils.toJson(result);
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * Android修改密码
	 */
	@RequestMapping(value = "/settingpwd.do", params = "client=android")
	@ResponseBody
	public String settingPwdMobile(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("userid") String userid,
			@RequestParam("input-password-old") String oldpwd,
			@RequestParam("input-password-re-new") String newpwd) {

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo isExits = ums.userLogin(userid, oldpwd);
		if (isExits == null || StringUtils.isBlank(newpwd)
				|| StringUtils.isBlank(oldpwd)) {
			result.setErr("旧密码错误!");
			return JsonUtils.toJson(result);
		} else {
			boolean updatePwd = ums.updatePwd(isExits, newpwd);
			if (updatePwd) {
				return JsonUtils.toJson(result);
			} else {
				result.setErr("修改密码失败!");
				return JsonUtils.toJson(result);
			}
		}
	}

	/*
	 * PC修改密码
	 */
	@RequestMapping("/settingpwd.do")
	public String settingPwd(HttpServletRequest req, HttpServletResponse rep,
			@RequestParam("input-password-old") String oldpwd,
			@RequestParam("input-password-re-new") String newpwd) {

		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		UserInfo isExits = ums.userLogin(user.getUserId(), oldpwd);
		if (isExits == null || StringUtils.isBlank(newpwd)
				|| StringUtils.isBlank(oldpwd)) {
			LOG.warn(user.getUserId() + "修改密码失败!");
		} else {
			boolean updatePwd = ums.updatePwd(user, newpwd);
			if (updatePwd) {
				LOG.info(user.getUserId() + "修改密码成功!");
			} else {
				LOG.warn(user.getUserId() + "修改密码失败!");
			}
		}
		return "redirect:/redirect.do?page=setting/password";
	}

	/*
	 * 基本信息：上传图片
	 */
	@RequestMapping("setting/uploadimg.do")
	@ResponseBody
	public String uploadImg(@RequestParam("image") MultipartFile uploadimg,
			HttpServletRequest req, HttpServletResponse rep,
			@RequestParam("userid") String userid, ModelMap m)
			throws IOException {

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();

		UserInfo userInfo = ums.getUserById(userid);

		boolean deleteOldImg = false;

		if (userInfo == null) {
			result.setErr("需要登录");
			return JsonUtils.toJson(result);
		}

		if (uploadimg == null || uploadimg.isEmpty()) {
			result.setErr("图片获取失败！");
			return JsonUtils.toJson(result);
		}

		// 获取图片的文件名
		String fileName = uploadimg.getOriginalFilename();
		// 获取图片的扩展名
		String extensionName = fileName
				.substring(fileName.lastIndexOf(".") + 1);
		// 新的图片文件名 = 获取时间戳+"."图片扩展名
		String newFileName = String.valueOf(System.currentTimeMillis()) + "."
				+ extensionName;

		// 图片保存在服务器上绝对地址
		ConfigProperties cp = new ConfigProperties("upload-download.properties");
		String bathPath = cp.getValue("upload.basepath");
		String realPath = bathPath + ConstantUtil.RELATIVE_PATH + newFileName;

		// 将图片复制到指定服务器地址
		InputStream in = uploadimg.getInputStream();
		File f = new File(realPath);
		FileUtils.copyInputStreamToFile(in, f);

		// 存储新图片删除旧图片
		String deleteRealPath = bathPath + userInfo.getPhoto();
		if (!userInfo.getPhoto().equals(ConstantUtil.ANONYMOUS_PHOTO)) {
			deleteOldImg = true;
		}

		// 保存图片路径到数据库
		if (!fileName.equals("")) {
			userInfo.setPhoto(ConstantUtil.RELATIVE_PATH + newFileName);
		}
		if (ums.updateUserInfo(userInfo)) {
			if (deleteOldImg) {
				StringUtils.deleteFile(deleteRealPath);
			}
			m.addAttribute("currentUser", userInfo);

			result.setErr(ConstantUtil.RELATIVE_PATH + newFileName);
			result.setErrno(1);
			in.close();
			return JsonUtils.toJson(result);
		} else {
			result.setErr("图片上传失败！");
			in.close();
			return JsonUtils.toJson(result);
		}

	}


	/*
	 * 修改基本信息
	 */
	@RequestMapping("/updateuserinfo.do")
	public String updateUserinfo(HttpServletRequest req,
			HttpServletResponse rep,
			@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam(value = "gender", required = false) String gender,
			@RequestParam("autograph") String autograph,
			@RequestParam("introduce") String introduce,
			@RequestParam("position") String position,
			@RequestParam("phonenumber") String phonenumber,
			@RequestParam("workExperience") String workExperience,
			@RequestParam("nickname") String nickname) {

		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);

		boolean isSuccess = true;

		if (!user.getNickName().equals(nickname)
				&& ums.isExistUserNickname(nickname)) {
			isSuccess = false;
		}

		if (isSuccess) {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setGender(gender);
			user.setAutograph(autograph);
			user.setIntroduce(introduce);
			user.setPhoneNumber(phonenumber);
			user.setPosition(position);
			user.setNickName(nickname);
			user.setWorkExperience(workExperience);
			if (ums.updateUserInfo(user) == false) {
				LOG.warn("修改信息失败!");
			}
		}

		return "redirect:/setting.do";
	}

	/*
	 * android端修改个人信息--昵称
	 */
	@RequestMapping(value = "/updateuserinfonickname.do", params = "client=android")
	@ResponseBody
	public String updateUserinfoNickname(HttpServletRequest req,
			@RequestParam("nickname") String nickname) {
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		if (user == null) {
			result.setErr("请登陆！");
			result.setErrno(3000);
			return JsonUtils.toJson(result);
		} else if (!user.getNickName().equals(nickname)
				&& ums.isExistUserNickname(nickname)) {
			result.setErr("昵称已存在！修改失败！");
			return JsonUtils.toJson(result);
		} else {
			user.setNickName(nickname);
			ums.updateUserInfo(user);
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
	}

	/*
	 * android端修改个人信息--性别（1男 2女）
	 */
	@RequestMapping(value = "/updateuserinfogender.do", params = "client=android")
	@ResponseBody
	public String updateUserinfoGender(HttpServletRequest req,
			@RequestParam("gender") String gender) {
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		if (user == null) {
			result.setErr("请登陆！");
			result.setErrno(3000);
			return JsonUtils.toJson(result);
		} else {
			user.setGender(gender);
			ums.updateUserInfo(user);
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
	}

	/*
	 * android端修改个人信息--职位
	 */
	@RequestMapping(value = "/updateuserinfoposition.do", params = "client=android")
	@ResponseBody
	public String updateUserinfoPosition(HttpServletRequest req,
			@RequestParam("position") String position) {
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		if (user == null) {
			result.setErr("请登陆！");
			result.setErrno(3000);
			return JsonUtils.toJson(result);
		} else {
			user.setPosition(position);
			ums.updateUserInfo(user);
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
	}

	/*
	 * android端修改个人信息--手机号
	 */
	@RequestMapping(value = "/updateuserinfophonenumber.do", params = "client=android")
	@ResponseBody
	public String updateUserinfoPhonenumber(HttpServletRequest req,
			@RequestParam("phonenumber") String phonenumber) {
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		if (user == null) {
			result.setErr("请登陆！");
			result.setErrno(3000);
			return JsonUtils.toJson(result);
		} else {
			user.setPhoneNumber(phonenumber);
			ums.updateUserInfo(user);
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
	}

	/*
	 * android端修改个人信息--个人介绍
	 */
	@RequestMapping(value = "/updateuserinfoautograph.do", params = "client=android")
	@ResponseBody
	public String updateUserinfoAutograph(HttpServletRequest req,
			@RequestParam("autograph") String autograph) {
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		if (user == null) {
			result.setErr("请登陆！");
			result.setErrno(3000);
			return JsonUtils.toJson(result);
		} else {
			user.setAutograph(autograph);
			ums.updateUserInfo(user);
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
	}

}
