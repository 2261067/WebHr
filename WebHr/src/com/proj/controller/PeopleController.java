package com.proj.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.Activity;
import com.proj.entity.AttentionTopic;
import com.proj.entity.AttentionUser;
import com.proj.entity.Question;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.service.ActivityService;
import com.proj.service.AttentionUserService;
import com.proj.service.TopicService;
import com.proj.service.UserManagerService;
import com.proj.service.UserQuestionService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.service.topic.TopicShowService;
import com.proj.session.SessionHelper;
import com.proj.utils.JsonUtils;

@Controller
@RequestMapping()
public class PeopleController {

	public static final int RESULT_STATUS_SUCCESS = 1;
	private static final Logger LOG = Logger.getLogger(SettingController.class);

	@Autowired
	private UserQuestionService uqs;

	@Autowired
	private QuestionAndAnswerService qas;

	@Autowired
	private UserManagerService ums;

	@Autowired
	private ActivityService as;

	@Autowired
	private AttentionUserService aus;

	@Autowired
	private TopicShowService tss;

	/*
	 * 用户主页定向
	 */
	@RequestMapping("/people.do")
	public String peoplePage(HttpServletRequest req, HttpServletResponse rep,
			@RequestParam("uid") String uid) {

		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		String userid = user.getUserId();
		if (!userid.equals(uid)) {
			return "otherPeople";
		}
		return "people";
	}

	/*
	 * 根据url中uid得到用户信息
	 */
	@RequestMapping("/peopleinfo.do")
	@ResponseBody
	public String peopleInfo(HttpServletRequest req, HttpServletResponse rep,
			@RequestParam("uid") String uid) {
		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		UserInfo user = ums.peopleInfo(uid);
		if (user == null) {
			result.setErr("用户不存在!");
			result.setRsm(user);
			return JsonUtils.toJson(result);
		}
		user.setPassword("?");
		result.setRsm(user);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户基本信息
	 */
	@RequestMapping("/people/information.do")
	@ResponseBody
	public String peopleInformation(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("uid") String uid) {

		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		UserInfo user = (UserInfo) req.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		String userId = user.getUserId();
		AttentionUser au = aus.getAttentionUser(userId, uid);
		String isAttention = "YES";
		if (au == null) {
			isAttention = "NO";
		}
		UserInfo currentUser = ums.peopleInfo(uid);
		currentUser.setPassword("?");
		long userAnswerVotedCount = qas.getUserAnswerVotedCount(uid);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("IsAttention", isAttention);
		map.put("UserInfo", currentUser);
		map.put("UserAnswerVotedCount", userAnswerVotedCount);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户发问展示
	 */
	@RequestMapping("/people/question.do")
	@ResponseBody
	public String peopleQuestionPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("page") String page,
			@RequestParam("uid") String uid) {

		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		UserInfo user = ums.peopleInfo(uid);
		List<Question> question = null;
		question = uqs.getUserQuestions(user, Integer.parseInt(page));
		HashMap<String, Object> map = new HashMap<String, Object>();
		// 取问题关注数
		long questionAttentionCounts[] = new long[question.size()];
		// 取问题回答数
		long questionAnswerCounts[] = new long[question.size()];
		int count = 0;// 计数
		for (Question q : question) {
			questionAttentionCounts[count] = qas.getQuestionAttentionCount(q
					.getQuestionId());
			questionAnswerCounts[count] = qas.getQuestionAnswerCount(q
					.getQuestionId());
			count++;
		}
		map.put("QuestionAttentionCounts", questionAttentionCounts);
		map.put("QuestionAnswerCounts", questionAnswerCounts);
		map.put("Question", question);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户回复问题展示
	 */
	@RequestMapping("/people/answer.do")
	@ResponseBody
	public String peopleAnswerPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("page") String page,
			@RequestParam("uid") String uid) {

		BaseResult<List<Object>> result = new BaseResult<List<Object>>();
		List<Object> answer = null;
		UserInfo user = ums.peopleInfo(uid);
		answer = qas.getUserAnswers(user, Integer.parseInt(page));
		result.setRsm(answer);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户发起活动展示
	 */
	@RequestMapping("/people/activity.do")
	@ResponseBody
	public String peopleActivityPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("page") String page,
			@RequestParam("uid") String uid) {

		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		UserInfo user = ums.peopleInfo(uid);
		List<Activity> activity = as.getUseActivity(user,
				Integer.parseInt(page));
		long activitySignCounts[] = new long[activity.size()];
		int count = 0;// 计数
		for (Activity a : activity) {
			activitySignCounts[count] = as.getActivitySignCount(a.getActivityId());
			count++;
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ActivitySignCounts", activitySignCounts);
		map.put("Activity", activity);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户关注人物列表
	 */
	@RequestMapping("/people/attentionpeople.do")
	@ResponseBody
	public String peopleAttentionPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("page") String page,
			@RequestParam("uid") String uid) {

		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Object> attentionUser = aus.getAttentionUserList(uid,
				Integer.parseInt(page));
		long attentionUserNum = aus.getAttentionUserNum(uid);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("AttentionUser", attentionUser);
		map.put("AttentionUserNum", attentionUserNum);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户粉丝列表
	 */
	@RequestMapping("/people/followpeople.do")
	@ResponseBody
	public String peopleFollowPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("page") String page,
			@RequestParam("uid") String uid) {

		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Object> followUser = aus.getFollowUserList(uid,
				Integer.parseInt(page));
		long followUserNum = aus.getFollowUserNum(uid);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("FollowUser", followUser);
		map.put("FollowUserNum", followUserNum);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 用户关注话题
	 */
	@RequestMapping("/people/attentiontopic.do")
	@ResponseBody
	public String peopleAttentionTopicPage(HttpServletRequest req,
			HttpServletResponse rep, @RequestParam("uid") String uid,
			@RequestParam("page") String page) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		List<Object> attentionTopic = tss.findAttentionTopic(uid,
				Integer.parseInt(page));
		long atentionTopicNum = tss.getAttentionCountByuid(uid);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("AttentionTopic", attentionTopic);
		map.put("AtentionTopicNum", atentionTopicNum);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

}
