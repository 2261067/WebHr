package com.proj.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.AdminInfo;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.service.UserManagerService;
import com.proj.utils.ConstantUtil;
import com.proj.utils.EncrypAES;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

/**
 * @author Hla ע��Controller
 */

@Controller
@RequestMapping()
public class RegisterController extends BaseController {

	public static final int RESULT_STATUS_SUCCESS = 1;
	private static final Logger LOG = Logger
			.getLogger(RegisterController.class);

	@Autowired
	private UserManagerService ums;

	/*
	 * ���username(user_id)�Ƿ��Ѿ�ע��
	 */
	@RequestMapping(value = "html/register/{username}/usernamecheck.do", method = RequestMethod.GET)
	public @ResponseBody String usernamecheck(@PathVariable String username) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistUserId(username)) {
			result.setErr("�û�����ע��");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * ������Աusername(admin_id)�Ƿ��Ѿ�ע��
	 */
	@RequestMapping(value = "html/adminregister/{username}/usernamecheck.do", method = RequestMethod.GET)
	public @ResponseBody String adminnamecheck(@PathVariable String username) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistAdminId(username)) {
			result.setErr("�û�����ע��");
		}
		return JsonUtils.toJson(result);
	}
	
	/*
	 * ���email�Ƿ��Ѿ�ע��
	 */
	@RequestMapping(value = "html/register/{email}/emailcheck.do", method = RequestMethod.GET)
	public @ResponseBody String emailcheck(@PathVariable String email) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistEmail(email)) {
			result.setErr("������ע��");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * ������Աemail�Ƿ��Ѿ�ע��
	 */
	@RequestMapping(value = "html/adminregister/{email}/emailcheck.do", method = RequestMethod.GET)
	public @ResponseBody String adminemailcheck(@PathVariable String email) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistAdminEmail(email)) {
			result.setErr("������ע��");
		}
		return JsonUtils.toJson(result);
	}
	
	/*
	 * ���nickname(�ǳ�)�Ƿ��Ѿ�ע��
	 */
	@RequestMapping(value = "html/register/{nickname}/nicknamecheck.do", method = RequestMethod.GET)
	public @ResponseBody String nicknamecheck(@PathVariable String nickname) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistUserNickname(nickname)) {
			result.setErr("�ǳ���ע��");
		}
		return JsonUtils.toJson(result);
	}
	

	/*
	 * ������Ա��Կ�Ƿ���ȷ
	 */
	@RequestMapping(value = "html/adminregister/{adminpwd}/adminpwdcheck.do", method = RequestMethod.GET)
	public @ResponseBody String adminpwdcheck(@PathVariable String adminpwd) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (!adminpwd.equals(ConstantUtil.ADMIN_PWD)) {
			result.setErr("��Կ����ȷ");
		}
		return JsonUtils.toJson(result);
	}
	
	/*
	 * Webע��
	 */
	@RequestMapping("/register.do")
	public String registerPc(HttpServletRequest req, ModelMap m,
			HttpServletResponse rep,
			@RequestParam("email") String email,
			@RequestParam("emailpwd") String emailpwd,
			@RequestParam("pwdcon") String pwdcon,
			@RequestParam("position") String position,
			@RequestParam("phone") String phone,
			@RequestParam("workexperienceid") String workexperienceid,
			@RequestParam("nickname") String nickname) throws IOException {

		boolean isSuccess = true;

//		if (ums.isExistUserId(username) || StringUtils.isBlank(username)) {
//			isSuccess = false;
//		}

		if (ums.isExistEmail(email) || StringUtils.isBlank(email)) {
			isSuccess = false;
		}
		
		if (ums.isExistUserNickname(nickname) || StringUtils.isBlank(nickname)) {
			isSuccess = false;
		}

		if (!emailpwd.equals(pwdcon) || StringUtils.isBlank(emailpwd)
				|| StringUtils.isBlank(pwdcon)) {
			isSuccess = false;
		}

		if (isSuccess) {
			UserInfo user = new UserInfo();
			Date now = new Date();
			user.setUserId(email);
			//��������
			user.setPassword(EncrypAES.getInstance().EncrytorToString(pwdcon));
			user.setEmail(email);
			user.setPhoneNumber(phone);
			user.setPosition(position);
			user.setWorkExperience(StringUtils.getWorkExperience(workexperienceid));
			user.setNickName(nickname);
			user.setPhoto(ConstantUtil.RELATIVE_PATH + "default.jpg");
			user.setFirstName("");
			user.setLastName("");
			user.setGender("1");
			user.setAutograph("");
			user.setIntroduce("");
			user.setCreateTime(StringUtils.dateFormatTransform(now,
					ConstantUtil.DATE_FORMAT));
			ums.saveUserInfo(user);
			LOG.info("ע��ɹ���");

			return "redirect:/html/signin.html";
		} else {
			LOG.info("ע��ʧ�ܣ�");
			return "redirect:/html/register.html";
		}

	}

	/*
	 * ����ԱWebע��
	 */
	@RequestMapping("/adminregister.do")
	public String adminregister(HttpServletRequest req, ModelMap m,
			HttpServletResponse rep, @RequestParam("username") String username,
			@RequestParam("email") String email,
			@RequestParam("emailpwd") String emailpwd,
			@RequestParam("pwdcon") String pwdcon,
			@RequestParam("adminpwd") String adminpwd) throws IOException {

		boolean isSuccess = true;

		if (ums.isExistAdminId(username) || StringUtils.isBlank(username)) {
			isSuccess = false;
		}

		if (ums.isExistAdminEmail(email) || StringUtils.isBlank(email)) {
			isSuccess = false;
		}

		if (!emailpwd.equals(pwdcon) || StringUtils.isBlank(emailpwd)
				|| StringUtils.isBlank(pwdcon)) {
			isSuccess = false;
		}
		
		if(!adminpwd.equals(ConstantUtil.ADMIN_PWD)){
			isSuccess = false;
		}

		if (isSuccess) {
			AdminInfo admin = new AdminInfo();
			Date now = new Date();
			admin.setAdminId(username);
			//��������
			admin.setPassword(EncrypAES.getInstance().EncrytorToString(pwdcon));
			admin.setEmail(email);
			admin.setCreateTime(StringUtils.dateFormatTransform(now,
					ConstantUtil.DATE_FORMAT));
			ums.saveAdminInfo(admin);
			LOG.info("����Աע��ɹ���");
			return "redirect:/html/admin_login.html";
		} else {
			LOG.info("����Աע��ʧ�ܣ�");
			return "redirect:/html/admin_register.html";
		}

	}

	
	/*
	 * Androidע��
	 */
	@RequestMapping(value = "/register.do", params = "client=android")
	@ResponseBody
	public String registerMobile(HttpServletRequest req, ModelMap m,
			@RequestParam("password") String password,
			@RequestParam("nickname") String nickname,
			@RequestParam("phone") String phone,
			@RequestParam("email") String email) {

		BaseResult<String> result = new BaseResult<String>();

//		if (ums.isExistUserId(username) || StringUtils.isBlank(username)) {
//			result.setErr("�û����ѱ�ע��");
//			result.setErrno(2004);
//			LOG.info("�û�����ע��--ע��ʧ�ܣ�");
//			return JsonUtils.toJson(result);
//		}

		if (ums.isExistEmail(email) || StringUtils.isBlank(email)) {
			result.setErr("�����ѱ�ע��");
			result.setErrno(2008);
			LOG.info("ע��ʧ�ܣ�");
			return JsonUtils.toJson(result);
		}
		
		if (ums.isExistUserNickname(nickname)|| StringUtils.isBlank(nickname)) {
			result.setErr("�ǳ��ѱ�ע��");
			result.setErrno(2008);
			LOG.info("ע��ʧ�ܣ�");
			return JsonUtils.toJson(result);
		}

		UserInfo user = new UserInfo();
		Date now = new Date();
		
		user.setUserId(email);
		//��������
		user.setPassword(EncrypAES.getInstance().EncrytorToString(password));
		user.setEmail(email);
		user.setPhoneNumber(phone);
		user.setPosition("");
		user.setWorkExperience("");
		user.setNickName(nickname);
		user.setPhoto(ConstantUtil.RELATIVE_PATH + "default.jpg");
		user.setFirstName("");
		user.setLastName("");
		user.setGender("1");
		user.setAutograph("");
		user.setIntroduce("");
		user.setCreateTime(StringUtils.dateFormatTransform(now,
				ConstantUtil.DATE_FORMAT));
		ums.saveUserInfo(user);
		LOG.info("ע��ɹ���");
		return JsonUtils.toJson(result);
	}

}
