package com.proj.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.google.gson.reflect.TypeToken;
import com.proj.entity.Question;
import com.proj.entity.UserInfo;
import com.proj.resultData.*;
import com.proj.service.*;
import com.proj.service.UserQuestionService.QuestionData;


@Controller

@SessionAttributes("currentUser")
public class FirstPageController extends BaseController{
	
	private static final String getQuestionUrl="getQuestionById.do";
	@Autowired
	UserQuestionService uqs;
    final int defaultPageSize=10;
	@RequestMapping(value="getQuestionByTime.do")
	@ResponseBody
	public String getQuestionByTime(@ModelAttribute("currentUser") UserInfo curUser,@RequestParam("page")int page,@RequestParam(value="page_size",required=false)Integer pageSize,ModelMap m) throws Exception{
				if(curUser==null){
					
					throw new Exception("��¼ʧЧ�������µ�¼��");
		
		
				
				}
		if(pageSize==null)pageSize=defaultPageSize;
		List<QuestionData> list=uqs.getQuestionAndRelativeUsers(curUser, page,pageSize);
        long currentPage=page;
		long pageCount=uqs.getQuestionPages();
		m.addAttribute("pageCount", pageCount);
		m.addAttribute("currentPage",currentPage);
		PageDataResult result=new PageDataResult();
		result.setRsm(list);
		result.setCurrentPage(currentPage);
		result.setPageSize(pageSize);
		result.setAllDataCount(uqs.getAllQuestionCount());
		return this.toJson(result);
		
	}
    public static String getQuestionUrl(String questionId){
    	if(questionId!=null)
    	return getQuestionUrl+"?questionId="+questionId;
    	return "";
    }
	
	@RequestMapping(value=getQuestionUrl)
	
	public String getQuestionById(@RequestParam("questionId")String questionId,ModelMap m) throws Exception{

		Question question=uqs.getQuestionById(questionId);
		m.addAttribute(question);
		return "questionAndAnswer";
		
	}

}
