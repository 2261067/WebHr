package com.proj.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.Article;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.ArticleService;
import com.proj.service.AttentionUserService;
import com.proj.service.UserManagerService;
import com.proj.session.SessionHelper;
import com.proj.utils.GUIDGenerator;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
public class ArticleController {

	@Autowired
	private ArticleService articleService;

	@Autowired
	UserManagerService userManagerService;

	@Autowired
	private AttentionUserService attentionUserService;

	@RequestMapping("/publish/ajax/publish_article.do")
	@ResponseBody
	public String ajaxPublishArticle(
			@RequestParam(value = "article_name", required = false) String articleName,
			@RequestParam(value = "article_context", required = false) String articleContext,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (StringUtils.isBlank(articleName)
				|| StringUtils.isBlank(articleContext)) {
			result.setError(ResultNo.NEED_ARTICLE_NAME);
			return JsonUtils.toJson(result);
		}

		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		Article article = new Article();
		article.setArticleId(GUIDGenerator.getGUID());
		article.setUserId(userInfo.getUserId());
		article.setArticleName(articleName);
		article.setArticleContext(articleContext);
		article.setCommentNum(0);
		article.setImportant(0);
		article.setCreateTime(new Date());
		article.setLastModifyTime(new Date());
		articleService.saveArticle(article);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/publish/ajax/edit_article.do")
	@ResponseBody
	public String ajaxEditArticle(
			@RequestParam(value = "article_id", required = false) String articleId,
			@RequestParam(value = "article_name", required = false) String articleName,
			@RequestParam(value = "article_context", required = false) String articleContext,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (StringUtils.isBlank(articleName)
				|| StringUtils.isBlank(articleContext)) {
			result.setError(ResultNo.NEED_ARTICLE_NAME);
			return JsonUtils.toJson(result);
		}

		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		Article article = articleService.getArticleById(articleId);
		article.setArticleName(articleName);
		article.setArticleContext(articleContext);
		article.setLastModifyTime(new Date());
		articleService.updateArticle(article);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/getAllArticles.do")
	@ResponseBody
	public String getArticles(@RequestParam("page") int page,
			@RequestParam("pageSize") Integer size) {
		BaseResult<List<Object>> result = new BaseResult<List<Object>>();
		List<Object> articleData = articleService.getArticleData(page, size);
		result.setRsm(articleData);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/getArticleParams.do")
	@ResponseBody
	public String getArticleParams(@RequestParam("pageSize") Integer pageSize) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		long articleCount = articleService.getAllArticleCount();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("allArticleCount", articleCount);
		map.put("pageSize", pageSize);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/getArticleById.do")
	public String getActivity() {
		return "articleDetail";
	}

	@RequestMapping("/ajax/getArticle.do")
	@ResponseBody
	public String Article(@RequestParam("article_id") String articleId,
			HttpServletRequest request) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		HashMap<String, Object> map = new HashMap<String, Object>();

		Article article = articleService.getArticleById(articleId);
		map.put("Article", article);
		UserInfo user = userManagerService.getUserById(article.getUserId());
		map.put("user", user);

		UserInfo currentUser = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (currentUser != null) {
			// 是否关注发起活动者
			map.put("isAttentionUser", attentionUserService.isAttention(
					currentUser.getUserId(), user.getUserId()));
		}
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	/*
	 * 专栏评论
	 */
	@RequestMapping("/ajax/getArticleComments.do")
	@ResponseBody
	public String getArticleComments(
			@RequestParam("article_id") String articleId) {
		BaseResult<Object> result = new BaseResult<Object>();
		List<Object> ArticleCommentsAndUsers = articleService
				.getArticleCommentsAndUsers(articleId);
		if (ArticleCommentsAndUsers != null
				&& !ArticleCommentsAndUsers.isEmpty()) {
			result.setRsm(ArticleCommentsAndUsers);
		}
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/saveArticleComments.do")
	@ResponseBody
	public String saveArticleComments(
			@RequestParam("article_id") String articleId,
			@RequestParam("article_context") String articleContext,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (StringUtils.isBlank(articleContext)) {
			result.setError(ResultNo.NEED_ARTICLE_CONTEXT);
			return JsonUtils.toJson(result);
		}

		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo != null) {
			articleService.saveArticleComment(userInfo.getUserId(), articleId,
					articleContext);
		} else {
			result.setError(ResultNo.NEED_LOGIN);
		}
		return JsonUtils.toJson(result);
	}

}
