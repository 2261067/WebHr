package com.proj.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BaseController {
	public String toJson(Object o) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		String json = gson.toJson(o);
		return json;

	}

}
