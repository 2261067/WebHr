package com.proj.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.annotation.IndexField;
import com.proj.entity.Activity;
import com.proj.entity.ActivitySign;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.ActivityService;
import com.proj.service.AttentionUserService;
import com.proj.service.UserManagerService;
import com.proj.session.SessionHelper;
import com.proj.utils.ConstantUtil;
import com.proj.utils.GUIDGenerator;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
public class ActivityController {

	@Autowired
	private ActivityService activityService;

	@Autowired
	UserManagerService userManagerService;
	@Autowired
	private AttentionUserService attentionUserService;

	@RequestMapping("/activity.do")
	public String activity() {
		return "activity";
	}

	@RequestMapping("/publish/ajax/publish_activity.do")
	@ResponseBody
	public String ajaxPublishActivity(
			@RequestParam(value = "activity_name", required = false) String activityName,
			@RequestParam(value = "activity_detail", required = false) String activityDetail,
			@RequestParam(value = "activity_end_time", required = false) String activityEndTime,
			@RequestParam(value = "activity_host_time", required = false) String activityHostTime,
			@RequestParam(value = "activity_address", required = false) String activityAddress,
			@RequestParam(value = "activity_host_people", required = false) String activityHostPeople,
			@RequestParam(value = "activity_people_num", required = false) String activityPeopleNum,
			@RequestParam(value = "activity_phone_num", required = false) String activityPhoneNum,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (StringUtils.isBlank(activityName)
				|| StringUtils.isBlank(activityDetail) || StringUtils.isBlank(activityAddress)) {
			result.setError(ResultNo.NEED_ACTIVITY_NAME);
			return JsonUtils.toJson(result);
		}
		Date endTime;
		Date hostTime;
		if (StringUtils.isBlank(activityEndTime) || StringUtils.isBlank(activityHostTime)) {
			result.setError(ResultNo.NEED_ACTIVITY_TIME);
			return JsonUtils.toJson(result);
		} else {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						ConstantUtil.ACTIVITY_DATE_FORMAT);
				endTime = dateFormat.parse(activityEndTime);
				hostTime = dateFormat.parse(activityHostTime);
				if(!endTime.after(new Date()) || !hostTime.after(new Date())){
					result.setError(ResultNo.WRONG_ACTIVITY_TIME);
					return JsonUtils.toJson(result);
				}
			} catch (Exception e) {
				result.setError(ResultNo.WRONG_ACTIVITY_TIME);
				return JsonUtils.toJson(result);
			}

		}
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		Activity activity = new Activity();
		activity.setActivityId(GUIDGenerator.getGUID());
		activity.setUserId(userInfo.getUserId());
		activity.setActivityName(activityName);
		activity.setActivityDetail(activityDetail);
		activity.setEndTime(endTime);
		activity.setAddress(activityAddress);
		activity.setHostTime(hostTime);
		activity.setHostPeople(activityHostPeople);
		activity.setPeopleNum(activityPeopleNum);
		activity.setPhoneNum(activityPhoneNum);
		activity.setLastMotify(new Date());
		activity.setCreateTime(new Date());
		activityService.saveActvity(activity);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/publish/ajax/edit_activity.do")
	@ResponseBody
	public String ajaxEditActivity(
			@RequestParam(value = "activity_id", required = true) String activityId,
			@RequestParam(value = "activity_name", required = false) String activityName,
			@RequestParam(value = "activity_detail", required = false) String activityDetail,
			@RequestParam(value = "activity_end_time", required = false) String activityEndTime,
			@RequestParam(value = "activity_host_time", required = false) String activityHostTime,
			@RequestParam(value = "activity_address", required = false) String activityAddress,
			@RequestParam(value = "activity_host_people", required = false) String activityHostPeople,
			@RequestParam(value = "activity_people_num", required = false) String activityPeopleNum,
			@RequestParam(value = "activity_phone_num", required = false) String activityPhoneNum,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (StringUtils.isBlank(activityName)
				|| StringUtils.isBlank(activityDetail) || StringUtils.isBlank(activityAddress)) {
			result.setError(ResultNo.NEED_ACTIVITY_NAME);
			return JsonUtils.toJson(result);
		}
		Date endTime;
		Date hostTime;
		if (StringUtils.isBlank(activityEndTime) || StringUtils.isBlank(activityHostTime)) {
			result.setError(ResultNo.NEED_ACTIVITY_TIME);
			return JsonUtils.toJson(result);
		} else {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						ConstantUtil.ACTIVITY_DATE_FORMAT);
				endTime = dateFormat.parse(activityEndTime);
				hostTime = dateFormat.parse(activityHostTime);
				if(!endTime.after(new Date()) || !hostTime.after(new Date())){
					result.setError(ResultNo.WRONG_ACTIVITY_TIME);
					return JsonUtils.toJson(result);
				}
			} catch (Exception e) {
				result.setError(ResultNo.WRONG_ACTIVITY_TIME);
				return JsonUtils.toJson(result);
			}

		}
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		Activity activity = new Activity();
		activity.setActivityId(activityId);
		activity.setUserId(userInfo.getUserId());
		activity.setActivityName(activityName);
		activity.setActivityDetail(activityDetail);
		activity.setEndTime(endTime);
		activity.setAddress(activityAddress);
		activity.setHostTime(hostTime);
		activity.setHostPeople(activityHostPeople);
		activity.setPeopleNum(activityPeopleNum);
		activity.setPhoneNum(activityPhoneNum);
		activity.setLastMotify(new Date());
		activity.setCreateTime(new Date());
		activityService.updateActivity(activity);
		return JsonUtils.toJson(result);
	}
	
	
	@RequestMapping("/ajax/getAllActivities.do")
	@ResponseBody
	public String getActivities(@RequestParam("page") int page,@RequestParam(value="pageSize",required =false) Integer size) {
		BaseResult<List<Object>> result = new BaseResult<List<Object>>();
		List<Object> activityData = activityService.getActivityData(page,size);
		result.setRsm(activityData);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/getActivityParams.do")
	@ResponseBody
	public String getActivityParams() {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		long activityCount = activityService.getAllActivityCount();
		int pageSize = activityService.pageSize;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("allActivityCount", activityCount);
		map.put("pageSize", pageSize);
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/getActivityById.do")
	public String getActivity() {
		return "activityDetail";
	}

	@RequestMapping("/ajax/getActivity.do")
	@ResponseBody
	public String getActivityById(
			@RequestParam("activity_id") String activityId,
			HttpServletRequest request) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		HashMap<String, Object> map = new HashMap<String, Object>();

		Activity activity = activityService.getActivityById(activityId);
		map.put("activity", activity);
		UserInfo user = userManagerService.getUserById(activity.getUserId());
		map.put("user", user);
		map.put("signNum", activityService.getActivitySignCount(activityId));

		UserInfo currentUser = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (currentUser != null) {
			// 是否报名
			map.put("isSign",
					activityService.isSign(currentUser.getUserId(), activityId));
			// 是否关注发起活动者
			map.put("isAttentionUser", attentionUserService.isAttention(
					currentUser.getUserId(), user.getUserId()));
		}
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/getActivitySignUsers.do")
	@ResponseBody
	public String getActivitySignUsers(
			@RequestParam("activity_id") String activityId) {
		BaseResult<List<UserInfo>> result = new BaseResult<List<UserInfo>>();
		// 最近报名的20人
		result.setRsm(activityService.getLastestActivitySignUser(activityId));
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/signActivity.do")
	@ResponseBody
	public String signActivity(@RequestParam("activity_id") String activityId,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);

		if (userInfo != null) {
			HashMap<String, String> map = new HashMap<String, String>();
			ActivitySign activitySign = activityService.getActivitySign(
					userInfo.getUserId(), activityId);
			Activity activity = activityService.getActivityById(activityId);
			Date now = new Date();
			if (activitySign != null) {
				if (activity.getEndTime().after(now)) {
					activityService.deleteActivitySign(activitySign);
					map.put("type", "delete");
				} else {
					result.setError(ResultNo.EXPIRED_ACTIVITY_CANCEL_TIME);
				}
			} else {
				if (activity.getEndTime().after(now)) {
					activityService.saveActivitySign(userInfo.getUserId(),
							activityId);
					map.put("type", "add");
				} else {
					result.setError(ResultNo.EXPIRED_ACTIVITY_SIGN_TIME);
				}
			}
			result.setRsm(map);
		} else {
			result.setError(ResultNo.NEED_LOGIN);
		}

		return JsonUtils.toJson(result);

	}

}
