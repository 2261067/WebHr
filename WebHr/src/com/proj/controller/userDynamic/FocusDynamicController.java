package com.proj.controller.userDynamic;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.proj.controller.BaseController;
import com.proj.entity.UserInfo;
import com.proj.entity.VUserDynamic;
import com.proj.resultData.PageDataResult;
import com.proj.service.UserQuestionService;
import com.proj.service.UserQuestionService.QuestionData;
import com.proj.service.userDynamic.FocusDynamicService;
import com.proj.service.userDynamic.FocusDynamicService.DynamicData;
import com.proj.service.userDynamic.FocusDynamicService.VUserDynamicDataLists;

//因为用户关注的话题和问题可能在查询出来后重复，所以这里加入去重的逻辑暂时，去重后可能会数据个数较少，那么直接加入下一页或几页的数据，
//所以返回值加入下一次应该请求第几页
@Controller
@SessionAttributes("currentUser")
public class FocusDynamicController extends BaseController{
	@Autowired
	FocusDynamicService focusDynamicService;
	@Autowired
	UserQuestionService uqsQuestionService;
	@RequestMapping("/Dynamic/getFocusDynamicData.do")
	@ResponseBody
	public String getFocusDynamicData(@ModelAttribute("currentUser") UserInfo curUser,
			@RequestParam("page")int currentPage,@RequestParam("pageSize")int pageSize,ModelMap m){
				
		
		List<DynamicData> resultList= focusDynamicService.getFocusDynamicByPage(currentPage, pageSize, curUser.getUserId());
		long nextPage=currentPage+1;
		PageDataResult<List<DynamicData>> result=new PageDataResult<List<DynamicData>>();
		
		result.setCurrentPage(currentPage);
		result.setNextPage(nextPage);
		result.setPageSize(pageSize);
		result.setRsm(resultList);
		return this.toJson(result);
		
		
		
	}
	
	@RequestMapping("/Dynamic/ajax/getFocusDynamicData.do")
	@ResponseBody
	public String getFocusDynamicData(@ModelAttribute("currentUser") UserInfo curUser,
			@RequestParam("page")int currentPage,@RequestParam("pageSize")int pageSize,@RequestParam("dynamicId")String dynamicId,HttpServletRequest request) throws Exception{
				
		Map<String,VUserDynamicDataLists> dynamicData=(Map<String,VUserDynamicDataLists>)request.getSession().getAttribute("DynamicData");
		//如果此时为第一页第一次请求，session中没有记录，需要新建
		if(dynamicData==null&&currentPage==1){
			Map<String,VUserDynamicDataLists> newSessionDynamic=new HashMap<String, FocusDynamicService.VUserDynamicDataLists>();
			request.getSession().setAttribute("DynamicData", newSessionDynamic);
			dynamicData=newSessionDynamic;
		}
		
		List<DynamicData> resultList=null;

		VUserDynamicDataLists sessionDynamicData=dynamicData.get(dynamicId);
		
		if(currentPage!=1&&sessionDynamicData==null)throw new Exception("动态数据有误，会话中不存在动态历史记录！");
		if(sessionDynamicData==null) 
			sessionDynamicData=focusDynamicService.newVUserDynamicDataLists(curUser.getUserId(),pageSize);
			//有重复数据，需要去重
		resultList=focusDynamicService.getFocusDynamicByPage(currentPage, pageSize, curUser.getUserId(), sessionDynamicData);
			
		//resultList = focusDynamicService.deleteRepeatData(middleResultsDynamics);

		long nextPage=currentPage+1;
		PageDataResult<List<DynamicData>> result=new PageDataResult<List<DynamicData>>();
		
		result.setCurrentPage(currentPage);
		result.setNextPage(nextPage);
		result.setPageSize(pageSize);
		result.setRsm(resultList);
		dynamicData.put(dynamicId, sessionDynamicData);
		return this.toJson(result);
		
		
		
	}
	
	
	
	@RequestMapping("/Dynamic/getFocusQuestionData.do")
	@ResponseBody
	public String getFocusQuestionData(@ModelAttribute("currentUser") UserInfo curUser,
			@RequestParam("page")int currentPage,@RequestParam("pageSize")int pageSize,ModelMap m){
				
		
		List<QuestionData> resultList= uqsQuestionService.getFocusQuestionAndRelativeUsers(curUser,currentPage, pageSize);
		long nextPage=currentPage+1;
		PageDataResult<List<QuestionData>> result=new PageDataResult<List<QuestionData>>();
		
		result.setCurrentPage(currentPage);
		result.setNextPage(nextPage);
		result.setPageSize(pageSize);
		result.setRsm(resultList);
		return this.toJson(result);
		
		
		
	}
	
	@RequestMapping("/Dynamic/getInvitedQuestionData.do")
	@ResponseBody
	public String getInvitedQuestionData(@ModelAttribute("currentUser") UserInfo curUser,
			@RequestParam("page")int currentPage,@RequestParam("pageSize")int pageSize,ModelMap m){
				
		
		List<QuestionData> resultList= uqsQuestionService.getInvitedQuestion(curUser,currentPage, pageSize);
		long nextPage=currentPage+1;
		PageDataResult<List<QuestionData>> result=new PageDataResult<List<QuestionData>>();
		
		result.setCurrentPage(currentPage);
		result.setNextPage(nextPage);
		result.setPageSize(pageSize);
		result.setRsm(resultList);
		return this.toJson(result);
		
		
		
	}
	
	
}
