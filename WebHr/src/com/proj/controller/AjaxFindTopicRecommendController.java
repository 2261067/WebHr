package com.proj.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.resultData.BaseResult;
import com.proj.service.TopicRecommendService;
import com.proj.utils.JsonUtils;

@Controller
public class AjaxFindTopicRecommendController {
	
	@Autowired
	private TopicRecommendService topicRecommendService;
	
	@RequestMapping("/ajax/findTopicRecommendPeople.do")
	@ResponseBody
	public String findTopicRecommendPeople(@RequestParam("topics") String topics){
		BaseResult<List<Object>> result = new BaseResult<List<Object>>();
		List<Object> recommendPeoples = new ArrayList<Object>();
		String[] topicsArray = topics.split(",");
		for (String topic : topicsArray) {
			recommendPeoples.addAll(topicRecommendService.getTopicRecommendAnswerPeople(topic));
		}
		result.setRsm(recommendPeoples);
		return JsonUtils.toJson(result);
	}
}
