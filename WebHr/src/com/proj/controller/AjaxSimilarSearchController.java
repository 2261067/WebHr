package com.proj.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.proj.entity.Activity;
import com.proj.entity.Answer;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.service.TopicService;
import com.proj.service.UserManagerService;
import com.proj.service.UserQuestionService;
import com.proj.service.search.SearchService;
import com.proj.utils.JsonUtils;

@Controller
@RequestMapping("/search/ajax/similarsearch.do")
public class AjaxSimilarSearchController {
	@Autowired
	SearchService searchService;
	@Autowired
	private TopicService topicService;
	@Autowired
	private UserManagerService ums;
	@Autowired
	private UserQuestionService uqs;
	private int defaultPageSize=10;
	@RequestMapping(params = "type=topics")
	@ResponseBody
	
	public String ajaxSearch(@RequestParam("q") String query,
			HttpServletResponse response) {
		// 搜索逻辑，在此直接虚构返回
		// ArrayList<HashMap<String, String>> resultList = new
		// ArrayList<HashMap<String,String>>() {
		//
		// {
		// add(new HashMap<String, String>(){
		//
		// {
		// put("name", "互联网");
		// }
		// });
		//
		// add(new HashMap<String, String>(){
		//
		// {
		// put("name", "互联网产品");
		// }
		// });
		//
		// add(new HashMap<String, String>(){
		//
		// {
		// put("name", "互联网产品金融");
		// }
		// });
		// }
		// };

		List<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();

//		List<Topic> topics = topicService.getAllTopics();
		List<Topic> topics  = topicService.findSimilarTopic(query);
		for (Topic topic : topics) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", topic.getTopicName());
			resultList.add(map);
		}

		response.setContentType("application/json");
		Gson gson = new Gson();
		String s = gson.toJson(resultList,
				new ArrayList<HashMap<String, String>>().getClass());
		return s;
	}

	@RequestMapping(params = "type=questions")
	@ResponseBody
	public String ajaxSearchQuestions(@RequestParam("q") String query,
			HttpServletResponse response) {
		response.setContentType("application/json");
		List<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map1 = new HashMap<String, String>();
		// map1.put("name", "做互联网项目的区域代理，有前途吗，怎么才能做好?");
		// map1.put("url", "http://ask.iheima.com/?/question/3269");
		// HashMap<String, String> map2 = new HashMap<String, String>();
		// map2.put("name", "做互联网项目的区域代理，有前途吗，怎么才能做好?");
		// map2.put("url", "http://ask.iheima.com/?/question/3269");
		// resultList.add(map1);
		// resultList.add(map2);
		List<Object> qList = searchService.searchEntity(query, 1, defaultPageSize, searchService.SEARCH_TYPE_QUESTION);
		for (Object o : qList) {
			Question q;
			if(o instanceof Question)q=(Question) o;
			else{continue;}
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", q.getQuestionName());
			map.put("url",
					FirstPageController.getQuestionUrl(q.getQuestionId()));
			resultList.add(map);
		}
		Gson gson = new Gson();

		String s = gson.toJson(resultList,
				new ArrayList<HashMap<String, String>>().getClass());
		return s;
	}

	@RequestMapping(params = "type=users")
	@ResponseBody
	public String ajaxSearchUser(@RequestParam("q") String query,
			HttpServletResponse response) {
		List<UserInfo> userInfos = ums.getSimilarUserInfos(query);
		List<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		for (UserInfo userInfo : userInfos) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", userInfo.getNickName());
			map.put("uid", userInfo.getUserId());
			map.put("img",userInfo.getPhoto());
			resultList.add(map);
		}
		return JsonUtils.toJson(resultList);
	}
	
	@RequestMapping(params = "type=test")
	@ResponseBody
	public String ajaxSearchAllTest(@RequestParam("q") String query,
			HttpServletResponse response) {
		List<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map1 = new HashMap<String, String>();
		List<Question> qList = searchService.searchQuestion(query, 1);
		for (Question q : qList) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", q.getQuestionName());
			map.put("url",
					FirstPageController.getQuestionUrl(q.getQuestionId()));
			resultList.add(map);
			map.put("type", "questions");
		}
		
		return JsonUtils.toJson(resultList);
	}
	
	@RequestMapping(params = "type=all")
	@ResponseBody
	public String ajaxSearchAll(@RequestParam("q") String query,
			HttpServletResponse response) {
		List<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map1 = new HashMap<String, String>();
		List<Object> oList = searchService.searchAllData(query, 1, 10);
		for (Object o : oList) {
			String nameString="";
			String urlString="";
			String typeString="";
			HashMap<String, String> map = new HashMap<String, String>();
			if(o instanceof Question){
				Question q=(Question) o;
				nameString=q.getQuestionName();
				urlString=FirstPageController.getQuestionUrl(q.getQuestionId());
				typeString=Question.class.getSimpleName().toLowerCase();
			}
			else if(o instanceof Activity){
				Activity a=(Activity) o;
				nameString=a.getActivityName();
				urlString="getActivityById.do?activity_id="+a.getActivityId();
				typeString=Activity.class.getSimpleName().toLowerCase();
			}
			else if(o instanceof Answer){
				Answer a=(Answer) o;
				Question q=uqs.getQuestionById(a.getQuestion());
				nameString=q.getQuestionName();
				urlString=FirstPageController.getQuestionUrl(q.getQuestionId());
				typeString=Question.class.getSimpleName().toLowerCase();
				
			}
			map.put("type", typeString);
			map.put("name", nameString);
			map.put("url",urlString);
			resultList.add(map);
			
		}
		
		return JsonUtils.toJson(resultList);
	}
	
	
	
}
