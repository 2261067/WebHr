package com.proj.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.proj.entity.AdminInfo;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.service.UserManagerService;
import com.proj.session.CookieHelper;
import com.proj.utils.EncrypAES;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
@RequestMapping()
@SessionAttributes("currentAdmin")
public class AdminLoginController extends BaseController {

	private static final Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	private UserManagerService ums;

	/*
	 * 检查管理员account是否存在
	 */
	@RequestMapping(value = "html/adminlogin/{account}/accountcheck.do", method = RequestMethod.GET)
	public @ResponseBody String admincheck(@PathVariable String account) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistAdminId(account) || ums.isExistAdminEmail(account)) {
			return JsonUtils.toJson(result);
		}
		result.setErr("账户不存在");
		return JsonUtils.toJson(result);
	}

	/*
	 * 检查管理员account和password是否匹配
	 */
	@RequestMapping(value = "html/adminlogin/{account}/{password}/accountpwdcheck.do", method = RequestMethod.GET)
	public @ResponseBody String adminidpwdcheck(@PathVariable String account,
			@PathVariable String password) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		AdminInfo admin = ums.adminLogin(account, password);
		if (admin == null) {
			result.setErr("账户不存在");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * 管理员登陆
	 */
	@RequestMapping("/adminlogin.do")
	public String adminlogin(HttpServletRequest req, HttpServletResponse rep,
			ModelMap m, @RequestParam("username") String username,
			@RequestParam("password") String passWord,
			@RequestParam(value = "remember", required = false) String remember)
			throws ServletException, IOException {
		boolean isSuccess = true;
		AdminInfo admin = ums.adminLogin(username, passWord);
		if (admin == null) {
			isSuccess = false;
		}

		if (isSuccess) {
			if (StringUtils.isNotBlank(remember)) {
				// 转码，防止中文字符在cookie中乱码
				String adminId = URLEncoder.encode(admin.getAdminId(), "utf-8");
				Cookie adminID = new Cookie(CookieHelper.ADMIN_ID, adminId);
				Cookie adminPW = new Cookie(CookieHelper.ADMIN_PW,
						admin.getPassword());
				CookieHelper.addCookie(CookieHelper.TWO_WEEKS, "/", rep,
						adminID, adminPW);
			}
			LOG.info("管理员登陆成功！");
			m.addAttribute("currentAdmin", admin);
			return "redirect:/redirect.do?page=adminTopic";
		} else {
			LOG.info("登陆失败！");
			return "redirect:/redirect.do?page=loginerror";
		}
	}
}
