package com.proj.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.proj.entity.Question;
import com.proj.resultData.PageDataResult;
import com.proj.service.AttentionUserService;
import com.proj.service.HotDataService;
import com.proj.service.UserQuestionService.QuestionData;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;

@Controller
@SessionAttributes("currentUser")
public class HotDatasController extends BaseController {
	double hotQuestionPer=0.5;
	double hotArticlePer=0.25;
	double hotActivityPer=0.25;
	@Autowired
	private HotDataService hds;
	@Autowired
	private static final String getQuestionUrl = "getHotDatas.do";

	@RequestMapping(value = getQuestionUrl)
	@ResponseBody
	public String getHotDatas(int num) {
		PageDataResult<HashMap<String, List>> result=new PageDataResult<HashMap<String, List>>();
		HashMap<String, List> resultMap=new HashMap<String, List>();
		int questionNum=(int) (num*hotQuestionPer);
		int articleNum=(int) (num*hotArticlePer);
		int activityNum=(int) (num*hotActivityPer);
		List<QuestionData> hotQuestions=hds.getHotQuestions(questionNum);
		if(hotQuestions!=null)resultMap.put("question", hotQuestions);
		result.setRsm(resultMap);
		return this.toJson(result);
	}
}
