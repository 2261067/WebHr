package com.proj.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proj.entity.AdminInfo;
import com.proj.entity.UserInfo;
import com.proj.session.CookieHelper;
import com.proj.session.SessionHelper;

@Controller
public class LogoutController {
	@RequestMapping("/logout.do")
	public String logOut(HttpServletRequest request,HttpServletResponse response){
//		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request, SessionHelper.SESSION_USER, UserInfo.class);
		if(userInfo != null){
			String[] removeCookies = {CookieHelper.USER_ID,CookieHelper.USER_PW};
			CookieHelper.removeCookies(removeCookies, request, response);
			request.getSession().invalidate();
		}
//		HashMap<String, String> map = new HashMap<String, String>();
		return "redirect:/html/signin.html";
	}
	
	@RequestMapping("/adminlogout.do")
	public String adminLogOut(HttpServletRequest request,HttpServletResponse response){
		AdminInfo adminInfo = SessionHelper.getSessionAttribute(request, SessionHelper.SESSION_ADMIN, AdminInfo.class);
		if(adminInfo != null){
			String[] removeCookies = {CookieHelper.ADMIN_ID,CookieHelper.ADMIN_PW};
			CookieHelper.removeCookies(removeCookies, request, response);
			request.getSession().invalidate();
		}
		return "redirect:/html/admin_login.html";
	}
}
