package com.proj.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/search.do")
public class SearchController {
	private static final Logger LOG = Logger.getLogger(SearchController.class);

	@RequestMapping()
	public String search(HttpServletRequest request,
			@RequestParam("searchText") String searchText) {

		LOG.info(searchText);
		return "question";

	}
}
