package com.proj.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.proj.utils.*;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;







import com.proj.entity.Activity;
import com.proj.entity.Answer;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.resultData.PageDataResult;
import com.proj.service.TopicService;
import com.proj.service.UserManagerService;
import com.proj.service.UserQuestionService;
import com.proj.service.search.SearchService;
import com.proj.utils.ConfigProperties;
import com.proj.utils.JsonUtils;
@Controller

public class AjaxSearchMainController {
	@Autowired
	SearchService searchService;
	@Autowired
	private TopicService topicService;
	@Autowired
	private UserManagerService ums;
	@Autowired
	private UserQuestionService uqs;

	static  ConfigProperties config = new ConfigProperties(
			"controller.properties");
    final String searchTypeConfigPreString="searchMain.type.";
	/**该方法搜索来自搜索引擎的数据
	 * @param query
	 * @param page
	 * @param pageSize
	 * @param searchType
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/search/ajax/searchMain.do")
	@ResponseBody
	public String ajaxSearchAll(@RequestParam("q") String query,@RequestParam("page") int page,
			@RequestParam("pageSize") int pageSize,@RequestParam("type") String searchType,HttpServletResponse response) {
		PageDataResult<List<HashMap<String, Object>>> result=new PageDataResult<List<HashMap<String, Object>>>();
		if(StringUtils.isBlank(query))
		{
			result.setErrno(0);
			result.setErr("查询为空，请检查输入!");
			return JsonUtils.toJson(result);
			}
		List<HashMap<String, Object>> resultList = new ArrayList<HashMap<String, Object>>();
		HashMap<String, String> map1 = new HashMap<String, String>();
		Integer typeNum=Integer.parseInt(config.getValue(searchTypeConfigPreString+searchType));
		if(typeNum==null)return "";
		
		List<Object> oList = searchService.searchEntity(query, page, pageSize,typeNum);
		for (Object o : oList) {
			String nameString="";
			String urlString="";
			String typeString="";
			HashMap<String, Object> map = new HashMap<String, Object>();
			if(o instanceof Question){
				Question q=(Question) o;
				nameString=q.getQuestionName();
				urlString=FirstPageController.getQuestionUrl(q.getQuestionId());
				typeString=Question.class.getSimpleName().toLowerCase();
				map.put("user", ums.peopleInfo(q.getUserId()));
						}
			else if(o instanceof Activity){
				Activity a=(Activity) o;
				nameString=a.getActivityName();
				urlString="getActivityById.do?activity_id="+a.getActivityId();
				typeString=Activity.class.getSimpleName().toLowerCase();
				map.put("user", ums.peopleInfo(a.getUserId()));
			}
			else if(o instanceof Answer){
				Answer a=(Answer) o;
				Question q=uqs.getQuestionById(a.getQuestion());
				nameString=q.getQuestionName();
				urlString=FirstPageController.getQuestionUrl(q.getQuestionId());
				typeString=Question.class.getSimpleName().toLowerCase();
				map.put("user", ums.peopleInfo(q.getUserId()));
			}
			map.put("type", typeString);
			map.put("data", o);
			map.put("url",urlString);
			resultList.add(map);
			
		}
		result.setRsm(resultList);
		result.setCurrentPage(page);
		return JsonUtils.toJson(result);
	}
	
	
	@RequestMapping(value="/search/ajax/searchUser.do")
	@ResponseBody
	public String ajaxSearchUser(@RequestParam("q") String query,@RequestParam("page") int page,
			@RequestParam("pageSize") int pageSize,@RequestParam("type") String searchType,HttpServletResponse response) {
		PageDataResult<List<UserInfo>> result=new PageDataResult<List<UserInfo>>();
		if(StringUtils.isBlank(query))
		{
			result.setErrno(0);
			result.setErr("查询为空，请检查输入!");
			return JsonUtils.toJson(result);
			}
		
		List<UserInfo> resultList=searchService.searchUser(query, page, pageSize);
		result.setRsm(resultList);
		result.setCurrentPage(page);
		return JsonUtils.toJson(result);
	}
	
	@RequestMapping(value="/search/ajax/searchTopic.do")
	@ResponseBody
	public String ajaxSearchTopic(@RequestParam("q") String query,@RequestParam("page") int page,
			@RequestParam("pageSize") int pageSize,@RequestParam("type") String searchType,HttpServletResponse response) {
		PageDataResult<List<Topic>> result=new PageDataResult<List<Topic>>();
		if(StringUtils.isBlank(query))
		{
			result.setErrno(0);
			result.setErr("查询为空，请检查输入!");
			return JsonUtils.toJson(result);
			}
		
		List<Topic> resultList=topicService.findSimilarTopicByPage(query,page,pageSize);
		result.setRsm(resultList);
		result.setCurrentPage(page);
		return JsonUtils.toJson(result);
	}
		
	
	@RequestMapping(value="/search/searchMore.do")
	
	public String searchMore(@RequestParam("q") String query,ModelMap m){
		m.addAttribute("q", query);
		return "/search/searchMain";
	}
}
