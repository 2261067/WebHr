package com.proj.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.proj.authority.Authority;
import com.proj.authority.Permission;
import com.proj.dao.QuestionDao;
import com.proj.entity.Question;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.TopicService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.session.SessionHelper;
import com.proj.utils.GUIDGenerator;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
@RequestMapping("/publish/ajax/publish_question.do")
public class AjaxPublishQuestionController {

	@Autowired
	private QuestionAndAnswerService questionService;
	@Autowired
	private TopicService topicService;
	@RequestMapping()
	@ResponseBody
	public String ajaxPublishQuestion(@RequestParam(value ="question_content",required = false) String title, @RequestParam(value="question_detail",required = false) String detail,@RequestParam(value = "topics[]",required = false) String topics,HttpServletRequest request, HttpServletResponse response){
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
		if(StringUtils.isBlank(title)){
			result.setError(ResultNo.NEED_QUESION_NAME);
		}else if (StringUtils.isBlank(topics)) {
			result.setError(ResultNo.NEED_QUESION_TOPIC);
		}
		else {
			List<Topic> topiclList = new ArrayList<Topic>();
			for (String topicString : topics.split(",")) {
				Topic topic;
				if((topic = topicService.getTopicByName(topicString)) == null){
					result.setError(ResultNo.WRONG_QUESION_TOPIC);
					result.setErr(ResultNo.WRONG_QUESION_TOPIC.err + "----" + topicString);
					return JsonUtils.toJson(result);
				}
				topiclList.add(topic);
			}
			UserInfo user = (UserInfo)request.getSession().getAttribute(SessionHelper.SESSION_USER);
			
			Question question = new Question();
			question.setUserId(user.getUserId());
			question.setQuestionName(title);
			question.setQuestionDesc(detail);
			question.setCreateTime(new Date());
			question.setQuestionId(GUIDGenerator.getGUID());
			question.setLastMotify(new Date());
			questionService.saveQuestion(question,topiclList);
		}
		
		
		response.setContentType("application/json");
//		response.getWriter().write(s);
//		response.flushBuffer();
		return JsonUtils.toJson(result);
	}
}
