package com.proj.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;





import com.proj.entity.Answer;
import com.proj.entity.AnswerComment;
import com.proj.entity.AnswerVote;
import com.proj.entity.AttentionQuestion;
import com.proj.entity.AttentionUser;
import com.proj.entity.Question;
import com.proj.entity.QuestionTopic;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.AttentionUserService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService.AnswerAndUser;
import com.proj.session.SessionHelper;
import com.proj.utils.ConstantUtil;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
@SessionAttributes("currentUser")
public class QuestionAndAnswerController extends BaseController {
	@Autowired
	private QuestionAndAnswerService qas;
	@Autowired
	private AttentionUserService attentionUserService;
	private static final String getQuestionUrl = "getQuestionAndAnswer.do";

	@RequestMapping(value = getQuestionUrl)
	@ResponseBody
	public String getQuestionAndAnswer(String questionId, int page) {
		List<Object> answerAndUserMap = qas.getQuestionAnswer(questionId, page);
		return this.toJson(answerAndUserMap);
	}

	public static String getQuestionUrl(String questionId) {
		if (questionId != null)
			return getQuestionUrl + "?questionId=" + questionId + "&page=1";
		return "";
	}

	// 获取问题、用户和话题，当前用户是否关注该问题和提问者
	@RequestMapping("/getQuestion.do")
	@ResponseBody
	public String getQuestionById(
			@RequestParam("questionId") String questionId,
			HttpServletRequest request) {
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String, Object>>();
		Object[] object = (Object[])qas.getQuestionAndUserInfo(questionId);
		// 取问题
		Question question = (Question)object[0];
		if (question != null) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("question", question);
			// 取问题的话题
			List<QuestionTopic> questionTopics = qas
					.getQuestionTopics(questionId);
			if (questionTopics != null && !questionTopics.isEmpty()) {
//				StringBuffer topics = new StringBuffer();
//				for (QuestionTopic questionTopic : questionTopics) {
//					topics.append(questionTopic.getTopicName());
//					topics.append(",");
//				}
				map.put("topics", questionTopics);
			}
//			取提问者的信息
			UserInfo user = (UserInfo)object[1];
			map.put("user", user);
//			取当前问题关注者数目
			Long count = qas.getQuestionAttentionCount(questionId);
			map.put("attentionNum", count);
			// 当前用户是否关注问题和提问者
			UserInfo userInfo = SessionHelper.getSessionAttribute(request,
					SessionHelper.SESSION_USER, UserInfo.class);
			if (userInfo != null) {
				map.put("isAttentionQuestion",
						qas.isAttention(userInfo.getUserId(),
								question.getQuestionId()));
				map.put("isAttentionUser", attentionUserService.isAttention(userInfo.getUserId(), question.getUserId()));
			}
			
			result.setRsm(map);
		}

		return JsonUtils.toJson(result);
	}

	// 按照参数获取问题答案
	@RequestMapping("/getQuestionAnswers.do")
	@ResponseBody
	public String getQuestionAnswers(
			@RequestParam("questionId") String questionId,
			@RequestParam("sort_key") String sortKey,
			@RequestParam(value = "sort",required = false) String sort,@RequestParam(value ="page") int page,@RequestParam(value="pageSize") int pageSize) {
		BaseResult<List<AnswerAndUser>> result = new BaseResult<List<AnswerAndUser>>();
		List<AnswerAndUser> answerAndUserList = null;
		if (sortKey.equals("vote")) {
			answerAndUserList = qas
					.getQuestionAnswerAndUserInfoOrderByVote(questionId,page,pageSize);
		} else {
			answerAndUserList = qas.getQuestionAnswerAndUserInfoOrderByTime(
					questionId, page,pageSize,sort);
		}
//对于匿名回答，设置属性
		for (AnswerAndUser answerAndUser : answerAndUserList) {
			if(answerAndUser.answer.getIfAnonymous() == 1){
				answerAndUser.user.setUserId(ConstantUtil.ANONYMOUS_NAME);
				answerAndUser.user.setNickName(ConstantUtil.ANONYMOUS_NAME);
				answerAndUser.user.setPhoto(ConstantUtil.ANONYMOUS_PHOTO);
				answerAndUser.user.setPassword(null);
			}
		}
		
		result.setRsm(answerAndUserList);
		// System.out.println(JsonUtils.toJson(result));
		return JsonUtils.toJson(result);
	}

	// 按照获取问题答案数目
		@RequestMapping("/getQuestionAnswersCount.do")
		@ResponseBody
		public String getQuestionAnswersCount(@RequestParam("questionId") String questionId){
			BaseResult<Long> result = new BaseResult<Long>();
			long count = qas.getQuestionAnswerCount(questionId);
			result.setRsm(count);
			return JsonUtils.toJson(result);
		}
	
	/**
	 * post_hash: question_id:[Ljava.lang.String;@2d076366
	 * attach_access_key:[Ljava.lang.String;@400ed4a2
	 * auto_focus:[Ljava.lang.String;@da9f889
	 * answer_content:[Ljava.lang.String;@11fbf161
	 * content:[Ljava.lang.String;@33a42080
	 * _post_type:[Ljava.lang.String;@5c51cdfc
	 * 
	 * @param currentUser
	 * @param questionId
	 * @param answerContent
	 * @param autoFocus
	 * @param m
	 * @return
	 */
	@RequestMapping(value = "/commitAnswer.do")
	@ResponseBody
	public String commitAnswer(
			@ModelAttribute("currentUser") UserInfo currentUser,
			@RequestParam("question_id") String questionId,
			@RequestParam(value="answer_content",required=false) String answerContent,
			@RequestParam(value="auto_focus",required =false) String autoFocus, @RequestParam(value="anonymous",required=false) String anonymous,ModelMap m) {

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		
		if(StringUtils.isBlank(answerContent)){
			result.setError(ResultNo.NEED_ANSWER_CONTENT);
			return JsonUtils.toJson(result);
		}
		if(StringUtils.isBlank(autoFocus)){
			autoFocus="0";
		}
		if(StringUtils.isBlank(anonymous)){
			anonymous="0";
		}
		qas.saveAnswer(currentUser, answerContent, questionId, autoFocus,anonymous);
		

		return this.toJson(result);
	}

	// 取消和关注问题
	@RequestMapping(value = "/ajax/attentionQuestion.do")
	@ResponseBody
	public String attentionQuestion(
			@RequestParam("question_id") String questionId,
			HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		HashMap<String, String> map = new HashMap<String, String>();
		if (userInfo != null) {
			// 已关注则取消，否则关注
			AttentionQuestion attentionQuestion = qas.getAttentionQuestion(
					userInfo.getUserId(), questionId);
			if (attentionQuestion != null) {
				qas.deleteAttentionQuestion(attentionQuestion);
				map.put("type", "delete");
			} else {
				qas.saveAttentionQuestion(userInfo.getUserId(), questionId);
				map.put("type", "add");
			}
			result.setRsm(map);
		}else {
			result.setError(ResultNo.NEED_LOGIN);
		}

		return JsonUtils.toJson(result);
	}

	// 问题投票
	@RequestMapping("/ajax/answerVote.do")
	@ResponseBody
	public String answerVote(@RequestParam("answer_id") String answerId,
			@RequestParam("value") byte value, HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		HashMap<String, String> map = new HashMap<String, String>();
		if (userInfo != null) {
			AnswerVote answerVote = qas.getAnswerVote(userInfo.getUserId(),
					answerId);
			if (answerVote != null) {
				map.put("status", "voted");
				map.put("type", answerVote.getVoteType().toString());
			} else {
				qas.saveAnswerVote(answerId, userInfo.getUserId(), value);
				map.put("status", "vote");
			}
			result.setRsm(map);
		} else {
			result.setError(ResultNo.NEED_LOGIN);
		}

		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/getAnswerComments.do")
	@ResponseBody
	public String getAnswerComments(@RequestParam("answer_id") String answerId) {
		BaseResult<Object> result = new BaseResult<Object>();
		List<Object> answerCommentsAndUsers = qas
				.getAnswerCommentsAndUsers(answerId);
		if (answerCommentsAndUsers != null && !answerCommentsAndUsers.isEmpty()) {
			result.setRsm(answerCommentsAndUsers);
		}

		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/saveAnswerComment.do")
	@ResponseBody
	public String saveAnswerComments(
			@RequestParam("answer_id") String answerId,
			@RequestParam(value = "question_id", required = false) String questionId,
			@RequestParam("message") String message, HttpServletRequest request) {

		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		if (userInfo != null) {
			qas.saveAnswerComment(userInfo.getUserId(), answerId, questionId,
					message);
		} else {
			result.setError(ResultNo.NEED_LOGIN);
		}
		// 为了使用前台js，返回item_id和type_name
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("item_id", answerId);
		map.put("type_name", "answer");
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}

	@RequestMapping("/ajax/saveInvite.do")
	@ResponseBody
	public String saveInviteUser(
			@RequestParam("question_id") String questionId,
			@RequestParam("invite_user_id") String inviteUserId,HttpServletRequest request) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request,
				SessionHelper.SESSION_USER, UserInfo.class);
		HashMap<String, String> map = new HashMap<String, String>();
		if (userInfo != null) {
//			不能邀请自己
			if(userInfo.getUserId().equals(inviteUserId)){
				result.setError(ResultNo.INVITE_SELF);
				return JsonUtils.toJson(result);
			}
//			该用户已被邀请
			if(qas.getQuestionInvite(questionId, inviteUserId) != null){
				result.setError(ResultNo.INVITE_ALREADY);
				return JsonUtils.toJson(result);
			}
			qas.saveQuestionInvite(userInfo.getUserId(), questionId, inviteUserId);
		} else {
			result.setError(ResultNo.NEED_LOGIN);
		}
		
		
		return JsonUtils.toJson(result);
	}
}
