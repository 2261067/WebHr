package com.proj.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.proj.utils.EncrypAES;
import com.proj.entity.AdminInfo;
import com.proj.entity.ThirdPartLanding;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.service.UserManagerService;
import com.proj.session.CookieHelper;
import com.proj.session.SessionHelper;
import com.proj.utils.JsonUtils;
import com.proj.utils.SimpleMailSender;
import com.proj.utils.StringUtils;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.oauth.Oauth;

@Controller
@RequestMapping()
@SessionAttributes("currentUser")
// currentUser的model域自动存入session范围。
public class LoginController extends BaseController {

	public static final int RESULT_STATUS_SUCCESS = 1;
	private static final Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	private UserManagerService ums;

	/*
	 * 找回密码时检查email是否存在并发送邮件
	 */
	@RequestMapping(value = "/login/forgetpwd.do")
	@ResponseBody
	public String forgetpwd(HttpServletRequest req, ModelMap m,
			HttpServletResponse rep, @RequestParam("email") String email) {

		BaseResult<String> result = new BaseResult<String>();
		if (ums.isExistEmail(email)) {
			// 发送邮件
			SimpleMailSender sms = new SimpleMailSender();
			String pwd = ums.findpwd(email);
			if (!sms.forgetPwdSendMail(email, pwd)) {
				result.setErr("邮件发送失败");
				result.setErrno(2009);
				LOG.error(email + "找回密码邮件发送失败！");
				return JsonUtils.toJson(result);
			}
			LOG.info(email + "找回密码邮件发送成功！");
			return JsonUtils.toJson(result);
		}
		result.setErr("邮箱不存在");
		result.setErrno(2003);
		LOG.warn(email + "邮箱不存在！");
		return JsonUtils.toJson(result);
	}

	/*
	 * 检查account是否存在
	 */
	@RequestMapping(value = "html/login/{account}/accountcheck.do", method = RequestMethod.GET)
	public @ResponseBody String usernamecheck(@PathVariable String account) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		if (ums.isExistUserId(account) || ums.isExistEmail(account)) {
			return JsonUtils.toJson(result);
		}
		result.setErr("账户不存在");
		return JsonUtils.toJson(result);
	}

	/*
	 * 检查account和password是否匹配
	 */
	@RequestMapping(value = "html/login/{account}/{password}/accountpwdcheck.do", method = RequestMethod.GET)
	public @ResponseBody String usernamepwdcheck(@PathVariable String account,
			@PathVariable String password) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String, String>>();
		UserInfo user = ums.userLogin(account, password);
		if (user == null) {
			result.setErr("账户不存在");
		}
		return JsonUtils.toJson(result);
	}

	/*
	 * qq登录
	 */
	@RequestMapping("/qqlogin.do")
	public void qqlogin(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html;charset=utf-8");
		try {
			response.sendRedirect(new Oauth().getAuthorizeURL(request));

		} catch (QQConnectException e) {
			e.printStackTrace();
		}

	}

	/*
	 * qq登录重定向
	 */
	@RequestMapping("/qqredirect.do")
	public String qqloginredirect(HttpServletRequest request, ModelMap m,
			HttpServletResponse response, @RequestParam("page") String page)
			throws IOException {
		UserInfo qquser = new UserInfo();
		try {
			AccessToken accessTokenObj = (new Oauth())
					.getAccessTokenByRequest(request);

			String accessToken = null, openID = null;
			long tokenExpireIn = 0L;

			if (accessTokenObj.getAccessToken().equals("")) {
				// 我们的网站被CSRF攻击了或者用户取消了授权
				// 做一些数据统计工作
				LOG.warn("qq登录没有获取到响应参数");
				return "redirect:/redirect.do?page=loginerror";
			} else {
				accessToken = accessTokenObj.getAccessToken();
				tokenExpireIn = accessTokenObj.getExpireIn();

				// 利用获取到的accessToken 去获取当前用的openid -------- start
				OpenID openIDObj = new OpenID(accessToken);
				openID = openIDObj.getUserOpenID();

				com.qq.connect.api.qzone.UserInfo qzoneUserInfo = new com.qq.connect.api.qzone.UserInfo(
						accessToken, openID);
				UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
				if (userInfoBean.getRet() == 0) {
					String qqUserName = userInfoBean.getNickname();
					qquser = ums.qqloginUserInfo(openID, qqUserName);
					m.addAttribute("currentUser", qquser);
				} else {
					LOG.warn("qq登录获取用户信息失败" + userInfoBean.getMsg());
					return "redirect:/redirect.do?page=loginerror";
				}
			}
		} catch (QQConnectException e) {
			LOG.warn("qq登录失败！");
			return "redirect:/redirect.do?page=loginerror";
		}
		return "redirect:/redirect.do?page=" + page;
	}

	// 加入注解的方法会被拦截器拦截，检查权限
	// @Authority(permissions = Permission.QUESTION)
	@RequestMapping("/login.do")
	public String loginPc(HttpServletRequest req, ModelMap m,
			HttpServletResponse rep, @RequestParam("username") String username,
			@RequestParam("password") String passWord,
			@RequestParam("securitycode") String securitycode,
			@RequestParam(value = "remember", required = false) String remember)
			throws ServletException, IOException {

		boolean isSuccess = true;
		String validateC = (String) req.getSession().getAttribute(
				"validateCode");
		securitycode = securitycode.toUpperCase();
		if (!validateC.equals(securitycode) || passWord == "") {
			isSuccess = false;
		}

		UserInfo user = ums.userLogin(username, passWord);
		if (user == null) {
			isSuccess = false;
		}

		if (isSuccess) {
			if (StringUtils.isNotBlank(remember)) {
				// 转码，防止中文字符在cookie中乱码
				String userId = URLEncoder.encode(user.getUserId(), "utf-8");
				Cookie userID = new Cookie(CookieHelper.USER_ID, userId);
				Cookie userPW = new Cookie(CookieHelper.USER_PW,
						user.getPassword());
				CookieHelper.addCookie(CookieHelper.TWO_WEEKS, "/", rep,
						userID, userPW);
			}
			LOG.info("登陆成功！");
			m.addAttribute("currentUser", user);
			return "redirect:/redirect.do?page=question";
		} else {
			LOG.info("登陆失败！");
			return "redirect:/redirect.do?page=loginerror";
		}
	}

	/*
	 * android端登陆
	 */
	@RequestMapping(value = "/login.do", params = "client=android")
	@ResponseBody
	public String loginMobile(HttpServletRequest req,
			@RequestParam("username") String username,
			@RequestParam("password") String password) {

		BaseResult<UserInfo> result = new BaseResult<UserInfo>();
		UserInfo user = ums.userLogin(username, password);
		if (user == null) {
			LOG.info("andriod登陆失败！");
			result.setErr("邮箱或密码错误");
			result.setErrno(2001);
			return JsonUtils.toJson(result);
		}
		LOG.info("andriod登陆成功！");
		req.getSession().setAttribute(SessionHelper.SESSION_USER, user);
		result.setRsm(user);
		return JsonUtils.toJson(result);
	}

}
