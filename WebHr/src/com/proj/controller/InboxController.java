package com.proj.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.Inbox;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.InboxService;
import com.proj.service.InboxService.InboxAndFromUser;
import com.proj.service.UserManagerService;
import com.proj.session.SessionHelper;
import com.proj.utils.JsonUtils;
import com.proj.utils.StringUtils;

@Controller
public class InboxController {

	@Autowired
	private InboxService inboxService;

	@Autowired
	private UserManagerService ums;
	
	@RequestMapping("/inbox.do")
	public String inboxPage(){
		return "inbox";
	}

	@RequestMapping("/inbox/save.do")
	@ResponseBody
	public String saveInboxMessage(HttpServletRequest request,@RequestParam("receive_name")String receiveName,@RequestParam("message")String message){
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
		if(StringUtils.isBlank(receiveName) || StringUtils.isBlank(message)){
			result.setError(ResultNo.INBOX_SEND_ERROR);
			return JsonUtils.toJson(result);
		}
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		
		UserInfo receiceUser = ums.isExistUserNick(receiveName);
		if(receiceUser == null){
			result.setError(ResultNo.INBOX_SEND_ERROR);
			return JsonUtils.toJson(result);
		}
		
		Inbox inbox = new Inbox();
		inbox.setContent(message);
		inbox.setCreateTime(new Date());
		inbox.setFromId(userInfo.getUserId());
		inbox.setUserId(receiceUser.getUserId());
		inbox.setIsRead((byte)0);
//		设置属性为对话
		inbox.setType("dialog");
		inboxService.save(inbox);
		
		return JsonUtils.toJson(result);
	}
	
	@RequestMapping(value = "/inbox/showall.do")
	@ResponseBody
	public String getAllInboxMessage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BaseResult<List<InboxAndFromUser>> result = new BaseResult<List<InboxAndFromUser>>();
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		List<Inbox> inboxs = inboxService.getLatestUserInboxMessage(userInfo
				.getUserId());
		// 去除相同对话
		if (inboxs != null && !inboxs.isEmpty()) {
			List<Inbox> rboxs = new ArrayList<Inbox>();
			HashMap<String, Inbox> map = new HashMap<String, Inbox>();
			for (Inbox box : inboxs) {
				String index = box.getUserId().compareTo(box.getFromId()) > 0 ? box
						.getUserId() + box.getFromId()
						: box.getFromId() + box.getUserId();
				if (map.containsKey(index)) {
					if (box.getCreateTime().compareTo(
							map.get(index).getCreateTime()) > 0) {
						rboxs.remove(map.get(index));
						rboxs.add(box);
					}
				} else {
					map.put(index, box);
					rboxs.add(box);
				}
			}
			if (!rboxs.isEmpty()) {
				result.setRsm(inboxService.getInboxAndFromUser(rboxs,userInfo.getUserId()));
			}
		}
		return JsonUtils.toJson(result);
	}

	@RequestMapping(value = "/inbox/delete.do")
	public String deleteUserInboxMessage(HttpServletRequest request,
			@RequestParam("interac_id") String interacId) {
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (userInfo != null) {
			inboxService.deleteUserInteractionInboxMessage(
					userInfo.getUserId(), interacId);
		}else {
			result.setError(ResultNo.NEED_LOGIN);
		}

		return JsonUtils.toJson(result);
	}

	@RequestMapping("/inbox/show.do")
	public String showSingalUserMessage(HttpServletRequest request) {
		// UserInfo userInfo = (UserInfo)
		// request.getSession().getAttribute(SessionHelper.SESSION_USER);
		// if(userInfo != null){
		//
		// }
		return "inboxDetail";
	}

	@RequestMapping("/inbox/getUserInteraction.do")
	@ResponseBody
	public String getUserInteractionMessage(
			@RequestParam("interac_id") String interacId,
			HttpServletRequest request) {
		BaseResult<List<InboxAndFromUser>> result = new BaseResult<List<InboxAndFromUser>>();
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (userInfo == null) {
			result.setError(ResultNo.NEED_LOGIN);
			return JsonUtils.toJson(result);
		}
		List<Inbox> inboxs = inboxService.updateUserInteractionInboxMessageStatus(userInfo.getUserId(),
				interacId);
		if(inboxs != null && !inboxs.isEmpty()){
			result.setRsm(inboxService.getInboxAndFromUser(inboxs,userInfo.getUserId()));
		}
		return JsonUtils.toJson(result);
	}
	

}
