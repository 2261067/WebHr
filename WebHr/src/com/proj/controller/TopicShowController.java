package com.proj.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.proj.dao.PageResults;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.PageDataResult;
import com.proj.service.UserManagerService;
import com.proj.service.UserQuestionService;
import com.proj.service.UserQuestionService.QuestionData;
import com.proj.service.topic.TopicShowService;
import com.proj.utils.JsonUtils;
@Controller
@SessionAttributes("currentUser")
public class TopicShowController extends BaseController{
	public static int hotTopicPageSize=6;
	@Autowired
	TopicShowService tss;
	@Autowired
	UserQuestionService uqs;
	@RequestMapping(value="getHotTopics.do")
	@ResponseBody
	public String getHotTopics(@ModelAttribute("currentUser") UserInfo curUser,@RequestParam("page")int page,ModelMap m) throws Exception{
		if(curUser==null){
			
			throw new Exception("登录失效，请重新登录！");
			}
		PageDataResult<List> result=new PageDataResult<List>();
		List hotTopicsData=tss.getHotTopics(page,hotTopicPageSize);
		result.setRsm(hotTopicsData);
		result.setCurrentPage(page);
		result.setPageSize(hotTopicPageSize);
		long topicCount=tss.findAllTopicCount();
		result.setAllDataCount(topicCount);
		return this.toJson(result);
	}
	
	@RequestMapping(value="getFocusTopics.do")
	@ResponseBody
	public String getFocusTopics(@ModelAttribute("currentUser") UserInfo curUser,@RequestParam("page")int page,ModelMap m) throws Exception{
		if(curUser==null){
			
			throw new Exception("登录失效，请重新登录！");
			}
		PageDataResult<List> result=new PageDataResult<List>();
		List hotTopicsData=tss.getFocusTopics(page,curUser.getUserId());
		result.setRsm(hotTopicsData);
		result.setCurrentPage(page);
		result.setPageSize(hotTopicPageSize);
		//待修改
		result.setAllDataCount(tss.getFocusTopicsCount(curUser.getUserId()));
		return this.toJson(result);
	}
	
	
	
	//返回话题卡片对应数据
	@RequestMapping(value="getTopicCardData.do")
	@ResponseBody
	public String getTopicCardData(@ModelAttribute("currentUser") UserInfo curUser,@RequestParam("topicId") String topicId,ModelMap m) throws Exception{
		if(curUser==null){
			
			throw new Exception("登录失效，请重新登录！");
			}
		BaseResult<Map> result=new BaseResult<Map>();
		Map resultMap=tss.getTopicData(curUser.getUserId(),topicId);
		resultMap.put("topicUrl", getTopic);
		result.setRsm(resultMap);
		return this.toJson(result);
	}
	
	//点击 关注或者取消关注触发该方法
	//页面使用的该方法
	@RequestMapping(value="/topic/ajax/focus_topic/topic_id-{topicId}/focusTopic.do")
	@ResponseBody
	public String focusTopic(@ModelAttribute("currentUser") UserInfo curUser,@PathVariable("topicId") String topicId,ModelMap m) throws Exception{
		if(curUser==null){
			
			throw new Exception("登录失效，请重新登录！");
			}
		BaseResult<Map> result=new BaseResult<Map>();
		String focusResult=tss.focusOrCancelFocusTopic(curUser.getUserId(),topicId);
		Map<String,Object> focusResultMap=new HashMap<String,Object>();
		focusResultMap.put("type", focusResult);
		result.setRsm(focusResultMap);
		return this.toJson(result);
	}
	//点击 关注或者取消关注触发该方法
	@RequestMapping(value="/topic/ajax/focus_topic/focusTopic.do")
	@ResponseBody
	public String focusTopicV2(@ModelAttribute("currentUser") UserInfo curUser,@RequestParam("topicId") String topicId,ModelMap m) throws Exception{
		if(curUser==null){
			
			throw new Exception("登录失效，请重新登录！");
			}
		BaseResult<Map> result=new BaseResult<Map>();
		String focusResult=tss.focusOrCancelFocusTopic(curUser.getUserId(),topicId);
		Map<String,Object> focusResultMap=new HashMap<String,Object>();
		focusResultMap.put("type", focusResult);
		result.setRsm(focusResultMap);
		return this.toJson(result);
	}
	
	@RequestMapping("/getTopicQuestion.do")
	@ResponseBody
	public String getTopicQuestions(@RequestParam("topicId") String topicId,@RequestParam("page")int page,@RequestParam("pageSize")int pageSize){
		BaseResult<List<QuestionData>> result = new BaseResult<List<QuestionData>>();
		List<QuestionData> questionDatas = uqs.getTopicQuestionDatas(topicId, page, pageSize);
		result.setRsm(questionDatas);
		return JsonUtils.toJson(result);
	}
	static final String getTopic="/showTopic.do";
	@RequestMapping(getTopic)
	public String showTopic(){
		return "topicDetail";
	}
}
