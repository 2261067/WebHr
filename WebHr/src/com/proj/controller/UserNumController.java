package com.proj.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.comet.CometUserConnectionsStore;
import com.proj.comet.UserDataStore;
import com.proj.resultData.BaseResult;
import com.proj.service.UserManagerService;
import com.proj.utils.JsonUtils;

@Controller
public class UserNumController {

	private CometUserConnectionsStore connectionsStore = CometUserConnectionsStore.getInstance();
	@Autowired
	private UserManagerService ums;
	
	@RequestMapping("/ajax/getUserNum.do")
	@ResponseBody
	public String getUserNum(){
		BaseResult<HashMap<String, Long>> result = new BaseResult<HashMap<String,Long>>();
		HashMap<String, Long> map = new HashMap<String, Long>();
		map.put("allUserNum", ums.getAllUserNum());
		map.put("onlineUserNum", connectionsStore.getUserNum());
		result.setRsm(map);
		return JsonUtils.toJson(result);
	}
	
}
