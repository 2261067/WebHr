package com.proj.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proj.entity.AttentionUser;
import com.proj.entity.UserInfo;
import com.proj.resultData.BaseResult;
import com.proj.resultData.ResultNo;
import com.proj.service.AttentionUserService;
import com.proj.session.SessionHelper;
import com.proj.utils.JsonUtils;

@Controller
public class AttentionUserController {

	@Autowired
	private AttentionUserService attentionUserService;
	
	@RequestMapping("/isAttentionUser.do")
	@ResponseBody
	public String isAttentionUser(@RequestParam("attention_user_id") String attentionUserId,HttpServletRequest request){
		BaseResult<HashMap<String, Object>> result = new BaseResult<HashMap<String,Object>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request, SessionHelper.SESSION_USER, UserInfo.class);
		if(userInfo  != null){
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("isAttentionUser", attentionUserService.getAttentionUser(userInfo.getUserId(), attentionUserId));
			result.setRsm(map);
		}else {
			result.setError(ResultNo.NEED_LOGIN);
		}
		
		return JsonUtils.toJson(result);
	}
	
	@RequestMapping("/ajax/attentionUser.do")
	@ResponseBody
	public String attentionUser(@RequestParam("attention_user_id") String attentionUserId,HttpServletRequest request){
		BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
		UserInfo userInfo = SessionHelper.getSessionAttribute(request, SessionHelper.SESSION_USER, UserInfo.class);
		if(userInfo  != null){
			if(userInfo.getUserId().equals(attentionUserId)){
				result.setError(ResultNo.Attention_SELF);
				return JsonUtils.toJson(result);
			}
			HashMap<String, String> map = new HashMap<String, String>();
			AttentionUser attentionUser = attentionUserService.getAttentionUser(userInfo.getUserId(), attentionUserId);
			if(attentionUser == null){
				attentionUserService.saveAttentionUser(userInfo.getUserId(), attentionUserId);
				map.put("type", "add");
			}else {
				attentionUserService.deleteAttentionUser(attentionUser);
				map.put("type", "delete");
			}
			result.setRsm(map);
		}else {
			result.setError(ResultNo.NEED_LOGIN);
		}
		
		return JsonUtils.toJson(result);
	}
	
}
