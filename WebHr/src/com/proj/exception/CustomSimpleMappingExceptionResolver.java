package com.proj.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.proj.resultData.BaseResult;
import com.proj.utils.JsonUtils;

public class CustomSimpleMappingExceptionResolver extends
		SimpleMappingExceptionResolver {

	private static final Logger LOGGER = Logger.getLogger(CustomSimpleMappingExceptionResolver.class);
	
	@Override
	protected ModelAndView doResolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		// Expose ModelAndView for chosen error view.aaa
		this.setDefaultErrorView("error/error");
		String viewName = determineViewName(ex, request);
//		日志记录exception
//		ex.printStackTrace();
		LOGGER.error(ex.getMessage(), ex);
		if (viewName != null) {// JSP格式返回
			if (!(request.getHeader("accept").indexOf("application/json") > -1 || (request
					.getHeader("X-Requested-With")!= null && request
					.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
				// 如果不是异步请求
//				Integer statusCode = determineStatusCode(request, viewName);
//				if (statusCode != null) {
//					applyStatusCodeIfPossible(request, response, statusCode);
//				}
				return getModelAndView(viewName, ex, request);
			} else {// JSON格式返回
				try {
					BaseResult<HashMap<String, String>> result = new BaseResult<HashMap<String,String>>();
					result.setErrno(0);
					result.setErr("当前请求发生错误，请联系管理员");
					PrintWriter writer = response.getWriter();
					writer.write(JsonUtils.toJson(result));
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;

			}
		} else {
			return null;
		}
	}
	
	//这里要获取到最内层的异常
    private static Throwable parseException(Throwable e){
        Throwable tmp = e;
        int breakPoint = 0;
        while(tmp.getCause()!=null){
            if(tmp.equals(tmp.getCause())){
                break;
            }
            tmp=tmp.getCause();
            breakPoint++;
            if(breakPoint>1000){
                break;
            }
        } 
        return tmp;
    }
}