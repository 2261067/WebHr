package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.AttentionTopic;
@Repository("attentionTopicDao")
public class AttentionTopicDao extends BaseDao<AttentionTopic,String>{
	public long getAttentionCountByTopic(String topicId){
		String hql="select count(*) from AttentionTopic a where a.topicId=?";
		return this.countByHql(hql, topicId);
	}
	public int getIfAttentionByTopic(String userId,String topicId){
		
		List<AttentionTopic> list=getIfAttentionByTopicAndUser(userId,topicId);
		if(list!=null&&list.size()>0)return 1;
		else return 0;
	}
	
	public List<AttentionTopic> getIfAttentionByTopicAndUser(String userId,String topicId){
		String hql="select a from AttentionTopic a where a.userId=? and a.topicId=?";
		List<AttentionTopic> list=this.getListByHQL(hql, userId,topicId);
		return list;
	}
	
	/*
	 * 返回用户关注话题
	 * */
	public List<Object> findAttentionTopic(String userId,int page, int pageSize){
		String hql = "select a,t from AttentionTopic a,Topic t where t.topicId=a.topicId and a.userId=?";
		List<Object> list=this.getListByHQLAndPageAndJoin(hql, page,
				pageSize, userId);
		return list;
	}
	/*
	 * 返回用户关注话题总数
	 * */
	public long getAttentionCountByuid(String userId){
		String hql="select count(*) from AttentionTopic a where a.userId=?";
		return this.countByHql(hql, userId);
	}
}
