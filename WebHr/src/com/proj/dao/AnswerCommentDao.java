package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.AnswerComment;

@Repository
public class AnswerCommentDao extends BaseDao<AnswerComment, Long> {
	public List<AnswerComment> getAnswerComment(String answerId){
		String queryString = "select ac from AnswerComment ac where ac.answerId =?";
		return this.getListByHQL(queryString, answerId);
	}
	
	public List<Object> getAnswerCommentAndUser(String answerId){
		String queryString = "select ac,user from AnswerComment ac , UserInfo user where  ac.userId = user.userId and ac.answerId =? order by ac.createTime";
		return this.getSession().createQuery(queryString).setParameter(0, answerId).list();
	}
}
