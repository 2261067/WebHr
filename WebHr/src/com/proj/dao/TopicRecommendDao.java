package com.proj.dao;

import java.util.List;

import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.proj.entity.UserInfo;

@Repository
public class TopicRecommendDao extends BaseDao {
	public List<Object> getTopicRecommendAnswerPeople(String topic, int limitNum) {
		String querysString = "select u.*,qt.topic_name as topic,sum(a.vote_count) as vote_num  from question_topic as qt  join answer as a on qt.question_id = a.question_id  join user_info u on a.user_id = u.user_id where qt.topic_name =? group by u.user_id having vote_num > 0 order by vote_num desc limit "
				+ limitNum;

		// Class[] classes = { UserInfo.class, Double.class };

		// return this.getListBySQL(querysString,classes, topic);
		// return this.getListBySQL(querysString, topic);
		return this.getSession().createSQLQuery(querysString)
				.addEntity(UserInfo.class)
				.addScalar("topic", StringType.INSTANCE)
				.addScalar("vote_num", LongType.INSTANCE)
				.setParameter(0, topic).list();
	}
	
	
}
