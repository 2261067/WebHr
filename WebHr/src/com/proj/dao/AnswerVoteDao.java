package com.proj.dao;

import org.springframework.stereotype.Repository;

import com.proj.entity.AnswerVote;

@Repository
public class AnswerVoteDao extends BaseDao<AnswerVote,Long>{
	public AnswerVote getAnswerVote(String answerId,String userId){
		String queryString = "select av from AnswerVote av where av.answerId = ? and av.userId = ?";
		return this.getByHQL(queryString, answerId,userId);
	}
}
