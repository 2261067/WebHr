package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.QuestionTopic;
@Repository
public class QuestionTopicDao extends BaseDao<QuestionTopic, String> {
	
 public	List<QuestionTopic> getQuestionTopics(String questionId){
	 String queryString = "select qt from QuestionTopic qt where qt.questionId =?";
	 return this.getListByHQL(queryString, questionId);
 }
 
 
	public long getQuestionCountByTopic(String topicId){
		String hql="select count(*) from QuestionTopic qt where qt.topicId=?";
		return this.countByHql(hql, topicId);
	}
}
