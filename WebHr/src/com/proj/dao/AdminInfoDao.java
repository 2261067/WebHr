package com.proj.dao;

import org.springframework.stereotype.Repository;

import com.proj.entity.AdminInfo;
import com.proj.entity.UserInfo;

@Repository("adminInfoDao")
public class AdminInfoDao extends BaseDao<AdminInfo,String>{

	/*
	 * 查询管理员用户名与密码是否存在
	 */
	public AdminInfo getAdminByAidAndPass(String aid, String password) {
		String query = "select a from AdminInfo a where a.adminId=? and a.password=?";
		Object[] params = { aid, password };
		AdminInfo admin = this.getByHQL(query, params);
		return admin;
	}

	/*
	 * 查询管理员邮箱与密码是否存在
	 */
	public AdminInfo getAdminByEmailAndPass(String email, String password) {
		String query = "select a from AdminInfo a where a.email=? and a.password=?";
		Object[] params = { email, password };
		AdminInfo admin = this.getByHQL(query, params);
		return admin;
	}
	
	/*
	 * 查询管理员注册或填写账户时的aid是否存在
	 */
	public AdminInfo isExistAdminId(String aid) {
		String query = "select a from AdminInfo a where a.adminId=? ";
		Object[] params = { aid };
		AdminInfo admin = this.getByHQL(query, params);
		return admin;
	}

	/*
	 * 查询用户注册或填写账户时的email是否存在
	 */
	public AdminInfo isExistEmail(String email) {
		String query = "select a from AdminInfo a where a.email=? ";
		Object[] params = { email };
		AdminInfo admin = this.getByHQL(query, params);
		return admin;
	}

}
