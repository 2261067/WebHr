package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.*;
@Repository("attentionQuestion")
public class AttentionQuestionDao  extends BaseDao<AttentionQuestion,Integer>{
	public List<AttentionQuestion> getQuestionAttentionUser (String questionId){
		String queryString = "select aq from AttentionQuestion aq where aq.question = ?";
		return this.getListByHQL(queryString, questionId);
	}
	
	public AttentionQuestion getQuestioAttention(String userId, String questionId){
		String queryString = "select aq from AttentionQuestion aq where aq.userId =? and aq.question = ?";
		return this.getByHQL(queryString, userId,questionId);
	}
	
	public Long getQuestionAttentionCount(String questionId){
		String queryString = "select count(*) from AttentionQuestion aq where aq.question = ?";
		return this.countByHql(queryString,questionId);
	}

}
