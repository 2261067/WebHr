package com.proj.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.ArticleComment;

@Repository
public class ArticleCommentDao extends BaseDao<ArticleComment,String>{

	public List<ArticleComment> getArticleComment(String articleId){
		String queryString = "select ac from ArticleComment ac where ac.articleId =?";
		return this.getListByHQL(queryString, articleId);
	}
	
	public List<Object> getArticleCommentAndUser(String articleId){
		String queryString = "select ac,user from ArticleComment ac , UserInfo user where  ac.userId = user.userId and ac.articleId =? order by ac.createTime";
		return this.getSession().createQuery(queryString).setParameter(0,articleId).list();
	}
	
	public long getArticleCommentCount() {
		String queryString = "select count(*) from ArticleComment";
		return this.countByHql(queryString);
	}
}
