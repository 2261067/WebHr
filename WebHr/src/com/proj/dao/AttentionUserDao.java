package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.AttentionUser;

@Repository
public class AttentionUserDao extends BaseDao<AttentionUser, Long> {

	public AttentionUser getAttentionUser(String userId, String attentionUserId) {
		String queryString = "select au from AttentionUser au where au.userId =? and au.attentionUserId =?";
		return this.getByHQL(queryString, userId, attentionUserId);
	}

	public long getFollowUserNum(String attentionUserId) {
		String queryString = " select count(*) from AttentionUser au where au.attentionUserId = ?";
		return this.countByHql(queryString, attentionUserId);
	}
	
	public long getAttentionUserNum(String userId){
		String queryString = " select count(*) from AttentionUser au where au.userId = ?";
		return this.countByHql(queryString, userId);
	}
	
	/*
	 * 用户关注人物列表
	 * */
	public List<Object> getAttentionUserList(String userId,int page, int pageSize) {
		String query = "select au,u from AttentionUser au,UserInfo u where au.attentionUserId=u.userId and au.userId =? order by au.createTime desc";
		List<Object> result = this.getListByHQLAndPageAndJoin(query, page,
				pageSize, userId);
		return result;
	}
	
	/*
	 * 用户粉丝列表
	 * */
	public List<Object> getFollowUserList(String attentionUserId, int page, int pageSize) {
		String query = "select au,u from AttentionUser au,UserInfo u where au.userId=u.userId and au.attentionUserId =? order by au.createTime desc";
		List<Object> result = this.getListByHQLAndPageAndJoin(query, page,
				pageSize, attentionUserId);
		return result;

	}
}
