package com.proj.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.proj.entity.Article;

@Repository
public class ArticleDao extends BaseDao<Article, String> {

	public List<Article> getArticlesByPage(int page, int pageSize) {
		//String queryString = "select new Article(a.activityId,a.activityName,a.userId,a.endTime) from Article a order by a.createTime desc";
		String queryString = "select a from Article a order by a.createTime desc";
		return this.getListByHQLAndPage(queryString, page, pageSize);
	}

	public Article getArticleById(String articleId) {
		String query = "select a from Article a where a.articleId=? order by a.createTime desc";
		return this.getByHQL(query, articleId);

	}

	public long getAllArticleCount() {
		String queryString = "select count(*) from Article";
		return this.countByHql(queryString);
	}

	/*
	 * 用户发起的活动
	 */
	public List<Article> getUserArticle(String userId, int page, int pageSize) {
		String query = "select a from Article a where a.userId=? order by a.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize, userId);

	}

	public List<Article> getAllArticles(int page, int pageSize) {
		String query = "select a from Article a order by a.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize);
	}
}
