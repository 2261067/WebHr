package com.proj.dao;

import java.math.BigInteger;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.proj.entity.Inbox;

@Repository
public class InboxDao extends BaseDao<Inbox, Long> {
	private static final Logger logger = Logger.getLogger(InboxDao.class);
//	未读消息是isread = 0 && 没有被该用户删除的消息
	public int getUserUnreadInboxNum(String userId){
		String queryString = "select count(*) from Inbox inbox where inbox.userId=? and inbox.isRead = 0 and (inbox.deleteUser != ? or inbox.deleteUser is null)";
		
		int num = 0;
		num = this.countByHql(queryString, userId ,userId).intValue();
		return num ;
	}
	
	public List<Inbox> getUserInboxMessage(String userId){
		String queryString = "select inbox from Inbox inbox where inbox.userId =?";
		
		return this.getListByHQL(queryString, userId);
	}

	public List<Inbox> getUserUndeleteInteractionMessage(String userId,String interacId){
//		String queryString = "select inbox from Inbox inbox where (inbox.userId =:userId and inbox.fromId =:interacId) or (inbox.userId =:interacId and inbox.fromId =:userId) order by inbox.createTime Desc";
		String queryString = "select * from inbox where ((inbox.user_id =:userId and inbox.from_id =:interacId) or (inbox.user_id =:interacId and inbox.from_id =:userId)) and (inbox.delete_user !=:userId or inbox.delete_user is null )order by inbox.create_time Desc";
		Query query = this.getSession().createSQLQuery(queryString).addEntity(Inbox.class).setParameter("userId", userId).setParameter("interacId", interacId);
		return query.list();
	}
	
	public List<Inbox> getUserInteractionMessage(String userId,String interacId){
//		String queryString = "select inbox from Inbox inbox where (inbox.userId =:userId and inbox.fromId =:interacId) or (inbox.userId =:interacId and inbox.fromId =:userId) order by inbox.createTime Desc";
		String queryString = "select * from inbox where (inbox.user_id =:userId and inbox.from_id =:interacId) or (inbox.user_id =:interacId and inbox.from_id =:userId)";
		Query query = this.getSession().createSQLQuery(queryString).addEntity(Inbox.class).setParameter("userId", userId).setParameter("interacId", interacId);
		return query.list();
	}
	
//	public List<Inbox> deleteUserInteractionMessage(String userId,String interacId){
//		String queryString = "delete inbox from Inbox where inbox.userId =:userId and inbox.fromId =:interacId or inbox.userId =:interacId and inbox.fromId =:userId";
//		Query query = this.getSession().createQuery(queryString).setParameter("userId", userId).setParameter("interacId", interacId);
//		return query.list();
//	}
	
	public List<Inbox> getLatestUserInboxMessage(String userId){
		String queryString = "select * from  (select * from inbox where (user_id =:userId or from_id=:userId) and (delete_user is null or delete_user !=:userId) order by inbox.create_time Desc ) as newbox group by from_id,user_id order by create_time Desc";
//		String queryString = "select inbox from  (select inbox from Inbox inbox where inbox.userId =:userId or inbox.fromId=:userId order by inbox.createTime Desc ) as newbox group by fromId,userId";
		Query query = this.getSession().createSQLQuery(queryString).addEntity(Inbox.class).setParameter("userId", userId);
		List<Inbox> inboxs = query.list();
		return inboxs;
	}
	
	public void updateUserInboxMessageRead(long id){
		String querysString = "update inbox set inbox.is_read = 1 where inbox.user_id =?";
		Query query = this.getSession().createSQLQuery(querysString).setParameter(0, id);
		query.list();
	}
}
