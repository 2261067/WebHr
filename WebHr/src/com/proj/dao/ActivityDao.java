package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.Activity;

@Repository
public class ActivityDao extends BaseDao<Activity,String> {
	
	public List<Activity> getActivitiesByPage(int page,int pageSize){
		String queryString = "select new Activity(a.activityId,a.activityName,a.userId,a.endTime) from Activity a order by a.createTime desc";
		return this.getListByHQLAndPage(queryString, page, pageSize);
	}
	
	public long getAllActivityCount(){
		String queryString = "select count(*) from Activity";
		return this.countByHql(queryString);
	}
	
	/*
	 * 用户发起的活动
	 * */
	public List<Activity> getUserActivity(String userId, int page, int pageSize) {
		String query = "select a from Activity a where a.userId=? order by a.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize,userId);

	}
	
	public List<Activity> getAllActivities( int page, int pageSize) {
		String query = "select a from Activity a order by a.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize);
	}
}
