package com.proj.dao;

import org.springframework.stereotype.Repository;

import com.proj.entity.QuestionInvite;

@Repository
public class QuestionInviteDao extends BaseDao<QuestionInvite, Long> {
	public QuestionInvite getQuestionInvite(String inviteUserId,String questionId){
		String queryString = "select qi from QuestionInvite qi where qi.inviteUserId = ? and qi.questionId = ?";
		return this.getByHQL(queryString, inviteUserId,questionId);
	}
}
