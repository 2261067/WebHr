package com.proj.dao;

import org.springframework.stereotype.Repository;

import com.proj.entity.ThirdPartLanding;


@Repository("thirdPartLandingDao")
public class ThirdPartLandingDao extends BaseDao<ThirdPartLanding,String>{
	/*
	 * 查询第三方账号是否登录过
	 */
	public ThirdPartLanding isExistqqpid(String pid) {
		String query = "select t from ThirdPartLanding t where t.pid=? ";
		Object[] params = { pid };
		ThirdPartLanding tpl = this.getByHQL(query, params);
		return tpl;
	}

	/*
	 * 保存第三方账号登录信息
	 */
	public void saveThirdPartLanding(ThirdPartLanding tpl) {
	    this.save(tpl);
	}
	
	/*
	 * 删除第三方账号登录信息
	 */
	public void deleteThirdPartLanding(ThirdPartLanding tpl) {
	    this.delete(tpl);
	}
}
