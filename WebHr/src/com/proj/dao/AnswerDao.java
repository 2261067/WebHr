package com.proj.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.Answer;
import com.proj.entity.UserInfo;

@Repository("answerDao")
public class AnswerDao extends BaseDao<Answer, String> {

	public static final int TOP_CONTRIBUTE_USER_COUNT = 2;

	public List<Object> getQuestionAnswer(String questionId) {
		String sql = "select a.*,u.* from answer a inner join user_info u on a.user_id=u.user_id where a.question_id=?";

		Class[] clist = { Answer.class, UserInfo.class };
		List<Object> result = this.getListBySQL(sql, clist, questionId);
		return result;
	}

	/*
	 * 用户回复的问题
	 */
	public List<Object> getUserAnswers(String userId, int page, int pageSize) {
		String query = "select a,q from Answer a ,Question q where a.question=q.questionId and a.userId=? order by a.createTime desc";
		List<Object> result = this.getListByHQLAndPageAndJoin(query, page,
				pageSize, userId);
		return result;

	}

	public List<Object> getQuestionAnswerOrderByVote(String questionId,
			int page, int pagesize) {
		String hql = "select a,u from Answer a   ,UserInfo u where a.userId=u.userId and a.question=? order by a.voteCount desc, a.createTime desc";

		List<Object> result = this.getListByHQLAndPageAndJoin(hql, page,
				pagesize, questionId);
		return result;
	}

	public Answer getLatestAnswer(String questionId) {
		Answer latestAns = null;
		String hql = "select a from Answer a  where a.question=? order by a.createTime desc";
		List<Answer> result = this.getListByHQLAndPage(hql, 1, 1, questionId);
		if (result != null && result.size() != 0)
			latestAns = result.get(0);
		return latestAns;

	}

//	public List<Object> getQuestionAnswerOrderByTime(String questionId) {
//		String hql = "select a,u from Answer a ,UserInfo u where a.userId=u.userId and a.question=? order by a.createTime desc";
//
//		List<Object> result = this.getSession().createQuery(hql)
//				.setParameter(0, questionId).list();
//		return result;
//	}
	
	public List<Object> getQuestionAnswerOrderByTime(String questionId,int page,int pageSize,String sort) {
		String hql = "select a,u from Answer a ,UserInfo u where a.userId=u.userId and a.question=? order by a.createTime " + sort;

		List<Object> result = this.getListByHQLAndPageAndJoin(hql, page, pageSize, questionId);
		return result;
	}

	/**
	 * 返回贡献值最前的n个用户
	 * 
	 * @param questionId
	 * @return
	 */
	public List<Answer> getTopContributeAnswers(String questionId) {
		Answer latestAns = null;
		String hql = "select a from Answer a  where a.question=? order by a.voteCount desc";
		List<Answer> resultAns = this.getListByHQLAndPage(hql, 1,
				TOP_CONTRIBUTE_USER_COUNT, questionId);
		return resultAns;

	}

	public List<Object> getQuestionAnswerOrderByVote(String questionId) {
		String hql = "select a,u from Answer a ,UserInfo u where a.userId=u.userId and a.question=? order by a.voteCount desc, a.createTime desc";

		List<Object> result = this.getSession().createQuery(hql)
				.setParameter(0, questionId).list();
		return result;
	}

	public int updateAnswerVoteNum(String answerId, byte voteChange) {
		String queryString = "update answer set answer.vote_count = answer.vote_count + :voteChange where answer_id = :answerId";
		return this.getSession().createSQLQuery(queryString)
				.setParameter("voteChange", voteChange)
				.setParameter("answerId", answerId).executeUpdate();
	}

	public List<Object> getQuestionAnswerOrderByTime(String questionId,
			String sort) {
		String hql = "select a,u from Answer a ,UserInfo u where a.userId=u.userId and a.question=:questionId order by a.createTime "
				+ sort;

		List<Object> result = this.getSession().createQuery(hql)
				.setParameter("questionId", questionId).list();
		return result;
	}
	
	public long getUserAnswerCount(String userId){
		String queryString = "select count(*) from Answer a where a.userId = ?";
		return this.countByHql(queryString, userId);
	}
	
	public long getUserAnswerVotedCount(String userId){
		String queryString = "select sum(a.vote_count) from answer a where a.user_id = ?";
		Object object= this.getSession().createSQLQuery(queryString).setParameter(0, userId).uniqueResult();
		if(object == null){
			return 0l;
		}else {
			BigDecimal num = (BigDecimal)object;
			return num.longValue();
		}
	}
	
	public long getQuestionAnswerCount(String questionId){
		String queryString = "select count(*) from Answer a where a.question= ?";
		return this.countByHql(queryString, questionId);
	}
	
	public long getAnswerCommentCount(String answerId){
		String queryString = "select count(*) from AnswerComment ac where ac.answerId= ?";
		return this.countByHql(queryString, answerId);
	}
}
