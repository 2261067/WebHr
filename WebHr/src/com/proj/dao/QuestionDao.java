package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.*;

@Repository("questionDao")
public class QuestionDao extends BaseDao<Question, String> {

	/**获取用户关注的问题
	 * @param userId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<Question> getQuestionByFollow(String userId, int page, int pageSize) {
		String query = "select q from Question q,AttentionQuestion aq where q.questionId=aq.question and aq.userId=? order by q.createTime desc";

		return this.getListByHQLAndPage(query, page, pageSize,userId);

	}
	/**获取用户被邀请的问题
	 * @param userId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<Question> getQuestionByInvite(String userId, int page, int pageSize) {
		String query = "select q from Question q,QuestionInvite qi where q.questionId=qi.questionId and qi.inviteUserId=? order by qi.createTime";

		return this.getListByHQLAndPage(query, page, pageSize,userId);

	}
	public List<Question> getQuestions(String userId, int page, int pageSize) {
		String query = "select q from Question q order by q.createTime desc";

		return this.getListByHQLAndPage(query, page, pageSize);

	}
	public List<Question> getAllQuestions( int page, int pageSize) {
		String query = "select q from Question q order by q.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize);
	}
	
	/*
	 * 用户发起的问题
	 * */
	public List<Question> getUserQuestions(String userId, int page, int pageSize) {
		String query = "select q from Question q where q.userId=? order by q.createTime desc";

		return this.getListByHQLAndPage(query, page, pageSize,userId);

	}

	public long getQuestionCount() {
		String query = "select count(*) from Question q";

		return this.countByHql(query);

	}

	public Object getQuestionAndUserInfo(String questionId){
		String queryString = "select q.*,u.* from question q,user_info u where q.question_id =? and q.user_id = u.user_id";
		return this.getSession().createSQLQuery(queryString).addEntity(Question.class).addEntity(UserInfo.class).setParameter(0, questionId).uniqueResult();
	}
	
	public void updateQuestionLastMotify(String questionId){
		String queryString = "update question set last_motify = now() where question_id =?";
		this.getSession().createSQLQuery(queryString).setParameter(0, questionId).executeUpdate();
	}
	
	public long getUserQuestionCount(String userId){
		String queryString = "select count(*) from Question q where q.userId = ?";
		return this.countByHql(queryString, userId);
	}
	
	public List<Question> getTopicQuestions(String topicId,int page,int pageSize){
		String queryString = "select q from Question q ,QuestionTopic qt where q.questionId = qt.questionId and qt.topicId = ? order by q.createTime desc";
//		String querystrString = "select question.* from question left join question_topic on question.question_id = question_topic.question_id and question_topic.topic_id = ? order by question.create_time desc";
//		return this.getSession().createSQLQuery(querystrString).addEntity(Question.class).setParameter(0, topicId).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).list();
		return this.getListByHQLAndPage(queryString, page, pageSize, topicId);
	}
	
	public List<Question> getHotQuestions(int page,int pageSize){
		String queryString = "select q from Question q order by COALESCE(q.answerTime,0)+COALESCE(q.attentionTime,0)+COALESCE(q.browseTime,0) desc";
//		String querystrString = "select question.* from question left join question_topic on question.question_id = question_topic.question_id and question_topic.topic_id = ? order by question.create_time desc";
//		return this.getSession().createSQLQuery(querystrString).addEntity(Question.class).setParameter(0, topicId).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).list();
		return this.getListByHQLAndPage(queryString, page, pageSize);
	}
}
