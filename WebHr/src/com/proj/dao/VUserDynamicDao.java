package com.proj.dao;


import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.proj.entity.*;
@Repository
public class VUserDynamicDao extends BaseDao<VUserDynamic,VUserDynamicId>{

	
	
	public List<VUserDynamic> getDynamicByPage(int currentPage,int pageSize,String uid){
		//originUserId存放当前用户的id，userid为数据作者
		String hqlString="select v from VUserDynamic v where v.originUserId=? order by v.lastModifyTime desc,v.originUserId";
		List<VUserDynamic> resultList=this.getListByHQLAndPage(hqlString, currentPage, pageSize, uid);
		return resultList;
	}
	
	/**获取
	 * @param currentPage
	 * @param pageSize
	 * @param uid
	 * @return
	 */
	public List<VUserDynamic> getFocusQuetionAnswer(int currentPage,int pageSize,String uid){
		//originUserId存放当前用户的id，userid为数据作者
		String hqlString="select a,aq from Answer a,AttentionQuestion aq where a.question=aq.question and aq.userId=? order by a.createTime desc ";
		List<VUserDynamic> result=new LinkedList<VUserDynamic>();
		List<Object> middleResultList=this.getListByHQLAndPageAndJoin(hqlString, currentPage, pageSize, uid);
		for(Object o:middleResultList){
			Object[] joinResult=(Object[])o;
			Answer answer=(Answer)joinResult[0];
			AttentionQuestion aq=(AttentionQuestion)joinResult[1];
			VUserDynamic vd=new VUserDynamic();
			vd.setLastModifyTime(answer.getLastModifyTime());
			vd.setDataType("回答");
			vd.setId(new VUserDynamicId("focus_question_answer", answer.getAnswerId()));
			vd.setOriginUserId(aq.getUserId());
			vd.setDataTitle("回答内容");
			vd.setQuestionId(answer.getQuestion());
			vd.setUserId(answer.getUserId());
			vd.setDataDesc(answer.getAnswerContext());
			vd.setCreateTime(answer.getCreateTime());
			result.add(vd);
		}
		return result;
	}
	public List<VUserDynamic> getFocusTopicQuetion(int currentPage,int pageSize,String uid){
		//originUserId存放当前用户的id，userid为数据作者
		String hqlString="select q,qt,at from Question q,QuestionTopic qt,AttentionTopic at where q.questionId=qt.questionId and qt.topicId=at.topicId and at.userId=? order by q.createTime desc ";
		List<VUserDynamic> result=new LinkedList<VUserDynamic>();
		List<Object> middleResultList=this.getListByHQLAndPageAndJoin(hqlString, currentPage, pageSize, uid);
		for(Object o:middleResultList){
			Object[] joinResult=(Object[])o;
			Question question=(Question)joinResult[0];
			QuestionTopic aq=(QuestionTopic)joinResult[1];
			AttentionTopic at=(AttentionTopic)joinResult[2];
			VUserDynamic vd=new VUserDynamic();
			vd.setLastModifyTime(question.getLastMotify());
			vd.setDataType("问题");
			vd.setId(new VUserDynamicId("focus_topic_question", question.getQuestionId()));
			vd.setOriginUserId(at.getUserId());
			vd.setDataTitle(question.getQuestionName());
			vd.setQuestionId(question.getQuestionId());
			vd.setUserId(question.getUserId());
			vd.setDataDesc(question.getQuestionDesc());
			vd.setCreateTime(question.getCreateTime());
			result.add(vd);
		}
		return result;
	}
	
	public List<VUserDynamic> getFocusUserQuetion(int currentPage,int pageSize,String uid){
		//originUserId存放当前用户的id，userid为数据作者
		String hqlString="select q,au from Question q,AttentionUser au where q.userId=au.attentionUserId and au.userId=? order by q.createTime desc ";
		List<VUserDynamic> result=new LinkedList<VUserDynamic>();
		List<Object> middleResultList=this.getListByHQLAndPageAndJoin(hqlString, currentPage, pageSize, uid);
		for(Object o:middleResultList){
			Object[] joinResult=(Object[])o;
			Question question=(Question)joinResult[0];
			AttentionUser au=(AttentionUser)joinResult[1];
			
			VUserDynamic vd=new VUserDynamic();
			vd.setLastModifyTime(question.getLastMotify());
			vd.setDataType("问题");
			vd.setId(new VUserDynamicId("focus_user_question", question.getQuestionId()));
			vd.setOriginUserId(au.getUserId());
			vd.setDataTitle(question.getQuestionName());
			vd.setQuestionId(question.getQuestionId());
			vd.setUserId(question.getUserId());
			vd.setDataDesc(question.getQuestionDesc());
			vd.setCreateTime(question.getCreateTime());
			result.add(vd);
		}
		return result;
	}
	public List<VUserDynamic> getFocusUserAnswer(int currentPage,int pageSize,String uid){
		//originUserId存放当前用户的id，userid为数据作者
		String hqlString="select a,au from Answer a,AttentionUser au where a.userId=au.attentionUserId and au.userId=? order by a.createTime desc ";
		List<VUserDynamic> result=new LinkedList<VUserDynamic>();
		List<Object> middleResultList=this.getListByHQLAndPageAndJoin(hqlString, currentPage, pageSize, uid);
		for(Object o:middleResultList){
			Object[] joinResult=(Object[])o;
			Answer answer=(Answer)joinResult[0];
			AttentionUser au=(AttentionUser)joinResult[1];
			
			VUserDynamic vd=new VUserDynamic();
			vd.setLastModifyTime(answer.getLastModifyTime());
			vd.setDataType("回答");
			vd.setId(new VUserDynamicId("focus_user_answer", answer.getAnswerId()));
			vd.setOriginUserId(au.getUserId());
			vd.setDataTitle("回答内容");
			vd.setQuestionId(answer.getQuestion());
			vd.setUserId(answer.getUserId());
			vd.setDataDesc(answer.getAnswerContext());
			vd.setCreateTime(answer.getCreateTime());
			result.add(vd);
		}
		return result;
	}
}
