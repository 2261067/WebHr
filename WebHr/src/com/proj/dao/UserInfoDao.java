package com.proj.dao;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;


import com.proj.entity.Role;
import com.proj.entity.UserInfo;

@Repository("userInfoDao")
public class UserInfoDao extends BaseDao<UserInfo,String> {

	/*
	 * 查询用户名与密码是否存在
	 */
	public UserInfo getUserByUidAndPass(String uid, String password) {
		String query = "select u from UserInfo u where u.userId=? and u.password=?";
		Object[] params = { uid, password };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}

	/*
	 * 查询邮箱与密码是否存在
	 */
	public UserInfo getUserByEmailAndPass(String email, String password) {
		String query = "select u from UserInfo u where u.email=? and u.password=?";
		Object[] params = { email, password };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}

	/*
	 * 查询用户注册或填写账户时的uid是否存在
	 */
	public UserInfo isExistUserId(String uid) {
		String query = "select u from UserInfo u where u.userId=? ";
		Object[] params = { uid };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}
	
	/*
	 * 查询用户注册或填写账户时的nick是否存在
	 */
	public UserInfo isExistUserNick(String nickName) {
		String query = "select u from UserInfo u where u.nickName=? ";
		Object[] params = { nickName };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}

	/*
	 * 查询用户注册或填写账户时的email是否存在
	 */
	public UserInfo isExistEmail(String email) {
		String query = "select u from UserInfo u where u.email=? ";
		Object[] params = { email };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}

	/*
	 * 忘记密码找回密码
	 */
	public UserInfo findpwd(String email) {
		String query = "select u from UserInfo u where u.email=? ";
		Object[] params = { email };
		UserInfo user = this.getByHQL(query, params);
		return user;
	}

	/*
	 * 修改密码
	 */
	public UserInfo updatePwd(UserInfo user, String pwd) {
		user.setPassword(pwd);
		this.update(user);
		return user;
	}

	/*
	 * 更新用户信息
	 */
	public UserInfo updateUserInfo(UserInfo user) {
		this.update(user);
		return user;
	}

	public Role getUserRoleByUidAndPass(String uid) {
		// String queryString =
		// "select r from UserInfo  u left join UserRole  ur on u.user_uid = ur.user_id  role as r on ur.role = r.role where u.user_uid = ?";
		String queryString = "select r from UserRole  ur  left join  ur.role  r  where ur.userInfo.userId = ?";
		Query query = this.getSession().createQuery(queryString)
				.setParameter(0, uid);

		return (Role) query.uniqueResult();
	}
	
	public List<UserInfo> getSimilarUserInfos(String uid){
		String queryString = "select u from UserInfo u where u.userId like ? or u.nickName like ?";
		return this.getListByHQL(queryString, "%" + uid + "%","%" + uid + "%");
	}
	
	public List<UserInfo> getAllUserInfos(){
		String queryString = "select u from UserInfo u";
		return this.getListByHQL(queryString);
	} 
	
	public long getAllUserNums(){
		String queryString = "select count(*) from UserInfo u";
		return this.countByHql(queryString);
	} 
	
	//搜索用户
	public List<UserInfo> searchUserInfos(String query,int page,int pageSize){
		String queryString = "select * from user_info where user_id like ? or nick_name like ? or concat(first_name,last_name) like ? limit ?,?";
		Class[] clist={UserInfo.class};
		String likeQueryString="%"+query+"%";
		Object[] params={likeQueryString,likeQueryString,likeQueryString,(page-1)*pageSize,pageSize};
		List<Object> middleResult=this.getListBySQL(queryString, clist,params);
				
		List<UserInfo> result = new LinkedList<UserInfo>();

		for (Object o : middleResult) {
					
					result.add((UserInfo)o);

		}
				
				
		return result;
	}
	
	/*
	 * 查找所有用户信息
	 * */
	public List<UserInfo> getAllUserInfo( int page, int pageSize) {
		String query = "select u from UserInfo u order by u.createTime desc";
		return this.getListByHQLAndPage(query, page, pageSize);
	}
}
