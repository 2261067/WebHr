package com.proj.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proj.entity.ActivitySign;
import com.proj.entity.UserInfo;

@Repository
public class ActivitySignDao extends BaseDao<ActivitySign,Long>{
	
//	选取某个活动最后一个报名者
	public ActivitySign getLastestSign(String activityId){
		String queryString = "select acs.* from activity_sign as acs where acs.activity_id = ? order by acs.create_time desc limit 1";
		return (ActivitySign)this.getSession().createSQLQuery(queryString).addEntity(ActivitySign.class).setParameter(0, activityId).uniqueResult();
	}
//	获得某个活动的全部报名人数
	public long getActivitySignCount(String activityId) {
		String queryString = "select count(*) from ActivitySign acs where acs.activityId =?";
		return this.countByHql(queryString, activityId);
	}
//	用户是否报名参加活动
	public ActivitySign getActivitySign(String userId,String activityId){
		String queryString = "select acs from ActivitySign acs where acs.userId =? and acs.activityId=?";
		return this.getByHQL(queryString, userId,activityId);
	}
	
//某个活动的所有报名
	public List<ActivitySign> getAllActivitySigns(String activityId){
		String queryString = "select acs from ActivitySign acs where acs.activityId=?";
		return this.getListByHQL(queryString, activityId);
	}
	
//某个活动的最新n个报名者
	public List<UserInfo> getActivitySignUser(String activityId,int n){
		String queryString ="select u.* from user_info u,activity_sign acs  where u.user_id = acs.user_id and acs.activity_id = ? order by acs.create_time limit " + n;
		return this.getSession().createSQLQuery(queryString).addEntity(UserInfo.class).setParameter(0, activityId).list();
	}
}
