package com.proj.dao;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.proj.entity.Answer;
import com.proj.entity.Topic;
import com.proj.entity.UserInfo;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService.AnswerAndUser;

@Repository("topicDao")
public class TopicDao extends BaseDao<Topic, String> {
	public Topic findTopicByName(String name){
		String querysString = "select topic from Topic topic where topic.topicName =:topicName";
		Query query = this.getSession().createQuery(querysString).setString("topicName", name);
		
		return (Topic)query.uniqueResult();
	}
	public Topic findTopicById(String topicid){
		String query = "select t from Topic t where t.topicId=?";
		Object[] params = { topicid };
		Topic t = this.getByHQL(query, params);
		return t;
	}
	public List<Topic> findAllTopic(){
		String querysString = "select topic from Topic topic ";
		List<Topic> topics = this.getListByHQL(querysString);
		
		return topics;
	}
	
	
	public List<Topic> findAllTopicByPage(int page,int pageSize){
		String querysString = "select topic from Topic topic ";
		
		List<Topic> topics =this.getListByHQLAndPage(querysString, page, pageSize);
		return topics;
	}
	
	public List<Topic> findAllTopicByPageAndTimeDesc(int page,int pageSize){
		String querysString = "select topic from Topic topic order by topic.createTime desc";
		
		List<Topic> topics =this.getListByHQLAndPage(querysString, page, pageSize);
		return topics;
	}
	
	public List<Topic> getHotTopicByPage(int page,int pageSize){
		String querysString = "select t.* from topic t left join question_topic q on t.topic_id=q.topic_id group by t.topic_id order by count(*) desc limit ?,?";
		Class[] cList=new Class[1];
		cList[0]=Topic.class;
		List<Object> middleResult =this.getListBySQL(querysString, cList,(page-1)*pageSize,pageSize);
		List<Topic> topics = new LinkedList<Topic>();

		for (Object o : middleResult) {
			
			topics.add((Topic)o);

		}
		
		return topics;
	}
	
	public List<Topic> findFocusTopic(String userId){
		String querysString = "select distinct topic from Topic topic ,AttentionTopic at where topic.topicId=at.topicId and at.userId=?";
		List<Topic> topics = this.getListByHQL(querysString,userId);
		
		return topics;
	}
	
	
	public List<Topic> findSimilarTopic(String topicName){
		String queryString = "select t from Topic t where t.topicName like ?";
		return this.getListByHQL(queryString, "%" + topicName + "%");
	}
	public List<Topic> findSimilarTopic(String topicName,int page,int pageSize){
		String queryString = "select t from Topic t where t.topicName like ?";
		return this.getListByHQLAndPage(queryString,page,pageSize, "%" + topicName + "%");
	}
	
	
	public long findAllTopicCount(){
		String queryString = "select count(*) from Topic t";
		return this.countByHql(queryString);
	}
	
	
	public List<Topic> getQuestionTopic(String questionId){
		String querysString = "select distinct topic from Topic topic ,QuestionTopic qt where topic.topicId=qt.topicId and qt.questionId=?";
		List<Topic> topics = this.getListByHQL(querysString,questionId);
		return topics;
	}
	
	public List<Topic> getAnswerTopic(String answerId){
		String querysString = "select distinct topic from Topic topic ,QuestionTopic qt,Answer a where topic.topicId=qt.topicId and a.question=qt.questionId and a.answerId=?";
		List<Topic> topics = this.getListByHQL(querysString,answerId);
		return topics;
		
	}
	public long getFocusTopicCount(String userId){
		String querysString = "select count(*) from Topic topic ,AttentionTopic at where topic.topicId=at.topicId and at.userId=?";
		long count = this.countByHql(querysString,userId);
		return count;
		
	}
}
