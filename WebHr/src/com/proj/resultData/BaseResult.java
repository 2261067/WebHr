
package com.proj.resultData;

/**
 * @author Wangqing
 *返回json格式父类，出错信息，code，实体
 */
public class BaseResult<T> {
	public String err;
	public Integer errno;
	public T rsm;
	
//	1成功，0错误
	public BaseResult(){
		this.setError(ResultNo.SUCCESS);
	}
	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.setErrno(0);
		this.err = err;
	}

	public Integer getErrno() {
		return errno;
	}

	public void setErrno(Integer errno) {
		this.errno = errno;
	}
	public T getRsm() {
		return rsm;
	}
	public void setRsm(T rsm) {
		this.rsm = rsm;
	}

	public void setError(ResultNo resultNo){
		this.setErr(resultNo.err);
		this.setErrno(resultNo.errno);
	}
	
}
