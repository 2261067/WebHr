package com.proj.resultData;

public enum ResultNo {
	
	SUCCESS("success",1),
	NEED_LOGIN("需要登录",3000),
//提问
	NEED_QUESION_NAME("需要填写问题标题",4001),
	NEED_QUESION_TOPIC("需要填写问题话题",4002),
	WRONG_QUESION_TOPIC("问题话题不存在",4003),
	NEED_ANSWER_CONTENT("回答内容不能为空",4005),
	INVITE_SELF("不能邀请自己回答问题",4008),
	INVITE_ALREADY("该用户已被邀请",4009),

//	私信
	INBOX_SEND_ERROR("需要填写收信人和私信内容,收信人不存在！",5001),
//	用户关注
	Attention_SELF("不能关注自己！",6001),
//	活动
	NEED_ACTIVITY_NAME("需要填写活动标题、内容和举办地点",7001),
	NEED_ACTIVITY_TIME("需要填写活动截止日期和举办日期",7002),
	WRONG_ACTIVITY_TIME("活动日期格式不正确  2015-05-08,且日期大于今天",7003),
	EXPIRED_ACTIVITY_SIGN_TIME("活动已截止，无法报名",7004),
	EXPIRED_ACTIVITY_CANCEL_TIME("活动已截止，无法取消报名",7005),
//专栏
	NEED_ARTICLE_NAME("需要填写文章标题、内容",8001),
	NEED_ARTICLE_CONTEXT("需要填评论内容",8002);
	public String err;
	public Integer errno;
	
	ResultNo(String err,int errno){
		this.err = err;
		this.errno = errno;
	}
}
