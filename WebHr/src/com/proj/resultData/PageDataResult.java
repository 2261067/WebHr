package com.proj.resultData;

public class PageDataResult<T> extends BaseResult<T>{
	long currentPage;
	long pageSize;
	long allDataCount;
	long nextPage;
	public long getNextPage() {
		return nextPage;
	}
	public void setNextPage(long nextPage) {
		this.nextPage = nextPage;
	}
	public long getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(long currentPage) {
		this.currentPage = currentPage;
	}
	public long getPageSize() {
		return pageSize;
	}
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}
	public long getAllDataCount() {
		return allDataCount;
	}
	public void setAllDataCount(long allDataCount) {
		this.allDataCount = allDataCount;
	}

}
