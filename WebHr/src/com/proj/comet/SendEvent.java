package com.proj.comet;

import java.util.ArrayList;
import java.util.List;


public abstract class SendEvent extends HrCometEvent {

	protected List<String> uids = new ArrayList<String>();
	protected Object message;

	public SendEvent(){
		
	}
	
	public SendEvent(String userId, Object message) {
		this.uids.add(userId);
		this.message = message;
	}

	public SendEvent(List<String> userIds, Object message) {
		this.uids.addAll(userIds);
		this.message = message;
	}
	
	public SendEvent(List<String> userIds) {
		this.uids.addAll(userIds);
	}
	
	public SendEvent(String userId) {
		this.uids.add(userId);
	}

	@Override
	public void run() {
		send();
	}
	
	 protected abstract void send();

	
}
