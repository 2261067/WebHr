package com.proj.comet;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.comet4j.core.CometContext;
import org.comet4j.core.CometEngine;

/**
 * @author Wangqing 用于发送消息，并且定期检查过期缓存
 */
public class CometUserManager {
	private static CometUserConnectionsStore cometUserConnectionsStore = CometUserConnectionsStore
			.getInstance();
	private static UserDataStore userDataStore = UserDataStore.getInstance();
	private static CometContext cometContext = CometContext.getInstance();
	private static CometEngine cometEngine = cometContext.getEngine();
	private static Thread cleaner;
	private static final String CHANNEL = "inbox";

	private static final int Frequency = 20;

	private boolean init = false;
	
	private static CometUserManager cometUserManager = new CometUserManager();
	
	public static void sendToUser(String userId, Object message) {
		List<String> connectionIds = cometUserConnectionsStore
				.getUserConnectionIds(userId);
		if (connectionIds != null && !connectionIds.isEmpty()) {
			for (String cid : connectionIds) {
				cometEngine.sendTo(CHANNEL, cometEngine.getConnection(cid),
						message);
			}
		}
	}

	public static void sendToConnection(String cid, Object message) {
		cometEngine.sendTo(CHANNEL, cometEngine.getConnection(cid), message);
	}

	private CometUserManager() {
		
	}

	public static CometUserManager getInstance(){
		return cometUserManager;
	}
	
	public void init(){
		cometContext.registChannel(CHANNEL);
		start();
	}
	
	private void start() {
		init = true;
		cleaner = new Thread(new CheckAndSendThread());
		cleaner.start();
	}

	class CheckAndSendThread implements Runnable {

		@Override
		public void run() {
			while (init) {
				try {
					TimeUnit.SECONDS.sleep(Frequency);
					checkAndSend();
				} catch (Exception e) {
				}
			}

		}

		// 检查缓存中，是否有过期链接，并向在线用户推送未读私信数目
		void checkAndSend() {
			Set<String> uidSet = cometUserConnectionsStore.getAllUser();
			if (uidSet == null || uidSet.isEmpty())
				return;
			// 检查所有在线用户
			for (String uid : uidSet) {
				// 检查单个用户连接
				List<String> cidList = cometUserConnectionsStore
						.getUserConnectionIds(uid);
				if (cidList != null && !cidList.isEmpty()) {
					for (String cid : cidList) {
						if ((cometEngine.getConnection(cid)) == null) {
							cometUserConnectionsStore.removeUserConnection(uid,
									cid);
						}
					}
				}
				cidList = cometUserConnectionsStore.getUserConnectionIds(uid);
				// 如果用户链接为空，删除该用户，并删除该用户数据
				if (cidList == null || cidList.isEmpty()) {
					cometUserConnectionsStore.removeUser(uid);
					userDataStore.removeUserData(uid);
				} else {
					InboxNumCheckAndSendEvent event = new InboxNumCheckAndSendEvent(
							uid);
					CometExecutor.execute(event);
				}

			}
		}

	}
	
	public  void destroy(){
		init = false;
		cleaner = null;
		cometUserConnectionsStore.destroy();
		userDataStore.destroy();
		CometExecutor.destroy();
	}
}
