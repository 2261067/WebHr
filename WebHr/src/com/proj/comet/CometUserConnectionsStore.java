package com.proj.comet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.comet4j.core.CometConnection;
import org.comet4j.core.CometContext;
import org.comet4j.core.CometEngine;

/**
 * @author Wangqing 存储用户id与在线链接id
 */
public class CometUserConnectionsStore {

	private static final int MAX_CONNECTIONS = 5;
	private static CometContext cometContext = CometContext.getInstance();
	private static CometEngine cometEngine = cometContext.getEngine();
	/**
	 * 第一参数用户id，第二参数对应的链接集合
	 */
	private ConcurrentHashMap<String, List<String>> userConnectionsMap;

	private static CometUserConnectionsStore cometUserConnectionsStore = new CometUserConnectionsStore();

	public static CometUserConnectionsStore getInstance() {
		return cometUserConnectionsStore;
	}

	private CometUserConnectionsStore() {
		userConnectionsMap = new ConcurrentHashMap<String, List<String>>();
	}

	public void pushUserConnectionId(String userId, String connectionId) {
		List<String> connectionsList = userConnectionsMap.get(userId);
		if (connectionsList == null) {
			connectionsList = Collections
					.synchronizedList(new LinkedList<String>());
			userConnectionsMap.putIfAbsent(userId, connectionsList);
			pushUserConnectionId(userId, connectionId);
			return;
		}
		// 如果连接数大于5，移除最先进入的链接
		synchronized (connectionsList) {
			if (connectionsList.size() >= MAX_CONNECTIONS) {
				String cid = connectionsList.remove(0);
				CometConnection cometConnection = cometEngine
						.getConnection(cid);
				if (cometConnection != null) {
					cometEngine.remove(cometConnection);
				}
			}
			connectionsList.add(connectionId);
		}
		userConnectionsMap.put(userId, connectionsList);

	}

	public List<String> getUserConnectionIds(String userId) {
		List<String> connectionsList = userConnectionsMap.get(userId);
		if (connectionsList == null)
			return null;

		return new ArrayList<String>(connectionsList);
	}

	public void removeUserConnection(String userId, String connectionId) {
		List<String> connectionsList = userConnectionsMap.get(userId);
		if (connectionsList != null) {
			connectionsList.remove(connectionId);
		}
	}

	public void removeUser(String userId) {
		userConnectionsMap.remove(userId);
	}

	public Set<String> getAllUser() {
		if (userConnectionsMap.keySet().isEmpty()) {
			return null;
		} else {
			return userConnectionsMap.keySet();
		}
	}

	public long getUserNum() {
		return userConnectionsMap.size();
	}

	public void destroy() {
		userConnectionsMap = null;
		cometUserConnectionsStore = null;
	}
}
