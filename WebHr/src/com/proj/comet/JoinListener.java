package com.proj.comet;

import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.RequestFacade;
import org.apache.log4j.Logger;
import org.comet4j.core.event.ConnectEvent;
import org.comet4j.core.listener.ConnectListener;

import com.proj.entity.UserInfo;
import com.proj.session.SessionHelper;

public class JoinListener extends ConnectListener {
	private static final Logger Log = Logger.getLogger(JoinListener.class);

	@Override
	public boolean handleEvent(ConnectEvent anEvent) {
		RequestFacade request = anEvent.getConn().getRequest();
		if (request != null) {
			HttpSession session = request.getSession();
			if (session != null) {
				UserInfo user = (UserInfo) session
						.getAttribute(SessionHelper.SESSION_USER);
				if (user == null) {
					return false;
				}
//				Log.info(user.getUserId() + "有新的链接" + anEvent.getConn().getId()
//						+ "进入");
				CometUserConnectionsStore.getInstance().pushUserConnectionId(
						user.getUserId(), anEvent.getConn().getId());
				InboxNumConnectionConnectEvent event = new InboxNumConnectionConnectEvent(
						user.getUserId(), anEvent.getConn().getId());
				CometExecutor.execute(event);
			}

		}
		return false;

	}
}
