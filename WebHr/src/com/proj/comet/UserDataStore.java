package com.proj.comet;

import java.util.concurrent.ConcurrentHashMap;

public class UserDataStore implements IUserDataStore<String, Integer> {

	private ConcurrentHashMap<String, Integer> userDataStoreMap;
	
	private static UserDataStore userDataStore = new UserDataStore();
	
	private UserDataStore() {
		userDataStoreMap = new ConcurrentHashMap<String, Integer>();
	}
	
	@Override
	public Integer getUserData(String user) {
		return userDataStoreMap.get(user);
	}

	@Override
	public void putUserData(String user, Integer data) {
		userDataStoreMap.put(user, data);
	}

	@Override
	public void removeUserData(String user) {
		userDataStoreMap.remove(user);
	}

	@Override
	public void updateUserData(String user, Integer newData) {
		putUserData(user, newData);
		
	}

	@Override
	public boolean containsUser(String user) {
		return userDataStoreMap.contains(user);
	}

	public static UserDataStore getInstance() {
		return userDataStore;
	}

	@Override
	public long getUserNum() {
		return userDataStoreMap.size();
	}

	public void destroy(){
		userDataStoreMap = null;
	}
	

}
