package com.proj.comet;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CometExecutor {

	private static final int CORE_SIZE = 5; 
	private static final int MAX_SIZE = 20; 
	private static final int KEEP_ALIVE = 10; 
	
	private static final int CAPACITY = 100000; 
	
	private static final ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(CORE_SIZE, MAX_SIZE, KEEP_ALIVE, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>(CAPACITY),new ThreadPoolExecutor.CallerRunsPolicy());
	
	
	public static void execute(Runnable r) {
		EXECUTOR.execute(r);
	}
	
	public static void destroy(){
		EXECUTOR.shutdown();
	}
}
