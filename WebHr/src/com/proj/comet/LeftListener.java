package com.proj.comet;

import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.RequestFacade;
import org.apache.log4j.Logger;
import org.comet4j.core.ExpiresCacheUseConcurrent;
import org.comet4j.core.event.DropEvent;
import org.comet4j.core.listener.DropListener;

import com.proj.entity.UserInfo;
import com.proj.session.SessionHelper;

public class LeftListener extends DropListener {
	private static final Logger Log = Logger.getLogger(LeftListener.class);

	@Override
	public boolean handleEvent(DropEvent anEvent) {
		RequestFacade request = anEvent.getConn().getRequest();
		if (request != null) {
			HttpSession session = request.getSession();
			if (session != null) {
				UserInfo user = (UserInfo) session
						.getAttribute(SessionHelper.SESSION_USER);
				if (user == null) {
					return true;
				}

				CometUserConnectionsStore.getInstance().removeUserConnection(
						user.getUserId(), anEvent.getConn().getId());
//				Log.info(user.getUserId() + "������" + anEvent.getConn().getId()
//						+ "�Ͽ���");
			} else {
				Log.warn(anEvent.getConn().getId()
						+ "left request session is null!!!");
			}
		}

		return true;
	}

}
