package com.proj.comet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.comet4j.core.CometContext;

public class CometWebHrAPPListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		CometUserManager.getInstance().destroy();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		CometUserManager manager = CometUserManager.getInstance();
		manager.init();
		CometContext.getInstance().getEngine()
				.addConnectListener(new JoinListener());
		CometContext.getInstance().getEngine()
				.addDropListener(new LeftListener());
	}

}
