package com.proj.comet;

import com.proj.resultData.BaseResult;
import com.proj.service.InboxService;
import com.proj.utils.SpringContextUtil;

/**
 * @author Wangqing 向单个链接发送未读私信数目
 */
public class InboxNumConnectionConnectEvent extends SendEvent {

	String userId;
	String cid;

	private  IUserDataStore<String,Integer> userDataStore = UserDataStore.getInstance();

	private InboxService inboxService;
	
	public InboxNumConnectionConnectEvent(String userId, String cid) {
		this.userId = userId;
		this.cid = cid;
	}

	@Override
	protected void send() {
		try {
			Integer message = userDataStore.getUserData(userId);
			if(message == null){
				inboxService = (InboxService) SpringContextUtil.getBean("inboxService");
				if(inboxService != null){
					message = inboxService.getUserUnreadInboxNum(userId);
					userDataStore.putUserData(userId, message);
				}
			}
			BaseResult<Integer> result = new BaseResult<Integer>();
			result.setRsm(message);
			CometUserManager.sendToConnection(cid, result);
		} catch (Exception e) {
		}
		
		
	}

}
