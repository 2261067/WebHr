package com.proj.comet;

public interface IUserDataStore<U,D> {
	D getUserData(U user);
	void putUserData(U user, D data);
	void removeUserData(U user);
	void updateUserData(U user, D newData);
	boolean containsUser(U user);
	long getUserNum();
}
