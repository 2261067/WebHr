package com.proj.comet;

import java.util.List;

import com.proj.resultData.BaseResult;
import com.proj.service.InboxService;
import com.proj.utils.SpringContextUtil;

public class InboxNumCheckAndSendEvent extends SendEvent {

	public InboxNumCheckAndSendEvent(String userId) {
		super(userId);
	}

	public InboxNumCheckAndSendEvent(List<String> userIds) {
		super(userIds);
	}

	private static IUserDataStore<String, Integer> userDataStore = UserDataStore
			.getInstance();
	private InboxService inboxService = (InboxService) SpringContextUtil
			.getBean("inboxService");


	@Override
	protected void send() {
		try {
			Integer message = null;
			for (String userId : uids) {
				if (inboxService != null) {
					message = inboxService.getUserUnreadInboxNum(userId);
					userDataStore.putUserData(userId, message);
				}
				if (message != null) {
					BaseResult<Integer> result = new BaseResult<Integer>();
					result.setRsm(message);
					CometUserManager.sendToUser(userId, result);
				}
			}

		} catch (Exception e) {
		}
	}

}
