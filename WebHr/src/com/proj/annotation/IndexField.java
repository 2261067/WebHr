package com.proj.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented

public @interface IndexField {
	
	String fieldName() default "content";
	String fieldValue() default "";
	int type() default 0;
	boolean indexed() default true;
	boolean stored() default false;
	boolean storeTermVectors() default true;
	
	boolean tokenized() default true;
	boolean storeTermVectorPositions() default true;
	boolean storeTermVectorOffsets() default true;
	
	
}
