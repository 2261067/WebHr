/**
 * 
 */
package com.proj.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.security.RunAs;

import com.proj.aspect.comet.ReturnType;

/**
 * @author Wangqing
 * 需要产生推送的动作
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CometInbox {
	ReturnType returnType();
}
