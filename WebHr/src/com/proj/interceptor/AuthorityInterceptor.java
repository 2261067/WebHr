package com.proj.interceptor;

import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.proj.authority.Authority;
import com.proj.authority.AuthorityUtils;
import com.proj.entity.Role;
import com.proj.entity.UserInfo;
import com.proj.service.UserManagerService;
import com.proj.session.CookieHelper;
import com.proj.session.SessionHelper;
import com.proj.utils.EncrypAES;
import com.proj.utils.StringUtils;

/**
 * @author Wangqing 拦截器，检查是否有权限注解，没有放行，有做权限检查
 */
public class AuthorityInterceptor extends HandlerInterceptorAdapter {

	private static final Logger LOG = Logger
			.getLogger(AuthorityInterceptor.class);

	@Autowired
	private UserManagerService userService;

	/*
	 * (non-Javadoc) 每个controller之前执行，先判断session是否有当前用户，没有判断是否免登陆，再判断页面权限
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {

		// 自动登录

		UserInfo user = (UserInfo) request.getSession().getAttribute(
				SessionHelper.SESSION_USER);
		if (user == null) {
			String userID = null;
			String userPW = null;
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {

				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(CookieHelper.USER_ID)) {
						userID = URLDecoder.decode(cookie.getValue(), "utf-8");
					}
					if (cookie.getName().equals(CookieHelper.USER_PW)) {
						userPW = cookie.getValue();
					}
				}

				if (StringUtils.isNotBlank(userID)
						&& StringUtils.isNotBlank(userPW)) {
					user = userService.userLogin(userID, EncrypAES
							.getInstance().DecryptorToString(userPW));
					if (user != null) {
						request.getSession().setAttribute(
								SessionHelper.SESSION_USER, user);
					}
				}
			}
		}
		// 权限

		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Authority authority = handlerMethod
				.getMethodAnnotation(Authority.class);
		if (authority == null) {
			return true;
		}

		if (user == null)
			return false;

		Role role = userService.userPermission(user.getUserId());
		// Role role = userService.userPermission("ztw");

		if (role == null)
			return false;

		if (AuthorityUtils.hasAuthority(authority.permissions(), role)) {
			return true;
		}

		return false;
	}
}