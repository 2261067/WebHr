package com.proj.aspect.search;

import java.lang.reflect.*;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.*;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.proj.annotation.DeleteIndex;
import com.proj.annotation.IndexField;
import com.proj.module.searchEngine.*;
import com.proj.utils.HtmlUtil;
import com.proj.utils.StringUtils;

@Component("searchIndexAspect")
public class SearchIndexAspect {
	LuceneDocumentTool documentTool = new LuceneDocumentTool();
	DefaultLuceneIndex luceneIndex = LuceneContext.getContextInstance().getDefaultLuceneIndex();
    //
	/**
	 * @param jp切入点，方法信息
	 * @param documentObj 切入方法的返回值
	 */
	public void addDocumentIndex(JoinPoint jp, Object documentObj) {
		MethodSignature ms = (MethodSignature) jp.getSignature();
		IndexField serviceMethodAnno = ms.getMethod().getAnnotation(
				IndexField.class);
		if (serviceMethodAnno == null)
			return;
		try {
			if (documentObj == null)
				return;
			List<Field> indexFields = new LinkedList<Field>();
			// HashMap<String,String> indexFields=new HashMap<String,String>();
			Method[] methods = documentObj.getClass().getMethods();
			int indexFieldCount=0;
			for (Method m : methods) {
				IndexField fieldAnnotation = m.getAnnotation(IndexField.class);

				if (fieldAnnotation == null)
					continue;
				Object field = m.invoke(documentObj);
				String fieldName = fieldAnnotation.fieldName();

				if (StringUtils.isNotBlank(field.toString())
						&& StringUtils.isNotBlank(fieldName)) {
					String fieldStr = fieldValuePreHandle(fieldAnnotation,
							field);

					Field f = documentTool.getField(
							fieldAnnotation.fieldName(), fieldStr,
							fieldAnnotation);
					indexFields.add(f);
					indexFieldCount++;
				} else
					continue;
			}
			if(indexFieldCount==0)return;
			IndexField classAnnotation = documentObj.getClass().getAnnotation(
					IndexField.class);
			if(classAnnotation==null)throw new Exception("索引失败，未发现该对象类别上有index标签");
			Field f = documentTool.getField(classAnnotation.fieldName(),
					classAnnotation.fieldValue(), classAnnotation);
			indexFields.add(f);

			Document doc = documentTool.getDocument(indexFields);
			luceneIndex.addDocumentIndex(doc);
		} catch (Exception e) {
			addDocumentIndexFailedHandle(documentObj);
		}

	}

	/**预处理对应域的字符串，比如去掉html标签等
	 * @param fieldAnnotation
	 * @param field
	 * @return
	 */
	public String fieldValuePreHandle(IndexField fieldAnnotation, Object field) {
		if (fieldAnnotation.fieldName().equals("content")) {
			return HtmlUtil.delHTMLTag(field.toString());
		} else {
			return field.toString();
		}
	}

	public void addDocumentIndexFailedHandle(Object doc) {

	}

	
	/**删除文档索引
	 * @param jp切入点，方法信息
	 * @param documentObj 切入方法的返回值
	 */
	public void deleteDocumentIndex(JoinPoint jp, Object documentObj) {
		//记录删除条件，比如根据id字段删除
		String dataId;
		HashMap<String, String> deletConditions=new HashMap<String, String>();
		MethodSignature ms = (MethodSignature) jp.getSignature();
		//获取方法参数
		Method m=ms.getMethod();
		DeleteIndex serviceMethodAnno = ms.getMethod().getAnnotation(DeleteIndex.class);
		if (serviceMethodAnno == null)
			return;
		Object[] methodArgs=jp.getArgs();
		try {
//			if (documentObj == null)
//				return;
//			List<Field> indexFields = new LinkedList<Field>();
//			// HashMap<String,String> indexFields=new HashMap<String,String>();
//			Method[] methods = documentObj.getClass().getMethods();
//			int indexFieldCount=0;
//			for (Method m : methods) {
//				IndexField fieldAnnotation = m.getAnnotation(IndexField.class);
//
//				if (fieldAnnotation == null||!fieldAnnotation.fieldName().equals("id"))
//					continue;
//				Object fieldValue = m.invoke(documentObj);
//				String fieldName = fieldAnnotation.fieldName();
//				deletConditions.put(fieldName, fieldValue.toString());
//		
//			}
//			
//			IndexField classAnnotation = documentObj.getClass().getAnnotation(
//					IndexField.class);
//			if(classAnnotation==null)throw new Exception("索引失败，未发现该对象类别上有index标签");
//			deletConditions.put(classAnnotation.fieldName(),
//					classAnnotation.fieldValue());
			if(methodArgs.length>0)
				{dataId=methodArgs[0].toString();
				deletConditions.put("id",dataId);
			
				luceneIndex.deleteDocumentIndex(deletConditions);
				}
				
		}
		catch (Exception e) {
			;
		}
	
	}
	
	
}
