package com.proj.aspect.search;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.comet4j.core.CometContext;

import com.proj.comet.CometUserManager;
import com.proj.comet.JoinListener;
import com.proj.comet.LeftListener;
import com.proj.module.searchEngine.DefaultLuceneIndex;
import com.proj.module.searchEngine.LuceneContext;
import com.proj.module.searchEngine.SearchStrategy;



public class SearchIndexAppListener implements ServletContextListener {
	DefaultLuceneIndex luceneIndex = LuceneContext.getContextInstance().getDefaultLuceneIndex();
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			LuceneContext.getContextInstance().distroy();
			SearchStrategy.getSearchStrategyInstanc().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
	
	}

}