package com.proj.aspect.exception;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("exceptionAspect")
public class ExceptionAspect {
	private static final Logger LOGGER = Logger.getLogger(ExceptionAspect.class);
	public void logException(Throwable e) throws Throwable{
		LOGGER.error(e);
//		throw e;
	}
}
