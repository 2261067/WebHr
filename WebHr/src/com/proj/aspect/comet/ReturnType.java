package com.proj.aspect.comet;

public enum ReturnType {

	ANSWER("answer", "saveAnswerProcessInboxEvent"),
	INBOX("inbox", "saveInboxProcessInboxEvent"),
	SAVE_ACTIVITY("save_activity","saveActivityProcessInboxEvent"),
	UPDATE_ACTIVITY("update_activity","updateActivityProcessInboxEvent"),
	QUESTION_INVITE("question_invite","saveQuestionInviteProcessInboxEvent");
	private String typeName;

	private String processClassName;

	// private Class<?> classType;

	ReturnType(String typeName, String processClassName) {
		this.typeName = typeName;
		this.setProcessClassName(processClassName);
		// this.classType = classType;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getProcessClassName() {
		return processClassName;
	}

	public void setProcessClassName(String processClassName) {
		this.processClassName = processClassName;
	}

	// public Class<?> getClassType() {
	// return classType;
	// }
	//
	// public void setClassType(Class<?> classType) {
	// this.classType = classType;
	// }
}
