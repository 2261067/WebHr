package com.proj.aspect.comet;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.eclipse.jdt.internal.compiler.apt.dispatch.IProcessorProvider;
import org.springframework.stereotype.Component;

import com.proj.annotation.CometInbox;
import com.proj.aspect.comet.process.IProcessInboxEvent;
import com.proj.entity.Answer;
import com.proj.utils.SpringContextUtil;

@Component("cometAspect")
public class CometAspect {
	
	public static final Logger Log = Logger.getLogger(CometAspect.class);
	
	public void sendInboxMessageAndComet(JoinPoint joinPoint,Object processObject){
		if(processObject == null)
			return;
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		CometInbox cometRefresh = methodSignature.getMethod().getAnnotation(CometInbox.class);
		if(cometRefresh == null)
			return;
		ReturnType returnType = cometRefresh.returnType();
		try {
			IProcessInboxEvent process = (IProcessInboxEvent)SpringContextUtil.getBean(returnType.getProcessClassName());
			process.process(processObject);
		} catch (Exception e) {
			Log.error(e.getMessage());
		}
		
	}
	
}
