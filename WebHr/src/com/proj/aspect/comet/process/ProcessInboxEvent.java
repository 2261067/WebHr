package com.proj.aspect.comet.process;

import org.springframework.beans.factory.annotation.Autowired;

import com.proj.service.InboxService;

public abstract class ProcessInboxEvent implements IProcessInboxEvent {

	@Autowired
	protected InboxService inboxService;
	
	@Override
	public void process(Object processObject) {

	}

}
