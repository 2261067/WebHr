package com.proj.aspect.comet.process;

public interface IProcessInboxEvent {
	void process(Object processObject);
}
