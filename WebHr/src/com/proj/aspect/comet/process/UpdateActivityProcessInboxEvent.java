package com.proj.aspect.comet.process;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.entity.Activity;
import com.proj.entity.ActivitySign;
import com.proj.entity.Inbox;
import com.proj.entity.UserInfo;
import com.proj.service.ActivityService;
import com.proj.service.UserManagerService;
import com.proj.utils.ConstantUtil;

@Component("updateActivityProcessInboxEvent")
public class UpdateActivityProcessInboxEvent extends ProcessInboxEvent {
	@Autowired
	private ActivityService activityService;
	@Autowired
	private UserManagerService ums;
	private Activity activity;
	
	@Override
	public void process(Object processObject){
		if(processObject instanceof Activity){
			activity = (Activity)processObject;
		}
		
		if(activity != null){
			List<ActivitySign> activitySigns = activityService.getAllActivitySigns(activity.getActivityId());
			if(activitySigns != null){
//				给报名用户发通知
				for (ActivitySign activitySign : activitySigns) {
					Inbox inbox = new Inbox();
					inbox.setFromId(ConstantUtil.SYSTEM_ID);
					inbox.setUserId(activitySign.getUserId());
					inbox.setIsRead((byte)0);
					inbox.setType("activity");
					
					UserInfo user = ums.getUserById(activitySign.getUserId());
					String nick = activitySign.getUserId();
					if(user != null){
						nick = user.getNickName();
					}
					
					Object[] params = {nick,activity.getActivityName()};
					inbox.setContent(String.format(ConstantUtil.EDIT_ACTIVITY_MESSAGE, params));
					inbox.setUrl(String.format(ConstantUtil.ACTIVITY_URL, activity.getActivityId()));
					inbox.setCreateTime(new Date());
					inboxService.save(inbox);
				}
			}
		}
	}
}
