package com.proj.aspect.comet.process;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.entity.Answer;
import com.proj.entity.AttentionQuestion;
import com.proj.entity.Inbox;
import com.proj.entity.Question;
import com.proj.entity.UserInfo;
import com.proj.service.UserManagerService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.utils.ConstantUtil;

@Component("saveAnswerProcessInboxEvent")
public class SaveAnswerProcessInboxEvent extends ProcessInboxEvent {

	@Autowired
	private QuestionAndAnswerService qas;
	@Autowired
	private UserManagerService ums;
	private Answer answer;

	@Override
	public void process(Object processObject) {
		if (processObject instanceof Answer) {
			answer = (Answer) processObject;
		}

		if (answer != null) {
			List<AttentionQuestion> attentionUsers = qas
					.getAttentionQuestionUsers(answer.getQuestion());
			if (attentionUsers != null) {
				Question question = qas.getQuestionById(answer.getQuestion());
				for (AttentionQuestion attentionQuestion : attentionUsers) {
//					自己回答问题不发私信
					if (!attentionQuestion.getUserId()
							.equals(answer.getUserId())) {
						// 生成私信
						Inbox inbox = new Inbox();
						inbox.setUserId(attentionQuestion.getUserId());
						inbox.setFromId(ConstantUtil.SYSTEM_ID);
						UserInfo user = ums.getUserById(attentionQuestion.getUserId());
						String nick = attentionQuestion.getUserId();
						if(user != null){
							nick = user.getNickName();
						}
						Object[] args = { nick,
								question.getQuestionName() };
						inbox.setContent(String.format(
								ConstantUtil.NEW_ANSWER_MESSAGE, args));
						inbox.setCreateTime(new Date());
						inbox.setIsRead((byte) 0);
						inbox.setType("question");
						inbox.setUrl(String.format(ConstantUtil.QUESTION_URL,
								question.getQuestionId()));
						inboxService.save(inbox);
					}
				}

				// InboxNumCheckAndSendEvent event = new
				// InboxNumCheckAndSendEvent(userIds);
				// CometExecutor.execute(event);
			}
		}

	}

}
