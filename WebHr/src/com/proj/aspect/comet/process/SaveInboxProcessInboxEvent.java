package com.proj.aspect.comet.process;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proj.comet.CometExecutor;
import com.proj.comet.InboxNumCheckAndSendEvent;
import com.proj.entity.Inbox;

@Component("saveInboxProcessInboxEvent")
public class SaveInboxProcessInboxEvent implements IProcessInboxEvent {

	private Inbox inbox;

	@Override
	public void process(Object processObject) {
		if (processObject instanceof Inbox) {
			inbox = (Inbox) processObject;
		} else {
			if (processObject instanceof List<?>) {
				List<Inbox> inboxs = (List<Inbox>) processObject;
				if (inboxs != null && !inboxs.isEmpty()) {
					inbox = inboxs.get(0);
				}
			}
		}
		if (inbox != null) {
			InboxNumCheckAndSendEvent event = new InboxNumCheckAndSendEvent(
					inbox.getUserId());

			CometExecutor.execute(event);
		}

	}

}
