package com.proj.aspect.comet.process;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.entity.Inbox;
import com.proj.entity.Question;
import com.proj.entity.QuestionInvite;
import com.proj.entity.UserInfo;
import com.proj.service.UserManagerService;
import com.proj.service.questionAndAnswer.QuestionAndAnswerService;
import com.proj.utils.ConstantUtil;

@Component("saveQuestionInviteProcessInboxEvent")
public class SaveQuestionInviteProcessInboxEvent extends ProcessInboxEvent {
	@Autowired
	private QuestionAndAnswerService questionAndAnswerService;
	@Autowired
	private UserManagerService ums;
	private QuestionInvite questionInvite;

	@Override
	public void process(Object processObject) {
		if (processObject instanceof QuestionInvite) {
			questionInvite = (QuestionInvite) processObject;
		}
		if (questionInvite != null) {
			Question question = questionAndAnswerService.getQuestionById(questionInvite.getQuestionId());
			Inbox inbox = new Inbox();
			inbox.setUserId(questionInvite.getInviteUserId());
			inbox.setFromId(ConstantUtil.SYSTEM_ID);
			UserInfo user = ums.getUserById(questionInvite.getInviteUserId());
			String nick = questionInvite.getInviteUserId();
			if(user != null){
				nick = user.getNickName();
			}
			Object[] args = { nick,
					question.getQuestionName() };
			inbox.setContent(String.format(ConstantUtil.INVITE_ANSWER_QUESTION,
					args));
			inbox.setCreateTime(new Date());
			inbox.setIsRead((byte) 0);
			inbox.setType("question");
			inbox.setUrl(String.format(ConstantUtil.QUESTION_URL,
					question.getQuestionId()));
			inboxService.save(inbox);
		}
	}
}
