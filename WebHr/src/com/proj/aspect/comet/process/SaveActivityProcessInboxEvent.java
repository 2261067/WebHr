package com.proj.aspect.comet.process;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.entity.Activity;
import com.proj.entity.Inbox;
import com.proj.entity.UserInfo;
import com.proj.service.ActivityService;
import com.proj.service.UserManagerService;
import com.proj.utils.ConstantUtil;
import com.proj.utils.GUIDGenerator;

@Component("saveActivityProcessInboxEvent")
public class SaveActivityProcessInboxEvent extends ProcessInboxEvent{
	@Autowired
	private ActivityService activityService;
	@Autowired
	private UserManagerService ums;
	private Activity activity;
	
	@Override
	public void process(Object processObject){
		if(processObject instanceof Activity){
			activity = (Activity)processObject;
		}
		
		if(activity != null){
//			List<ActivitySign> activitySigns = activityService.getAllActivitySigns(activity.getActivityId());
			List<UserInfo> userInfos = ums.getAllUserInfos();
			if(userInfos != null){
//				给网站所由用户发通知，除了发起者
				for (UserInfo userInfo : userInfos) {
					if(userInfo.getUserId().equals(activity.getUserId()))
						continue;
					Inbox inbox = new Inbox();
					inbox.setFromId(ConstantUtil.SYSTEM_ID);
					inbox.setUserId(userInfo.getUserId());
					inbox.setIsRead((byte)0);
					inbox.setType("activity");
					UserInfo currentUser = ums.getUserById(userInfo.getUserId());
					UserInfo activityUser = ums.getUserById(activity.getUserId());
					String currentNick = userInfo.getUserId();
					String activityNick = activity.getUserId();
					if(currentUser != null && activityUser != null){
						currentNick = currentUser.getNickName();
						activityNick = activityUser.getNickName();
					}
					Object[] params = {currentNick,activityNick,activity.getActivityName()};
					inbox.setContent(String.format(ConstantUtil.NEW_ACTIVITY_MESSAGE, params));
					inbox.setUrl(String.format(ConstantUtil.ACTIVITY_URL, activity.getActivityId()));
					inbox.setCreateTime(new Date());
					inboxService.save(inbox);
				}
			}
		}
	}
}
