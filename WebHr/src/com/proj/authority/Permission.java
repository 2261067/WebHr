package com.proj.authority;

public enum Permission {

	QUESTION("question",1l),
	ANSWER("answer",2l),
	;
	
	private String name;
	
	private Long permission;
	
	Permission(String name,Long permission){
		this.setName(name);
		this.setPermission(permission);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPermission() {
		return permission;
	}

	public void setPermission(Long permission) {
		this.permission = permission;
	}
}
