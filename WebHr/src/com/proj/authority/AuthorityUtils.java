package com.proj.authority;


import com.proj.entity.Role;
import com.proj.utils.StringUtils;

public class AuthorityUtils {
	/**
	 * @param needPermissions需要的权限
	 * @param userRole
	 * @return
	 */
	public static boolean hasAuthority(Permission[] needPermissions, Role userRole){
		for (Permission permission : needPermissions) {
			if(hasAuthority(permission.getPermission(), userRole.getPermissionNum())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param permissinNum 权限数字
	 * @param roleString 用户所有权限，二进制数字
	 * @return 通过二者相与，判断是否有权限
	 */
	public static boolean hasAuthority(Long permissinNum, String roleString){
		if(StringUtils.isBlank(roleString))
			return false;
		if((permissinNum & Long.parseLong(roleString, 2)) == 0){
			return false;
		}
		
		return true;
	}
	
}
