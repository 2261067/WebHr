package com.proj.session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieHelper {
	public static final String USER_ID = "userid";
	public static final String USER_PW = "userpassword";
	public static final String ADMIN_ID = "adminid";
	public static final String ADMIN_PW = "adminpassword";
	public static final int TWO_WEEKS = 60 * 60 * 24 * 14;

	public static void addCookie(int maxAge, String path,
			HttpServletResponse response,Cookie... cookies ) {
		for (Cookie cookie : cookies) {
			cookie.setMaxAge(maxAge);
			cookie.setPath(path);
			response.addCookie(cookie);
		}
	}

	public static void removeCookies(String[] removeCookies,
			HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		for (String removeCookie : removeCookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(removeCookie)) {
					cookie.setMaxAge(0);
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			}
		}
	}
}
