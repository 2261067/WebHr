package com.proj.session;

import javax.servlet.http.HttpServletRequest;

public class SessionHelper {
	public static final String SESSION_USER = "currentUser";
	public static final String SESSION_ADMIN = "currentAdmin";

	public static <T> T getSessionAttribute(HttpServletRequest request,
			String name, Class<T> className) {
		T object = className.cast(request.getSession().getAttribute(name));
		return object;
	}
	
	public static void sessionLogOut(){
		
	}
}
