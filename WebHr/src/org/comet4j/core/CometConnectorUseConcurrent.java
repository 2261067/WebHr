package org.comet4j.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.comet4j.event.Observable;

@SuppressWarnings("rawtypes")
public class CometConnectorUseConcurrent extends Observable {
	private final long timespan;
	private final long frequency;
	boolean init = false;
	private Thread cleanner;

	// private List<CometConnection> connections =
	// Collections.synchronizedList(new ArrayList<CometConnection>());

	private ConcurrentHashMap<String, CometConnection> connectionMaps = new ConcurrentHashMap<String, CometConnection>();

	private static final Logger Log = Logger
			.getLogger(CometConnectorUseConcurrent.class);

	/**
	 * @param aTimespan
	 * @param aFrequency
	 */

	public CometConnectorUseConcurrent(long aTimespan, long aFrequency) {
		init = true;
		frequency = aFrequency;
		timespan = aTimespan;
		startCleanner();
	}

	private void startCleanner() {
		cleanner = new Thread(new CacheCleaner(), "Comet4J-ConnectorCleaner");
		cleanner.setDaemon(true);
		cleanner.start();
	}

	CometConnection getConnection(HttpServletRequest request) {

		for (CometConnection connection : connectionMaps.values()) {
			if (connection.getRequest() == request) {
				return connection;
			}
		}
		return null;
	}

	CometConnection getConnection(String id) {
		return connectionMaps.get(id);
	}

	void addConnection(CometConnection connection) {
		connectionMaps.put(connection.getId(), connection);
	}

	void removeConnection(CometConnection connection) {
		removeConnection(connection.getId());
	}

	void removeConnection(String id) {
		connectionMaps.remove(id);
	}

	boolean contains(String anId) {
		return connectionMaps.containsKey(anId);
	}

	boolean contains(CometConnection conn) {
		return contains(conn.getId());
	}

	List<CometConnection> getConnections() {
		Collection<CometConnection> connections = connectionMaps.values();
		if (connections == null) {
			return null;
		}
		return new ArrayList<CometConnection>(connections);
	}

	// ----------定时清理过期连接---------------
	class CacheCleaner implements Runnable {

		private final List<CometConnection> toDeleteList = new ArrayList<CometConnection>();

		public void run() {
			while (init) {
				try {
//					Log.info("【CometDebug】开始清理连接");
					Thread.sleep(frequency);
					checkExpires();
//					Log.info("【CometDebug】清理连接完毕");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		private void checkExpires() {
			CometEngine engine = CometContext.getInstance().getEngine();
			// CometContext.getInstance().log(
			// "【CometDebug】连接数量:" + connectionMaps.size());
			// System.out.println(connectionMaps.size());
			Log.info("当前共有" + connectionMaps.values().size() + "个链接");
			if (!connectionMaps.isEmpty()) {
				for (CometConnection connection : connectionMaps.values()) {
					if (connection == null)
						continue;
					// System.out.println("check " + connection.getId());
					long expireMillis = connection.getDyingTime() + timespan;
					if (CometProtocol.STATE_DYING.equals(connection.getState())
							&& expireMillis < System.currentTimeMillis()) {
						toDeleteList.add(connection);
					}
				}
			}

			if (!toDeleteList.isEmpty()) {
				Log.info("【CometDebug】检测到过期闲置连接" + toDeleteList.size() + "个");
				for (CometConnection c : toDeleteList) {
					try {// 外部事件处理可能会引发异常
						Log.info("【CometDebug】移除闲置连接:" + c.getId());
						engine.remove(c);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				toDeleteList.clear();
			}
		}

		// 过期检查
	}

	@Override
	public void destroy() {
		init = false;
		cleanner = null;
		connectionMaps.clear();
		connectionMaps = null;
	}
}
