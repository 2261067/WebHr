package org.comet4j.core;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class ExpiresCacheUseConcurrent {
	private final long timespan;
	private final long frequency;
	boolean init = false;
	private long size = 0l; // 临时保留缓存数量
	// private final Map<CometConnection, List<CometMessage>> cache =
	// Collections
	// .synchronizedMap(new WeakHashMap<CometConnection, List<CometMessage>>());
	private static final Logger Log = Logger
			.getLogger(ExpiresCacheUseConcurrent.class);
	private final ConcurrentHashMap<CometConnection, List<CometMessage>> caches = new ConcurrentHashMap<CometConnection, List<CometMessage>>();

	/**
	 * 构造
	 * 
	 * @param aTimespan
	 *            过期时长，毫秒
	 * @param aFrequency
	 *            过期检测频率，毫秒
	 */

	public ExpiresCacheUseConcurrent(long aTimespan, long aFrequency) {
		init = true;
		frequency = aFrequency;
		timespan = aTimespan;
		startCleanner();

	}

	private void startCleanner() {
		Thread cleanerThread = new Thread(new CacheCleaner(),
				"Comet4J-MessageCleaner");
		cleanerThread.setDaemon(true);
		cleanerThread.start();
	}

	public void push(CometConnection c, CometMessage e) {
		if (c == null)
			return;
		List<CometMessage> list = caches.get(c);
		if (list == null) {
			list = Collections.synchronizedList(new LinkedList<CometMessage>());
			caches.putIfAbsent(c, list);
			push(c, e);
			return;
		}
		list.add(e);
		caches.put(c, list);
	}

	public void push(CometConnection c, List<CometMessage> e) {
		if (c == null)
			return;
		List<CometMessage> list = caches.get(c);
		if (list == null) {
			list = Collections.synchronizedList(new LinkedList<CometMessage>());
			caches.putIfAbsent(c, list);
			push(c, e);
			return;
		}
		list.addAll(e);
		caches.put(c, list);
	}

	/**
	 * 获取连接的失败消息列表，取出后将此连接及信息从缓存清除
	 * 
	 * @param c
	 *            连接
	 * @return 如果没有连接对应的消息返回null，若取到此边接的消息列表
	 */
	public List<CometMessage> get(CometConnection c) {
		List<CometMessage> list = caches.remove(c);
		// if (list != null) {
		// caches.remove(c);
		// }
		// 这里需要使用浅复制，否则外层迭代可能导致fail-fast错误
		if (list != null) {
			List<CometMessage> cloneList = new LinkedList<CometMessage>(list);
			return cloneList;
		}

		return list;
	}

	public void remove(CometConnection c) {
		caches.remove(c);
	}

	/**
	 * 获取缓存的第一个消息，适用于长轮询模式
	 * 
	 * @param c
	 * @return
	 */
	public CometMessage getFirstMessage(CometConnection c) {
		List<CometMessage> list = caches.get(c);
		CometMessage firstMessage = null;
		if (list != null && !list.isEmpty()) {
			synchronized (list) {
				if (!list.isEmpty()) {
					firstMessage = list.get(0);
					list.remove(0);
				}
			}
		}
		return firstMessage;
	}

	// ----------定时清理过期信息---------------
	class CacheCleaner implements Runnable {
		public void run() {
			while (init) {
				try {
//					Log.info("【CometDebug】开始清理消息缓存");
					Thread.sleep(frequency);
					checkExpires();
//					Log.info("【CometDebug】开始清理消息缓存完毕");
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}

		private void checkExpires() {
			size = 0;
			for (CometConnection o : caches.keySet()) {
				List<CometMessage> list = caches.get(o);
				if (!list.isEmpty()) {
					synchronized (list) {
						long now = System.currentTimeMillis();
						for (Iterator<CometMessage> iterator = list.iterator(); iterator
								.hasNext();) {
							CometMessage cometMessage = iterator.next();
							long expireMills = cometMessage.getTime()
									+ timespan;
							if (expireMills < now) {
								iterator.remove();
							}
						}
						if (list.isEmpty()) {
							caches.remove(o);
						}
					}
				}
			}
		}

	}

	public void destroy() {
		init = false;
		for (Object o : caches.keySet()) {
			caches.get(o).clear();
		}
		caches.clear();
	}
}
