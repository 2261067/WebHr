<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+活动</title>


<jsp:include page="header.jsp" />
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-activity.js"></script>
<script type="text/javascript">
	init_pagination();
</script>
</head>

<body class="grey-bg">

	<jsp:include page="navigation.jsp" />


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">

			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question">
						<!-- tab切换 -->




						<!--<li><a href="http://ask.iheima.com/?/explore/is_recommend-1">推荐</a></li>-->



						<!-- <h2 class="hidden-xs">发现</h2> -->

						<!-- end tab切换 -->



						<div class="aw-mod aw-dynamic-index aw-question-box-mod">

							<div class="aw-mod-head">
								<h2>
									<a href="javascript:;"
										onclick="$.dialog('publishActivity', '');"
										class="pull-right btn btn-mini btn-success">发起新的活动</a> 最新活动
								</h2>
							</div>
							<div class="aw-mod-body">
								<div id="activity_list" class="aw-question-list"></div>
							</div>
						</div>

						<div class="page-control clearfix" id="page-control">
							<ul class="pagination pull-right">

							</ul>
						</div>
					</div>

					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp" />


	
	


</body>
</html>
