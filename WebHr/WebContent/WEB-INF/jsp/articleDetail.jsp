<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title>专栏详情</title>

<jsp:include page="header.jsp" />
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="/WebHr/js/hr-article.js"></script>
<script type="text/javascript">
	//活动页展示
	var article_id = $.getUrlParam("article_id");
	getArticleDetails();
</script>


</head>
<body class="grey-bg">


	<jsp:include page="navigation.jsp" />


	<div class="aw-container-wrap">

		<div class="aw-container aw-wecenter">

			<div class="container">
				<div class="row">
					<div class="col-sm-12 aw-global-tips"></div>
				</div>
			</div>

			<div class="container">
				<div class="row aw-content-wrap">

					<div class="col-sm-12 col-md-9 aw-main-content">


						<div class="aw-mod aw-item aw-question-detail-title">

							<div class="aw-mod-head">
								<h1 id="article_name"></h1>

								<div class="aw-question-follow clearfix">
									<span id="article_edit"> </span>
								</div>

								<div class="aw-topic-editor" id="question_topic_editor"
									data-type="question"></div>
							</div>


							<div class="aw-mod-body">
								<div class="aw-question-detail-meta"></div>
								<div id="article_context"
									class="aw-question-detail-txt markitup-box"></div>
								<div class="aw-question-detail-meta"></div>
							</div>

						</div>

						<div class="aw-mod aw-question-comment-box">

							<div class="aw-mod-head">
								<ul class="nav nav-tabs aw-reset-nav-tabs">
									<h2 id="article_comment_num"></h2>
								</ul>
							</div>

							<!-- 专栏评论 -->
							<div class="aw-mod-body aw-dynamic-topic"
								id="comment_context_list"></div>
							<!-- end 专栏评论 -->

							<!-- 回复编辑器 -->
							<div class="aw-mod aw-mod-article-replay-box"
								id="article_comment_box"></div>
							<!-- end 回复编辑器 -->

						</div>

					</div>

					<!-- 侧边栏 -->
					<div class="col-md-3 aw-side-bar hidden-xs hidden-sm">


						<!-- 发起人 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>发起人</h3>
							</div>
							<div class="aw-mod-body">
								<dl>
									<dt id="side_bar_user" class="pull-left aw-border-radius-5">

									</dt>
									<dd id="side_bar_atttion_user" class="pull-left">
										<p></p>
									</dd>
								</dl>
							</div>
						</div>

						<!-- end 发起人 -->


						<!-- 专栏状态 -->
						<div class="aw-side-bar-mod aw-side-bar-mod-question-status">
							<div class="aw-mod-head">
								<h3>专栏状态</h3>
							</div>
							<div id="question_status" class="aw-mod-body">
								<ul>
									<li>文章发布时间: <span id="article_create_time"
										class="aw-text-color-blue"></span></li>
									<li>最近修改时间: <span id="article_last_modify_time"
										class="aw-text-color-blue"></span></li>

								</ul>
							</div>
						</div>
						<!-- end 专栏状态 -->

					</div>



				</div>
			</div>
		</div>
	</div>


	<jsp:include page="footer.jsp" />
</body>
</html>