<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<!-- top顶部 -->
	<div class="topbar-wrap white">
		<div class="topbar-inner clearfix">
			<div class="topbar-logo-wrap clearfix">
				<h1 class="topbar-logo none">
					<a href="#" class="navbar-brand">后台管理</a>
				</h1>
				<ul class="navbar-list clearfix">
					<li><a class="on" href="#">首页</a></li>
					<li>HR+后台管理</li>
				</ul>
			</div>
			<div class="top-info-wrap">
				<ul class="top-info-list clearfix">
					<li><a href="../WebHr/html/admin_login.html">登陆</a></li>
					<li><a href="../WebHr/adminlogout.do">退出</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- ENDtop顶部 -->
