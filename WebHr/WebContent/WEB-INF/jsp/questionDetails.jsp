<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title></title>


<script type="text/javascript">
	var questionId = ${question.questionId};
</script>
<jsp:include page="header.jsp" />
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="/WebHr/js/question_details.js"></script>

<script type="text/javascript">
getQuestion();

</script>


</head>
<body class="grey-bg">
	<jsp:include page="navigation.jsp" />
	<style type="text/css">
#rich_text_editor1 {
	min-height: 300px;
	max-height: 500px;
	overflow-y: scroll;
	border: 1px solid #eee;
}
</style>
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 aw-global-tips"></div>
				</div>
			</div>
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<div class="aw-mod aw-item aw-question-detail-title">
							<div class="aw-mod-head">
								<h1 id= "question_name"></h1>

								<div id = "question_follow" class="aw-question-follow clearfix"></div>

								<div class="aw-topic-editor" id="question_topic_editor"
									data-type="question"></div>

							</div>
							<div class="aw-mod-body">
								<div id = "question_details"class="aw-question-detail-txt markitup-box"></div>
								<div class="aw-question-detail-meta">
									<span class="aw-text-color-999"></span> <a
										style="color: #5CB85C" href="#answer_form1">回答问题</a> <a
										class="aw-text-color-999 aw-invite-replay"><i
										class="fa fa-retweet"></i>邀请回答 </a>
								</div>
							</div>
							<!-- 站内邀请 -->

							<!-- end 站内邀请 -->
							<!-- 相关链接 -->

							<!-- end 相关链接 -->
						</div>

						<div class="aw-mod aw-question-comment-box">
							<div class="aw-mod-head">
								<ul class="nav nav-tabs aw-reset-nav-tabs">
									<li id='vote' class="active" onclick="answer_sort('vote')"><a
										href="">票数</a></li>
									<li><a id='time' href="" onclick="answer_sort('time')">时间<i
											class="fa fa-caret-up"></i></a></li>

									<h2 id="answer_num" class="hidden-xs"></h2>
								</ul>
							</div>
							<div class="aw-mod-body aw-dynamic-topic" id="answer_list">


							</div>
							<div class="aw-mod-footer">
								<div class="aw-load-more-content hide"
									id="load_uninterested_answers">
									<span class="aw-text-color-999 aw-alert-box aw-text-color-999"
										href="javascript:;" tabindex="-1"
										onclick="$.alert('被折叠的回复是被你或者被大多数用户认为没有帮助的回复');">为什么被折叠?</span>
									<a href="javascript:;"><span class="hide_answers_count">0</span>
										个回复被折叠</a>
								</div>

								<div class="hide aw-dynamic-topic"
									id="uninterested_answers_list"></div>
							</div>

						</div>

						<div class="aw-mod aw-mod-replay-box">
							<a name="answer_form1" id="answer_form1"> &nbsp;</a>
							<form action="commitAnswer.do" onsubmit="return false;"
								method="post" id="answer_form">
								<input type="hidden" name="question_id" id="answer_question_id"
									value=""> 
								<div class="aw-mod-head">
									<p>
										<span class="pull-right"> <label> <input
												type="checkbox" checked="checked" value="1"
												name="auto_focus"> 关注问题
										</label> &nbsp; <label> <input type="checkbox" value="1"
												name="anonymous"> 匿名回复
										</label>
										</span>
									</p>
								</div>

								<div class="aw-mod-body">
									<input type="hidden" name="answer_content"
										id="advanced_editor1">

									<div class="aw-mod-head" style="padding-left: 0px;">
										<script id="editor" name="content" type="text/plain"></script>

										<!-- 实例化编辑器 -->
										<script type="text/javascript">
											UE.delEditor('editor');
											UE
													.getEditor(
															'editor',
															{
																toolbars : UEDITOR_CONFIG.toolbars_answer_base,
																filterRules : function() {
																	return {
																	//a:{$:{}}
																	}
																}()
															});
											function fwb_save() {
												$('#advanced_editor1').val(
														UE.getEditor('editor')
																.getContent());
											}

											ATTACH_ACCESS_KEY = '78f660a3033298287c7c42d7ac3d2f16';
											ITEM_IDS = '';
											COMMENT_UNFOLD = '';

											QUESTION_ID = 5705;
											UNINTERESTED_COUNT = 5;
										</script>


									</div>
									<div class="aw-replay-box-control clearfix">
										<a href="javascript:;"
											onclick="javascript:fwb_save();ajax_post($('#answer_form'));"
											class="btn btn-large btn-success pull-right btn-publish-submit">回复</a>


									</div>
								</div>
							</form>
						</div>



					</div>
					<!-- 侧边栏 -->
					<div class="col-md-3 aw-side-bar hidden-xs hidden-sm">
						<!-- 发起人 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>发起人</h3>
							</div>
							<div class="aw-mod-body">
								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a href="http://ask.iheima.com/?/people/wertyliii"><img
											alt="wertyliii"
											src="http://ask.iheima.com/static/common/avatar-mid-img.png" /></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name"
											href="http://ask.iheima.com/?/people/wertyliii"
											data-id="11540">wertyliii</a> <i data-placement="bottom"
											title="" onclick="follow_people($(this), 11540);"
											data-toggle="tooltip" class="fa fa-check aw-active"
											data-original-title="关注"></i>
										<p></p>
									</dd>
								</dl>
							</div>
						</div>

						<!-- end 发起人 -->




						<!-- 问题状态 -->
						<div class="aw-side-bar-mod aw-side-bar-mod-question-status">
							<div class="aw-mod-head">
								<h3>问题状态</h3>
							</div>
							<div class="aw-mod-body">
								<ul>
									<li>最新活动: <span class="aw-text-color-blue">2 小时前</span></li>
									<li>浏览: <span class="aw-text-color-blue">67</span></li>
									<li>关注: <span class="aw-text-color-blue">3</span> 人
									</li>

									<li class="aw-side-bar-user-list aw-border-radius-5"
										id="focus_users"></li>
								</ul>
							</div>
						</div>
						<!-- end 问题状态 -->

					</div>
					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>
	<div class="aw-mod aw-free-editor hide">
		<form action="commitAnswer.do" onsubmit="return false;" method="post">
			<input type="hidden" name="post_hash" value="e6eedb7826954d8f" /> <input
				type="hidden" name="question_id" value="5548" /> <input
				type="hidden" name="attach_access_key"
				value="f4678f76f90df6a46563263baba78fab" />
			<div class="aw-mod-head">
				<div class="aw-wecenter">
					<a href="#" class="btn btn-large btn-default pull-left"
						onclick="toggleFullScreen('hide');return false;">退出全屏模式</a> 全屏模式 <a
						href="#" class="btn btn-large btn-default pull-right"
						onclick="ajax_post($(this).parents('#answer_form'));return false;">回复</a>
				</div>
			</div>
			<div class="aw-mod-body">
				<div class="aw-mod-side-left pull-left">
					<!--<textarea name="answer_content" id="advanced_editor" rows="15" class="autosize advanced_editor"></textarea>-->

					<textarea name="answer_content" id="advanced_editor" rows="15"
						class="autosize advanced_editor"></textarea>
				</div>
				<div class="aw-mod-side-right pull-left">
					<div id="markItUpPreviewFrame" class="markItUpPreviewFrame">
						<div id="markItUpPreviewFrames" class="markItUpPreviewFrames"></div>
					</div>
				</div>
			</div>
		</form>
	</div>



	<script type="text/javascript"
		src="http://ask.iheima.com/static/js/app/question_detail.js"></script>

	<div class="aw-footer-wrap">
		<div class="aw-footer aw-wecenter">
			<br /> <br /> Copyright © 2014 - 创业问答_黑问, All Rights Reserved<br />
			</span> <span class="hidden-xs">Powered By <a
				href="http://www.iheima.com" target="blank">黑马网 <!--?php echo G_VERSION; ?--></a></span>

			<span class="hidden-xs">当前时区: GMT +8 &nbsp; <a
				href="http://ask.iheima.com/?/feed/" target="blank"><i
					class="fa fa-rss-square"></i> RSS Feed</a></span> <br />

		</div>
	</div>



	<a class="aw-back-top hidden-xs" href="javascript:;"
		onclick="$.scrollTo(1, 600, {queue:true});"><i
		class="fa fa-arrow-up"></i></a>


	<!-- DO NOT REMOVE -->
	<div id="aw-ajax-box" class="aw-ajax-box"></div>


	<!-- Escape time: 0.091999053955078 -->
	<script type="text/javascript" id="bdshare_js" data="type=tools"></script>

	<!-- / DO NOT REMOVE -->

</body>
</html>