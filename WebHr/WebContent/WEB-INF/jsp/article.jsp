<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+专栏</title>


<jsp:include page="header.jsp" />
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-article.js"></script>
<script type="text/javascript">
	init_pagination();
</script>
</head>

<body class="grey-bg">

	<jsp:include page="navigation.jsp" />


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">

			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question">

						<div class="aw-mod aw-dynamic-index aw-question-box-mod">

							<div class="aw-mod-head">
								<h2>
									<a href="javascript:;"
										onclick="$.dialog('publishArticle', '');"
										class="pull-right btn btn-mini btn-success">写专栏</a> 最新专栏
								</h2>
							</div>
							<div class="aw-mod-body">
								<div id="article_list" class="aw-question-list"></div>
							</div>
						</div>

						<div class="page-control clearfix" id="page-control">
							<ul class="pagination pull-right">

							</ul>
						</div>
					</div>

					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp" />


</body>
</html>
