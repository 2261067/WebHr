<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>后台管理--问题</title>

<jsp:include page="adminHeader.jsp" />
<script type="text/javascript" src="/WebHr/js/admin_questionmanage.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	 getQuestionFirstPage();
});
</script>

</head>
<body>

	<!-- top顶部 -->
	<jsp:include page="adminTop.jsp" />
	<!-- ENDtop顶部 -->

	<div class="container clearfix">

		<!-- 导航栏 -->
		<jsp:include page="adminNavigation.jsp" />
		<!-- END导航栏 -->

		<!-- MAIN -->
		<div class="main-wrap">
			<div class="crumb-wrap">
				<div class="crumb-list">
					<a href="">首页</a><span class="crumb-step">&gt;</span><span
						class="crumb-name">问题管理</span>
				</div>
			</div>
			<div class="search-wrap">
				<div class="search-content">
				<!-- 
					<form action="#" method="post">
						<table class="search-tab">
							<tr>
								<th width="70">搜索问题:</th>
								<td><input class="common-text" placeholder="关键字"
									name="keywords" value="" id="" type="text"></td>
								<td><input class="btn btn-primary btn2" name="sub"
									value="搜索" type="submit"></td>
							</tr>
						</table>
					</form>
				-->
				</div>
			</div>
			<div class="result-wrap">
				<div class="result-content">
					<table class="result-tab" width="100%">

					</table>
					<div id="Pagination" class="pagination"></div>
				</div>
			</div>
		</div>
		<!-- ENDMAIN -->


	</div>



</body>
</html>