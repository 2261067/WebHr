<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />

<script type="text/javascript">
var G_ADMIN_ID = '${sessionScope.currentAdmin == null ? "null_admin":sessionScope.currentAdmin.adminId}';
var G_BASE_URL = 'http://www.027team.com/WebHr';
</script>

<script type="text/javascript" src="/WebHr/js/jquery.2.js" ></script>
<script type="text/javascript" src="/WebHr/js/hr-template.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-common.js"></script>
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>

<link href="/WebHr/css/pagination.css" rel="stylesheet">
<link href="/WebHr/css/admin-common.css" rel="stylesheet">
<link href="/WebHr/css/admin-main.css" rel="stylesheet">