<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+设置--申请认证</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="../header.jsp" />
<script src="/WebHr/js/setting-uploadimg.js"></script>
<link href="/WebHr/css/user-setting.css" rel="stylesheet">
</head>

<body class="grey-bg">

	<!-- 导航 -->
	<jsp:include page="../navigation.jsp" />
	<!-- 导航结束 -->


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter aw-user">
			<div class="aw-user-setting container">
				<div class="tabbable">
					<ul class="nav nav-tabs aw-reset-nav-tabs">
						<li class="active"><a
							href="../WebHr/redirect.do?page=setting/verify">申请认证</a></li>
						<li><a href="../WebHr/redirect.do?page=setting/password">修改密码</a></li>
						<li><a href="../WebHr/redirect.do?page=setting/bind">账号绑定</a></li>
						<li><a href="../WebHr/setting.do">基本资料</a></li>
						<h2>用户设置</h2>
					</ul>
				</div>

				<div class="tab-content clearfix">
					<div class="aw-mod aw-user-setting-bind">
						<div class="aw-mod-head">
							<h4>申请认证</h4>
						</div>
						<form id="verify_form" method="post" action="#"
							enctype="multipart/form-data" class="form-horizontal">
							<div class="aw-mod-body">


								<div class="form-group">
									<label class="col-lg-2 control-label">当前状态:</label>
									<div class="col-lg-10">未认证</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label verify-name"><span>真实姓名</span>:</label>
									<div class="col-lg-4">
										<input class="form-control" name="name" type="text" value="" />
									</div>
								</div>


								<div class="form-group">
									<label class="col-lg-2 control-label">联系方式:</label>
									<div class="col-lg-8">
										<input class="form-control" name="contact" type="text"
											value="" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">认证说明:</label>
									<div class="col-lg-8">
										<input class="form-control" name="reason" type="text" value="" />
									</div>
								</div>

								<div class="form-group" id="upload-attach">
									<label class="col-lg-2 control-label">附件:</label>
									<div class="col-lg-8">
										<a class="btn btn-mini btn-default">上传附件</a> <span
											class="aw-text-color-999 upload-url"></span>
										<p class="aw-text-color-999">请提交对应的身份证明</p>
										<input name="attach" type="file"
											class="upload-attach form-control hide"
											onchange="$('.upload-url').html($(this).val());" />
									</div>
								</div>
							</div>

							<div style="text-align: center;">
								<button class="btn btn-primary savebt" type="submit">
									提交</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- footer部分 -->
	<jsp:include page="../footer.jsp" />
	<!-- footer部分结束 -->

</body>
</html>
