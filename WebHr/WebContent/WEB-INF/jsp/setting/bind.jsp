<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+设置--账号绑定</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="../header.jsp" />
<script src="/WebHr/js/setting-uploadimg.js"></script>
<link href="/WebHr/css/user-setting.css" rel="stylesheet">
</head>

<body class="grey-bg">

	<!-- 导航 -->
	<jsp:include page="../navigation.jsp" />
	<!-- 导航结束 -->

	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter aw-user">
			<div class="aw-user-setting container">
				<div class="tabbable">
					<ul class="nav nav-tabs aw-reset-nav-tabs">
						<!-- <li><a href="../WebHr/redirect.do?page=setting/verify">申请认证</a></li> -->
						<li><a href="../WebHr/redirect.do?page=setting/password">修改密码</a></li>
						<li class="active"><a
							href="../WebHr/redirect.do?page=setting/bind">账号绑定</a></li>
						<li><a href="../WebHr/setting.do">基本资料</a></li>
						<h2>用户设置</h2>
					</ul>
				</div>


				<div class="tab-content clearfix">
					<div class="aw-mod aw-user-setting-bind">
						<div class="aw-mod-head">
							<h4>账号绑定</h4>
						</div>

						<div class="form-group">
							<div class="bind">
							<span>暂无此功能！</span>
								<!-- 
								<img src="../WebHr/img/qqalone.png" title="qq" alt="qq" /> <a
									href="#">绑定QQ账号</a>
									 -->
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</div>
	<!-- footer部分 -->
	<jsp:include page="../footer.jsp" />
	<!-- footer部分结束 -->

</body>
</html>
