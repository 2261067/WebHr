<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+设置--修改密码</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="../header.jsp" />
<script src="/WebHr/js/setting.js"></script>
<link href="/WebHr/css/user-setting.css" rel="stylesheet">
</head>

<body class="grey-bg">

	<!-- 导航 -->
	<jsp:include page="../navigation.jsp" />
	<!-- 导航结束 -->


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter aw-user">
			<div class="aw-user-setting container">
				<div class="tabbable">
					<ul class="nav nav-tabs aw-reset-nav-tabs">
						<!-- 	<li><a href="../WebHr/redirect.do?page=setting/verify">申请认证</a></li> -->
						<li class="active"><a
							href="../WebHr/redirect.do?page=setting/password">修改密码</a></li>
						<li><a href="../WebHr/redirect.do?page=setting/bind">账号绑定</a></li>
						<li><a href="../WebHr/setting.do">基本资料</a></li>
						<h2>用户设置</h2>
					</ul>
				</div>


				<div class="tab-content clearfix">
					<div class="aw-mod aw-user-setting-bind">
						<div class="aw-mod-head">
							<h4>修改密码</h4>
						</div>
						<form class="form-horizontal" action="./settingpwd.do"
							method="post" id="setting_form_pwd">
							<div class="aw-mod-body">
								<div class="form-group">
									<label class="control-label" for="input-password-old">当前密码</label>
									<div class="controls">
										<div class="col-lg-4">
											<input type="password" class="form-control"
												id="input-password-old" name="input-password-old" />
										</div>
										<label id="pwdcheck" class="Validform_checktip"
											style="display: none"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label" for="input-password-new">新的密码</label>
									<div class="controls">
										<div class="col-lg-4">
											<input type="password" class="form-control"
												id="input-password-new" name="password" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label" for="input-password-re-new">确认密码</label>
									<div class="controls">
										<div class="col-lg-4">
											<input type="password" class="form-control"
												id="input-password-re-new" name="input-password-re-new" />
										</div>
										<label id="newpwd" class="Validform_checktip"
											style="display: none"></label>
									</div>
								</div>
							</div>

							<div style="text-align: center;">
								<button class="btn btn-primary savebt" type="submit">
									保存</button>
							</div>

						</form>
					</div>


				</div>
			</div>
		</div>

	</div>

	<!-- footer部分 -->
	<jsp:include page="../footer.jsp" />
	<!-- footer部分结束 -->

</body>
</html>
