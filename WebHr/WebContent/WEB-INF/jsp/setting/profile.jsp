<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<title>HR+设置--基本信息</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="../header.jsp" />

<script src="js/setting-uploadimg.js"></script>
<script src="js/setting-userinfo.js"></script>

<link href="/WebHr/css/user-setting.css" rel="stylesheet">


<%
	String realPath = "http://" + request.getServerName() + ":"
			+ request.getServerPort() + request.getContextPath();
%>
</head>

<body class="grey-bg">

	<!-- 导航 -->
	<jsp:include page="../navigation.jsp" />
	<!-- 导航结束 -->

	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter aw-user">
			<div class="aw-user-setting container">
				<div class="tabbable">
					<ul class="nav nav-tabs aw-reset-nav-tabs">
						<!-- 	<li><a href="../WebHr/redirect.do?page=setting/verify">申请认证</a></li> -->
						<li><a href="../WebHr/redirect.do?page=setting/password">修改密码</a></li>
						<li><a href="../WebHr/redirect.do?page=setting/bind">账号绑定</a></li>
						<li class="active"><a href="../WebHr/setting.do">基本资料</a></li>
						<h2>用户设置</h2>
					</ul>
				</div>

				<div class="tab-content clearfix">
					<!-- 基本信息 -->
					<div class="aw-mod">
						<div class="aw-mod-head">
							<h4>基本信息</h4>
						</div>
						<form id="setting_form" method="post" action="./updateuserinfo.do">
							<input class="common-text required" id="userid" name="userid"
								type="text" style="display: none;" value='${sessionScope.currentUser.userId}' >
							<div
								class="aw-mod-body aw-user-setting-layout aw-user-setting-base"
								style="font-size: 14px;">

								<dl>
									<dt>邮箱:</dt>
									<dd>${sessionScope.currentUser.email}</dd>
								</dl>

								<dl>
									<dt>
										<label>昵称:</label>
									</dt>
									<dd class="aw-user-setting-base-introduce">
										<input class="form-control" id="nickname" name="nickname"
											maxlength="128" type="text"
											value='${sessionScope.currentUser.nickName}' />
									</dd>
								</dl>

								<dl>
									<dt>真实姓名:</dt>
									<dd>
										<label>姓：<input class="form-control" name="firstName"
											type="text" value='${sessionScope.currentUser.firstName}'
											style="width: 80px;" />
										</label> <label>名：<input class="form-control" name="lastName"
											type="text" value='${sessionScope.currentUser.lastName}'
											style="width: 88px;" /></label>
									</dd>
								</dl>

								<dl>
									<dt>性别:</dt>
									<dd>
										<label> <input name="gender" id="gender" value="1"
											type="radio"
											${(sessionScope.currentUser.gender =='1')?'checked' : ''} />
											男
										</label><label> <input name="gender" id="gender" value="2"
											type="radio"
											${(sessionScope.currentUser.gender =='2')?'checked' : ''} />
											女
										</label>
									</dd>
								</dl>

								<dl>
									<dt>
										<label>职位:</label>
									</dt>
									<dd class="aw-user-setting-base-introduce">
										<input class="form-control" name="position" maxlength="128"
											type="text" value='${sessionScope.currentUser.position}' />
									</dd>
								</dl>


								<dl>
									<dt>
										<label>电话:</label>
									</dt>
									<dd class="aw-user-setting-base-introduce">
										<input class="form-control" name="phonenumber"
											id="phonenumber" maxlength="128" type="text"
											value='${sessionScope.currentUser.phoneNumber}' />
									</dd>
								</dl>

								<dl>
									<dt>
										<label>工作年限:</label>
									</dt>
									<dd class="aw-user-setting-base-introduce">
										<input class="form-control" name="workExperience"
											maxlength="128" type="text"
											value='${sessionScope.currentUser.workExperience}' />
									</dd>
								</dl>

								<dl>
									<dt>
										<label>一句话介绍:</label>
									</dt>
									<dd class="aw-user-setting-base-introduce">
										<input class="form-control" name="autograph" maxlength="128"
											type="text" value='${sessionScope.currentUser.autograph}' />
									</dd>
								</dl>


								<dl>
									<dt>
										<label>个人简介:</label>
									</dt>
									<dd>
										<textarea id="work_experience" name="introduce"
											class="form-control" rows="5" cols="75">${sessionScope.currentUser.introduce}</textarea>
									</dd>
								</dl>

								<!-- 上传头像 -->
								<div class="aw-user-setting-upload-img-box">
									<dl>
										<dt class="pull-left">
											<img class="aw-border-radius-5"
												src="<%=realPath%>${sessionScope.currentUser.photo}" alt=""
												id="avatar_src" />
										</dt>
										<dd class="pull-left">
											<h5>头像设置</h5>
											<p>支持 jpg、gif、png 等格式的图片</p>
											<a class="btn btn-mini btn-success" id="avatar_uploader">上传头像</a>
											<input type="file" id="image" name="image"
												style="display: none;" /><span id="avatar_uploading_status"
												class="hide"><i class="aw-loading"></i> 文件上传中...</span>
										</dd>
									</dl>
								</div>
								<!-- end 上传头像 -->
							</div>

							<div style="text-align: center;">
								<button class="btn btn-primary savebt" type="submit">
									保存</button>
							</div>

						</form>
					</div>
					<!-- end 基本信息 -->
				</div>
			</div>
		</div>
	</div>



	<!-- footer部分 -->
	<jsp:include page="../footer.jsp" />
	<!-- footer部分结束 -->

</body>
</html>
