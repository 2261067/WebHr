<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title>搜索</title>

<jsp:include page="../header.jsp" />
<script src="/WebHr/js/hr-search.js"></script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->

<script type="text/javascript">
	
	var base_query="${q}";
	var currentPage=1;
	var currentType="question";
	$(document).ready(getSearchData("#search_result",currentPage,base_query,currentType));
</script>


</head>
<body class="grey-bg">
<jsp:include page="../navigation.jsp"/>
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter aw-serch-result">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="aw-mod aw-all-question aw-mod-search-result">
							<div class="aw-mod-head">
								<div class="tabbable">
									<ul class="nav nav-tabs aw-reset-nav-tabs" id="list_nav">
										<li><a onclick="getSearchData('#search_result',1,base_query,'user')" href="#users" data-toggle="tab">用户</a></li>
										<li><a onclick="getSearchData('#search_result',1,base_query,'topic')" data-toggle="tab">话题</a></li>
										<li class="active"><a onclick="getSearchData('#search_result',1,base_query,'question')" data-toggle="tab">问题</a></li>
										<li><a onclick="getSearchData('#search_result',1,base_query,'activity')" data-toggle="tab">活动</a></li>
										<li class=""><a class="hide" href="#all" data-toggle="tab">全部</a></li>
										<h2 class="hidden-xs">
											<p>
												搜索 - <span id="aw-search-type">结果</span>
											</p>
										</h2>
									</ul>
								</div>
							</div>
							<div class="aw-mod-body">
								<div class="tab-content">
									<div class="tab-pane active">
										<div id="search_result">
										</div>

										<!-- 加载更多内容 -->
										<a class="aw-load-more-content" id="search_result_more" onclick="getSearchData('#search_result',currentPage,base_query,currentType)"> <span>更多...</span>
										</a>
										<!-- end 加载更多内容 -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<jsp:include page="../footer.jsp" />

</body>
</html>