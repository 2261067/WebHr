<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+问题</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">


<jsp:include page="header.jsp"/>
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-topic.js"></script>
<script type="text/javascript">
//var pagecount=${pageCount};
//var currentPage=${currentPage};
//查询当前用户数目

var params= {page:1,
		pageSize:10,
		//pageCount:pagecount,
		moduleName:"#questionModule",
		containerName:"#all-questions",
		url:"getQuestionByTime.do",
		type:TYPE_QUESTION_DATA,
		pageControlId:"#page-control"
		};
$(function() {getFirstPageData(params);});

//$(document).ready(getQuestionPageList(params,"#questionModule","div.aw-question-list","getQuestionByTime.do",1));
check_user_num();
</script>
</head>

<body class="grey-bg">
	
<jsp:include page="navigation.jsp"/>


	<div class="aw-container-wrap" id="iframe">
		<div class="aw-container aw-wecenter">

			<div class="container">
				<div class="row aw-content-wrap">
				
				<div class="col-sm-12 col-md-9 aw-main-content aw-all-question" id="hotQuestions">
						<!-- tab切换 -->
						<ul class="nav nav-tabs aw-reset-nav-tabs hidden-xs">

							<li class="active"><a
								href="">热门</a></li>

							<li><a class=""
								href="#">精华</a></li>

							<li><a class="hide"
								href="#"
								id="sort_control_hot">热门</a></li>



				



							<!-- <h2 class="hidden-xs">发现</h2> -->
						</ul>
						<!-- end tab切换 -->



						<div class="aw-mod aw-question-box-mod">
							<div class="aw-mod-body">
								<div class="aw-question-list" id="hot-content">


									
								</div>
							</div>
						</div>

						
							
						</div>
						
						
											<div
						class="col-sm-12 col-md-3 aw-side-bar aw-index-side-bar hidden-xs hidden-sm">
						<div id ="user_num" class="aw-mod-head">
								<h3></h3>
						</div>
						
					</div>
					<div
						class="col-sm-12 col-md-3 aw-side-bar aw-index-side-bar hidden-xs hidden-sm">
					
					
						<div class="aw-side-bar-mod aw-text-align-justify ">
							<div class="aw-mod-head">
								<h3>热门话题</h3>
							</div>
							<div class="aw-mod-body" id="hotTopics">
							
							
							
							
							</div>
							
							<dl>
			<dt class="pull-left aw-border-radius-5">
				
			</dt>
			<dd class="pull-left">
				<a href="/redirect.do?page=topics" class="btn btn-small btn-success">更多话题</a>
			</dd>
		</dl>
						</div>
						<div
							class="hide aw-side-bar-mod aw-text-align-justify aw-no-border-bottom">
							<div class="aw-mod-head">
								<h3>热门用户</h3>
							</div>
							<div class="aw-mod-body">

								</div>
						</div>
						
					</div>
				
					</div>
					

					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question">
						<!-- tab切换 -->
						<ul class="nav nav-tabs aw-reset-nav-tabs hidden-xs">

							<li class="active"><a
								href="">问答</a></li>

							<li><a class=""
								href="#">专栏</a></li>

							<li><a class="hide"
								href="#"
								id="sort_control_hot">热门</a></li>



				



							<!-- <h2 class="hidden-xs">发现</h2> -->
						</ul>
						<!-- end tab切换 -->



						<div class="aw-mod aw-question-box-mod">
							<div class="aw-mod-body">
								<div class="aw-question-list" id="all-questions">


									
								</div>
							</div>
						</div>

						<div class="page-control clearfix pull-left" id="page-control">
							
						</div>
					</div>

					<!-- 侧边栏 -->
					

					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp" />



	<a class="aw-back-top hidden-xs" href="javascript:;"
		onclick="$.scrollTo(1, 600, {queue:true});"><i
		class="fa fa-arrow-up"></i></a>
<!-- module -->
	<div class="aw-item hide" id="questionModule">
		<a class="aw-user-name hidden-xs" data-id="" href="javascript:;"><img
			src=""
			alt="匿名用户" title="匿名用户"></a>
		<div class="aw-question-content">
			<h4>
				<a target="_blank" href=""></a>

			</h4>

			<div class="pull-right hidden-xs contribute hide">
				<span>贡献:</span> <a class="aw-user-name" data-id="" href="#"
					rel="nofollow"><img
					src=""
					alt="@way"></a> <a class="aw-user-name" data-id="" href="#"
					rel="nofollow"><img
					src=""
					alt="sispher"></a>
			</div>

			<p>


				<a href="#" class="aw-user-name" data-id=""></a> <span
					class="aw-text-color-999">回复了问题 • 0 人关注 • 0 个回复 • 0 次浏览 • 0
					小时前 </span>
			</p>
		</div>
		<!-- AND $key<3 -->
	</div>
	<!-- DO NOT REMOVE -->
	<div id="aw-ajax-box" class="aw-ajax-box"></div>



	<!-- Escape time: 0.092878103256226 -->
	<script type="text/javascript" id="bdshare_js" data="type=tools"></script>

	<!-- / DO NOT REMOVE -->


</body>
</html>
