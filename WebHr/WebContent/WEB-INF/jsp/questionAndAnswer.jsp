<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title>问题详情</title>

<jsp:include page="header.jsp" />

<script type="text/javascript" src="/WebHr/js/question_details.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-question-answer.js"></script>
<script type="text/javascript">
//问题页展示
getQuestion();

</script>


</head>
<body class="grey-bg">
	<jsp:include page="navigation.jsp" />
	<style type="text/css">
#edui1_iframeholder {
	height: 150px;
}
</style>
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 aw-global-tips"></div>
				</div>
			</div>
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<div class="aw-mod aw-item aw-question-detail-title">
							<div class="aw-mod-head">
								<h1 id="question_name"></h1>

								<div id="question_follow" class="aw-question-follow clearfix"></div>

								<div class="aw-topic-editor" id="question_topic_editor"
									data-type="question"></div>

							</div>
							<div class="aw-mod-body">
								<div id="question_details"
									class="aw-question-detail-txt markitup-box"></div>
								<div class="aw-question-detail-meta">
									<span id="question_time" class="aw-text-color-999"></span> <a
										style="color: #5CB85C" href="#answer_form1">回答问题</a> <a
										class="aw-text-color-999 aw-invite-replay"><i
										class="fa fa-retweet"></i>邀请回答 </a>
								</div>
							</div>
							<!-- 站内邀请 -->
							<!-- 邀请回答问题 -->
							<div class="aw-invite-box" style="display: none">
								<div class="aw-mod-head clearfix">
									<div class="search-box pull-left">
										<input id="invite-input" class="form-control" type="text"
											placeholder="搜索你想邀请的人...">
										<div class="aw-dropdown">
											<i class="aw-icon i-dropdown-triangle"> </i>
											<p class="title">没有找到相关结果</p>
											<ul class="aw-dropdown-list">
											</ul>
										</div>
										<i class="fa fa-search"> </i>
									</div>

								</div>

								<div class="aw-mod-body clearfix"></div>
								<div class="aw-mod-footer">
									<a class="prev active"> 上一页 </a> &nbsp; <a class="next">
										下一页 </a>
								</div>
								<i class="i-dropdown-triangle"> </i>



							</div>
							<!-- 邀请回答问题end -->
							<!-- end 站内邀请 -->
							<!-- 相关链接 -->

							<!-- end 相关链接 -->
						</div>

						<div class="aw-mod aw-question-comment-box">
							<div class="aw-mod-head">
								<ul class="nav nav-tabs aw-reset-nav-tabs">
									<li id="vote" class="active"><a
										onclick="answer_sort(this,'vote')"> 票数</a></li>
									<li id="time"><a type="desc"
										onclick="answer_sort(this,'time')">时间<i
											class="fa fa-caret-down"></i></a></li>

									<h2 id="answer_num" class="hidden-xs"></h2>
								</ul>
							</div>
							<!-- 答案列表 -->
							<div class="aw-mod-body aw-dynamic-topic" id="answer_list">

							</div>
							<div>
								<a id="answer_more" class="aw-load-more-content"> <span>加载更多答案...</span>
								</a>
							</div>

							<div class="aw-mod-footer">
								<div class="aw-load-more-content hide"
									id="load_uninterested_answers">
									<span class="aw-text-color-999 aw-alert-box aw-text-color-999"
										href="javascript:;" tabindex="-1"
										onclick="$.alert('被折叠的回复是被你或者被大多数用户认为没有帮助的回复');">为什么被折叠?</span>
									<a href="javascript:;"><span class="hide_answers_count">0</span>
										个回复被折叠</a>
								</div>

								<div class="hide aw-dynamic-topic"
									id="uninterested_answers_list"></div>
							</div>





						</div>

						<!-- 回答问题编辑器 -->
						<div class="aw-mod aw-mod-replay-box">
							<a name="answer_form1" id="answer_form1"> &nbsp;</a>
							<form action="commitAnswer.do" onsubmit="return false;"
								method="post" id="answer_form">
								<input type="hidden" name="question_id" id="answer_question_id"
									value="">
								<div class="aw-mod-head">
									<p>
										<span class="pull-right"> <label> <input
												type="checkbox" checked="checked" value="1"
												name="auto_focus"> 关注问题
										</label> &nbsp; <label> <input type="checkbox" value="1"
												name="anonymous"> 匿名回复
										</label>
										</span>
									</p>
								</div>

								<div class="aw-mod-body">
									<input type="hidden" name="answer_content"
										id="advanced_editor1">

									<div class="aw-mod-head" style="padding-left: 0px;">
										<script id="editor" name="content" type="text/plain"></script>

										<!-- 实例化编辑器 -->
										<script type="text/javascript">
												UE.delEditor('editor');
												UE
														.getEditor(
																'editor',
																{
																	toolbars : UEDITOR_CONFIG.toolbars_answer_base,
																	filterRules : function() {
																		return {
																		//a:{$:{}}
																		}
																	}()
																});
												function fwb_save() {
													$('#advanced_editor1')
															.val(
																	UE
																			.getEditor(
																					'editor')
																			.getContent());
												}

												ATTACH_ACCESS_KEY = '78f660a3033298287c7c42d7ac3d2f16';
												ITEM_IDS = '';
												COMMENT_UNFOLD = '';

												QUESTION_ID = $
														.getUrlParam("questionId");
												UNINTERESTED_COUNT = 5;
											</script>


									</div>
									<div class="aw-replay-box-control clearfix">
										<a href="javascript:;"
											onclick="javascript:fwb_save();ajax_post($('#answer_form'));"
											class="btn btn-large btn-success pull-right btn-publish-submit">回复</a>


									</div>
								</div>
							</form>
						</div>

					</div>

					<!-- 侧边栏 -->
					<div class="col-md-3 aw-side-bar hidden-xs hidden-sm">
						<!-- 发起人 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>发起人</h3>
							</div>
							<div class="aw-mod-body">
								<dl>
									<dt id="side_bar_user" class="pull-left aw-border-radius-5">

									</dt>
									<dd id="side_bar_atttion_user" class="pull-left">


										<p></p>
									</dd>
								</dl>
							</div>
						</div>

						<!-- end 发起人 -->




						<!-- 问题状态 -->
						<div class="aw-side-bar-mod aw-side-bar-mod-question-status">
							<div class="aw-mod-head">
								<h3>问题状态</h3>
							</div>
							<div id="question_status" class="aw-mod-body">
								<ul>
									<li>最新活动: <span id="question_last_motify"
										class="aw-text-color-blue"></span></li>
									<li>关注: <span id="question_attention_count"
										class="aw-text-color-blue"></span> 人
									</li>

									<li class="aw-side-bar-user-list aw-border-radius-5"
										id="focus_users"></li>
								</ul>
							</div>
						</div>
						<!-- end 问题状态 -->

					</div>
					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
		<div class="aw-mod aw-free-editor hide">
			<form action="commitAnswer.do" onsubmit="return false;" method="post">
				<input type="hidden" name="post_hash" value="e6eedb7826954d8f" /> <input
					type="hidden" name="question_id" value="5548" /> <input
					type="hidden" name="attach_access_key"
					value="f4678f76f90df6a46563263baba78fab" />
				<div class="aw-mod-head">
					<div class="aw-wecenter">
						<a href="#" class="btn btn-large btn-default pull-left"
							onclick="toggleFullScreen('hide');return false;">退出全屏模式</a> 全屏模式
						<a href="#" class="btn btn-large btn-default pull-right"
							onclick="ajax_post($(this).parents('#answer_form'));return false;">回复</a>
					</div>
				</div>
				<div class="aw-mod-body">
					<div class="aw-mod-side-left pull-left">
						<!--<textarea name="answer_content" id="advanced_editor" rows="15" class="autosize advanced_editor"></textarea>-->

						<textarea name="answer_content" id="advanced_editor" rows="15"
							class="autosize advanced_editor"></textarea>
					</div>
					<div class="aw-mod-side-right pull-left">
						<div id="markItUpPreviewFrame" class="markItUpPreviewFrame">
							<div id="markItUpPreviewFrames" class="markItUpPreviewFrames"></div>
						</div>
					</div>
				</div>
			</form>
		</div>



		<jsp:include page="footer.jsp" />




		<!-- / DO NOT REMOVE -->
</body>
</html>