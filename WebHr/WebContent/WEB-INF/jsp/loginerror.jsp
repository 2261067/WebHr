<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<title>登录出错！</title>
<jsp:include page="header.jsp" />

<script type="text/javascript">
	function show_time(t) {
		var time = t;
		time =time- 1;
		if (time == 0) {
			window.location.href = G_BASE_URL + '/html/signin.html';
		}else {
			setTimeout("show_time("+time+")", 1000);
		}
	}
	show_time(3);
</script>
</head>
<body>
	登录失败，请稍后重试...
</body>


</html>