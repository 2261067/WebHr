<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+话题详情</title>


<jsp:include page="header.jsp" />
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-topic-detail.js"></script>
<script type="text/javascript">
	var topicId = $.getUrlParam("topicId");
	init_topic_details();
</script>
</head>

<body class="grey-bg">

	<jsp:include page="navigation.jsp" />


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">

			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question">
						<div class="aw-mod aw-topic-detail-title">
							<div class="aw-mod-body" id="topic_header"></div>
						</div>

						<div class="aw-mod aw-topic-list-mod">
							<div class="aw-mod aw-question-box-mod">
								<div class="aw-mod-body">
									<div class="aw-question-list" id="c_question_list"></div>

								</div>
							</div>
						</div>


						<div class="page-control clearfix" id="page-control">
							<ul class="pagination pull-right">

							</ul>
						</div>
					</div>

					<div
						class="col-sm-12 col-md-3 aw-side-bar aw-side-bar-topic-detail hidden-xs">
						<!-- 话题描述 -->
						<div class="aw-side-bar-mod aw-text-align-justify">
							<div class="aw-mod-head">
								<h3>话题描述</h3>
							</div>
							<div class="aw-mod-body">
								<p>暂无描述</p>
							</div>
						</div>
						<!-- end 话题描述 -->


						<!-- 最佳回复者 -->
						
						<!-- end 最佳回复者 -->

						<!-- xx人关注该话题 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3></h3>
							</div>
							<div class="aw-mod-body">
								<div class="aw-side-bar-user-list aw-border-radius-5"
									id="focus_users">
									
								</div>
							</div>
						</div>
						<!-- end xx人关注该话题 -->

						<!-- 话题修改记录 -->
					
						<!-- end 话题修改记录 -->

					</div>

					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp" />





</body>
</html>
