<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>后台管理--添加话题</title>

<jsp:include page="adminHeader.jsp" />
<script src="/WebHr/js/admin_topicadd.js"></script>
</head>
<body>

	<!-- top顶部 -->
	<jsp:include page="adminTop.jsp" />
	<!-- ENDtop顶部 -->

	<div class="container clearfix">

		<!-- 导航栏 -->
		<jsp:include page="adminNavigation.jsp" />
		<!-- END导航栏 -->

		<!-- MAIN -->
		<div class="main-wrap">
			<div class="crumb-wrap">
				<div class="crumb-list">
					<a href="">首页</a><span class="crumb-step">&gt;</span><span
						class="crumb-name">添加话题</span>
				</div>
			</div>

			<div class="result-wrap">
				<form action="../WebHr/adminmanage/addtopic.do" method="post" id="addtopicform" enctype="multipart/form-data">
					<table class="insert-tab" width="100%">
						<tbody>							
							<tr>
								<th><i class="require-red">*</i>话题名：</th>
								<td><input class="common-text required" id="topicname"
									name="topicname" size="50" value="" type="text"></td>
							</tr>
							
							<tr>
								<th>话题详细描述：</th>
								<td><textarea name="topicdesc" class="common-textarea"
										id="topicdesc" cols="30" style="width: 98%;" rows="10"></textarea></td>
							</tr>
							<tr>
								<th></th>
								<td><input class="btn btn-primary btn6 mr10" value="添加"
									type="submit"></td>
							</tr>
						</tbody>
					</table>
				</form>

			</div>
		</div>
		<!-- ENDMAIN -->


	</div>



</body>
</html>