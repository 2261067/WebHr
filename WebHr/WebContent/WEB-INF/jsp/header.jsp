<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />

<script type="text/javascript">

var G_UNREAD_NOTIFICATION = 0;
var G_USER_ID = '${sessionScope.currentUser == null ? "null_user":sessionScope.currentUser.userId}';
var G_USER_NICK = '${sessionScope.currentUser == null ? "null_user":sessionScope.currentUser.nickName}';
var G_USER_IMG = '${sessionScope.currentUser == null ? "":sessionScope.currentUser.photo}';
var G_UPLOAD_ENABLE = 'Y';
var G_POST_HASH='_D61C82BDB5AD044F8E14DF9C8D3B69ED';
var G_BASE_URL='http://localhost:8080/WebHr';
var G_STATIC_URL = 'http://www.027team.com/WebHr';
var G_PIC_BASE_URL=G_BASE_URL;
</script>

<!-- ueditor js -->
<script src="/WebHr/ueditor/ueditor.parse.js"></script>
<script src="/WebHr/ueditor/ueditor.config.js"></script>
<script src="/WebHr/ueditor/ueditor.all.js"></script>

<!--other  js -->
<script src="/WebHr/js/jquery.1.js" type="text/javascript"></script>
<script src="/WebHr/js/jquery.form.js" type="text/javascript"></script>
<script src="/WebHr/js/plug-in_module.js" type="text/javascript"></script>
<script src="/WebHr/js/relativeUrl.js" type="text/javascript"></script>
<script src="/WebHr/js/functions.js" type="text/javascript"></script>
<script src="/WebHr/js/aw_template.js" type="text/javascript"></script>
<script src="/WebHr/js/common.js" type="text/javascript"></script>
<script src="/WebHr/js/app.js" type="text/javascript"></script>
<script type="text/javascript" src="/WebHr/js/compatibility.js"></script>
<script src="/WebHr/js/prettify.js"></script>
<script src="/WebHr/js/jquery.hotkeys.js"></script>
<script src="/WebHr/js/bootstrapwysiwyg.js"></script>
<script src="/WebHr/js/ajaxfileupload.js"></script>
<!-- 推送 -->
<script type="text/javascript" src="/WebHr/js/comet4j.js"></script>
<!-- our js -->
<script type="text/javascript" src="/WebHr/js/hr-common.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-template.js"></script>

<script type="text/javascript" src="/WebHr/js/bootstrap.min.js"></script>
<!--分页-->
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->

<link href="/WebHr/css/bootstrap.min.css" rel="stylesheet">
<link href="/WebHr/css/common.css" rel="stylesheet">
<link href="/WebHr/css/font-awesome.css" rel="stylesheet">
<link href="/WebHr/css/aw-font.css" rel="stylesheet">
<link href="/WebHr/css/wendaStyle.css" rel="stylesheet">
<link href="/WebHr/css/link.css" rel="stylesheet">
<link href="/WebHr/css/iheima.css" rel="stylesheet">
<link href="/WebHr/css/plug-style.css" rel="stylesheet">
<link href="/WebHr/css/ueditor.css" rel="stylesheet">
<link href="/WebHr/css/hr.css" rel="stylesheet">
<link href="/WebHr/ueditor/themes/iframe.css" rel="stylesheet">

<!--[if lte IE 8]>
	<script type="text/javascript" src="/WebHr/js/respond.js"></script>
<![endif]-->
