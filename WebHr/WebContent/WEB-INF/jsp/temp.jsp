<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title>${question.questionName}</title>


<script type="text/javascript">
	var questionId = ${question.questionId};

</script>
<jsp:include page="header.jsp" />
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->

<script type="text/javascript">

var params= {page:1,questionId:"${question.questionId}"};
	$(document).ready(
			getQuestionPageList(params, "#anwserModule", "#answer_list",
					"getQuestionAndAnswer.do", 2));
</script>

<script type="text/javascript">

</script>

</head>
<body class="grey-bg">
	<jsp:include page="navigation.jsp"/>
	<style type="text/css">
#rich_text_editor1 {
	min-height: 300px;
	max-height: 500px;
	overflow-y: scroll;
	border: 1px solid #eee;
}
</style>
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 aw-global-tips"></div>
				</div>
			</div>
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<div class="aw-mod aw-item aw-question-detail-title">
							<div class="aw-mod-head">
								<h1>${question.questionName}</h1>

								<div class="aw-question-follow clearfix">
									<a href="javascript:;" onclick="focus_question($(this), 5548);"
										class="btn btn-normal btn-default pull-left aw-follow-question">取消关注</a>
									
								</div>

								<div class="aw-topic-editor" id="question_topic_editor"
									data-type="question" data-id="5548">
									<a href="http://ask.iheima.com/?/topic/2" class="aw-topic-name"
										data-id="2"><span>创业</span></a>

								</div>

							</div>
							<div class="aw-mod-body">
								<div class="aw-question-detail-txt markitup-box">${question.questionDesc} </div>
								<div class="aw-question-detail-meta">
									<span class="aw-text-color-999">4 小时前</span> <a
										style="color: #5CB85C"
										href="http://ask.iheima.com/?/question/5548#answer_form1">回答问题</a>

									<a class="aw-text-color-999 aw-invite-replay"><i
										class="fa fa-retweet"></i>邀请回答 </a> <a href="javascript:;"
										class="aw-text-color-999"
										onclick="$.dialog('report', {item_type:'question', item_id:5548})"><i
										class="fa fa-flag"></i>举报</a> <a data-placement="bottom" title=""
										data-toggle="tooltip" data-original-title="感谢提问者"
										onclick="question_thanks(5548, this);"
										class="aw-icon-thank-tips aw-text-color-999"><i
										class="fa fa-heart"></i>感谢</a> <a class="aw-text-color-999"
										onclick="$.dialog('shareOut', {item_type:'question', item_id:5548});"><i
										class="fa fa-share"></i>分享</a>




								</div>
							</div>
							<!-- 站内邀请 -->
							
							<!-- end 站内邀请 -->
							<!-- 相关链接 -->
							
							<!-- end 相关链接 -->
						</div>

						<div class="aw-mod aw-question-comment-box">
							<div class="aw-mod-head">
								<ul class="nav nav-tabs aw-reset-nav-tabs">
									<li><a
										href="http://ask.iheima.com/?/question/5548?uid=focus">关注的人</a></li>
									<li><a
										href="http://ask.iheima.com/?/question/5548?sort_key=add_time&sort=ASC">时间<i
											class="fa fa-caret-up"></i></a></li>
									<li class="active"><a
										href="http://ask.iheima.com/?/question/5548&sort_key=agree_count&sort=DESC">票数</a></li>

									<h2 class="hidden-xs">1 个回复</h2>
								</ul>
							</div>
							<div class="aw-mod-body aw-dynamic-topic" id="answer_list">
								<div class="aw-item" id="anwserModule" uninterested_count="0"
									force_fold="0">
									<a class="anchor" name=""></a>
									<!-- 用户头像 -->
									<a class="aw-user-img aw-border-radius-5 pull-right"
										href="http://ask.iheima.com/?/people/fanglong" data-id="4226"><img
										src="http://ask.iheima.com/uploads/avatar/000/00/42/26_avatar_mid.jpg"
										alt="" /></a>
									<!-- end 用户头像 -->
									<div class="aw-mod-body clearfix">
										<!-- 投票栏 -->
										<em class="aw-border-radius-5 aw-vote-count pull-left"
											onclick="insertVoteBar({element:this,agree_count:0,flag:0,user_name:G_USER_NAME,answer_id:5379})">0</em>
										<!-- end 投票栏 -->
										<div class="pull-left aw-dynamic-topic-content">
											<div class="mod-head" id="answer_user">
												<p>
													<a class="aw-user-name" href="" data-id="4226">user</a> - <span
														class="aw-text-color-999">xxx</span> <i
														class="fa fa-mobile-phone"></i>
												</p>
												<p class="aw-text-color-999 aw-agree-by hide">赞同来自:</p>
											</div>
											<div class="mod-body" id="answer_text">
												<!-- 评论内容 -->

												<div class="markitup-box">xx</div>


												<!-- end 评论内容 -->

											</div>
											<!-- 社交操作 -->
											<div class="mod-footer aw-dynamic-topic-meta">
												<span class="aw-text-color-999" id="answer_time">2 小时前</span> <a
													style="color: #5CB85C"
													class="aw-add-comment aw-text-color-999" data-id="5379"
													data-type="answer" data-comment-count="0"
													data-first-click="hide" href="javascript:;"><i
													class="fa fa-comment"></i>添加评论</a> 
													<a href="javascript:;" id="fenxiang"
													onclick="$.dialog('shareOut', {item_type:'answer', item_id:5379});"
													class="aw-text-color-999"><i class="fa fa-share"></i>分享</a>

												<!-- 可显示/隐藏的操作box -->
												<span class="aw-dynamic-topic-more-operate"> <a
													class="aw-icon-thank-tips aw-text-color-999"
													data-original-title="这是一个没有价值的回复" data-toggle="tooltip"
													title="" data-placement="bottom"
													onclick="answer_user_rate(5379, 'uninterested', this);"><i
														class="fa fa-flag"></i>没有帮助</a> <a href="javascript:;"
													onclick="$.dialog('favorite', 5379);"
													class="aw-text-color-999"><i class="fa fa-bookmark"></i>收藏</a>

													<a href="javascript:;"
													onclick="answer_user_rate(5379, 'thanks', this);"
													class="aw-icon-thank-tips aw-text-color-999"
													data-original-title="感谢热心的回复者" data-toggle="tooltip"
													title="" data-placement="bottom"><i class="fa fa-heart"></i>感谢</a>

												</span>
												<!-- end 可显示/隐藏的操作box -->

											</div>
											<!-- end 社交操作 -->
										</div>
									</div>
								</div>
							</div>
							<div class="aw-mod-footer">
								<div class="aw-load-more-content hide"
									id="load_uninterested_answers">
									<span class="aw-text-color-999 aw-alert-box aw-text-color-999"
										href="javascript:;" tabindex="-1"
										onclick="$.alert('被折叠的回复是被你或者被大多数用户认为没有帮助的回复');">为什么被折叠?</span>
									<a href="javascript:;"><span class="hide_answers_count">0</span>
										个回复被折叠</a>
								</div>

								<div class="hide aw-dynamic-topic"
									id="uninterested_answers_list"></div>
							</div>

						</div>
						
						<div class="aw-mod aw-mod-replay-box">
							<a name="answer_form1" id="answer_form1"> &nbsp;</a>
							<form action="commitAnswer.do"
								onsubmit="return false;" method="post" id="answer_form">
								<input type="hidden" name="post_hash" value="d8be3f719b91e60f">
								<input type="hidden" name="question_id" value="${question.questionId}"> <input
									type="hidden" name="attach_access_key"
									value="92a0261ec50065d5376d1a4c18f359d0">
								<div class="aw-mod-head">
									<a href="http://ask.iheima.com/?/people/" class="aw-user-name"><img
										alt="张泰玮"
										src="http://ask.iheima.com/static/common/avatar-mid-img.png"></a>
									<p>
										<a class="aw-user-name">张泰玮</a>
									</p>
									<p>
										<span class="pull-right"> <label> <input
												type="checkbox" checked="checked" value="1"
												name="auto_focus"> 关注问题
										</label> &nbsp; <label> <input type="checkbox" value="1"
												name="anonymous"> 匿名回复
										</label>
										</span>
									</p>
								</div>

								<div class="aw-mod-body">
									<input type="hidden" name="answer_content"
										id="advanced_editor1">

									<div class="aw-mod-head" style="padding-left: 0px;">
										  <script id="editor" name="content" type="text/plain"></script>
										  
										  <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
    <!-- 实例化编辑器 -->
   <script type="text/javascript">
	UE.delEditor('editor');
	UE.getEditor('editor',{ 
				toolbars:UEDITOR_CONFIG.toolbars_answer_base,
				filterRules: function () {
			return{
				//a:{$:{}}
			}
		}()
	});
	function fwb_save(){
		$('#advanced_editor1').val(UE.getEditor('editor').getContent());
	}

	ATTACH_ACCESS_KEY = '78f660a3033298287c7c42d7ac3d2f16';
	ITEM_IDS = '';
	COMMENT_UNFOLD = '';

	QUESTION_ID = 5705;
	UNINTERESTED_COUNT = 5;


	
</script>
										  
										  
									</div>
									<div class="aw-replay-box-control clearfix">
										<a href="javascript:;"
											onclick="javascript:fwb_save();ajax_post($('#answer_form'));"
											class="btn btn-large btn-success pull-right btn-publish-submit">回复</a>
										<a href="http://ask.iheima.com/?/page/integral"
											class="pull-right" target="_blank">[积分规则]</a>

									</div>
								</div>
							</form>
						</div>



					</div>
					<!-- 侧边栏 -->
					<div class="col-md-3 aw-side-bar hidden-xs hidden-sm">
						<!-- 发起人 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>发起人</h3>
							</div>
							<div class="aw-mod-body">
								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a href="http://ask.iheima.com/?/people/wertyliii"><img
											alt="wertyliii"
											src="http://ask.iheima.com/static/common/avatar-mid-img.png" /></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name"
											href="http://ask.iheima.com/?/people/wertyliii"
											data-id="11540">wertyliii</a> <i data-placement="bottom"
											title="" onclick="follow_people($(this), 11540);"
											data-toggle="tooltip" class="fa fa-check aw-active"
											data-original-title="关注"></i>
										<p></p>
									</dd>
								</dl>
							</div>
						</div>

						<!-- end 发起人 -->

						<div>
							问答二维码（手机扫描分享） <img style="width: 198px; height: 198px"
								src="http://s.jiathis.com/qrcode.php?url=http://ask.iheima.com/?/question/5548">
						</div>

						<!-- 邀请别人回复 -->
						<div class="aw-side-bar-mod invite-replay">
							<div class="aw-mod-head">
								<h3>邮件邀请别人回复</h3>
							</div>
							<div class="aw-mod-body clearfix">
								<!-- 侧边栏邀请box -->
								<form method="post"
									action="http://ask.iheima.com/?/question/ajax/email_invite/question_id-5548"
									onsubmit="return false;" id="email_invite_form">
									<input class="form-control" type="text" name="email"
										placeholder="邮件邀请回复..." /> <a
										class="pull-right btn btn-mini btn-success"
										onclick="ajax_post($('#email_invite_form'));">邀请</a>
								</form>
								<!-- end 侧边栏邀请box -->
							</div>
						</div>
						<!-- end 邀请别人回复 -->

						<!-- 相关问题 -->
						<div class="aw-side-bar-mod question_related_list">
							<div class="aw-mod-head">
								<h3>相关问题</h3>
							</div>
							<div class="aw-mod-body">
								<ul class="aw-li-border-bottom">
									<li><a href="http://ask.iheima.com/?/question/27">你为什么要创业?</a></li>
									<li><a href="http://ask.iheima.com/?/question/23">女友和创业该怎么选择?</a></li>
									<li><a href="http://ask.iheima.com/?/question/1164">除了给钱之外，你怎么让别人加入你的创业团队?</a></li>
									<li><a href="http://ask.iheima.com/?/question/1173">互联网思维下，硬件是否应该免费?</a></li>
									<li><a href="http://ask.iheima.com/?/question/46">创始人创业时应该读那些书，有那些书可以推荐?</a></li>
									<li><a href="http://ask.iheima.com/?/question/1833">服务行业（法律、生活、金融、财会、物流、信息化、人力资源、文化创意等等）互联网化还要多久?</a></li>
									<li><a href="http://ask.iheima.com/?/question/1609">免费模式适用于任何领域吗，如何在互联网时代重构商业模式?</a></li>
									<li><a href="http://ask.iheima.com/?/question/15">这是创业时代，但怎样白手起家?</a></li>
									<li><a href="http://ask.iheima.com/?/question/2735">
											刚刚准备创业，但是老婆意外怀孕，那到底是把这两年的时间留给家人，还是追逐梦想?</a></li>
									<li><a href="http://ask.iheima.com/?/question/112">创业初期如何招聘优秀人才；如何留住人才?</a></li>
									<li><a href="http://ask.iheima.com/?/question/1358">怎么克服创业中的焦虑感?</a></li>
								</ul>
							</div>
						</div>
						<!-- end 相关问题 -->

						<!-- 问题状态 -->
						<div class="aw-side-bar-mod aw-side-bar-mod-question-status">
							<div class="aw-mod-head">
								<h3>问题状态</h3>
							</div>
							<div class="aw-mod-body">
								<ul>
									<li>最新活动: <span class="aw-text-color-blue">2 小时前</span></li>
									<li>浏览: <span class="aw-text-color-blue">67</span></li>
									<li>关注: <span class="aw-text-color-blue">3</span> 人
									</li>

									<li class="aw-side-bar-user-list aw-border-radius-5"
										id="focus_users"></li>
								</ul>
							</div>
						</div>
						<!-- end 问题状态 -->

					</div>
					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>
	<div class="aw-mod aw-free-editor hide">
		<form action="commitAnswer.do"
			onsubmit="return false;" method="post" >
			<input type="hidden" name="post_hash" value="e6eedb7826954d8f" /> <input
				type="hidden" name="question_id" value="5548" /> <input
				type="hidden" name="attach_access_key"
				value="f4678f76f90df6a46563263baba78fab" />
			<div class="aw-mod-head">
				<div class="aw-wecenter">
					<a href="#" class="btn btn-large btn-default pull-left"
						onclick="toggleFullScreen('hide');return false;">退出全屏模式</a> 全屏模式 <a
						href="#" class="btn btn-large btn-default pull-right"
						onclick="ajax_post($(this).parents('#answer_form'));return false;">回复</a>
				</div>
			</div>
			<div class="aw-mod-body">
				<div class="aw-mod-side-left pull-left">
					<!--<textarea name="answer_content" id="advanced_editor" rows="15" class="autosize advanced_editor"></textarea>-->

					<textarea name="answer_content" id="advanced_editor" rows="15"
						class="autosize advanced_editor"></textarea>
				</div>
				<div class="aw-mod-side-right pull-left">
					<div id="markItUpPreviewFrame" class="markItUpPreviewFrame">
						<div id="markItUpPreviewFrames" class="markItUpPreviewFrames"></div>
					</div>
				</div>
			</div>
		</form>
	</div>



	<script type="text/javascript"
		src="http://ask.iheima.com/static/js/app/question_detail.js"></script>

	<div class="aw-footer-wrap">
		<div class="aw-footer aw-wecenter">
			<br /> <br /> Copyright © 2014 - 创业问答_黑问, All Rights Reserved<br />
			</span> <span class="hidden-xs">Powered By <a
				href="http://www.iheima.com" target="blank">黑马网 <!--?php echo G_VERSION; ?--></a></span>

			<span class="hidden-xs">当前时区: GMT +8 &nbsp; <a
				href="http://ask.iheima.com/?/feed/" target="blank"><i
					class="fa fa-rss-square"></i> RSS Feed</a></span> <br />
			<div style="display: block;">


				<script type="text/javascript">
					var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://"
							: " http://");
					document
							.write(unescape("%3Cscript src='"
									+ _bdhmProtocol
									+ "hm.baidu.com/h.js%3F9723485e19f163e8e518ca694e959cb9' type='text/javascript'%3E%3C/script%3E"));
				</script>

				<!--<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F92dd26030df72ae77efd74a134805214' type='text/javascript'%3E%3C/script%3E"));
</script>-->
				<!--<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_5023045'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/stat.php%3Fid%3D5023045%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script>-->

				<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1000524616'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/z_stat.php%3Fid%3D1000524616%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script>
			</div>
		</div>
	</div>



	<a class="aw-back-top hidden-xs" href="javascript:;"
		onclick="$.scrollTo(1, 600, {queue:true});"><i
		class="fa fa-arrow-up"></i></a>


	<!-- DO NOT REMOVE -->
	<div id="aw-ajax-box" class="aw-ajax-box"></div>

	<div style="display: none;" id="__crond">
		<script type="text/javascript">
		$(document).ready(function () {
			$('#__crond').html(unescape('%3Cimg%20src%3D%22' + G_BASE_URL + '/crond/run/1417086438%22%20width%3D%221%22%20height%3D%221%22%20/%3E'));
		});

	</script>
	</div>

	<!-- Escape time: 0.091999053955078 -->
	<script type="text/javascript" id="bdshare_js" data="type=tools"></script>

	<!-- / DO NOT REMOVE -->

</body>
</html>