<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- 导航栏 -->
<div class="sidebar-wrap">
	<div class="sidebar-title">
		<h1>菜单</h1>
	</div>
	<div class="sidebar-content">
		<ul class="sidebar-list">
			<li><a href="#">话题</a>
				<ul class="sub-menu">
					<li><a href="/WebHr/redirect.do?page=adminTopic">话题展示</a></li>
					<li><a href="/WebHr/redirect.do?page=adminTopicAdd">添加话题</a></li>
				</ul></li>
			<li><a href="#">问题</a>
				<ul class="sub-menu">
					<li><a href="/WebHr/redirect.do?page=adminQuestion">问题管理</a></li>
				</ul></li>
			<li><a href="#">活动</a>
				<ul class="sub-menu">
					<li><a href="/WebHr/redirect.do?page=adminActivity">活动管理</a></li>
				</ul></li>
			<li><a href="#">用户</a>
				<ul class="sub-menu">
					<li><a href="/WebHr/redirect.do?page=adminUser">用户管理</a></li>
				</ul></li>
		</ul>
	</div>
</div>
<!-- END导航栏 -->
