
<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>私信</title>
<jsp:include page = "header.jsp" />
<script type="text/javascript" src="/WebHr/js/hr-inbox.js"></script>
<script type="text/javascript">
getAllInboxMessage();
</script>
</head>
<body>
	<jsp:include page="navigation.jsp"/>
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<div class="aw-mod aw-dynamic-index aw-my-inbox">
							<div class="aw-mod-head">
								<h2>
									<a href="javascript:;" onclick="$.dialog('inbox', '');"
										class="pull-right btn btn-mini btn-success">新私信</a> 私信
								</h2>
							</div>
							<div class="aw-mod-body">
								
							</div>

						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
		<jsp:include page="footer.jsp" />
</body>
</html>