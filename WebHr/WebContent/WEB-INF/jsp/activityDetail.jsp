<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />

<title>活动详情</title>

<jsp:include page="header.jsp" />
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="/WebHr/js/hr-activity.js"></script>
<script type="text/javascript">
	//活动页展示
	var activity_id = $.getUrlParam("activity_id");
	getActivityDetails();
</script>


</head>
<body class="grey-bg">
	<jsp:include page="navigation.jsp" />
	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 aw-global-tips"></div>
				</div>
			</div>
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<div class="aw-mod aw-item aw-question-detail-title">
							<div class="aw-mod-head">
								<h1 id="activity_name"></h1>


								<div class="aw-question-follow clearfix">
									<span id="activity_sign"></span>
									
									<span id="activity_edit">
									</span>	
									
								</div>

								<div class="aw-topic-editor" id="question_topic_editor"
									data-type="question"></div>

							</div>

							<div class="aw-mod-body">
								<div class="aw-question-detail-meta">
									<span id="activity_address" class="aw-text-color-999"></span> <span
										id="activity_end_time" class="aw-text-color-999"></span> <span
										id="activity_host_time" class="aw-text-color-999"></span>
								</div>
								<div id="activity_details"
									class="aw-question-detail-txt markitup-box"></div>
								<div class="aw-question-detail-meta">
									<!-- <span id="activity_people_num" class="aw-text-color-999"></span>  -->
									<span id="activity_host_people" class="aw-text-color-999"></span>
									<span id="activity_phone_num" class="aw-text-color-999"></span>
								</div>
							</div>

						</div>

						<div class="aw-mod aw-question-comment-box">

							<div class="aw-mod-head">

								<ul class="nav nav-tabs aw-reset-nav-tabs">
									<h2 id="sign_num" class="hidden-xs">还没有人报名噢！</h2>
								</ul>
							</div>

							<div class="aw-mod-body aw-dynamic-topic" id="sign_list"></div>


						</div>



					</div>

					<!-- 侧边栏 -->
					<div class="col-md-3 aw-side-bar hidden-xs hidden-sm">
						<!-- 发起人 -->
						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>发起人</h3>
							</div>
							<div class="aw-mod-body">
								<dl>
									<dt id="side_bar_user" class="pull-left aw-border-radius-5">

									</dt>
									<dd id="side_bar_atttion_user" class="pull-left">


										<p></p>
									</dd>
								</dl>
							</div>
						</div>

						<!-- end 发起人 -->




						<!-- 问题状态 -->
						<div class="aw-side-bar-mod aw-side-bar-mod-question-status">
							<div class="aw-mod-head">
								<h3>活动状态</h3>
							</div>
							<div id="question_status" class="aw-mod-body">
								<ul>
									<li>最新动态: <span id="activity_last_motify"
										class="aw-text-color-blue"></span></li>
									<!-- <li>报名: <span id="activity_sign_count"
										class="aw-text-color-blue"></span> 人
									</li> -->

									<li class="aw-side-bar-user-list aw-border-radius-5"
										id="focus_users"></li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

</div>

		<jsp:include page="footer.jsp" />
</body>
</html>