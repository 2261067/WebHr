<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+个人主页</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="header.jsp" />
<script type="text/javascript" src="/WebHr/js/hr-people.js"></script>
<script type="text/javascript" src="/WebHr/js/otherpeople.js"></script>
<link href="/WebHr/css/user.css" rel="stylesheet">


</head>


<body class="grey-bg">

	<!-- 导航 -->
	<jsp:include page="navigation.jsp" />
	<!-- 导航结束 -->

	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<!-- 用户数据内容 -->
						<div class="aw-mod aw-user-detail-box"></div>
						<!-- end 用户数据内容 -->

						<div class="aw-user-center-tab">
							<div class="tab-content">
								<div class="tab-pane active" id="overview">

									<!-- 发问 -->
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>
												<a class="pull-right aw-more-content" href="javascript:;"
													onclick="$('#page_questions').click();">更多 »</a>发问
											</h3>
										</div>
										<div class="aw-mod-body aw-mod-body-question"></div>
									</div>
									<!-- end 发问 -->

									<!-- 回复 -->
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>
												<a class="pull-right aw-more-content" href="javascript:;"
													onclick="$('#page_answers').click();">更多 »</a>回复
											</h3>
										</div>
										<div class="aw-mod-body aw-mod-body-answer"></div>
									</div>
									<!-- end 回复 -->


									<!-- 活动-->
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>
												<a class="pull-right aw-more-content" href="javascript:;"
													onclick="$('#page_actions').click();">更多 »</a>活动
											</h3>
										</div>
										<div class="aw-mod-body aw-mod-body-activity"></div>
									</div>
									<!-- end 活动 -->

								</div>


								<div class="tab-pane" id="questions">
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>发问</h3>
										</div>
										<div class="aw-mod-body">
											<div class="aw-user-center-mod aw-user-center-ask"
												id="contents_user_actions_questions"></div>
										</div>
										<div class="aw-mod-footer">
											<!-- 加载更多内容 -->
											<a class="aw-load-more-content"
												id="bp_user_actions_questions_more"> <span>更多...</span>
											</a>
											<!-- end 加载更多内容 -->
										</div>
									</div>
								</div>


								<div class="tab-pane" id="answers">
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>回复</h3>
										</div>
										<div class="aw-mod-body">
											<div class="aw-user-center-mod"
												id="contents_user_actions_answers"></div>
										</div>
										<div class="aw-mod-footer">
											<!-- 加载更多内容 -->
											<a class="aw-load-more-content"
												id="bp_user_actions_answers_more"> <span>更多...</span>
											</a>
											<!-- end 加载更多内容 -->
										</div>
									</div>
								</div>


								<div class="tab-pane" id="actions">
									<div class="aw-mod">
										<div class="aw-mod-head">
											<h3>活动</h3>
										</div>
										<div class="aw-mod-body">
											<ul id="contents_user_actions_activity"></ul>
										</div>
										<div class="aw-mod-footer">
											<!-- 加载更多内容 -->
											<a class="aw-load-more-content"
												id="bp_user_actions_activiy_more"> <span>更多...</span>
											</a>
											<!-- end 加载更多内容 -->
										</div>
									</div>
								</div>


							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 aw-side-bar">
						<div class="aw-side-bar-mod profile-side-following">
							<div class="row">
								<div class="col-sm-12">
									<span> 关注 <em
										class="aw-text-color-blue attention-user-num"></em> 人
									</span>
									<p class="attention-user"></p>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<span> 被 <em class="aw-text-color-blue follow-user-num"></em>
										人关注
									</span>
									<p class="follow-user"></p>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									关注 <em class="aw-text-color-blue attention-topic-num"></em> 话题
									<p class="aw-user-list"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="aw-user-center"></div>
		</div>
	</div>

	<!-- footer部分 -->
	<jsp:include page="footer.jsp" />
	<!-- footer部分结束 -->

</body>
</html>