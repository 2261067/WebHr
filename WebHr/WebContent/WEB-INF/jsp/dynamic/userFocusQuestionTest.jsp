
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+问题</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">


<jsp:include page="../header.jsp" />
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-topic.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-dynamic.js"></script>
<script type="text/javascript">
	//var pagecount=${pageCount};
	//var currentPage=${currentPage};
	var pageSize = 10;
	var uf_nextPage = 1;
	var data_url = "Dynamic/getFocusDynamicData.do";
	var dynamicParams = {
		page : 1,
		pageSize : pageSize,
		//pageCount:pagecount,
		moduleName : "",
		containerName : "#main_contents",
		url : data_url,
		type : TYPE_DEFAULT_DATA,
	};

	

	//$(document).ready(getQuestionPageList(params,"#questionModule","div.aw-question-list","getQuestionByTime.do",1));
</script>
</head>

<body class="grey-bg">

	<jsp:include page="../navigation.jsp" />


	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content">
						<!-- 新消息通知 -->
						<!--<div class="aw-mod aw-new-message-tips aw-border-radius-5 hide" id="index_notification">-->
						<!--<div class="aw-mod-head">
							<h3>
								<span class="pull-right"><a href="http://ask.iheima.com/?/account/setting/privacy/#notifications" class="aw-text-color-999"><i class="fa fa-cog"></i> 通知设置</a></span>
								<span>新通知</span><em class="badge badge-important" name="notification_unread_num">0</em>
							</h3>
						</div>-->
						<!--<div class="aw-mod-body">
							<ul id="notification_list"></ul>
							<p>
								<a href="javascript:;" onclick="read_notification(0, false, false);" class="pull-left btn btn-mini btn-default">我知道了</a>
								<a href="http://ask.iheima.com/?/notifications/" class="pull-right btn btn-mini btn-success">查看所有</a>
							</p>
						</div>-->
						<!--</div>-->
						<!-- end 新消息通知 -->

						<a name="c_contents"></a>
						<div class="aw-mod aw-dynamic-topic clearfix">
							<div class="aw-mod-head">
								<h2 id="main_title">
									<i class="fa fa-check-square-o"></i>我关注的问题
								</h2>
							</div>

							<div class="aw-mod-body" id="main_contents">
								<link
									href="http://ask.iheima.com/static/pc/css/font-awesome.css"
									type="text/css" rel="stylesheet">
								<link href="http://ask.iheima.com/static/pc/css/aw-font.css"
									type="text/css" rel="stylesheet">
								<link
									href="http://ask.iheima.com/static/pc/css/wenda_user_style.css"
									type="text/css" rel="stylesheet">
								<a href="javascript:;" class="aw-load-more-content warmming"
									style="display: none" onclick="reload_list(0);"
									id="new_actions_tip"><span><span id="new_action_num"></span>
										条新动态, 点击查看</span></a>

								<script type="text/javascript">
									if (typeof (check_actions_new) == 'function') {
										if (typeof checkactionsnew_handle != 'undefined') {
											clearInterval(checkactionsnew_handle);
										}

										checkactionsnew_handle = setInterval(
												function() {
													check_actions_new('0',
															'1423220256');
												}, 60000);
									}
								</script>



								<div class="aw-item" data-history-id="">


									<div class="aw-item-left">


										<a data-id="15740"
											class="wd100 aw-user-img aw-border-radius-5"
											href="http://ask.iheima.com/?/people/ourcun"><img
											src="http://ask.iheima.com/static/common/avatar-mid-img.png"
											title="李兴华" alt="李兴华" style="width: 50px; height: 50px"></a>
										<!-- 投票栏 -->

										<div class="aw-mod-body clearfix"></div>

										<!-- end 投票栏 -->

									</div>


									<div class="aw-item-right">

										<div class="aw-mod-head">
											<p>
												<span class=" pull-right"> 1970-01-01 08:00 </span>
											</p>
											<p class="aw-text-color-999 aw-agree-by  hide">赞同来自:</p>


											<a class="btn btn-default btn-mini pull-right "
												onclick="focus_question($(this),5834);">取消关注</a>
											<h4>
												<a href="http://ask.iheima.com/?/question/5834">组织一个团队，创我们的事业，有吗?</a>
											</h4>


										</div>
										<div class="aw-mod-body clearfix">

											<div class="pull-left ">
												<div style="clear: both">

													<!-- 评论内容 -->
													<div id="detail_" class="markitup-box"></div>

												</div>

												<div id="detail_more_" class="hide markitup-box"
													style="width: 650px;">
													<a href="javascript:;" class="showMore"
														onclick="content_switcher($('#detail_more_'),$('#detail_'));">
														收起 </a>
												</div>

											</div>
											<!-- end 评论内容 -->
										</div>
										<!-- 社交操作 -->

										<div class="aw-dynamic-topic-meta" style="clear: both">
											<a class="aw-add-comment aw-text-color-999"
												href="javascript:void();"><i class="fa fa-comment"></i>0
												条评论</a> <a href="http://ask.iheima.com/?/question/5834"
												title="回答数" class="aw-text-color-999"><i
												class="fa fa-pencil-square"></i> 7 条回答</a> <a
												href="javascript:void();" title="浏览数"
												class="aw-text-color-999"><i class="fa fa-eye"></i> 1034
												人浏览</a> <a data-toggle="dropdown"
												class="aw-text-color-999 dropdown-toggle"
												onclick="$.dialog('shareOut', {item_type:'question', item_id:5834});"><i
												class="fa fa-share"></i>分享</a>
										</div>
										<!-- end 社交操作 -->
									</div>

								</div>


								<div class="aw-item" data-history-id="">


									<div class="aw-item-left">


										<a data-id="11540"
											class="wd100 aw-user-img aw-border-radius-5"
											href="http://ask.iheima.com/?/people/wertyliii"><img
											src="http://ask.iheima.com/static/common/avatar-mid-img.png"
											title="wertyliii" alt="wertyliii"
											style="width: 50px; height: 50px"></a>
										<!-- 投票栏 -->

										<div class="aw-mod-body clearfix"></div>

										<!-- end 投票栏 -->

									</div>


									<div class="aw-item-right">

										<div class="aw-mod-head">
											<p>
												<span class=" pull-right"> 1970-01-01 08:00 </span>
											</p>
											<p class="aw-text-color-999 aw-agree-by  hide">赞同来自:</p>


											<a class="btn btn-default btn-mini pull-right "
												onclick="focus_question($(this),5549);">取消关注</a>
											<h4>
												<a href="http://ask.iheima.com/?/question/5549">想做一个app，安卓还是IOS好?</a>
											</h4>


										</div>
										<div class="aw-mod-body clearfix">

											<div class="pull-left ">
												<div style="clear: both">

													<!-- 评论内容 -->
													<div id="detail_" class="markitup-box"></div>

												</div>

												<div id="detail_more_" class="hide markitup-box"
													style="width: 650px;">
													<a href="javascript:;" class="showMore"
														onclick="content_switcher($('#detail_more_'),$('#detail_'));">
														收起 </a>
												</div>

											</div>
											<!-- end 评论内容 -->
										</div>
										<!-- 社交操作 -->

										<div class="aw-dynamic-topic-meta" style="clear: both">
											<a class="aw-add-comment aw-text-color-999"
												href="javascript:void();"><i class="fa fa-comment"></i>0
												条评论</a> <a href="http://ask.iheima.com/?/question/5549"
												title="回答数" class="aw-text-color-999"><i
												class="fa fa-pencil-square"></i> 2 条回答</a> <a
												href="javascript:void();" title="浏览数"
												class="aw-text-color-999"><i class="fa fa-eye"></i> 390
												人浏览</a> <a data-toggle="dropdown"
												class="aw-text-color-999 dropdown-toggle"
												onclick="$.dialog('shareOut', {item_type:'question', item_id:5549});"><i
												class="fa fa-share"></i>分享</a>
										</div>
										<!-- end 社交操作 -->
									</div>

								</div>


								<div class="aw-item" data-history-id="">


									<div class="aw-item-left">


										<a data-id="11540"
											class="wd100 aw-user-img aw-border-radius-5"
											href="http://ask.iheima.com/?/people/wertyliii"><img
											src="http://ask.iheima.com/static/common/avatar-mid-img.png"
											title="wertyliii" alt="wertyliii"
											style="width: 50px; height: 50px"></a>
										<!-- 投票栏 -->

										<div class="aw-mod-body clearfix"></div>

										<!-- end 投票栏 -->

									</div>


									<div class="aw-item-right">

										<div class="aw-mod-head">
											<p>
												<span class=" pull-right"> 1970-01-01 08:00 </span>
											</p>
											<p class="aw-text-color-999 aw-agree-by  hide">赞同来自:</p>


											<a class="btn btn-default btn-mini pull-right "
												onclick="focus_question($(this),5548);">取消关注</a>
											<h4>
												<a href="http://ask.iheima.com/?/question/5548">现在做互联网创业有前途么?</a>
											</h4>


										</div>
										<div class="aw-mod-body clearfix">

											<div class="pull-left ">
												<div style="clear: both">

													<!-- 评论内容 -->
													<div id="detail_" class="markitup-box"></div>

												</div>

												<div id="detail_more_" class="hide markitup-box"
													style="width: 650px;">
													<a href="javascript:;" class="showMore"
														onclick="content_switcher($('#detail_more_'),$('#detail_'));">
														收起 </a>
												</div>

											</div>
											<!-- end 评论内容 -->
										</div>
										<!-- 社交操作 -->

										<div class="aw-dynamic-topic-meta" style="clear: both">
											<a class="aw-add-comment aw-text-color-999"
												href="javascript:void();"><i class="fa fa-comment"></i>0
												条评论</a> <a href="http://ask.iheima.com/?/question/5548"
												title="回答数" class="aw-text-color-999"><i
												class="fa fa-pencil-square"></i> 1 条回答</a> <a
												href="javascript:void();" title="浏览数"
												class="aw-text-color-999"><i class="fa fa-eye"></i> 370
												人浏览</a> <a data-toggle="dropdown"
												class="aw-text-color-999 dropdown-toggle"
												onclick="$.dialog('shareOut', {item_type:'question', item_id:5548});"><i
												class="fa fa-share"></i>分享</a>
										</div>
										<!-- end 社交操作 -->
									</div>

								</div>


								<div class="aw-item" data-history-id="">


									<div class="aw-item-left">


										<a data-id="2789" class="wd100 aw-user-img aw-border-radius-5"
											href="http://ask.iheima.com/?/people/wowklaus"><img
											src="http://ask.iheima.com/uploads/avatar/000/00/27/89_avatar_mid.jpg"
											title="wowklaus" alt="wowklaus"
											style="width: 50px; height: 50px"></a>
										<!-- 投票栏 -->

										<div class="aw-mod-body clearfix"></div>

										<!-- end 投票栏 -->

									</div>


									<div class="aw-item-right">

										<div class="aw-mod-head">
											<p>
												<span class=" pull-right"> 1970-01-01 08:00 </span>
											</p>
											<p class="aw-text-color-999 aw-agree-by  hide">赞同来自:</p>


											<a class="btn btn-default btn-mini pull-right "
												onclick="focus_question($(this),4226);">取消关注</a>
											<h4>
												<a href="http://ask.iheima.com/?/question/4226">房地产派单不知道如何发展下去?</a>
											</h4>


										</div>
										<div class="aw-mod-body clearfix">

											<div class="pull-left ">
												<div style="clear: both">

													<!-- 评论内容 -->
													<div id="detail_" class="markitup-box"></div>

												</div>

												<div id="detail_more_" class="hide markitup-box"
													style="width: 650px;">
													<a href="javascript:;" class="showMore"
														onclick="content_switcher($('#detail_more_'),$('#detail_'));">
														收起 </a>
												</div>

											</div>
											<!-- end 评论内容 -->
										</div>
										<!-- 社交操作 -->

										<div class="aw-dynamic-topic-meta" style="clear: both">
											<a class="aw-add-comment aw-text-color-999"
												href="javascript:void();"><i class="fa fa-comment"></i>0
												条评论</a> <a href="http://ask.iheima.com/?/question/4226"
												title="回答数" class="aw-text-color-999"><i
												class="fa fa-pencil-square"></i> 9 条回答</a> <a
												href="javascript:void();" title="浏览数"
												class="aw-text-color-999"><i class="fa fa-eye"></i> 823
												人浏览</a> <a data-toggle="dropdown"
												class="aw-text-color-999 dropdown-toggle"
												onclick="$.dialog('shareOut', {item_type:'question', item_id:4226});"><i
												class="fa fa-share"></i>分享</a>
										</div>
										<!-- end 社交操作 -->
									</div>

								</div>


							</div>

							<!-- 加载更多内容 -->
							<a id="bp_more" class="aw-load-more-content"> <span>更多...</span>
							</a>
							<!-- end 加载更多内容 -->
						</div>
					</div>
					<!-- 侧边栏 -->
					<div class="col-sm-12 col-md-3 aw-side-bar hidden-xs hidden-sm">

						<div class="aw-side-bar-mod side-nav">
							<h2 class="aw-content-title">个人操作中心</h2>
							<div class="aw-mod-body">
								<ul>
									<li><a href="/index.php#all" rel="all"><i
											class="fa fa-list-alt"></i>最新动态</a></li>
									<li><a href="/index.php#draft_list__draft"
										rel="draft_list__draft"><i class="fa fa-file"></i>我的草稿</a></li>
									<li><a href="http://ask.iheima.com/?/favorite/"><i
											class="fa fa-bookmark"></i>我收藏的回复</a></li>
									<li class="active"><a href="/index.php#all__focus"
										rel="all__focus" class="active"><i
											class="fa fa-check-square-o"></i>我关注的问题</a></li>
									<li><a href="/index.php#all__public" rel="all__public"><i
											class="fa fa-list"></i>所有问题</a></li>
									<li><a href="/index.php#invite_list__invite"
										rel="invite_list__invite"><i class="fa fa-location-arrow"></i>邀请我回答的问题</a></li>
								</ul>
							</div>
						</div>

						<!--<div class="aw-side-bar-mod invite-join">
						<div class="aw-mod-head">
							<h3>邀请好友加入社区<em class="badge badge-info">5</em></h3>
						</div>
						<div class="aw-mod-body">
							<a href="http://www.iheima.com/home.php?mod=spacecp&ac=invite" class="btn btn-mini btn-success">发送邀请</a>
						</div>
					</div>-->


						<!--<div class="aw-side-bar-mod interest_user">
	<div class="aw-mod-head"><h3>可能感兴趣的人或话题</h3></div>
	<div class="aw-mod-body">
	    	    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/susans" data-id="18358" class="aw-user-name"><img alt="苏三说" src="http://ask.iheima.com/uploads/avatar/000/01/83/58_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/susans" data-id="18358" class="aw-user-name"><span>苏三说</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 18358);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/startup_xu" data-id="16379" class="aw-user-name"><img alt="startup_xu" src="http://ask.iheima.com/uploads/avatar/000/01/63/79_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/startup_xu" data-id="16379" class="aw-user-name"><span>startup_xu</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 16379);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/O2O'>O2O</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/%E8%B6%A3%E7%81%AB%E6%98%9F" data-id="16564" class="aw-user-name"><img alt="趣火星" src="http://ask.iheima.com/uploads/avatar/000/01/65/64_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/%E8%B6%A3%E7%81%AB%E6%98%9F" data-id="16564" class="aw-user-name"><span>趣火星</span><i class="icon-v i-ve" title="企业认证"></i></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 16564);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/6'>互联网</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/liudezhong" data-id="15725" class="aw-user-name"><img alt="刘德仲" src="http://ask.iheima.com/uploads/avatar/000/01/57/25_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/liudezhong" data-id="15725" class="aw-user-name"><span>刘德仲</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 15725);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/qw0111" data-id="16978" class="aw-user-name"><img alt="qw0111" src="http://ask.iheima.com/uploads/avatar/000/01/69/78_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/qw0111" data-id="16978" class="aw-user-name"><span>qw0111</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 16978);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/nanyibang" data-id="17917" class="aw-user-name"><img alt="gucqc" src="http://ask.iheima.com/uploads/avatar/000/01/79/17_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/nanyibang" data-id="17917" class="aw-user-name"><span>gucqc</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 17917);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/%E8%83%A1%E7%BF%94" data-id="15682" class="aw-user-name"><img alt="胡翔" src="http://ask.iheima.com/uploads/avatar/000/01/56/82_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/%E8%83%A1%E7%BF%94" data-id="15682" class="aw-user-name"><span>胡翔</span><i class="icon-v i-ve" title="企业认证"></i></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 15682);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/hcfong" data-id="15991" class="aw-user-name"><img alt="hcfong" src="http://ask.iheima.com/static/common/avatar-min-img.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/hcfong" data-id="15991" class="aw-user-name"><span>hcfong</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 15991);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/%E8%93%9D%E8%89%B2%E4%B8%8A%E7%A9%BA" data-id="17471" class="aw-user-name"><img alt="蓝色上空" src="http://ask.iheima.com/uploads/avatar/000/01/74/71_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/%E8%93%9D%E8%89%B2%E4%B8%8A%E7%A9%BA" data-id="17471" class="aw-user-name"><span>蓝色上空</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 17471);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    		<dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/people/oolige" data-id="16970" class="aw-user-name"><img alt="章立" src="http://ask.iheima.com/uploads/avatar/000/01/69/70_avatar_min.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/people/oolige" data-id="16970" class="aw-user-name"><span>章立</span><i class="icon-v" title="个人认证"></i></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="follow_people($(this), 16970);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p>TA 也关注 <a href='http://ask.iheima.com/?/topic/2'>创业</a></p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E4%BA%92%E8%81%94%E7%BD%91%E5%B7%A8%E5%A4%B4"><img alt="互联网巨头" src="http://ask.iheima.com/static/common/topic-mid-img.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E4%BA%92%E8%81%94%E7%BD%91%E5%B7%A8%E5%A4%B4"><span>互联网巨头</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 44);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/xiaochuang" class="aw-user-name" data-id="57">小创</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E8%8B%B9%E6%9E%9C%E4%BA%A7%E5%93%81"><img alt="苹果产品" src="http://ask.iheima.com/uploads/topic/20140910/c2bb91faa78acc9e9b1354721f79c33b_50_50.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E8%8B%B9%E6%9E%9C%E4%BA%A7%E5%93%81"><span>苹果产品</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 121);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/xiaochuang" class="aw-user-name" data-id="57">小创</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E6%89%BE%E5%90%88%E4%BC%99%E4%BA%BA"><img alt="找合伙人" src="http://ask.iheima.com/static/common/topic-mid-img.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E6%89%BE%E5%90%88%E4%BC%99%E4%BA%BA"><span>找合伙人</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 967);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/heimage" class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/jingdong"><img alt="京东" src="http://ask.iheima.com/uploads/topic/20140813/ae5bf6a6a837738c74ab6f931f1b3e2b_50_50.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/jingdong"><span>京东</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 199);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/xiaochuang" class="aw-user-name" data-id="57">小创</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/77"><img alt="合伙人" src="http://ask.iheima.com/uploads/topic/20140724/e74dbc2a138b27ca5a78eedc86180159_50_50.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/77"><span>合伙人</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 77);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/heimage" class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/95"><img alt="移动互联网" src="http://ask.iheima.com/uploads/topic/20141125/38b433546f7bccdaae9d3416471718df_50_50.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/95"><span>移动互联网</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 95);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/heimage" class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/3"><img alt="融资" src="http://ask.iheima.com/uploads/topic/20140723/104cc849ad497d5f746c3a7c124a0787_50_50.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/3"><span>融资</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 3);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/heimage" class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E9%BB%91%E9%A9%AC%E5%BE%AE%E8%B7%AF%E6%BC%94"><img alt="黑马微路演" src="http://ask.iheima.com/uploads/topic/20140812/a4680f52f318d030e1db9ed0b3376c21_50_50.jpg" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E9%BB%91%E9%A9%AC%E5%BE%AE%E8%B7%AF%E6%BC%94"><span>黑马微路演</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 880);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/xiaochuang" class="aw-user-name" data-id="57">小创</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E5%88%9B%E4%B8%9A%E5%88%9D%E6%9C%9F"><img alt="创业初期" src="http://ask.iheima.com/static/common/topic-mid-img.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E5%88%9B%E4%B8%9A%E5%88%9D%E6%9C%9F"><span>创业初期</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 78);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/heimage" class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题</p>
			</dd>
		</dl>
					    	    <dl>
			<dt class="pull-left aw-border-radius-5">
				<a href="http://ask.iheima.com/?/topic/%E5%88%9B%E4%B8%9A%E5%85%AC%E5%8F%B8"><img alt="创业公司" src="http://ask.iheima.com/uploads/topic/20140723/1de3e596ccf4f088cbd4ebe5f7cc4abc_50_50.png" /></a>
			</dt>
			<dd class="pull-left">
				<a href="http://ask.iheima.com/?/topic/%E5%88%9B%E4%B8%9A%E5%85%AC%E5%8F%B8"><span>创业公司</span></a><i data-placement="bottom" title="" data-toggle="tooltip" class="fa fa-check aw-active" data-original-title="关注" onclick="focus_topic($(this), 51);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
				<p><a href="http://ask.iheima.com/?/people/xiaochuang" class="aw-user-name" data-id="57">小创</a> 关注了该话题</p>
			</dd>
		</dl>
					</div>
</div>
-->

						<!-- 可能感兴趣的人/或话题1 -->

						<div class="aw-side-bar-mod">
							<div class="aw-mod-head">
								<h3>专访</h3>
							</div>
							<div class="aw-mod-body">
								<ul>
									<li><a href="http://ask.iheima.com/?/yiwenyida/5825"
										target="_blank">对话黑马基金胡翔：创业者如何融到第一笔钱?</a></li>
									<li><a href="http://ask.iheima.com/?/yiwenyida/5812"
										target="_blank">对话明势资本黄明明：2015年是资本寒冬么?</a></li>
									<li><a href="http://ask.iheima.com/?/yiwenyida/5709"
										target="_blank">【黑问访谈】创业者，该如何管理你的财务?</a></li>
								</ul>
							</div>
						</div>


						<div class="aw-side-bar-mod ">
							<h2 class="aw-content-title">
								可能感兴趣的话题<span class="pull-right"><a
									href="http://ask.iheima.com/?/topic/" title="">更多》</a></span>
							</h2>












							<dl>
								<dt class="pull-left aw-border-radius-5">
									<a
										href="http://ask.iheima.com/?/topic/%E4%BA%92%E8%81%94%E7%BD%91%E5%B7%A8%E5%A4%B4"><img
										src="http://ask.iheima.com/static/common/topic-mid-img.png"
										alt="互联网巨头"></a>
								</dt>
								<dd class="pull-left">
									<a
										href="http://ask.iheima.com/?/topic/%E4%BA%92%E8%81%94%E7%BD%91%E5%B7%A8%E5%A4%B4"><span>互联网巨头</span></a><i
										data-placement="bottom" title="" data-toggle="tooltip"
										class="fa fa-check aw-active" data-original-title="关注"
										onclick="focus_topic($(this), 44);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
									<p>
										<a href="http://ask.iheima.com/?/people/xiaochuang"
											class="aw-user-name" data-id="57">小创</a> 关注了该话题
									</p>
								</dd>
							</dl>


							<dl>
								<dt class="pull-left aw-border-radius-5">
									<a
										href="http://ask.iheima.com/?/topic/%E8%8B%B9%E6%9E%9C%E4%BA%A7%E5%93%81"><img
										src="http://ask.iheima.com/uploads/topic/20140910/c2bb91faa78acc9e9b1354721f79c33b_50_50.jpg"
										alt="苹果产品"></a>
								</dt>
								<dd class="pull-left">
									<a
										href="http://ask.iheima.com/?/topic/%E8%8B%B9%E6%9E%9C%E4%BA%A7%E5%93%81"><span>苹果产品</span></a><i
										data-placement="bottom" title="" data-toggle="tooltip"
										class="fa fa-check aw-active" data-original-title="关注"
										onclick="focus_topic($(this), 121);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
									<p>
										<a href="http://ask.iheima.com/?/people/xiaochuang"
											class="aw-user-name" data-id="57">小创</a> 关注了该话题
									</p>
								</dd>
							</dl>


							<dl>
								<dt class="pull-left aw-border-radius-5">
									<a
										href="http://ask.iheima.com/?/topic/%E6%89%BE%E5%90%88%E4%BC%99%E4%BA%BA"><img
										src="http://ask.iheima.com/static/common/topic-mid-img.png"
										alt="找合伙人"></a>
								</dt>
								<dd class="pull-left">
									<a
										href="http://ask.iheima.com/?/topic/%E6%89%BE%E5%90%88%E4%BC%99%E4%BA%BA"><span>找合伙人</span></a><i
										data-placement="bottom" title="" data-toggle="tooltip"
										class="fa fa-check aw-active" data-original-title="关注"
										onclick="focus_topic($(this), 967);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
									<p>
										<a href="http://ask.iheima.com/?/people/heimage"
											class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题
									</p>
								</dd>
							</dl>


							<dl>
								<dt class="pull-left aw-border-radius-5">
									<a href="http://ask.iheima.com/?/topic/jingdong"><img
										src="http://ask.iheima.com/uploads/topic/20140813/ae5bf6a6a837738c74ab6f931f1b3e2b_50_50.jpg"
										alt="京东"></a>
								</dt>
								<dd class="pull-left">
									<a href="http://ask.iheima.com/?/topic/jingdong"><span>京东</span></a><i
										data-placement="bottom" title="" data-toggle="tooltip"
										class="fa fa-check aw-active" data-original-title="关注"
										onclick="focus_topic($(this), 199);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
									<p>
										<a href="http://ask.iheima.com/?/people/xiaochuang"
											class="aw-user-name" data-id="57">小创</a> 关注了该话题
									</p>
								</dd>
							</dl>


							<dl>
								<dt class="pull-left aw-border-radius-5">
									<a href="http://ask.iheima.com/?/topic/77"><img
										src="http://ask.iheima.com/uploads/topic/20140724/e74dbc2a138b27ca5a78eedc86180159_50_50.png"
										alt="合伙人"></a>
								</dt>
								<dd class="pull-left">
									<a href="http://ask.iheima.com/?/topic/77"><span>合伙人</span></a><i
										data-placement="bottom" title="" data-toggle="tooltip"
										class="fa fa-check aw-active" data-original-title="关注"
										onclick="focus_topic($(this), 77);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>
									<p>
										<a href="http://ask.iheima.com/?/people/heimage"
											class="aw-user-name" data-id="5636">黑马哥</a> 关注了该话题
									</p>
								</dd>
							</dl>





						</div>


						<!-- 可能感兴趣的人/或话题1 -->
						<div class="aw-side-bar-mod interest_user">

							<h2 class="aw-content-title">
								可能感兴趣的人<span class="pull-right"><a
									href="http://ask.iheima.com/?/people/list/group_id-105"
									title="">更多》</a></span>
							</h2>
							<div class="aw-mod-body">

								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a class="aw-user-name" data-id="18358"
											href="http://ask.iheima.com/?/people/susans"><img
											src="http://ask.iheima.com/static/common/avatar-min-img.png"
											alt="http://ask.iheima.com/uploads/avatar/000/01/83/58_avatar_min.jpg"></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name" data-id="18358"
											href="http://ask.iheima.com/?/people/susans"><span>苏三说</span></a>

										<!--<span onclick="follow_people($(this), 18358);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');" class="pull-right aw-user-fork" >+关注</span>-->

										<i data-placement="bottom" title="" data-toggle="tooltip"
											class="fa fa-check aw-active" data-original-title="关注"
											onclick="follow_people($(this), 18358);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>



										<p class="aw-user-intro"></p>

										<p>
											TA 也关注 <a href="http://ask.iheima.com/?/topic/2">创业</a>
										</p>
									</dd>
								</dl>

								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a class="aw-user-name" data-id="16379"
											href="http://ask.iheima.com/?/people/startup_xu"><img
											src="http://ask.iheima.com/static/common/avatar-min-img.png"
											alt="http://ask.iheima.com/uploads/avatar/000/01/63/79_avatar_min.jpg"></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name" data-id="16379"
											href="http://ask.iheima.com/?/people/startup_xu"><span>startup_xu</span></a>

										<!--<span onclick="follow_people($(this), 16379);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');" class="pull-right aw-user-fork" >+关注</span>-->

										<i data-placement="bottom" title="" data-toggle="tooltip"
											class="fa fa-check aw-active" data-original-title="关注"
											onclick="follow_people($(this), 16379);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>



										<p class="aw-user-intro"></p>

										<p>
											TA 也关注 <a href="http://ask.iheima.com/?/topic/O2O">O2O</a>
										</p>
									</dd>
								</dl>

								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a class="aw-user-name" data-id="16564"
											href="http://ask.iheima.com/?/people/%E8%B6%A3%E7%81%AB%E6%98%9F"><img
											src="http://ask.iheima.com/static/common/avatar-min-img.png"
											alt="http://ask.iheima.com/uploads/avatar/000/01/65/64_avatar_min.jpg"></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name" data-id="16564"
											href="http://ask.iheima.com/?/people/%E8%B6%A3%E7%81%AB%E6%98%9F"><span>趣火星</span><i
											class="icon-v i-ve" title="企业认证"></i></a>

										<!--<span onclick="follow_people($(this), 16564);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');" class="pull-right aw-user-fork" >+关注</span>-->

										<i data-placement="bottom" title="" data-toggle="tooltip"
											class="fa fa-check aw-active" data-original-title="关注"
											onclick="follow_people($(this), 16564);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>



										<p class="aw-user-intro"></p>

										<p>
											TA 也关注 <a href="http://ask.iheima.com/?/topic/6">互联网</a>
										</p>
									</dd>
								</dl>

								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a class="aw-user-name" data-id="15725"
											href="http://ask.iheima.com/?/people/liudezhong"><img
											src="http://ask.iheima.com/static/common/avatar-min-img.png"
											alt="http://ask.iheima.com/uploads/avatar/000/01/57/25_avatar_min.jpg"></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name" data-id="15725"
											href="http://ask.iheima.com/?/people/liudezhong"><span>刘德仲</span></a>

										<!--<span onclick="follow_people($(this), 15725);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');" class="pull-right aw-user-fork" >+关注</span>-->

										<i data-placement="bottom" title="" data-toggle="tooltip"
											class="fa fa-check aw-active" data-original-title="关注"
											onclick="follow_people($(this), 15725);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>



										<p class="aw-user-intro"></p>

										<p>
											TA 也关注 <a href="http://ask.iheima.com/?/topic/2">创业</a>
										</p>
									</dd>
								</dl>

								<dl>
									<dt class="pull-left aw-border-radius-5">
										<a class="aw-user-name" data-id="16978"
											href="http://ask.iheima.com/?/people/qw0111"><img
											src="http://ask.iheima.com/static/common/avatar-min-img.png"
											alt="http://ask.iheima.com/uploads/avatar/000/01/69/78_avatar_min.jpg"></a>
									</dt>
									<dd class="pull-left">
										<a class="aw-user-name" data-id="16978"
											href="http://ask.iheima.com/?/people/qw0111"><span>qw0111</span></a>

										<!--<span onclick="follow_people($(this), 16978);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');" class="pull-right aw-user-fork" >+关注</span>-->

										<i data-placement="bottom" title="" data-toggle="tooltip"
											class="fa fa-check aw-active" data-original-title="关注"
											onclick="follow_people($(this), 16978);ajax_request(G_BASE_URL + '/account/ajax/clean_user_recommend_cache/');"></i>



										<p class="aw-user-intro"></p>

										<p>
											TA 也关注 <a href="http://ask.iheima.com/?/topic/2">创业</a>
										</p>
									</dd>
								</dl>



								<div class="aw-side-bar-mod">
									<div class="aw-mod-head">
										<h3>黑问公告栏</h3>
									</div>
									<div class="aw-mod-body">
										<ul>
											<li><a href="http://ask.iheima.com/?/page/zhuanlan"
												target="_blank">在【黑问】如何发布专栏文章？</a></li>
											<li><a href="http://ask.iheima.com/?/page/huida"
												target="_blank">在【黑问】如何编辑一个好答案？</a></li>
											<li><a href="http://ask.iheima.com/?/page/tiwen"
												target="_blank"> 在【黑问】如何提一个好问题？</a></li>
											<li><a href="http://ask.iheima.com/?/page/help"
												target="_blank">正确使用【黑问】的姿势公告</a></li>
											<li><a href="http://ask.iheima.com/?/page/jifen"
												target="_blank">如何在【黑问】赚取积分？</a></li>
										</ul>
									</div>
								</div>
							</div>

							<!--
					<div class="aw-side-bar-mod invite-join">
						<div class="aw-mod-head">
							<h3>邀请好友加入社区<em class="badge badge-info">5</em></h3>
						</div>
						<div class="aw-mod-body">
							<a href="http://www.iheima.com/home.php?mod=spacecp&ac=invite" class="btn btn-mini btn-success">发送邀请</a>
						</div>
					</div>-->
						</div>
					</div>
					<!-- end 侧边栏 -->




				</div>
			</div>
		</div>
	</div>



	<jsp:include page="../footer.jsp" />
</body>
</html>