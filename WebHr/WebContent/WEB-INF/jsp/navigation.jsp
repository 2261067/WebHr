
<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>

<div class="aw-top-menu-wrap">
	<div class="aw-wecenter aw-top-menu clearfix">
		<div class="container">
			<!-- logo -->
			<!-- <div class="aw-logo hidden-xs">
					<a href="http://www.iheima.com" title="" target=""></a>
				</div> -->
			<!-- end logo -->

			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
					<button class="navbar-toggle pull-left">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<nav role="navigation"
					class="collapse navbar-collapse bs-navbar-collapse">
					<ul class="nav navbar-nav" id="first-page-nav" style="">
						<a class="hr-brand" href="/WebHr/redirect.do?page=question">HR+</a>
						<li>

							<form class="navbar-form navbar-left" action="/WebHr/search/searchMore.do"
								method="get">
								<div class="hr-input-group form-inline">
									<input type="text" class="form-control fa-search"
										style="width: 320px; background-color: #fff;"
										id="hr_serachBox" name="q" placeholder="输入搜索的问题或活动">
									<!--  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button> 
									<span class="input-group-btn">
										<button type="submit" class="btn btn-default">go</button>
									</span> -->
									<div class="aw-top-search-box  hidden-xs hidden-sm">
										<div class="clearfix"></div>
										<div class="aw-dropdown" style="display: none;">
											<i class="i-dropdown-triangle"></i>
											<p class="title">输入关键字进行搜索</p>
											<ul class="aw-dropdown-list"></ul>
											<p>
											<script type="text/javascript">
											function searchMore(){
												
												var query=$('#hr_serachBox').val();
												if(!query||query==""){alert("请输入搜索关键词!");return;}
												window.location.href=G_BASE_URL+"/search/searchMore.do?q="+query; 
											}
											</script>
											<a href="javascript:;<? } else { ?>publish"
													onclick="searchMore();"
													class="pull-right btn btn-normal btn-success ">更多...</a>
												<a href="javascript:;<? } else { ?>publish"
													onclick="$('#header_publish_question').click();"
													class="pull-right btn btn-normal btn-success publish" style="margin-right:5px">提问</a>
											</p>
										</div>
									</div>
								</div>
							</form>

						</li>
						<li>
							<button class="btn btn-info navbar-btn"
								id="header_publish_question"
								onClick="$.dialog('publish', {'category_enable':'0', 'category_id':'0', 'topic_title':'','topic_enable_edit':'1'}); return false;">提问</button>
						</li>


						<li><a href="/WebHr/redirect.do?page=question">首页</a></li>

						<li><a href="/WebHr/redirect.do?page=topics">话题</a></li>
						<li><a href="/WebHr/activity.do">活动</a></li>
						<li><a href="/WebHr/redirect.do?page=article">专栏</a></li>
						<li><a href="/WebHr/redirect.do?page=userFocusDynamic">动态</a></li>

					</ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->

				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<!--<php if ($this->user_id) { ?>-->

			<!--<php } ?>-->
			<!-- end 发起 -->
		</div>
	</div>
</div>
