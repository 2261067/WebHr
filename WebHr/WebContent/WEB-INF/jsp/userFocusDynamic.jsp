
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>

<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title>HR+问题</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">


<jsp:include page="header.jsp" />
<link href="/WebHr/css/pagination.css" rel="stylesheet">
<script type="text/javascript" src="/WebHr/js/jquery.pagination.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-topic.js"></script>
<script type="text/javascript" src="/WebHr/js/hr-dynamic.js"></script>
<script type="text/javascript">
	//var pagecount=${pageCount};
	//var currentPage=${currentPage};
	
	var pageSize=10;
	var uf_nextPage=1;
	var data_url="Dynamic/ajax/getFocusDynamicData.do";
	var dynamicParams = {
		page : 1,
		pageSize:pageSize,
		//pageCount:pagecount,
		moduleName : "",
		containerName : "#main_contents",
		url : data_url,
		type : TYPE_DEFAULT_DATA,
		dynamicId:Math.uuid()
	};

			$(function() {getDynamicPageList(dynamicParams, dynamicParams.moduleName,
					dynamicParams.containerName, dynamicParams.url, dynamicParams.type,getUserDynamicData);});
	
	//$(document).ready(getQuestionPageList(params,"#questionModule","div.aw-question-list","getQuestionByTime.do",1));
</script>
</head>

<body class="grey-bg">

	<jsp:include page="navigation.jsp" />





	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row aw-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question">


						
						<div class="aw-mod aw-dynamic-topic clearfix" id="dynamic_list">
							<div class="aw-mod-head">
								<h2 id="main_title">
									<i class="fa fa-list-alt"></i>最新动态
								</h2>
							</div>

							<div class="aw-mod-body" id="main_contents">
						
								<a href="javascript:;" class="aw-load-more-content warmming"
									style="display: none" onclick="reload_list(0);"
									id="new_actions_tip"><span><span id="new_action_num"></span>
										条新动态, 点击查看</span></a>

								<script type="text/javascript">
									if (typeof (check_actions_new) == 'function') {
										if (typeof checkactionsnew_handle != 'undefined') {
											clearInterval(checkactionsnew_handle);
										}

										checkactionsnew_handle = setInterval(
												function() {
													check_actions_new('0',
															'1422586622');
												}, 60000);
									}
								</script>



								</div>

							<!-- 加载更多内容 -->
							<a id="bp_more" onclick="getNextDynamicPage(dynamicParams,pageSize,data_url,getUserDynamicData)" class="aw-load-more-content"> <span>更多...</span>
							</a>
							<!-- end 加载更多内容 -->
						</div>
					</div>
					<!-- 侧边栏 -->
					<div class="col-sm-12 col-md-3 aw-side-bar hidden-xs hidden-sm">

						<div class="aw-side-bar-mod side-nav">
							<h2 class="aw-content-title">个人操作中心</h2>
							<div class="aw-mod-body">
								<ul>
									<li class="active"><a href="./redirect.do?page=userFocusDynamic" rel="all"
										class="active"><i class="fa fa-list-alt"></i>最新动态</a></li>
									<li class="hide"><a href=""
										rel="draft_list__draft"><i class="fa fa-file"></i>我的草稿</a></li>
									<li class="hide"><a href=""><i
											class="fa fa-bookmark"></i>我收藏的回复</a></li>
									<li><a href="./redirect.do?page=dynamic/userFocusQuestions" rel="all__focus"><i
											class="fa fa-check-square-o"></i>我关注的问题</a></li>
									<li class="hide"><a href="" rel="all__public"><i
											class="fa fa-list"></i>所有问题</a></li>
									<li><a href="./redirect.do?page=dynamic/userInvitedQuestions"
										rel="invite_list__invite"><i class="fa fa-location-arrow"></i>邀请我回答的问题</a></li>
								</ul>
							</div>
						</div>

						

						</div>
					<!-- end 侧边栏 -->




				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>