<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
<title>后台管理--修改用户信息</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />


<jsp:include page="adminHeader.jsp" />
<script type="text/javascript" src="/WebHr/js/admin_usermanage.js"></script>
<script type="text/javascript" src="/WebHr/js/admin_userupdate.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		userUpdate();
	});
</script>

</head>
<body>

	<!-- top顶部 -->
	<jsp:include page="adminTop.jsp" />
	<!-- ENDtop顶部 -->

	<div class="container clearfix">

		<!-- 导航栏 -->
		<jsp:include page="adminNavigation.jsp" />
		<!-- END导航栏 -->

		<!-- MAIN -->
		<div class="main-wrap">
			<div class="crumb-wrap">
				<div class="crumb-list">
					<a href="">首页</a><span class="crumb-step">&gt;</span><span
						class="crumb-name">修改用户信息</span>
				</div>
			</div>

			<div class="result-wrap">
				<form action="../WebHr/adminmanage/updateuser.do" method="post"
					id="updateuserform" enctype="multipart/form-data">
					<input class="common-text required" id="userid" name="userid"
						type="text" style="display: none;">
					<table class="insert-tab" width="100%">
						<tbody>
						
							<tr>
								<th>用户ID:</th>
								<td><label id="user_id" ></label></td>
							</tr>

							<tr>
								<th>邮箱：</th>
								<td><input class="common-text required" id="email"
									name="email" size="50" type="text"></td>
							</tr>
							
							<tr>
								<th>密码：</th>
								<td><input type="password" class="common-text required" id="password"
									name="password" size="50"  type="text"></td>
							</tr>
							<tr>
								<th>姓：</th>
								<td><label id="firstname"></label></td>
							</tr>
							<tr>
								<th>名：</th>
								<td><label id="lastname"></label></td>
							</tr>
							<tr>
								<th>手机号：</th>
								<td><label id="phonenumber"></label></td>
							</tr>
							<tr>
								<th>昵称：</th>
								<td><label id="nickname"></label></td>
							</tr>
							
							<tr>
								<th>职位：</th>
								<td><label id="position"></label></td>
							</tr>
							
							<tr>
								<th>工作年限：</th>
								<td><label id="workexperience"></label></td>
							</tr>
							
							<tr>
								<th>注册时间：</th>
								<td><label id="createtime"></label></td>
							</tr>

							<tr>
								<th></th>
								<td><input class="btn btn-primary btn6 mr10" value="修改"
									type="submit"></td>
							</tr>
						</tbody>
					</table>
				</form>

			</div>
		</div>
		<!-- ENDMAIN -->


	</div>



</body>
</html>