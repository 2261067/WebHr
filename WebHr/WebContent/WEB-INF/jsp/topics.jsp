<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>


<html>
<head>
<meta charset="utf-8">
<title>HR+话题</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">


<jsp:include page="header.jsp" />
<script src="/WebHr/js/hr-topic.js"></script>
<script type="text/javascript">
var btnHotTopics=$('#hotTopics');
$(document).ready(getHotTopic(btnHotTopics));

</script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="http://ask.iheima.com/static/js/respond.js"></script>
<![endif]-->
</head>
<body class="grey-bg">
<jsp:include page="navigation.jsp"/>

	<div class="aw-container-wrap">
		<div class="aw-container aw-wecenter">
			<div class="container">
				<div class="row aw-content-wrap aw-topic-content-wrap">
					<div class="col-sm-12 col-md-9 aw-main-content aw-all-question aw-topic-content">
						<!-- 我关注的话题 -->
						<div class="aw-mod aw-topic-list">
							<div class="aw-mod-head">
								<h2 id="topicsType">热门话题</h2>
							</div>
							<div class="aw-mod-body" id="topics-desc">
							</div>

							<div class="page-control clearfix" id="page-control">
							
							</div>
						</div>
						<!-- end 我关注的话题 -->
					</div>
					<!-- 侧边栏 -->
					<div
						class="col-sm-3 col-md-3 aw-side-bar aw-side-bar-topic hidden-sm hidden-xs">
						<!-- 话题侧边栏导航 -->
						<div class="aw-side-bar-mod aw-side-bar-topic-nav hidden-xs">
							<div class="aw-mod-body">
								<ul>
									<li><a id="focusTopics" onclick="getFocusTopic($(this));"><i
											class="fa fa-tags"></i> 我关注的话题</a></li>

									<li><a id="hotTopics" class="" onclick="getHotTopic($(this));"
										><i class="fa fa-fire"></i> 热门话题</a></li>
								</ul>
							</div>
						</div>
						<!-- end 话题侧边栏导航 -->
					
						<!-- 新增话题 -->
						<div class="hide aw-side-bar-mod aw-side-bar-topic-edit">
							<div class="aw-mod-head">
								<h3>新增话题</h3>
							</div>
							<div class="aw-mod-body">
								<a
									href="#"
									data-id="1545" class="aw-topic-name"><span>绩效考核 </span></a> 
							</div>
						</div>
						<!-- end 新增话题 -->
					</div>
					<!-- end 侧边栏 -->
				</div>
			</div>
		</div>
	</div>

	
	<jsp:include page="footer.jsp" />


</body>
</html>