// 获取url中"?"符后参数
function getRequest() {
	var url = location.search; // 获取url中"?"符后的字串
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 获取话题第一页
function getTopicFirstPage() {
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/gettopiccount.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				var topicCount = result.rsm;
				var optInit = getOptionsFromForm();
				getTopicMessage(1, 5);
				$("#Pagination").pagination(topicCount, optInit);
			}

		}, 'json');
	} else {
		alert("请登陆！");
	}

}

// 删除话题
function deleteTopic(topicid, topicname) {
	if (check_admin()) {
		var deletetopicisok = confirm("确定删除话题：" + topicname + "?请慎重！");
		if (deletetopicisok == true) {
			$.get(G_BASE_URL + "/adminmanage/deletetopic.do?topicid=" + topicid,
					function(result) {
						if (result.errno != 1) {
							$.alert(result.err);
						} else {
							alert("成功删除话题：" + topicname + "!");
							window.location.href = window.location.href;// 刷新
						}

					}, 'json');
		}
	}else {
		alert("请登陆！");
	}
	
}

//获取话题展示信息
function getTopicMessage(page, pagesize) {
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/" + page + "/" + pagesize + "/"
				+ "/gettopic.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var topicHeaderHtml = '<tr>' 
										  + '<th>话题名</th>'
										  + '<th>发布时间</th>'
										  + '<th>操作</th>' + '</tr>';
					var topicMainHtml = '';
					$.each(result.rsm.Topic, function(i, item) {

						topicMainHtml += '<tr>'
										+ '<td>' + item.topicName+ '</td>' 
										+ '<td>' + item.createTime + '</td>'
										+ '<td><a class="link-update" href='+G_BASE_URL+'/redirect.do?page=adminTopicAdd>添加</a>&nbsp;&nbsp;'
										+ '<a class="link-del" href="javascript:void(0);" onclick="deleteTopic('+'\''+item.topicId+'\''+','+'\''+item.topicName+'\''+');">删除</a>&nbsp;&nbsp;'
										+'<a class="link-update" href='+G_BASE_URL+'/redirect.do?page=adminTopicUpdate&topicid='+item.topicId+'>修改</a></td>'
										+ '</tr>';
					});
					$(".result-tab").html(topicHeaderHtml + topicMainHtml);
				}
			}

		}, 'json');
	}else{
		alert("请登陆！");
	}
}

//修改话题
function topicUpdate(){
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/topicinfo.do?topicid="+request["topicid"], function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var topic = result.rsm;
					$('#topicid').val(topic.topicId);//隐藏Id
					$('#topicname').val(topic.topicName);
					$('#topicdesc').text(topic.topicDesc);
				}
			}

		}, 'json');
	}else{
		alert("请登陆！");
	}
}

// 分页返回函数
function pageselectCallback(page_index, jq) {
	getTopicMessage(page_index + 1, 5);
	return false;
}

// 得到分页属性
function getOptionsFromForm() {
	var opt = {
		callback : pageselectCallback,
		items_per_page : 5, // 每页显示个数
		num_display_entries : 8, // 显示分页链接个数
		current_page : 0, // 当前页
		num_edge_entries : 2, // 开始和结束显示分页链接个数
		link_to : "#",
		prev_text : "上一页",
		next_text : "下一页",
		ellipse_text : "...",
		prev_show_always : true,
		next_show_always : true
	};
	return opt;
}
