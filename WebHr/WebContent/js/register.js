$(function() {

	var emailissuccess = 0;
//	var usernameissuccess = 0;
	var nicknameissuccess = 0;
	var pwdisok = 0;
	var phoneisok = 0;
	var workexperienceisok = 0;

	/*
	 * 注释的地方主要是为了解决浏览器兼容问题
	 */

	var register = {

		init : function() {
			var _this = register;

			$('#email').on('focus', _this.__emailFocusProcess);
//			$('#username').on('focus', _this.__usernameFocusProcess);

			$("#registerform").submit(function() {
				// 手机号码检测
				_this.__phoneCheckProcess();
				if (phoneisok == 0)
					return false;
				// 检测密码
				_this.__pwdConProcess();
				if (pwdisok == 0)
					return false;

				
/*				// 用户名检测
				_this.__usernameCheckProcess();
				if (usernameissuccess == 0)
					return false;*/
				
				
				// 邮箱检测
				_this.__emailCheckProcess();
				if (emailissuccess == 0)
					return false;
				// 昵称检测
				_this.__nicknameCheckProcess();
				if (nicknameissuccess == 0)
					return false;
				// 工作年限检测
				_this.__workExperienceCheckProcess();
				if (workexperienceisok == 0)
					return false;
				alert("注册成功！");
				return true;
			});
		},

		// 手机号码检测
		__phoneCheckProcess : function() {
			var re = /^1\d{10}$/;
			var phone = $("#phone").val();
			if (re.test(phone)) {
				// $('#phonecheck').hide();
				phoneisok = 1;
			} else {
				// $('#phonecheck').show();
				// var message = "请输入规范的手机号";
				// document.getElementById("phonecheck").innerText = message;
				alert("请输入规范的11位数字手机号!");
				phoneisok = 0;
			}
		},

		// 工作年限检测
		__workExperienceCheckProcess : function() {
			var workexperienceid = $('#workexperienceid').val();
			if (workexperienceid == "0") {
				alert("请选择您的工作年限!");
				workexperienceisok = 0;
			} else {
				workexperienceisok = 1;
			}
		},

		// email检测
		__emailFocusProcess : function() {
			$('#emailcheck').hide();
		},

		__emailCheckProcess : function() {
			var email = $("#email").val();
			var myreg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			if (myreg.test(email)) {
				var _this = register;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "register/" + email + "/emailcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__emailcallback
				});
			} else {
				alert("邮箱格式不正确!");
				emailissuccess = 0;
			}
		},

		__emailcallback : function(data) {

			if (data.errno == 0) {
				// $('#emailcheck').show();
				// var message = "邮箱已被注册";
				// document.getElementById("emailcheck").innerText = message;
				alert("邮箱已被注册!");
				emailissuccess = 0;
			} else {
				$('#emailcheck').hide();
				emailissuccess = 1;
			}
		},

		/*
		// 用户名检测
		__usernameFocusProcess : function() {
			$('#usernamecheck').hide();
		},

		__usernameCheckProcess : function() {
			var username = $("#username").val();
			var _this = register;
			if (username.replace(/( )/g, "").length == 0) {
				alert("用户名不能为空！");
				usernameissuccess = 0;
			} else if (_this.__illegalCharacterCheck(username)) {
				alert("用户名请勿包含非法字符如[#%&$'/ ,;:=!^]");
				usernameissuccess = 0;
			} else {
				var _this = register;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "register/" + username + "/usernamecheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__usernamecallback
				});
			}
		},

		__usernamecallback : function(data) {
			if (data.errno == 0) {
				// $('#usernamecheck').show();
				// var message = "用户名已被注册";
				// document.getElementById("usernamecheck").innerText = message;
				alert("用户名已被注册");
				usernameissuccess = 0;
			} else {
				$('#usernamecheck').hide();
				usernameissuccess = 1;
			}
		},
		*/
		
		
		// 昵称检测
		__nicknameCheckProcess : function() {
			var nickname = $("#nickname").val();
			var _this = register;
			if (nickname.replace(/( )/g, "").length == 0) {
				alert("昵称不能为空！");
				nicknameissuccess = 0;
			} else if (_this.__illegalCharacterCheck(nickname)) {
				alert("昵称请勿包含非法字符如[#%&$'/,;:=!^]或空格");
				nicknameissuccess = 0;
			} else {
				var _this = register;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "register/" + nickname + "/nicknamecheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__nicknamecallback
				});
			}
		},

		__nicknamecallback : function(data) {
			if (data.errno == 0) {
				alert("昵称已被注册");
				nicknameissuccess = 0;
			} else {
				nicknameissuccess = 1;
			}
		},

		// 密码检测
		__pwdConProcess : function() {
			var emailpwd = $("#emailpwd").val();
			var pwdcon = $("#pwdcon").val();
			var _this = register;
			if (emailpwd.replace(/( )/g, "").length == 0
					|| pwdcon.replace(/( )/g, "").length == 0) {
				// $('#pwdconcheck').show();
				// var message = "密码、确认密码不能为空！";
				// document.getElementById("pwdconcheck").innerText = message;
				alert("密码、确认密码不能为空或空格！");
				pwdisok = 0;
			} else if (_this.__illegalCharacterCheck(emailpwd)) {
				alert("密码请勿包含非法字符如[#%&$'/,;:=!^]或空格");
				pwdisok = 0;
			} else if (emailpwd == pwdcon) {
				// $('#pwdconcheck').hide();
				pwdisok = 1;
			} else {
				// $('#pwdconcheck').show();
				// var message = "两次输入密码不一致";
				// document.getElementById("pwdconcheck").innerText = message;
				alert("两次输入密码不一致");
				pwdisok = 0;
			}
		},

		// 非法字符检测(包含返回true)
		__illegalCharacterCheck : function(data) {
			szMsg = "[#%&$'/ ,;:=!^]";
			flag = 1;
			for (i = 1; i < szMsg.length + 1; i++) {
				if (data.indexOf(szMsg.substring(i - 1, i)) > -1) {
					flag = 0;
					break;
				}
			}
			if (flag == 1) {
				return false;
			}
			return true;
		},

	}

	register.init();
});
