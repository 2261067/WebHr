var pageSize = 8; // 页面大小
function init_topic_details() {
	// 话题详情首页分页
	var questionNum = 0;
	$(function() {
		$
				.get(
						G_BASE_URL + "/getTopicCardData.do",{topicId:topicId},
						function(result) {
							if (result.rsm) {
								var active = 'aw-active';
								var focus = '关注';
								if (result.rsm.focus == 1) {
									active = '';
									focus = '取消关注';
								}
								questionNum = result.rsm.topicQuestionCount;
								var template = Hogan
										.compile(HR_TEMPLATE.topic_detail_head)
										.render(
												{
													'topic_id':result.rsm.topic.topicId,
													'topic_img' : result.rsm.topic.topicImage,
													'topic_name' : result.rsm.topic.topicName,
													'attention_user_num' : result.rsm.topicAttentionCount,
													'active':active,
													'focus':focus,
												});

								$('#topic_header').html(template);
								
								
								if (!$("#page-control"))
									return;
								// 分页，PageCount是总条目数，这是必选参数，其它参数都是可选

								$("#page-control").pagination(questionNum, {
									callback : _PageCallback,
									prev_text : '上一页', // 上一页按钮里text
									next_text : '下一页', // 下一页按钮里text
									items_per_page : pageSize, // 显示条数
									num_display_entries : 6, // 连续分页主体部分分页条目数
									// current_page : params.currentPage - 1, //当前页索引 0代表第一页
									num_edge_entries : 2
								// 两侧首尾分页条目数
								});

								_PageCallback(0);
								// 翻页调用
								function _PageCallback(page_index, jq) {
									// InitTable(index);
									
									var page = page_index + 1;
									getTopicQuestionData(page);
									
								}
								
							}
						}, 'json');

		
	});

}

function getTopicQuestionData(page) {
	$.loading("show");
	$.get(G_BASE_URL + "/getTopicQuestion.do", {
		topicId : topicId,
		page:page,
		pageSize:pageSize,
	}, function(result) {
		
		$.loading("hide");
		
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			if (result.rsm && result.rsm.length > 0) {
				var questionHtml = '';
				$.each(result.rsm,function(i,item){
					var user = item.questionUser;
					var question = item.question;
					var last_user_id= user.userId;
					var last_user= user.nickName;
					var text = '发起了问题•';
					if(item.latestAnswerUser){
						last_user = item.latestAnswerUser.nickName;
						last_user_id= item.latestAnswerUser.userId;
						text ='回复了问题•';
					}
					text += item.attentionNum +'人关注•' + item.answerNum +'个回复•' + item.lastModifyTime;
					var template = Hogan.compile(HR_TEMPLATE.topic_detail_question).render({
						'user_id':user.userId,
						'user_img':user.photo,
						'last_user_id':last_user_id,
						'last_user_name':last_user,
						'question_id':question.questionId,
						'question_name':question.questionName,
						'text':text
					});
					
					questionHtml += html_entity_decode(template);
					
				});
				
				$('#c_question_list').html(questionHtml);
				
			}else{
				var questionHtml = '该话题下还没有问题噢！';
				$('#c_question_list').html(questionHtml);
			}
		}
	}, 'json');
}