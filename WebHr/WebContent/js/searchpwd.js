$(function() {

	var issuccess = 0;
	
	/*
	 * 注释的地方主要是为了解决浏览器兼容问题
	 * */
	var searchpwd = {

		init : function() {
			var _this = searchpwd;
			$('#searchpwdemail').on('focus', _this.__emailFocusProcess);

			$("#searchform").submit(function() {
				if (issuccess == 0) {
					_this.__emailCheckProcess();
					return false;
				}
				return true;
			});

		},

		__emailFocusProcess : function() {
			$('#searchpwdcheck').hide();
		},

		__emailCheckProcess : function() {
			var _this = searchpwd;
			var email = $("#searchpwdemail").val();
			var myreg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			if (email.length == 0) {
				// $('#searchpwdcheck').show();
				// var message = "请填写账号";
				// document.getElementById("searchpwdcheck").innerText =
				// message;
				alert("请填写账号");
			} else if (!myreg.test(email)) {
				// $('#searchpwdcheck').show();
				// var message = "邮箱格式不正确";
				// document.getElementById("searchpwdcheck").innerText =
				// message;
				alert("邮箱格式不正确!");
			} else {
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos);
				pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "login/" + "forgetpwd.do?email=" + email;
				$.ajax({
					async : false,
					url : url,
					success : _this.__emailcallback
				});
			}
		},

		__emailcallback : function(data) {
			if (data.errno == 1) {
//				$('#searchpwdcheck').show();
//				var message = "发送成功！请登陆邮箱找回密码。";
//				document.getElementById("searchpwdcheck").innerText = message;
				alert("发送成功！请登陆邮箱找回密码!");
				issuccess = 1;
			} else if (data.errno == 2009) {
//				$('#searchpwdcheck').show();
//				var message = "邮件发送失败，请稍后重试！";
//				document.getElementById("searchpwdcheck").innerText = message;
				alert("邮件发送失败，请稍后重试！");
				issuccess = 0;
			} else {
//				$('#searchpwdcheck').show();
//				var message = "该邮箱未注册过！";
//				document.getElementById("searchpwdcheck").innerText = message;
				alert("该邮箱未注册过！");
				issuccess = 0;
			}
		},

	}

	searchpwd.init();
});
