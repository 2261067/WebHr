$(function() {

	var emailisok = 0;
	var passwordisok = 0;

	var userupdate = {

		init : function() {
			var _this = userupdate;

			$("#updateuserform").submit(function() {
				if (check_admin()) {
					
					// 密码检测
					_this.__pwdConProcess();
					if (passwordisok == 0)
						return false;
					
					// 邮箱检测
					_this.__emailCheckProcess();
					if (emailisok == 0)
						return false;
					
					alert("修改用户信息成功！");
					return true;
				} else {
					alert("请登陆！");
					return false;
				}

			});
		},
		
		// email检测
		__emailCheckProcess : function() {
			var email = $("#email").val();
			var myreg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			if (myreg.test(email)) {
				var request = new Object();
				request = getRequest();
				var _this = userupdate;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "adminmanage/" + request["userid"] + "/"
				+ email + "/userupdatecheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__emailcallback
				});
			} else {
				alert("邮箱格式不正确!");
				emailisok = 0;
			}
		},

		__emailcallback : function(data) {
			if (data.errno == 0) {
				alert("邮箱已存在，修改失败!");
				emailisok = 0;
			} else {
				emailisok = 1;
			}
		},
		
		
		// 密码检测
		__pwdConProcess : function() {
			var pwdcon = $("#password").val();
			var _this = userupdate;
			if (pwdcon.replace(/( )/g, "").length == 0) {
				alert("密码不能为空或空格！");
				passwordisok = 0;
			} else if (_this.__illegalCharacterCheck(pwdcon)) {
				alert("密码请勿包含非法字符如[#%&$'/,;:=!^]或空格");
				passwordisok = 0;
			} else {
				passwordisok= 1;
			}
		},
		
		
		// 非法字符检测(包含返回true)
		__illegalCharacterCheck : function(data) {
			szMsg = "[#%&$'/ ,;:=!^]";
			flag = 1;
			for (i = 1; i < szMsg.length + 1; i++) {
				if (data.indexOf(szMsg.substring(i - 1, i)) > -1) {
					flag = 0;
					break;
				}
			}
			if (flag == 1) {
				return false;
			}
			return true;
		},

	}

	userupdate.init();
});
