
// 获取url中"?"符后参数
function getRequest() {
	var url = location.search; // 获取url中"?"符后的字串
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 获取活动第一页
function getActivityFirstPage() {
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/getactivitycount.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				var activityCount = result.rsm;
				var optInit = getOptionsFromForm();
				getActivityMessage(1,10);
	            $("#Pagination").pagination(activityCount, optInit);
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
	
}

//删除活动
function deleteActivity(activityid) {
	if (check_admin()) {
		var deleteactivityisok = confirm("确定删除该活动吗？");
		if(deleteactivityisok==true){
			$.get(G_BASE_URL + "/adminmanage/deleteactivity.do?activityid="+activityid, function(result) {
				if (result.errno != 1) {
					$.alert(result.err);
				} else {
					alert("成功删除该活动!");
					window.location.href = window.location.href;//刷新
				}

			}, 'json');
		}	
	}else {
		alert("请登陆！");
	}
	
}

// 获取活动展示信息
function getActivityMessage(page,pagesize) {
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/" + page + "/" + pagesize + "/"
				+ "/getactivity.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var activityHeaderHtml = '<tr>' 
										+ '<th>活动名</th>'
										+ '<th>发起人</th>' 
										+ '<th>开始时间</th>'
										+ '<th>结束时间</th>'
										+ '<th>操作</th>' 
										+ '</tr>';
					var activityMainHtml = '';
					$.each(result.rsm.Activity, function(i, item) {
						activityMainHtml += '<tr>' 
							    + '<td>' + item.activityName + '</td>'
								+ '<td>' + item.userId + '</td>' 								
								+ '<td>' + item.createTime + '</td>'
								+ '<td>' + item.endTime + '</td>'
								+ '<td><a class="link-del" href="javascript:void(0);" onclick="deleteActivity('+'\''+item.activityId+'\''+');">删除</a></td>'
								+ '</tr>';
					});
					$(".result-tab").html(activityHeaderHtml + activityMainHtml);
				}
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
}

//分页返回函数
function pageselectCallback(page_index, jq) {
	getActivityMessage(page_index+1,10);
	return false;
}

//得到分页属性
function getOptionsFromForm() {
	var opt = {
		callback : pageselectCallback,
		items_per_page : 10, // 每页显示个数
		num_display_entries : 8, // 显示分页链接个数
		current_page : 0, // 当前页
		num_edge_entries : 2, // 开始和结束显示分页链接个数
		link_to : "#",
		prev_text : "上一页",
		next_text : "下一页",
		ellipse_text : "...",
		prev_show_always : true,
		next_show_always : true
	};
	return opt;
}
