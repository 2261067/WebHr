$(function() {

	var setting_uploadimg = {

		init : function() {
			var _this = setting_uploadimg;

			$("#image").on('change', _this.ajaxFileUpload);
			$("#avatar_uploader").on('click', _this.__avatar_uploaderProcess);

			$("#setting_form_pwd").submit(function() {

			});

		},

		// 上传图片
		__avatar_uploaderProcess : function() {
			$("#image").click();

		},

		ajaxFileUpload : function() {

			var img = $("#image").val();
			var userid = $("#userid").val();
			
			if (!/\.(gif|jpg|jpeg|png|JPG|PNG)$/.test(img)) {
				alert("不支持的图片格式.图片类型必须是.jpeg,jpg,png,gif格式.");
				return false;
			}

			var path = document.location.href;
			var pos = path.lastIndexOf("/");
			path = path.substring(0, pos + 1);
			var url = path + "setting/" + "uploadimg.do?userid="+userid;
			$.ajaxFileUpload({
				url : url, // 用于文件上传的服务器端请求地址
				secureuri : false, // 一般设置为false
				fileElementId : 'image', // 文件上传控件的id属性
				dataType : 'json', // 返回值类型 一般设置为json
				success : function(data) // 服务器成功响应处理函数
				{
					if (data.errno != 1) {
						alert("图片上传失败,请稍后重试！");
					} else {
						window.location.href = window.location.href;
					}
				},
				error : function(data)// 服务器响应失败处理函数
				{
					alert("图片上传失败,请稍后重试！");
				}
			});
			return true;
		},

	}

	setting_uploadimg.init();
});
