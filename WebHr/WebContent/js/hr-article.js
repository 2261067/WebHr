//获取专栏首页数据
function getArticlePageData(page) {
	$.loading("show");
	$.get(G_BASE_URL + "/ajax/getAllArticles.do", {
		page : page,
		pageSize : 10
	}, function(result) {
		$.loading("hide");
		if (result.errno != 1) {
			$.alert(result.err);
		} else {

			if (result.rsm) {
				var articleHtml = '';
				$.each(result.rsm, function(i, item) {
					var user = item.user;
					var article = item.article;
					var text = '发表了文章•' + article.commentNum + '人评论';
					text += '•' + article.createTime;
					var template = Hogan.compile(HR_TEMPLATE.article_item)
							.render({
								'user_id' : user.userId,
								'user_nick' : user.nickName,
								'user_img' : user.photo,
								'article_id' : article.articleId,
								'article_name' : article.articleName,
								'text' : text
							});
					articleHtml += template;
				});
				$('#article_list').html(articleHtml);
			}
		}
	}, 'json');
}
// 专栏首页分页
function init_pagination() {
	var pageSize = 10;
	$(function() {
		$.get(G_BASE_URL + "/ajax/getArticleParams.do?pageSize=" + pageSize,
				function(pageData) {

					if (!$("#page-control"))
						return;
					// 分页，PageCount是总条目数，这是必选参数，其它参数都是可选

					$("#page-control").pagination(pageData.rsm.allArticleCount,
							{
								callback : _PageCallback,
								prev_text : '上一页', // 上一页按钮里text
								next_text : '下一页', // 下一页按钮里text
								items_per_page : pageSize, // 显示条数
								num_display_entries : 6, // 连续分页主体部分分页条目数
								// current_page : params.currentPage - 1,
								// //当前页索引 0代表第一页
								num_edge_entries : 2
							// 两侧首尾分页条目数
							});

					_PageCallback(0);
					// 翻页调用
					function _PageCallback(page_index, jq) {
						// InitTable(index);

						var page = page_index + 1;
						getArticlePageData(page);

					}

				}, 'json');
	});
}

// 活动详情页数据
function getArticleDetails() {
	$.get(G_BASE_URL + "/ajax/getArticle.do", {
		article_id : article_id
	}, function(result) {
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			if (result.rsm) {
				var article = result.rsm.Article;
				$("#article_name").html(article.articleName);
				$("#article_context").html(article.articleContext);
				$("#article_create_time").html(article.createTime);
				$("#article_last_modify_time").html(article.lastModifyTime);
				$("#article_comment_num").html(article.commentNum+"人评论");
				// 设置修改按钮
				if (article.userId == G_USER_ID) {
					var template = Hogan.compile(HR_TEMPLATE.edit_article)
							.render();
					$('#article_edit').html(template);
				}

				// 设置专栏者信息，同问题页
				var template = Hogan.compile(HR_TEMPLATE.question_user).render(
						{
							'user_id' : result.rsm.user.userId,
							'user_nick' : result.rsm.user.nickName,
							'user_img' : result.rsm.user.photo
						});
				$("#side_bar_user").html(template);

				var active = 'aw-active';
				var attention = '关注';
				if (result.rsm.isAttentionUser) {
					active = '';
					attention = '取消关注';
				}
				var template = Hogan.compile(
						HR_TEMPLATE.question_attention_user).render({
					'user_id' : result.rsm.user.userId,
					'user_nick' : result.rsm.user.nickName,
					'active' : active,
					'attention' : attention
				});
				$("#side_bar_atttion_user").html(template);

 
				// 设置评论框
				var template = Hogan.compile(HR_TEMPLATE.article_comment)
						.render({
							'user_id' : result.rsm.user.userId,
							'user_img' : result.rsm.user.photo,
							'article_id' : article.articleId,
							'article_context' : article.articleContext
						});
				$("#article_comment_box").html(template);

				//专栏评论列表
				getCommentContextShow(article.articleId);
			}
		}
	});

}

// 专栏评论列表
function getCommentContextShow(article_id)
{
	$.get(G_BASE_URL + "/ajax/getArticleComments.do", {
		article_id : article_id
	}, function(result) {
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			if (result.rsm) {
				var articleCommentHtml = '';
				$.each(result.rsm, function(i, item) {
					var user = item.user;
					var article_comment = item.comment;
					var template = Hogan.compile(
							HR_TEMPLATE.article_comment_show).render({
						'user_id' : user.userId,
						'user_nickname' : user.nickName,
						'user_img' : user.photo,
						'comment_context' : article_comment.commentContext,
						'create_time' : article_comment.createTime
					});
					articleCommentHtml += template;
				});
				$('#comment_context_list').html(articleCommentHtml);
			}
		}
	});
}

// 修改专栏
function edit_article() {
	var data = {};
	data['article_id'] = article_id;
	data['article_name'] = $('#article_name').text();
	data['article_context'] = $('#article_context').html();

	$.dialog('editArticle', data);

	function _split(string) {
		return string.split("：")[1];
	}
}