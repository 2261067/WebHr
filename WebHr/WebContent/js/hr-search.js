var searchPageSize=10;
function getSearchData(container,page,query,type) {
	// 重置页面当前页
	currentPage=page;
	currentType=type;
	var dataHandleFunc=null;
	var searchDefaultUrl="/search/ajax/searchMain.do";
	if(page==1)$(container).html("");
	switch(type){
		case "question":
			dataHandleFunc=questionDataHandle;
			break;
		case "activity":
			dataHandleFunc=activityHandle;
			break;
		case "user":
			dataHandleFunc=searchUserHandle;
			searchDefaultUrl="/search/ajax/searchUser.do";
			break;
		case "topic":
			dataHandleFunc=searchTopicHandle;
			searchDefaultUrl="/search/ajax/searchTopic.do";
			break;
	}
	$.get(G_BASE_URL + searchDefaultUrl, {
		q:query,
		type : type,
		page : page,
		pageSize : searchPageSize
	}, function(result) {
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			
			if(result.rsm.length == 0){
				$("#search_result_more").text("没有更多结果");
				$("#search_result_more").removeAttr("onclick");
				return;
			}
			// 添加answer内容
			var dataHtml ='';
			var dataContainer = $(container);
			$.each(result.rsm, function(i, item) {
			
					dataHtml += dataHandleFunc(item);
				
			});
			$(container).append(dataHtml);
			currentPage=currentPage+1;
		}
	}, 'json');
}



function questionDataHandle(item) {
	
	var data = item.data;
	var url = item.url;
	var user = item.user;

		var template = Hogan.compile(HR_TEMPLATE.search_question)
				.render({
					'question_url' : G_BASE_URL+'/'+url,
					'question_title':  data.questionName, 
					'question_create_time' : data.createTime,
					'nick_name':user.nickName,
				
				});
		return html_entity_decode(template);
	

	
	
}


function activityHandle(item) {
	
	var user = item.user;
	var activity = item.data;
	var text = '发起了活动';
	
	text += '•截止日期' + activity.endTime;
	var template = Hogan.compile(HR_TEMPLATE.search_activity)
			.render({
				'user_id' : activity.userId,
				'nick_name':user.nickName,
				'activity_id' : activity.activityId,
				'activity_name' : activity.activityName,
				'text' : text
			});


		return html_entity_decode(template);
	

	
	
}
function searchUserHandle(item) {
	var nickName="暂无昵称";
	var userFullName="未填写";
	if(item.nickName)nickName=item.nickName;
	if(item.firstName&&item.lastName)userFullName=item.firstName+item.lastName;
	var template = Hogan.compile(HR_TEMPLATE.search_user)
			.render({
				'user_id' : item.userId,
				
				'user_img' : G_PIC_BASE_URL+item.photo,
				'user_name':userFullName,
				'nick_name':nickName,
			});


		return html_entity_decode(template);
	

	
	
}


function searchTopicHandle(item) {

	var template = Hogan.compile(HR_TEMPLATE.search_topic)
			.render({
				'topic_id' : item.topicId,
				'topic_name' : item.topicName,
				'topic_url' :G_BASE_URL+topicDescUrl+"?topicId="+item.topicId,
				'topic_img':G_PIC_BASE_URL+item.topicImage,
				'topic_question_count':0,
				'topic_attention_count':0,
			});


		return html_entity_decode(template);
	

	
	
}

