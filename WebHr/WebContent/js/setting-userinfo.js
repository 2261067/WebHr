$(function() {

	var phoneisok = 0;
	var nicknameisok = 0;

	var setting_userinfo = {

		init : function() {
			var _this = setting_userinfo;
			$("#setting_form").submit(function() {
				// 手机号码检测
				_this.__phoneCheckProcess();
				if (phoneisok == 0)
					return false;
				// 昵称检测
				_this.__nicknameCheckProcess();
				if (nicknameisok == 0)
					return false;
				alert("保存成功！");
				return true;
			});
		},

		// 手机号码检测
		__phoneCheckProcess : function() {
			var re = /^1\d{10}$/;
			var phone = $("#phonenumber").val();
			if (re.test(phone)) {
				phoneisok = 1;
			} else {
				alert("请输入规范的11位数字手机号!");
				phoneisok = 0;
			}
		},

		// 昵称检测
		__nicknameCheckProcess : function() {
			var nickname = $("#nickname").val();
			var _this = setting_userinfo;
			if (nickname.replace(/( )/g, "").length == 0) {
				alert("昵称不能为空！");
				nicknameisok = 0;
			} else if (_this.__illegalCharacterCheck(nickname)) {
				alert("昵称请勿包含非法字符如[#%&$'/,;:=!^]或空格");
				nicknameisok = 0;
			} else {
				var _this = setting_userinfo;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "setting/" + nickname + "/nicknamecheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__nicknamecallback
				});
			}
		},

		__nicknamecallback : function(data) {
			if (data.errno == 0) {
				alert("昵称已存在！保存失败！");
				nicknameisok = 0;
			} else {
				nicknameisok = 1;
			}
		},

		// 非法字符检测(包含返回true)
		__illegalCharacterCheck : function(data) {
			szMsg = "[#%&$'/ ,;:=!^]";
			flag = 1;
			for (i = 1; i < szMsg.length + 1; i++) {
				if (data.indexOf(szMsg.substring(i - 1, i)) > -1) {
					flag = 0;
					break;
				}
			}
			if (flag == 1) {
				return false;
			}
			return true;
		},

	}

	setting_userinfo.init();
});
