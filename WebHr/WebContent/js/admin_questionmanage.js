
// 获取url中"?"符后参数
function getRequest() {
	var url = location.search; // 获取url中"?"符后的字串
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 获取问题第一页
function getQuestionFirstPage() {
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/getquestioncount.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				var questionCount = result.rsm;
				var optInit = getOptionsFromForm();
				getQuestionMessage(1,10);
	            $("#Pagination").pagination(questionCount, optInit);
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
	
}

//删除问题
function deleteQuestion(questionid) {
	if (check_admin()) {
		var deletequestionisok = confirm("确定删除该问题吗？删除后,该问题及其所有回答会一同删除！");
		if(deletequestionisok==true){
			$.get(G_BASE_URL + "/adminmanage/deletequestion.do?questionid="+questionid, function(result) {
				if (result.errno != 1) {
					$.alert(result.err);
				} else {
					alert("成功删除该问题!");
					window.location.href = window.location.href;//刷新
				}

			}, 'json');
		}	
	}else {
		alert("请登陆！");
	}
	
}

// 获取问题展示信息
function getQuestionMessage(page,pagesize) {
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/" + page + "/" + pagesize + "/"
				+ "/getquestion.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var questionHeaderHtml = '<tr>' 
										+ '<th>发问人</th>'
										+ '<th>问题名</th>' 
										+ '<th>发布时间</th>'
										+ '<th>操作</th>' 
										+ '</tr>';
					var questionMainHtml = '';
					$.each(result.rsm.Question, function(i, item) {

						questionMainHtml += '<tr>' 
								+ '<td>' + item.userId + '</td>' 
								+ '<td>' + item.questionName + '</td>'
								+ '<td>' + item.createTime + '</td>'
								+ '<td><a class="link-del" href="javascript:void(0);" onclick="deleteQuestion('+'\''+item.questionId+'\''+');">删除</a></td>'
								+ '</tr>';
					});
					$(".result-tab").html(questionHeaderHtml + questionMainHtml);
				}
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
}

//分页返回函数
function pageselectCallback(page_index, jq) {
	getQuestionMessage(page_index+1,10);
	return false;
}

//得到分页属性
function getOptionsFromForm() {
	var opt = {
		callback : pageselectCallback,
		items_per_page : 10, // 每页显示个数
		num_display_entries : 8, // 显示分页链接个数
		current_page : 0, // 当前页
		num_edge_entries : 2, // 开始和结束显示分页链接个数
		link_to : "#",
		prev_text : "上一页",
		next_text : "下一页",
		ellipse_text : "...",
		prev_show_always : true,
		next_show_always : true
	};
	return opt;
}
