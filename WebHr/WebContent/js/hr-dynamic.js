function getDynamicPageList(data, moduleName, containerName, url, type,
		ajaxSuccessEleFunction) {
	$.loading("show");
	if (!data) {
		alert("页面异常，请重新操作！");
		return;
	}
	//$("#bp_more").addClass('loading');
	//var ajaxSuccessEleFun = getUserDynamicData;
    if(ajaxSuccessEleFunction!=null)ajaxSuccessEleFun=ajaxSuccessEleFunction;
	$.ajax({
		timeout:10000,
		type : "get",
		contentType : "application/json;charset=utf-8",
		url : G_BASE_URL + "/" + url,
		dataType : "json",
		data : data,
		success : function(redata, textStatus) {
			$.loading("hide");
			if (!redata || !redata.rsm || redata.rsm.length == 0) {
				$("#bp_more").text("没有更多结果");
				$("#bp_more").removeAttr("onclick");
				return;
			}
			var getQuestionPageListRev = {
				pageControlId : data.pageControlId,
				currentPage : redata.currentPage,
				dataCount : redata.allDataCount,
				pageSize : redata.pageSize,
				moduleName : moduleName,
				containerName : containerName,
				url : url,
				type : type
			};
			getPageData(getQuestionPageListRev);
			var template = $(moduleName).clone();
			// $(containerName).html("");
			if (redata.nextPage)
				data.nextPage = redata.nextPage;
			$(redata.rsm).each(
					function(i, domEle) {

						template = template.clone();
						ajaxSuccessEleFunction(i, domEle, containerName,
								moduleName, template);

					});

		},

		complete : function(XMLHttpRequest, textStatus) {
			if (uParse) {
				uParse('.markitup-box', {
					rootPath : 'ueditor/'
				});
			}
			// HideLoading();
		},
		error : function() {
			//$("#bp_more").removeClass('loading');
			// 请求出错处理
			$.loading("hide");
			alert("请求数据出错，请检查网络或联系管理员！");
		}
	});
}

function getUserDynamicData(i, domEle, containerName) {

	var userData = domEle.dataUserInfo;
	var dynamicData = domEle.userDynamicData;
	var sourceQuestion = domEle.sourceQuestion;
	var dataTopics = domEle.dataTopics;
	var userImg = G_PIC_BASE_URL + userData.photo;
	var typeAndTemplate = getFocusTypeAndDes(domEle);
	var titleDescHtml = typeAndTemplate.template;
	var ifAttention = typeAndTemplate.ifUserAttention;
	// 根据关注情况设置关注按钮是否隐藏
	var btnAttentionHide = "";
	if ((dynamicData.id.type!="focus_user_question"&& dynamicData.id.type!="focus_topic_question")||domEle.ifAttention)
		btnAttentionHide = "hide";

	var template = Hogan.compile(HR_TEMPLATE.user_focus_dynamic).render(
			{
				'user_id' : userData.userId,
				'nick_name' : userData.nickName,
				'user_url' : G_BASE_URL + userDescUrl + "?uid="
						+ userData.userId,
				'user_img' : userImg,
				'topic_name_list' : "",
				'data_time' : dynamicData.createTime,
				'question_url' : G_BASE_URL + questionDescUrl + "?questionId="
						+ dynamicData.questionId,
				'question_title' : sourceQuestion.questionName,
				'data_id' : dynamicData.id.dataId,
				'data_desc_short' : dynamicData.dataDesc,
				'data_desc' : dynamicData.dataDesc,
				'question_answer_count' : domEle.answerNum,
				'question_attention_count' : domEle.attentionNum,
				'question_id' : dynamicData.questionId,
				'title_string' : titleDescHtml,
				'if_attention' : btnAttentionHide
			});

	$(containerName).append(html_entity_decode(template));

}
function getUserFocusQuestionData(i, domEle, containerName) {

	var userData = domEle.questionUser;
	
	var sourceQuestion = domEle.question;
	var dataTopics = domEle.dataTopics;
	var userImg = G_PIC_BASE_URL + userData.photo;
	
	


	var template = Hogan.compile(HR_TEMPLATE.normal_question).render(
			{
				'user_id' : userData.userId,
				'nick_name' : userData.nickName,
				'user_url' : G_BASE_URL + userDescUrl + "?uid="
						+ userData.userId,
				'user_img' : userImg,
				'question_name' : sourceQuestion.questionName,
				'last_modify_time' : domEle.lastModifyTime,
				'question_url' : G_BASE_URL + questionDescUrl + "?questionId="
						+ sourceQuestion.questionId,

				'question_focus_count' : domEle.attentionNum,
				'question_answer_count' : domEle.answerNum,
				'question_id' : sourceQuestion.questionId,

			});

	$(containerName).append(html_entity_decode(template));

}
function getInvitedQuestionData(i, domEle, containerName) {

	var userData = domEle.questionUser;
	
	var sourceQuestion = domEle.question;
	var dataTopics = domEle.dataTopics;
	var userImg = G_PIC_BASE_URL + userData.photo;
	
	


	var template = Hogan.compile(HR_TEMPLATE.invited_question).render(
			{
				'user_id' : userData.userId,
				'nick_name' : userData.nickName,
				'user_url' : G_BASE_URL + userDescUrl + "?uid="
						+ userData.userId,
				'user_img' : userImg,
				'question_name' : sourceQuestion.questionName,
				'last_modify_time' : domEle.lastModifyTime,
				'question_url' : G_BASE_URL + questionDescUrl + "?questionId="
						+ sourceQuestion.questionId,

				'question_focus_count' : domEle.attentionNum,
				'question_answer_count' : domEle.answerNum,
				'question_id' : sourceQuestion.questionId,

			});

	$(containerName).append(html_entity_decode(template));

}
function getFocusTypeAndDes(data) {
	var dataTopics = data.dataTopics;
	var dynamicData = data.userDynamicData;
	var userData = data.dataUserInfo;
	var temp_params = {
		name_list : "",
		title_string : "",
		data_type : dynamicData.dataType
	};
	var returnParams = {
		ifUserAttention : false
	};
	switch (dynamicData.id.type) {
	case "focus_question_answer":
		temp_params.name_list = getTemplate(
				HR_TEMPLATE.user_focus_dynamic_data_question, {
					'question_url' : G_BASE_URL + questionDescUrl + "?questionId="
					+ dynamicData.questionId,
				});
		temp_params.title_string = "有了新的";
		returnParams.ifUserAttention = true;
		break;
	case "focus_user_question":
		var user_url = G_BASE_URL + userDescUrl + "?uid=" + userData.userId;
		temp_params.name_list = getTemplate(
				HR_TEMPLATE.user_focus_dynamic_data_user, {
					'user_url' : user_url,
					'user_id' : dynamicData.userId,
					'nick_name' : userData.nickName
				});
		temp_params.title_string = "发表了新的";
		break;
	case "focus_user_answer":
		var user_url = G_BASE_URL + userDescUrl + "?uid=" + userData.userId;
		temp_params.name_list = getTemplate(
				HR_TEMPLATE.user_focus_dynamic_data_user, {
					'user_url' : user_url,
					'user_id' : dynamicData.userId,
					'nick_name' : userData.nickName
				});
		temp_params.title_string = "发表了新的";
		break;
	case "focus_topic_question":
		if (dataTopics) {
			$(dataTopics).each(
					function(i, domEle) {
						var topic_url = "#";
						temp_params.name_list += getTemplate(
								HR_TEMPLATE.user_focus_dynamic_data_topic, {
									'topic_url' : G_BASE_URL + topicDescUrl
											+ "?topicId=" + domEle.topicId,
									'topic_name' : domEle.topicName,
									'topic_id' : domEle.topicId
								});
						temp_params.name_list += " ";

					});
		}

		temp_params.title_string = "有了新的";
		break;
	default:
		break;
	}
	returnParams.template = html_entity_decode(getTemplate(
			HR_TEMPLATE.user_focus_dynamic_title, temp_params));
	return returnParams;
}
function getTemplate(templateString, templateParams) {
	var template = Hogan.compile(templateString).render(templateParams);
	return template;
}

function getNextDynamicPage(dynamicParams, pageSize, url,eleFunction) {
	var params = dynamicParams;
	params.page = params.nextPage;

	getDynamicPageList(params, params.moduleName, params.containerName,
			params.url, params.type, eleFunction);

}
var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
Math.uuid = function (len, radix) {
    var chars = CHARS, uuid = [], i;
    radix = radix || chars.length;
    if (len) {
      // Compact form
      for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;
      // rfc4122 requires these characters
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';
      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | Math.random()*16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }
    return uuid.join('');
  };