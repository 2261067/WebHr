$(function() {

	var settingpwdissuccess = 0;

	/*
	 * 注释的地方主要是为了解决浏览器兼容问题
	 */
	var setting = {
		init : function() {
			var _this = setting;
			$("#input-password-old").on('focus', _this.__oldpwdFocusProcess);
			$("#input-password-re-new").on('focus',
					_this.__renewpwdFocusProcess);

			$("#setting_form_pwd").submit(function() {
				if (_this.__pwdConProcess()) {
					_this.__oldpwdCheckProcess();
					if (settingpwdissuccess == 1) {
						alert("修改密码成功！");
						return true;
					}
				}
				return false;
			});
		},

		// 修改密码

		__oldpwdFocusProcess : function() {
			$('#pwdcheck').hide();
		},

		__renewpwdFocusProcess : function() {
			$('#newpwd').hide();
		},

		__oldpwdCheckProcess : function() {
			var oldpwd = $("#input-password-old").val();
			if (oldpwd.replace(/( )/g, "").length == 0) {
				alert("当前密码不能为空或空格！");
			} else {
				var _this = setting;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos);
				pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "setting/" + oldpwd + "/oldpwdcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__oldpwdcallback
				});
			}

		},

		__oldpwdcallback : function(data) {
			if (data.errno == 0) {
				// $("#pwdcheck").show();
				// var message = "输入当前账户密码不正确";
				// document.getElementById("pwdcheck").innerText = message;
				alert("输入当前账户密码不正确!");
				settingpwdissuccess = 0;
			} else {
				settingpwdissuccess = 1;
			}
		},

		__pwdConProcess : function() {
			var newpwd = $("#input-password-new").val();
			var renewcon = $("#input-password-re-new").val();
			var _this = setting;
			if (newpwd.replace(/( )/g, "").length == 0
					|| renewcon.replace(/( )/g, "").length == 0) {
				// $('#newpwd').show();
				// var message = "新的密码、确认密码不能为空！";
				// document.getElementById("newpwd").innerText = message;
				alert("新的密码、确认密码不能为空或空格！");
				return false;
			} else if (_this.__illegalCharacterCheck(newpwd)) {
				alert("密码请勿包含非法字符如[#%&$'/,;:=!^]或空格");
				return false;
			} else if (newpwd == renewcon) {
				return true;
			} else {
				// $('#newpwd').show();
				// var message = "两次输入密码不一致";
				// document.getElementById("newpwd").innerText = message;
				alert("新的密码、确认密码不一致!");
				return false;
			}
		},

		// 非法字符检测(包含返回true)
		__illegalCharacterCheck : function(data) {
			szMsg = "[#%&$'/ ,;:=!^]";
			flag = 1;
			for (i = 1; i < szMsg.length + 1; i++) {
				if (data.indexOf(szMsg.substring(i - 1, i)) > -1) {
					flag = 0;
					break;
				}
			}
			if (flag == 1) {
				return false;
			}
			return true;
		},

	}

	setting.init();
});
