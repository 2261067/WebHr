$(function() {

	var topicnameisok = 0;
	var nullcheckisok = 0;

	var adminupdate = {

		init : function() {
			var _this = adminupdate;

			$("#updatetopicform").submit(function() {
				if (check_admin()) {
					// 是否为空
					_this.__nullCheckProcess();
					if (nullcheckisok == 0)
						return false;
					// 话题检测
					_this.__topicCheckProcess();
					if (topicnameisok == 0)
						return false;
					alert("修改话题成功！");
					return true;
				} else {
					alert("请登陆！");
					return false;
				}

			});
		},

		// 为空检测
		__nullCheckProcess : function() {
			var topicname = $("#topicname").val();
			var topicdesc = $("#topicdesc").val();
			if (topicname.replace(/( )/g, "").length == 0) {
				alert("话题不能为空！");
				nullcheckisok = 0;
			} else if (topicdesc.replace(/( )/g, "").length == 0) {
				alert("话题详细描述不能为空！");
				nullcheckisok = 0;
			} else {
				nullcheckisok = 1;
			}

		},

		// 话题检测
		__topicCheckProcess : function() {
			var topicname = $("#topicname").val();
			var request = new Object();
			request = getRequest();
			var _this = adminupdate;
			var path = document.location.href;
			var pos = path.lastIndexOf("/");
			path = path.substring(0, pos + 1);
			var url = path + "adminmanage/" + request["topicid"] + "/"
					+ topicname + "/topicupdatecheck.do";
			$.ajax({
				async : false,
				url : url,
				success : _this.__topicnamecallback
			});
		},

		__topicnamecallback : function(data) {
			if (data.errno == 0) {
				alert("话题名已经存在!修改失败！");
				topicnameisok = 0;
			} else {
				topicnameisok = 1;
			}
		},

	}

	adminupdate.init();
});
