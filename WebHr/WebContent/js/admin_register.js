$(function() {

	var emailissuccess = 0;
	var usernameissuccess = 0;
	var pwdisok = 0;
	var adminpwdisok = 0;

	var register = {

		init : function() {
			var _this = register;

			$('#email').on('focus', _this.__emailFocusProcess);
			$('#username').on('focus', _this.__usernameFocusProcess);
			$('#adminpwd').on('focus', _this.__adminpwdFocusProcess);

			$("#registerform").submit(function() {

				// 检查密钥
				_this.__adminpwdCheckProcess();
				if (adminpwdisok == 0)
					return false;
				// 检测密码
				_this.__pwdConProcess();
				if (pwdisok == 0)
					return false;
				// 用户名检测
				_this.__usernameCheckProcess();
				if (usernameissuccess == 0)
					return false;
				// 邮箱检测
				_this.__emailCheckProcess();
				if (emailissuccess == 0)
					return false;
				alert("注册成功！");
				return true;
			});
		},

		// email检测
		__emailFocusProcess : function() {
			$('#emailcheck').hide();
		},

		__emailCheckProcess : function() {
			var email = $("#email").val();
			var myreg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			if (myreg.test(email)) {
				var _this = register;
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "adminregister/" + email + "/emailcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__emailcallback
				});
			} else {
				alert("邮箱格式不正确!");
				emailissuccess = 0;
			}
		},

		__emailcallback : function(data) {

			if (data.errno == 0) {
				// $('#emailcheck').show();
				// var message = "邮箱已被注册";
				// document.getElementById("emailcheck").innerText = message;
				alert("邮箱已被注册");
				emailissuccess = 0;
			} else {
				$('#emailcheck').hide();
				emailissuccess = 1;
			}
		},

		// 用户名检测
		__usernameFocusProcess : function() {
			$('#usernamecheck').hide();
		},

		__usernameCheckProcess : function() {
			var username = $("#username").val();
			var _this = register;
			if (username.replace(/( )/g, "").length == 0) {
				alert("用户名不能为空！");
				usernameissuccess = 0;
			} else if (_this.__illegalCharacterCheck(username)) {
				alert("用户名请勿包含非法字符如[#%&$'/ ,;:=!^]");
				usernameissuccess = 0;
			} else {
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "adminregister/" + username
						+ "/usernamecheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__usernamecallback
				});
			}

		},

		__usernamecallback : function(data) {
			if (data.errno == 0) {
				// $('#usernamecheck').show();
				// var message = "用户名已被注册";
				// document.getElementById("usernamecheck").innerText = message;
				alert("用户名已被注册");
				usernameissuccess = 0;
			} else {
				$('#usernamecheck').hide();
				usernameissuccess = 1;
			}
		},

		// 密码检测
		__pwdConProcess : function() {
			var emailpwd = $("#emailpwd").val();
			var pwdcon = $("#pwdcon").val();
			if (emailpwd == '' || pwdcon == '') {
				// $('#pwdconcheck').show();
				// var message = "密码、确认密码不能为空！";
				// document.getElementById("pwdconcheck").innerText = message;
				alert("密码、确认密码不能为空！");
				pwdisok = 0;
			} else if (emailpwd == pwdcon) {
				$('#pwdconcheck').hide();
				pwdisok = 1;
			} else {
				// $('#pwdconcheck').show();
				// var message = "两次输入密码不一致";
				// document.getElementById("pwdconcheck").innerText = message;
				alert("两次输入密码不一致！");
				pwdisok = 0;
			}
		},

		// 密钥检测
		__adminpwdFocusProcess : function() {
			$('#adminpwdcheck').hide();
		},

		__adminpwdCheckProcess : function() {
			var adminpwd = $("#adminpwd").val();
			var _this = register;
			var path = document.location.href;
			var pos = path.lastIndexOf("/");
			path = path.substring(0, pos + 1);
			if (adminpwd.replace(/( )/g, "").length == 0) {
				alert("密钥不为空！");
				adminpwdisok = 0;
			} else {
				var url = path + "adminregister/" + adminpwd
						+ "/adminpwdcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__adminpwdcallback
				});
			}

		},

		__adminpwdcallback : function(data) {
			if (data.errno == 0) {
				// $('#adminpwdcheck').show();
				// var message = "密钥不正确";
				// document.getElementById("adminpwdcheck").innerText = message;
				alert("密钥不正确");
				adminpwdisok = 0;
			} else {
				$('#usernamecheck').hide();
				adminpwdisok = 1;
			}
		},

		// 非法字符检测(包含返回true)
		__illegalCharacterCheck : function(data) {
			szMsg = "[#%&$'/ ,;:=!^]";
			flag = 1;
			for (i = 1; i < szMsg.length + 1; i++) {
				if (data.indexOf(szMsg.substring(i - 1, i)) > -1) {
					flag = 0;
					break;
				}
			}
			if (flag == 1) {
				return false;
			}
			return true;
		},

	}

	register.init();
});
