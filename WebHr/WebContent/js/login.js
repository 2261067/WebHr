$(function() {

	var accountissuccess = 0;
	var codeissuccess = 0;
	var useraccountisok = 0;

	var login = {
		init : function() {
			var _this = login;
			$('#username').on('focus', _this.__userNameTipsProcess);
			$('#password').on('focus', _this.__PwdTipsProcess);
			$('#securitycode').on('focus', _this.__codeTipsProcess);

			$("#loginform").submit(function() {

				// 检验验证码
				_this.__codeProcess();
				if (codeissuccess == 0)
					return false;

				// 检验账户是否存在
				_this.__userNameProcess();
				if (useraccountisok == 0)
					return false;

				// 检验账户密码是否匹配
				_this.__userPwdProcess();
				if (accountissuccess == 0) {
					$('#passwordError').show();
					return false;
				}
				return true;
			});

		},

		// 用户名检测
		__userNameTipsProcess : function() {
			$('#userNameError').hide();
			$('#userNameTips').hide();
			$('#userNameOk').hide();
		},

		__userNameProcess : function() {
			var _this = login;
			var account = $("#username").val();
			if (account.replace(/( )/g, "").length == 0) {
				$('#userNameTips').show();
				$('#userNameError').hide();
				$('#userNameOk').hide();
			} else {
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "login/" + account + "/accountcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__accountcallback
				});
			}
		},

		__accountcallback : function(data) {
			if (data.errno == 1) {
				$('#userNameTips').hide();
				$('#userNameError').hide();
				$('#userNameOk').show();

				useraccountisok = 1;
			} else {
				$('#userNameTips').hide();
				$('#userNameError').show();
				$('#userNameOk').hide();
				useraccountisok = 0;
			}
		},

		// 密码检测
		__PwdTipsProcess : function() {
			$('#passwordTips').hide();
			$('#passwordError').hide();
		},

		__userPwdProcess : function() {
			var _this = login;
			var password = $("#password").val();
			var account = $("#username").val();
			if (password.replace(/( )/g, "").length == 0) {
				$('#passwordTips').show();
			} else {
				if (useraccountisok == 1) {
					var path = document.location.href;
					var pos = path.lastIndexOf("/");
					path = path.substring(0, pos + 1);
					var url = path + "login/" + account + "/" + password
							+ "/accountpwdcheck.do";
					$.ajax({
						url : url,
						async : false,
						success : _this.__accountpwdcallback
					});
				}
			}
		},

		__accountpwdcallback : function(data) {
			if (data.errno == 1) {
				accountissuccess = 1;
			}
		},

		// 验证码检测
		__codeTipsProcess : function() {
			$('#codeTips').hide();
			$('#codeError').hide();
			$('#codeOk').hide();
		},

		__codeProcess : function() {
			var password = $("#password").val();
			var account = $("#username").val();
			if (password.replace(/( )/g, "").length == 0
					|| account.replace(/( )/g, "").length == 0) {
				alert("请填写邮箱或密码！");
			} else {
				var _this = login;
				var code = $("#securitycode").val();
				code = "c=" + code;
				$.ajax({
					type : "POST",
					async : false,
					url : "../resultServletUtils",
					data : code,
					success : _this.__callback
				});
			}

		},

		__callback : function(data) {
			if (data == "null") {
				$('#codeTips').show();
				$('#codeError').hide();
				$('#codeOk').hide();
			}
			if (data == "0") {
				$('#codeTips').hide();
				$('#codeError').show();
				$('#codeOk').hide();
			}
			if (data == "1") {
				$('#codeTips').hide();
				$('#codeError').hide();
				$('#codeOk').show();
				codeissuccess = 1;
			}
		},
	}

	login.init();
});
