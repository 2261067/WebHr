var category_id = (typeof AW_CATE == 'undefined') ? 1 : AW_CATE;
var add_article_link  = '';//(typeof G_CAN_PUBLISH_ARTICLE != 'undefined' && G_CAN_PUBLISH_ARTICLE == '1') ? '<a href="'+G_BASE_URL+'/publish/article/" class="addArticle">发起文章</a>' : '';
var yiwenyida_str = (typeof G_IS_YIWENYIDA != 'undefined' && G_IS_YIWENYIDA ) ? '<input type="hidden" name="type" value="yiwenyida" /><input type="hidden" name="pid" value="'+G_YIWENYIDA_ID+'" /><input type="hidden" class="form-control" name="specify_answer_uid" value="'+G_YIWENYIDA_ANSWER_UIDS+'" />' : '';

var AW_TEMPLATE = {
	'loadingBox':
		'<div id="aw-loading" class="hide">'+
			'<div id="aw-loading-box"></div>'+
		'</div>',
	
	'userCard':
			'<div id="aw-card-tips" class="aw-card-tips aw-card-tips-user">'+
				'<div class="aw-mod">'+
					'<div class="aw-mod-head">'+
						'<a href=' + G_BASE_URL+ '/people.do?uid={{uid}} class="aw-head-img">'+
							'<img src= ' + G_BASE_URL+'{{avatar_file}} alt="" / style="height:50px;width:50px">'+
						'</a>'+
						'<p class="title">'+
							'<a href=' + G_BASE_URL+ '/people.do?uid={{uid}} class="name" data-id="{{uid}}">{{user_nick}}</a>'+
							'<i class="{{verified_enterprise}}" title="{{verified_title}}"></i>'+
						'</p>'+
						'<p class="aw-user-center-follow-meta">'+
							//'<span>' + _t('威望') + ': <em class="aw-text-color-green">{{reputation}}</em></span>'+
							'<span>' + _t('提问') + ': <em class="aw-text-color-green">{{question_count}}</em></span>'+
							'<span>' + _t('回答') + ': <em class="aw-text-color-green">{{answer_count}}</em></span>'+
							'<span>' + _t('赞同') + ': <em class="aw-text-color-orange">{{agree_count}}</em></span>'+
						'</p>'+
					'</div>'+
					'<div class="aw-mod-body">'+
						'<p>{{signature}}</p>'+
					'</div>'+
					'<div class="aw-mod-footer">'+
						'<span class="pull-right">'+
							'<a href="javascript:;" onclick="$.dialog(\'inbox\', \'{{user_nick}}\');">' + _t('私信') + '</a>&nbsp;&nbsp;&nbsp;&nbsp;' +
//							+ '<a href="javascript:;" onclick="$.dialog(\'publish\', {category_enable:{{category_enable}}, ask_user_id:{{uid}}, ask_user_name:{{ask_name}}});">' + _t('问Ta') + '</a>'+
							
						'</span>'+
						'<a class="btn btn-mini btn-default focus {{focus}}" onclick="follow_people($(this), \'{{uid}}\');">{{focusTxt}}</a>'+
					'</div>'+
				'</div>'+
			'</div>',
	
	'topicCard' : 
			'<div id="aw-card-tips" class="aw-card-tips aw-card-tips-topic">'+
				'<div class="aw-mod">'+
					'<div class="aw-mod-head">'+
						'<a href="{{url}}" class="aw-head-img">'+
							'<img src="{{topic_pic}}" alt="" title=""/>'+
						'</a>'+
						'<p class="title">'+
							'<a href="{{url}}" class="name" data-id="{{topic_id}}">{{topic_title}}</a>'+
						'</p>'+
						'<p>'+
							'{{topic_description}}'+
						'</p>'+
					'</div>'+
					'<div class="aw-mod-footer">'+
						'<span class="pull-right">'+
							_t('问题数') + ' {{discuss_count}} • ' + _t('关注者') + ' {{focus_count}}'+
						'</span>'+
						'<a class="btn btn-mini btn-default focus {{focus}}" onclick="focus_topic($(this), {{topic_id}});">{{focusTxt}}</a>'+
					'</div>'+
				'</div>'+
			'</div>',

	'alertBox' : 
			'<div class="modal fade alert-box aw-tips-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('提示信息') + '</h3>'+
						'</div>'+
						'<div class="modal-body">'+
							'<p>{{message}}</p>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',

	'imagevideoBox' : 
			'<div id="aw-image-box" class="modal fade alert-box aw-image-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">{{title}}</h3>'+
						'</div>'+
						'<div class="modal-body">'+
							// '<form id="addTxtForms" onsubmit=";return false;" method="post" enctype="multipart/form-data">'+
		     //                    '<p>' + _t('上传图像') + '</p>'+
							// 	'<input class="" type="file" id="imgurl" name="{{url}}" />'+
							// 	'<p>' + _t('文字说明') + ':</p>'+
							// 	'<input class="form-control" type="text" name="{{tips}}"/>'+
							// '<div class="modal-footer"><a data-dismiss="modal" aria-hidden="true" class="btn">' + _t('取消') + '</a><input type="submit" name="submit" value="上传"  class="btn btn-large btn-success" onclick="$.{{add_func}}($.{{add_func}});"></div></form>'+
							// '<p class="aw-text-color-999">{{type_tips}}</p>'+


							'<ul class="nav nav-tabs" role="tablist">'+
							  '<li class="active"><a href="#upload-img" role="tab" data-toggle="tab">' + _t('上传图像') + '</a></li>'+
							  '<li><a href="#insert-img" role="tab" data-toggle="tab">' + _t('外链图片') + '</a></li>'+
							'</ul>'+
							'<div class="tab-content">'+
							  '<div class="tab-pane active" id="upload-img">'+
							  	'<form id="addTxtForms" onsubmit=";return false;" method="post" enctype="multipart/form-data">'+
							  		'<p>' + _t('选择图片') + '</p>'+
							  		'<input class="form-control" type="file" id="imgurl" name="imgsUrl">'+
							  		'<div class="modal-footer">'+
							  			'<a data-dismiss="modal" aria-hidden="true" class="btn">' + _t('取消') + '</a>'+
							  			'<input type="submit" name="submit" value="' + _t('上传') + '" class="btn btn-large btn-success" onclick="$.addTextpicTure($.addTextpicTure);">'+
							  		'</div>'+
							  	'</form>'+
							  '</div>'+
							  '<div class="tab-pane" id="insert-img">'+
							  		'<p> ' + _t('链接地址') + '</p>'+
									'<input class="form-control" type="text" value="http://" id="insert-img-inp" />'+
									'<div class="modal-footer">'+
							  			'<a data-dismiss="modal" aria-hidden="true" class="btn">' + _t('取消') + '</a>'+
							  			'<input type="submit" name="submit" value="' + _t('插入') + '" class="btn btn-large btn-success" onclick="$.addTextpicTure($.addTextpicTure);">'+
							  		'</div>'+
							  '</div>'+
							'</div>'+
							'<p class="aw-text-color-999"></p>'+

						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',

	'editCommentBox' : 
				'<div class="modal fade alert-box aw-edit-comment-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('编辑回复') + '</h3>'+
						'</div>'+
						'<form action="' + G_BASE_URL + '/question/ajax/update_answer/answer_id-{{answer_id}}" method="post" onsubmit="return false" id="answer_edit">'+
						'<div class="modal-body">'+
							'<input type="hidden" name="attach_access_key" value="{{attach_access_key}}" />'+
							'<input type="hidden" id="answer_content" name="answer_content" />'+
							'<textarea id="editor3"></textarea>'+
							'<script type="text/javascript">'+
								'$(function (){UE.delEditor("editor3");'+
								'UE.getEditor("editor3",{'+
									(G_UPLOAD_ENABLE == 'Y' ? "toolbars:UEDITOR_CONFIG.toolbars_base" :"toolbars:UEDITOR_CONFIG.toolbars_answer_base")+
									",initialFrameHeight:320"+
								"})　});"+
								'function fwb_save(){'+
										'$("#answer_content").val(UE.getEditor("editor3").getContent());'+
									'}'+
							'</script>'+
//							'<div class="aw-file-upload-box">'+
//								'<span id="file_uploader_answer_edit"></span>'+
//							'</div>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<span><input id="aw-do-delete" type="checkbox" value="1" name="do_delete" /><label for="aw-do-delete">' + _t('删除回复') + '</label></span>'+
							'<button class="btn btn-large btn-success" onclick="fwb_save();ajax_post($(\'#answer_edit\'), _ajax_post_alert_processer);return false;">' + _t('确定') + '</button>'+
						'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>',
	'addCommentBox' : 
				'<div class="modal fade alert-box aw-edit-comment-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('回复问题') + '</h3>'+
						'</div>'+
						'<form action="' + G_BASE_URL + '/question/ajax/save_answer/" method="post" onsubmit="return false" id="answer_add">'+
						'<div class="modal-body">'+
							'<input type="hidden" name="type" value="{{type}}" />'+
							'<input type="hidden" name="attach_access_key" value="{{attach_access_key}}" />'+
							'<input type="hidden" name="answer_content"  id="editor_reply"/>'+
								'<input type="hidden" name="question_id" value={{question_id}} />'+
								'<input type="hidden" name="post_hash" value="'+ G_POST_HASH +'" />'+
								'<input type="hidden" name="pid" value="{{pid}}" />'+
							'<textarea id="rich_text_editor" ></textarea>'+
							'<script type="text/javascript">'+
								'UE.delEditor("rich_text_editor");'+
								'UE.getEditor("rich_text_editor",{'+
									"toolbars:UEDITOR_CONFIG.toolbars_answer_base"+
									",initialFrameWidth:640"+
									",initialFrameHeight:160"+
								"});"+
								'function fwb_save(){'+
									'$("#editor_reply").val(UE.getEditor("rich_text_editor").getContent());'+
								'}'+
							'</script>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<button class="btn btn-large btn-success" onclick="fwb_save();ajax_post($(\'#answer_add\'), _ajax_post_alert_processer);return false;">' + _t('确定') + '</button>'+
						'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>',
	'articleCommentBox' :
		'<div class="aw-article-comment-box clearfix">'+
			'<form action="'+ G_BASE_URL +'/article/ajax/save_comment/" onsubmit="return false;" method="post">'+
				'<div class="aw-mod-body">'+
					'<input type="hidden" name="at_uid" value="{{at_uid}}">'+
					'<input type="hidden" name="post_hash" value="' + G_POST_HASH + '" />'+
					'<input type="hidden" name="article_id" value="{{article_id}}" />'+
					'<textarea placeholder="' + _t('写下你的评论...') + '" class="form-control" id="comment_editor" name="message" rows="2"></textarea>'+
				'</div>'+
				'<div class="aw-mod-footer">'+
					'<a href="javascript:;" onclick="ajax_post($(this).parents(\'form\'));" class="btn btn-large btn-success pull-right btn-submit">' + _t('回复') + '</a>'+
				'</div>'+
			'</form>'+
		'</div>',

	'favoriteBox' : 
			'<div class="modal hide fade alert-box aw-favorite-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('收藏') + '</h3>'+
						'</div>'+
						'<form action="' + G_BASE_URL + '/favorite/ajax/update_favorite_tag/" method="post" onsubmit="return false;">'+
						'<input type="hidden" name="answer_id" value="{{answer_id}}" />'+
							'<div class="modal-body">'+
								'<p>' + _t('添加话题标签') + ': <input type="text" name="tags" id="add_favorite_tags" class="form-control" /></p>'+
								'<p id="add_favorite_my_tags" class="hide">' + _t('常用标签') + ': </p>'+
							'</div>'+
							'<div class="modal-footer">'+
								'<a href="javascript:;" data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
								'<button href="javascript:;" class="btn btn-large btn-success" onclick="ajax_post($(this).parents(\'form\'), _ajax_post_modal_processer);">' + _t('确认') + '</button>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>',

	'questionRedirect' : 
		'<div class="modal fade alert-box aw-question-redirect-box">'+
			'<div class="modal-dialog">'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
						'<h3 class="modal-title" id="myModalLabel">' + _t('问题重定向至') + '</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p>' + _t('将问题重定向至') + '</p>'+
						'<div class="aw-question-drodpwon">'+
							'<input id="question-input" class="form-control" type="text" data-id="{{data_id}}" placeholder="' + _t('搜索问题') + '" />'+
							'<div class="aw-dropdown"><i class="aw-icon i-dropdown-triangle active"></i><p class="title">' + _t('没有找到相关结果') + '</p><ul class="aw-dropdown-list"></ul></div>'+
						'</div>'+
						'<p class="clearfix"><a href="javascript:;" class="btn btn-large btn-success pull-right" onclick="$(\'.alert-box\').modal(\'hide\');">' + _t('放弃操作') + '</a></p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>',

	'publishBox' : 
			'<div class="modal fade alert-box aw-publish-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('发起问题') + ' &nbsp;'+add_article_link+'</h3>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
							'<form action="' + G_BASE_URL + '/publish/ajax/publish_question.do" method="post" id="quick_publish" onsubmit="return false">'+
								'<input type="hidden" id="quick_publish_category_id" name="category_id" value="'+category_id+'" />'+
								'<input type="hidden" name="post_hash" value="' + G_POST_HASH + '" />'+
								'<input type="hidden" name="ask_user_id" value="{{ask_user_id}}" />'+
								'<div>'+
									'<textarea class="form-control" placeholder="' + _t('写下你的问题') + '..." name="question_content" id="quick_publish_question_content" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
									'<div class="aw-publish-suggest-question hide">'+
										'<p class="aw-text-color-999">你的问题可能已经有答案</p>'+
										'<ul class="aw-dropdown-list">'+
										'</ul>'+
									'</div>'+
								'</div>'+
								//'<p onclick="$(this).parents(\'form\').find(\'.aw-publish-box-supplement-content\').fadeIn().focus();$(this).hide();"><span class="aw-publish-box-supplement"><i class="aw-icon i-edit"></i>' + _t('补充说明') + ' »</span></p>'+
								'<input name="question_detail" type="hidden" id="question_detail_sm" >'+
								'<div class="aw-mod-head" style="padding-left:0px;height:270px">'+						
									'<textarea id="rich_text_editor_sm"  >'+
									'</textarea>'+
									'<script type="text/javascript">'+
									'function fwb_sm_save(){'+
											'$("#question_detail_sm").val(UE.getEditor("rich_text_editor_sm").getContent());'+
										'}'+
										"UE.delEditor('rich_text_editor_sm');UE.getEditor('rich_text_editor_sm',{ toolbars:UEDITOR_CONFIG.toolbars_question_base,initialFrameHeight:230});"+			
									'</script>'+

									'<p class="aw-text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>'+
								'</div>'+yiwenyida_str+
//								'<div class="aw-publish-title-dropdown" id="quick_publish_category_chooser">'+
//									'<p class="dropdown-toggle" data-toggle="dropdown">'+
//										'<span id="aw-topic-tags-select">' + _t('选择分类') + '</span>'+
//										'<a><i class="fa fa-chevron-down"></i></a>'+
//									'</p>'+
//								'</div>'+
								'<div id="quick_publish_topic_chooser">'+
									'<span class="aw-topic-editor" data-type="publish">'+
									'<span class="aw-edit-topic"><i class="fa fa-edit"></i>' + _t('编辑话题') + '</span>'+
									'</span>'+
								'</div>'+
								'<div class="clearfix hide" id="quick_publish_captcha">'+
									'<input type="text" class="pull-left form-control" name="seccode_verify" placeholder="' + _t('验证码') + '" onfocus="$(\'#qp_captcha\').click();" />'+
									'<img id="qp_captcha" class="pull-left" onclick="this.src = \'' +G_BASE_URL + '/account/captcha/\' + Math.floor(Math.random() * 10000);" src="" />'+
								'</div>'+
							'</form>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<span class="pull-right">'+
								'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
								'<button class="btn btn-large btn-success" onclick="fwb_sm_save();ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('发起') + '</button>'+
							'</span>'+
//							'<a href="javascript:;" tabindex="-1" onclick="$(\'form#quick_publish\').attr(\'action\', \'' + G_BASE_URL + '/publish/\');document.getElementById(\'quick_publish\').submit();" class="pull-left">' + _t('高级模式') + '</a>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',

	'inbox' :
			'<div class="modal fade alert-box aw-inbox">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('新私信') + '</h3>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
							'<form action="' + G_BASE_URL + '/inbox/save.do" method="post" id="quick_publish" onsubmit="return false">'+
								'<input type="hidden" name="post_hash" value="' + G_POST_HASH + '" />'+
								'<input id="invite-input" class="form-control" type="text" placeholder="' + _t('搜索用户') + '" name="receive_name" value="{{receive_name}}" />'+
								'<div class="aw-dropdown">'+
									'<i class="aw-icon i-dropdown-triangle"></i>'+
									'<p class="title">' + _t('没有找到相关结果') + '</p>'+
									'<ul class="aw-dropdown-list">'+
									'</ul>'+
								'</div>'+
								'<textarea class="form-control" name="message" rows="3" placeholder="' + _t('私信内容...') + '"></textarea>'+
							'</form>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
							'<button class="btn btn-large btn-success" onclick="ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('发送') + '</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',
	'shareBox' : 
			'<div class="modal fade alert-box aw-share-box">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('分享') + '</h3>'+
						'</div>'+
		

						'<div class="modal-body clearfix"><h3>赶紧把这条精彩问答分享给您身边的小伙伴们吧</h3>'+
							'<div class="modal-body"><div id="ckepop" style="position:relative;top:5px;left:5px;"><a class="jiathis_button_weixin">微信</a><div style="position:relative;top:38px;left:-55px;"><a class="jiathis_button_cqq">QQ</a></div><span style="margin-left:5px"></span></div> <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1" charset="utf-8"></script>'+
								'<ul id="bdshare" class="bdshare_t bds_tools get-codes-bdshare">'+
								'{{#items}}'+
									'<li style="position:relative;left:100px;top:-20px;"><a title="' + _t('分享到') + ' {{title}}" class="{{className}}"><i class="bds"></i>{{name}}</a></li>'+
								'{{/items}}'+
								'</ul>'+
								'<script type="text/javascript" src="http://bdimg.share.baidu.com/static/js/bds_s_v2.js?cdnversion=' + new Date().getHours() + '"></script>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',
		
	'shareList' : [ //分享外网icon列表
		{'className':'bds_qzone','name':'QQ空间','title':'QQ空间'},
		{'className':'bds_tsina','name':'新浪微博','title':'新浪微博'},
		{'className':'bds_tqq','name':'腾讯微博','title':'腾讯微博'},
		//{'className':'bds_baidu-zone','name':'百度空间','title':'百度空间'},
		{'className':'bds_t163','name':'网易微博','title':'网易微博'},
		//{'className':'bds_tqf','name':'朋友网','title':'朋友网'},
		//{'className':'bds_kaixin','name':'开心网','title':'开心网'},
		{'className':'bds_renren','name':'人人网','title':'人人网'},
		{'className':'bds_douban','name':'豆瓣网','title':'豆瓣网'},
		//{'className':'bds_taobao','name':'淘宝网','title':'淘宝网'},
		//{'className':'bds_fbook','name':'Facebook','title':'Facebook'},
		//{'className':'bds_twi','name':'Twitter','title':'Twitter'}
		//{'className':'bds_ms','name':'Myspace','title':'Myspace'},
		//{'className':'bds_deli','name':'Delicious','title':'Delicious'},
		//{'className':'bds_linkedin','name':'linkedin','title':'linkedin'}
	],
	
	'editTopicBox' : 
		'<div class="aw-edit-topic-box form-inline">'+
			'<input type="text" class="form-control fa-search" id="aw_edit_topic_title" autocomplete="off"  placeholder="' + _t('请搜索选择相关话题') + '...">'+
			'<a class="btn btn-large btn-success submit-edit">' + _t('添加') + '</a>'+
			'<a class="btn btn-large btn-default close-edit">' + _t('取消') + '</a>'+
			'<div class="aw-dropdown">'+
				'<i class="aw-icon i-dropdown-triangle active"></i>'+
				'<p class="title">' + _t('没有找到相关结果') + '</p>'+
				'<ul class="aw-dropdown-list">'+
				'</ul>'+
			'</div>'+
		'</div>',
			
	'ajaxData' :
		'<div class="modal fade alert-box aw-topic-edit-note-box aw-question-edit" aria-labelledby="myModalLabel" role="dialog">'+
			'<div class="modal-dialog">'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
						'<h3 class="modal-title" id="myModalLabel">{{title}}</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p>{{data}}</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>',

	'commentBox' : 
			'<div class="aw-comment-box" id="{{comment_form_id}}">'+
				'<div class="aw-comment-list"><p align="center" class="aw-padding10"><i class="aw-loading"></i></p></div>'+
				'<form action="{{comment_form_action}}" method="post" onsubmit="return false">'+
					'<div class="aw-comment-box-main">'+
						'<textarea class="aw-comment-txt form-control" rows="2" name="message" placeholder="' + _t('评论一下') + '..."></textarea>'+
						'<div class="aw-comment-box-btn">'+
							'<span class="pull-right">'+
								'<a href="javascript:;" class="btn btn-mini btn-success" onclick="save_comment(this);">' + _t('评论') + '</a>'+
								'<a href="javascript:;" class="btn btn-mini btn-default close-comment-box">' + _t('取消') + '</a>'+
							'</span>'+
						'</div>'+
					'</div>'+
				'</form>'+
				'<i class="i-dropdown-triangle"></i>'+
			'</div>',
			
	'commentBoxClose' : 
			'<div class="aw-comment-box" id="{{comment_form_id}}">'+
				'<div class="aw-comment-list"><p align="center" class="aw-padding10"><i class="aw-loading"></i></p></div>'+
				'<i class="i-dropdown-triangle"></i>'+
			'</div>',

	'dropdownList' : 
		'<div aria-labelledby="dropdownMenu" role="menu" class="dropdown-menu aw-category-dropdown">'+
			'<span><i class="i-dropdown-triangle"></i></span>'+
			'<ul class="aw-category-dropdown-list">'+
			'{{#items}}'+
				'<li><a data-value="{{id}}">{{title}}</a></li>'+
			'{{/items}}'+
			'</ul>'+
		'</div>',

	'reportBox' :
			'<div class="modal fade alert-box aw-share-box aw-share-box-message aw-report-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 class="modal-title" id="myModalLabel">' + _t('举报问题') + '</h3>'+
						'</div>'+
						'<form id="quick_publish" method="post" action="' + G_BASE_URL + '/question/ajax/save_report/">'+
							'<input type="hidden" name="type" value="{{item_type}}" />'+
							'<input type="hidden" name="target_id" value="{{item_id}}" />'+
							'<div class="modal-body">'+
								'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
								'<textarea class="form-control" name="reason" rows="5" placeholder="' + _t('请填写举报理由') + '..."></textarea>'+
							'</div>'+
							'<div class="modal-footer">'+
								'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
								'<button class="btn btn-large btn-success" onclick="ajax_post($(\'#quick_publish\'), _quick_publish_processer);return false;">' + _t('提交') + '</button>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>',

	'searchDropdownListQuestions' : 
		'<li class="{{active}} question clearfix"><i class="icon-g-replay pull-left"></i><a class="aw-hide-txt pull-left" href="{{url}}">{{content}} </a><span class="pull-right aw-text-color-999">{{discuss_count}} ' + _t('个回复') + '</span></li>',
	'searchDropdownListTopics' : 
		'<li class="topic clearfix"><a href="{{url}}" class="aw-topic-name" data-id="{{topic_id}}"><span>{{name}}</span></a> <span class="pull-right aw-text-color-999">{{discuss_count}} ' + _t('个问题') + '</span></li>',
	'searchDropdownListUsers' : 
		'<li class="user clearfix"><a href="{{url}}"><img src="{{img}}" />{{name}}<span class="aw-hide-txt">{{intro}}</span></a></li>',
	'searchDropdownListArticles' : 
		'<li class="question clearfix"><a class="aw-hide-txt pull-left" href="{{url}}">{{content}} </a><span class="pull-right aw-text-color-999">{{comments}} ' + _t('条评论') + '</span></li>',
	'inviteDropdownList' :
		'<li class="user"><a data-id="{{uid}}" data-value="{{name}}"><img class="img" src='+G_BASE_URL+ '{{img}} />{{name}}</a></li>',
	'editTopicDorpdownList' : 
		'<li class="question"><a>{{name}}</a></li>',
	'questionRedirectList' : 
		'<li class="question"><a class="aw-hide-txt" onclick="ajax_request({{url}})">{{name}}</a></li>',
	'questionDropdownList' : 
		'<li class="question"><a class="aw-hide-txt" href="{{url}}">{{name}}</a></li>',

	'inviteUserList' : 
		'<li>'+
			'<a class="pull-right btn btn-mini btn-default" onclick="disinvite_user($(this),{{uid}});$(this).parent().detach();">' + _t('取消邀请') + '</a>'+
			'<a class="aw-user-name" data-id="{{uid}}">'+
				'<img src="{{img}}" alt="" />'+
			'</a>'+
			'<span class="aw-text-color-666">{{name}}</span>'+
		'</li>',
	
	'voteBar' : 
		'<div class="aw-vote-bar pull-left">'+
			'<div class="vote-container">'+
				'<em class="aw-border-radius-5 aw-vote-bar-count aw-hide-txt active">{{agree_count}}</em>'+
				'<a class="aw-border-radius-5 {{up_class}}" href="javascript:;" onclick="agreeVote(this, \'{{user_name}}\', {{answer_id}})">'+
				'<i data-original-title="' + _t('赞同回复') + '" class="fa fa-arrow-up active" data-toggle="tooltip" title="" data-placement="right"></i>'+
				'<span class="vote-btn-text" >' + _t('赞同') + '</span>'+
				'</a>'+
				'<a class="aw-border-radius-5 {{down_class}}" onclick="disagreeVote(this, \'{{user_name}}\', {{answer_id}})">'+
					'<i data-original-title="' + _t('对回复持反对意见') + '" class="fa fa-arrow-down" data-toggle="tooltip" title="" data-placement="right"></i>'+
					'<span class="vote-btn-text" >' + _t('反对') + '</span>'+
				'</a>'+
			'</div>'+
		'</div>',

	'educateInsert' :
			'<td class="e1" data-txt="{{school}}">{{school}}</td>'+
			'<td class="e2" data-txt="{{departments}}">{{departments}}</td>'+
			'<td class="e3" data-txt="{{year}}">{{year}} ' + _t('年') + '</td>'+
			'<td><a class="delete-educate">' + _t('删除') + '</a>&nbsp;&nbsp;<a class="edit-educate">' + _t('编辑') + '</a></td>',

	'educateEdit' : 
			'<td><input type="text" value="{{school}}" class="school form-control"></td>'+
			'<td><input type="text" value="{{departments}}" class="departments form-control"></td>'+
			'<td><select class="edityear">'+
				'</select> ' + _t('年') + '</td>'+
			'<td><a class="delete-educate">' + _t('删除') + '</a>&nbsp;&nbsp;<a class="save-educate">' + _t('保存') + '</a></td>',

	'workInsert' : 
			'<td class="w1" data-txt="{{company}}">{{company}}</td>'+
			'<td class="w2" data-txt="{{workid}}">{{work}}</td>'+
			'<td class="w3" data-s-val="{{syear}}" data-e-val="{{eyear}}">{{syear}} ' + _t('年') + ' ' + _t('至') + ' {{eyear}}</td>'+
			'<td><a class="delete-work">' + _t('删除') + '</a>&nbsp;&nbsp;<a class="edit-work">' + _t('编辑') + '</a></td>',

	'workEidt' : 
			'<td><input type="text" value="{{company}}" class="company form-control"></td>'+
			'<td>'+
				'<select class="work editwork">'+
				'</select>'+
			'</td>'+
			'<td><select class="editsyear">'+
				'</select>&nbsp;&nbsp;' + _t('年') + ' &nbsp;&nbsp; ' + _t('至') + '&nbsp;&nbsp;&nbsp;&nbsp;'+
				'<select class="editeyear">'+
				'</select> ' + _t('年') +
			'</td>'+
			'<td><a class="delete-work">' + _t('删除') + '</a>&nbsp;&nbsp;<a class="save-work">' + _t('保存') + '</a></td>',

	'linkBox' : 
			'<div id="aw-link-box" class="modal alert-box aw-link-box fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
							'<h3 id="myModalLabel">{{title}}</h3>'+
						'</div>'+
						'<div class="modal-body">'+
							'<form id="addTxtForms">'+
								'<p>' + _t('链接文字') + '</p>'+
								'<input type="text" value="" name="{{text}}" class="link-title form-control" placeholder="'+ _t('链接文字') +'" />'+
								'<p>' + _t('链接地址') + ':</p>'+
								'<input type="text" name="{{url}}" class="link-url form-control" value="http://" />'+
							'</form>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
							'<button class="btn btn-large btn-success" data-dismiss="modal" aria-hidden="true" onclick="$.{{add_func}}($.{{add_func}});">' + _t('确定') + '</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',
	'alertImg' :
		'<div class="modal fade alert-box aw-tips-box aw-alert-img-box">'+
			'<div class="modal-dialog">'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
						'<h3 class="modal-title" id="myModalLabel">' + _t('提示信息') + '</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p class="hide {{hide}}">{{message}}</p>'+
						'<img src="{{url}}" />'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>',

	'rewards' :
		'<div class="modal fade alert-box aw-tips-box aw-alert-img-box">'+
			'<div class="modal-dialog">'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
						'<h3 class="modal-title" id="myModalLabel">' + _t('打赏积分') + '</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						 '<p>该条回答已有 <b><font style="font-size:20px;color:#5CB85C">{{rewards_answer_number}}</font></b> 人打赏,一共被打赏了 <b><font style="font-size:20px;color:#5CB85C;">{{rewards_answer_total}}</font></b> 积分</p>'+
						'<form id="reward_answer" action="?/question/ajax/rewards/" method="post" accept-charset="utf-8">'+
							'<input type="hidden" name="answer_id"       value="{{item_id}}" />'+
						 	'<input type="hidden" name="rewards_id"       value="{{rewards_id}}" />'+
                            '<input type="hidden" name="rewards_name"     value="{{rewards_name}}" />'+
							'<input type="hidden" name="give_id"          value="{{give_id}}" />'+
							'<input type="hidden" name="give_name"        value="{{give_name}}" />'+
							'<input type="hidden" name="question_id"      value="{{question_id}}" />'+
							'<input type="hidden" name="question_content" value="{{question_content}}" />'+
							'<div>'+
								'我给该条回答打赏<input class="rd-money-inp" type="text" name="give_money" onpaste="return false" onkeyup="this.value=this.value.replace(/\\D/g,\'\')" size="6" value="" placeholder="" >积分'+
								'<p>您当前可用积分是<b>{{check_user_integral}}</b>积分</p>'+
							'</div>'+
						'</form>'+
						'<script>'+
							'function rewardSubmit(){'+
								'if(/^[1-9]\\d*$/.test($(".rd-money-inp").val())){'+
									'$(\'#reward_answer\').submit();'+
								'}else{ alert("请输入正确的整数积分")}'+
							'}'+
						'</script>'+
					'</div>'+
					'<div class="modal-footer">'+
					'<span class="pull-right">'+
					'<a data-dismiss="modal" aria-hidden="true">取消</a>'+
					'<button class="btn btn-large btn-success" onclick="rewardSubmit();">赏TA</button>'+
					'</span>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'

}



function openfile()
{
	var addTxtForms=document.getElementById("addTxtForms");
     
	var  imgurl=document.getElementById("imgurl").value;

addTxtForms.target="_blank";
addTxtForms.action="/models/uploadfile.php?act=upload";
//window.open('http://ask2.iheima.com/models/uploadfile.php?imgurl='+imgurl,'win','width=300,height=300,resizable=no');
addTxtForms.submit(); 
	
	
}
function ajaxFileUpload(){
	$.ajaxFileUpload(
	{
        url:'/models/uploadfile.php?act=upload',            //需要链接到服务器地址
        secureuri:false,								// 是否安全传输
        fileElementId:'imgurl',                        //文件选择框的id属性
        dataType: 'text',                                     //服务器返回的格式，可以是json
        success: function (data, status)            //相当于java中try语句块的用法
        {     
        	$("#rich_text_editor").append('<img src="http://'+data+'">');
        	$(".modal-header .close").click();
        },
        error: function (data, status, e)            //相当于java中catch语句块的用法
        {
        	alert(data);
        }
    }

    );

}
function richInsertImg(){
	$("#rich_text_editor").append('<img src="'+$("#insert-img-inp").val()+'">');
    $(".modal-header .close").click();
}
