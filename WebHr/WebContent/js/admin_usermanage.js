
// 获取url中"?"符后参数
function getRequest() {
	var url = location.search; // 获取url中"?"符后的字串
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 获取用户第一页
function getUserFirstPage() {
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/getusercount.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				var userCount = result.rsm;
				var optInit = getOptionsFromForm();
				getUserMessage(1,10);
	            $("#Pagination").pagination(userCount, optInit);
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
	
}

//删除用户
function deleteUser(userid) {
	if (check_admin()) {
		var deleteuserisok = confirm("确定删除该用户吗？删除后，该用户提出的问题及发起的活动都会被删除！");
		if(deleteuserisok==true){
			$.get(G_BASE_URL + "/adminmanage/deleteuser.do?userid="+userid, function(result) {
				if (result.errno != 1) {
					$.alert(result.err);
				} else {
					alert("成功删除该用户!");
					window.location.href = window.location.href;//刷新
				}

			}, 'json');
		}	
	}else {
		alert("请登陆！");
	}
	
}

// 获取用户展示信息
function getUserMessage(page,pagesize) {
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/" + page + "/" + pagesize + "/"
				+ "/getuser.do", function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var userHeaderHtml = '<tr>' 
										+ '<th>用户ID</th>'
										+ '<th>邮箱</th>' 
										+ '<th>昵称</th>'
										+ '<th>注册时间</th>'
										+ '<th>操作</th>' 
										+ '</tr>';
					var userMainHtml = '';
					$.each(result.rsm.User, function(i, item) {
						userMainHtml += '<tr>' 
							    + '<td>' + item.userId + '</td>'
								+ '<td>' + item.email + '</td>' 								
								+ '<td>' + item.nickName + '</td>'
								+ '<td>' + item.createTime + '</td>'
								+ '<td><a class="link-del" href="javascript:void(0);" onclick="deleteUser('+'\''+item.userId+'\''+');">删除</a>&nbsp;&nbsp;'
								+ '<a class="link-update" href='+G_BASE_URL+'/redirect.do?page=adminUserUpdate&userid='+item.userId+'>修改</a></td>'
								+ '</tr>';
					});
					$(".result-tab").html(userHeaderHtml + userMainHtml);
				}
			}

		}, 'json');
	}else {
		alert("请登陆！");
	}
}


//修改用户信息
function userUpdate(){
	var request = new Object();
	request = getRequest();
	if (check_admin()) {
		$.get(G_BASE_URL + "/adminmanage/userinfo.do?userid="+request["userid"], function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var user = result.rsm;
					$('#userid').val(user.userId);
					$('#user_id').html(user.userId);					
					$('#email').val(user.email);
					$('#password').val(user.password);
					$('#firstname').html(user.firstName);
					$('#lastname').html(user.lastName);
					$('#phonenumber').html(user.phoneNumber);
					$('#nickname').html(user.nickName);
					$('#position').html(user.position);
					$('#workexperience').html(user.workExperience);
					$('#createtime').html(user.createTime);
				}
			}

		}, 'json');
	}else{
		alert("请登陆！");
	}
}


//分页返回函数
function pageselectCallback(page_index, jq) {
	getUserMessage(page_index+1,10);
	return false;
}

//得到分页属性
function getOptionsFromForm() {
	var opt = {
		callback : pageselectCallback,
		items_per_page : 10, // 每页显示个数
		num_display_entries : 8, // 显示分页链接个数
		current_page : 0, // 当前页
		num_edge_entries : 2, // 开始和结束显示分页链接个数
		link_to : "#",
		prev_text : "上一页",
		next_text : "下一页",
		ellipse_text : "...",
		prev_show_always : true,
		next_show_always : true
	};
	return opt;
}
