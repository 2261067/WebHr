


//注意第三个参数，最外层容器名
function getTopicsDes(i, domEle, containerName){
	var picUrl=G_PIC_BASE_URL+domEle.topic.topicImage;
var template = Hogan
							.compile(HR_TEMPLATE.question_topic_desc).render({
								'topic_id':domEle.topic.topicId,
								'topic_name' : domEle.topic.topicName,
								'topic_img' : picUrl,
//								'topic_url' : domEle.topic.topicUrl,
								'topic_url' : G_BASE_URL + '/showTopic.do?topicId=' + domEle.topic.topicId,
								'topic_question_count':domEle.topicQuestionCount,
								'topic_attention_count':domEle.topicAttentionCount,
							});

					$(containerName).append(template);
					
}

function getFirstPageTopic(i, domEle, containerName){
	
	var picUrl=G_PIC_BASE_URL+domEle.topic.topicImage;
	var template = Hogan
								.compile(HR_TEMPLATE.first_page_topic).render({
									'topic_id':domEle.topic.topicId,
									'topic_name' : domEle.topic.topicName,
									'topic_img' : picUrl,
									'topic_url' : G_BASE_URL + '/showTopic.do?topicId=' + domEle.topic.topicId,
									'topic_question_count':domEle.topicQuestionCount,
									'topic_attention_count':domEle.topicAttentionCount,
								});

						$(containerName).append(template);
						
	}

function getFocusTopic(el){
	var params= {page:1,
			
			moduleName:"",
			containerName:"#topics-desc",
			url:"getFocusTopics.do",
			type:TYPE_TOPIC_DES,
			pageControlId:"#page-control"
			};
	
	getQuestionPageList(params, params.moduleName, params.containerName, params.url, params.type);
	el.next().removeClass('aw-active');
	el.addClass('aw-active');
	$("#topicsType").html("关注的话题");
	$(params.containerName).html(" ");
}

function getHotTopic(el){
	var params= {page:1,
			
			moduleName:"",
			containerName:"#topics-desc",
			url:"getHotTopics.do",
			type:TYPE_TOPIC_DES,
			pageControlId:"#page-control"
			};
	getQuestionPageList(params, params.moduleName, params.containerName, params.url, params.type);
	el.prev().removeClass('aw-active');
	el.addClass('aw-active');
	$("#topicsType").html("热门话题");
	$(params.containerName).html(" ");
}

function getFirstPageTopics(){
	var params= {page:1,
			
			moduleName:"",
			containerName:"#hotTopics",
			url:"getHotTopics.do",
			type:0,
			};
	getQuestionPageList(params, params.moduleName, params.containerName, params.url, params.type,getFirstPageTopic);
	
}
