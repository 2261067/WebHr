//获取活动首页数据
function getActivityPageData(page) {
	$.loading("show");
	$.get(G_BASE_URL + "/ajax/getAllActivities.do", {
		page : page
	}, function(result) {
		$.loading("hide");
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			
			if (result.rsm) {
				var activityHtml = '';
				$.each(result.rsm, function(i, item) {
					var user = item.user;
					var activity = item.activity;
					var text = '发起了活动•' + item.signUserCount + '人报名';
					if (item.lastSignUser) {
						text += '•';
						text += item.lastSignUser.createTime;
						text +='有人报名';
					}
					text += '•截止日期' + activity.endTime;
					var template = Hogan.compile(HR_TEMPLATE.activity_item)
							.render({
								'user_id' : user.userId,
								'user_nick': user.nickName,
								'img' : user.photo,
								'activity_id' : activity.activityId,
								'activity_name' : activity.activityName,
								'text' : text
							});
					activityHtml += template;
				});
				if (activityHtml == '') {
					activityHtml = '暂无活动!';
				}
				$('#activity_list').html(activityHtml);
			}
		}
	}, 'json');
}
// 活动首页分页
function init_pagination() {

	$(function() {
		$.get(G_BASE_URL + "/ajax/getActivityParams.do", function(pageData) {

			if (!$("#page-control"))
				return;
			// 分页，PageCount是总条目数，这是必选参数，其它参数都是可选

			$("#page-control").pagination(pageData.rsm.allActivityCount, {
				callback : _PageCallback,
				prev_text : '上一页', // 上一页按钮里text
				next_text : '下一页', // 下一页按钮里text
				items_per_page : pageData.rsm.pageSize, // 显示条数
				num_display_entries : 6, // 连续分页主体部分分页条目数
				// current_page : params.currentPage - 1, //当前页索引 0代表第一页
				num_edge_entries : 2
			// 两侧首尾分页条目数
			});

			_PageCallback(0);
			// 翻页调用
			function _PageCallback(page_index, jq) {
				// InitTable(index);
				
				var page = page_index + 1;
				getActivityPageData(page);
				
			}

		}, 'json');
	});
}

// 活动详情页数据
function getActivityDetails() {
	$.get(G_BASE_URL + "/ajax/getActivity.do", {
		activity_id : activity_id
	}, function(result) {
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			if (result.rsm) {
				var activity = result.rsm.activity;
				$("#activity_name").html(activity.activityName);
				$("#activity_details").html(activity.activityDetail);
				$("#activity_address").html('活动地点：'+ activity.address);
				$("#activity_host_people").html('联系人：' + activity.hostPeople);
//				$("#activity_people_num").html('人数限制：' + activity.peopleNum);
				$("#activity_phone_num").html('联系方式：' + activity.phoneNum);
				$("#activity_host_time").html('活动举办时间：' + activity.hostTime);
				$("#activity_time").html('活动发起时间：' + activity.createTime);
				$("#activity_end_time").html('报名截止时间：' + activity.endTime);
				// 设置活动状态
				$("#activity_last_motify").html(activity.lastMotify);
				$("#activity_sign_count").html(result.rsm.signNum);
				// 设置活动关注按钮
				var activity_id = activity.activityId;
				var is_sign = result.rsm.isSign;
				var active = 'aw-active';
				var sign = '我要报名';
				if (is_sign) {
					active = '';
					sign = '取消报名';
				}

				var template = Hogan.compile(HR_TEMPLATE.sign_activity).render(
						{
							'activity_id' : activity_id,
							'active' : active,
							'user_id' : result.rsm.user.userId,
							'sign' : sign
						});

				$("#activity_sign").html(template);

//				设置修改按钮
				if(activity.userId == G_USER_ID){
					var template = Hogan.compile(HR_TEMPLATE.edit_activity).render();
					$('#activity_edit').html(template);
				}
				
				// 设置活动者信息，同问题页
				var template = Hogan.compile(HR_TEMPLATE.question_user).render(
						{
							'user_id' : result.rsm.user.userId,
							'user_nick':result.rsm.user.nickName,
							'user_img' : result.rsm.user.photo
						});
				$("#side_bar_user").html(template);

				var active = 'aw-active';
				var attention = '关注';
				if (result.rsm.isAttentionUser) {
					active = '';
					attention = '取消关注';
				}
				var template = Hogan.compile(
						HR_TEMPLATE.question_attention_user).render({
					'user_id' : result.rsm.user.userId,
					'user_nick':result.rsm.user.nickName,
					'active' : active,
					'attention' : attention
				});
				$("#side_bar_atttion_user").html(template);
				getSignUsers();
			}
		}
	});

}

// 加载前20个报名的人
function getSignUsers() {
	$.get(G_BASE_URL + "/ajax/getActivitySignUsers.do", {
		activity_id : activity_id
	}, function(result) {
		if (result.errno != 1) {
			$.alert(result.err);
		} else {
			if (result.rsm) {
				// 设置报名人数
				if (result.rsm.length > 0) {
					$('#sign_num').html('截止目前共有' + result.rsm.length + '人报名');
					var signHtml = '<div>';
					$.each(result.rsm, function(i, user) {
						
						var template = Hogan.compile(HR_TEMPLATE.sign_activity_user).render({
							'user_id':user.userId,
							'user_nick':user.nickName,
							'user_img':user.photo
						});
						signHtml += template;
					});
					
					signHtml += '</div>';
					$('#sign_list').html(signHtml);
				}else{
					$('#sign_num').html('还没有人报名！');
					$('#sign_list').html('');
				}
				
			}
		}

	}, 'json');
}

function edit_activity(){
	var data ={};
	data['activity_id'] = activity_id;
	data['activity_name'] = $('#activity_name').text();
	data['activity_address'] = _split($('#activity_address').text());
	data['activity_host_time'] = _split($('#activity_host_time').text());
	data['activity_end_time'] = _split($('#activity_end_time').text());
	data['activity_details'] = $('#activity_details').html();
	data['activity_host_people'] = _split($('#activity_host_people').text());
	data['activity_phone_num'] = _split($('#activity_phone_num').text());
	
	$.dialog('editActivity',data);
	
	function _split(string){
		return string.split("：")[1];
	}
}