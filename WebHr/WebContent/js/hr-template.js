var HR_TEMPLATE = {
// 未登录导航栏右上角显示
		login_register:'<span> <a href= ' + G_BASE_URL + '/html/register.html target="_blank">注册</a><a href=' + G_BASE_URL + '/html/signin.html>登录</a></span>',
// 已登录导航栏右上角显示
		already_login: '<a href= ' + G_BASE_URL + '/people.do?uid={{user_id}} class="aw-user-nav-dropdown aw-hide-txt">'
			+ '<img alt="" src='+ G_BASE_URL + '{{user_img}}>'
			+ '&nbsp' + G_USER_NICK
			+ '<span class="badge badge-important" style="display:none;"></span>'
			+ '</a>'
			+ '<div class="aw-user-nav-dropdown-list pull-right">'
			+	'<ul><li><a href=' + G_BASE_URL + '/inbox.do><i class="fa fa-envelope"></i> 私信<span class="badge badge-important" id="inbox_unread" style="display:none;"></span></a></li>'
			+		'<li class="hidden-xs"><a href='+ G_BASE_URL + '/setting.do><i class="fa fa-cog"></i> 设置</a></li> '
			+		'<li><a href=' + G_BASE_URL+ '/logout.do ><i class="fa fa-power-off"></i> 退出</a></li>'
			+	'</ul> </div>',
// 私信页每个私信对话显示
		inbox_item: '<div class="aw-item {{active}}">'
				+ '<p> <a class="aw-user-name" data-id="{{interac_id}}" href=' + G_BASE_URL + '/people.do?uid={{interac_id}}>{{interac_name}}</a> </p>'
				+ '<p class="content"><a href=' + G_BASE_URL + '/inbox/show.do?interac_id={{interac_id}}>{{content}}</a></p>'
				+ '<p class="aw-text-color-999">' 
				+ '<span class="pull-right">' 
				+ '<a href="javascript:;" class="aw-text-color-999" onclick="if (confirm(\'确认删除对话?\')) {deleteDialog(\'{{interac_id}}\'); window.location.reload(true); }">删除</a></span>'
			    + '<span>{{create_time}}</span></p>'
			    + '</div>',
// 私信详情页页首
		inbox_detail_header: '<h2><a href=' + G_BASE_URL + '/inbox.do class="pull-right">返回私信列表»</a> 私信对话：{{interac_name}}</h2>',
// 私信详情页每条私信
		inbox_detail_item: '<li class={{active}}><a href=' + G_BASE_URL+ '/people.do?uid={{from_id}} data-id="{{from_id}}"  class="aw-user-img">{{from_name}}</a> '
				 +'<div class="aw-item">'
				 +'<p> <span>{{from_name}} </span> : <a href="{{url}}"> {{content}} </a></p> <p class="aw-text-color-999">{{create_time}}</p> <i class="i-private-replay-triangle"></i> '
				 + '</div></li>',
// 私信详情页发送部分
		inbox_send:'<form action= ' + G_BASE_URL + '/inbox/save.do method="post" id="recipient_form">'
        	     +'<input type="hidden" name="receive_name" value="{{interac_name}}">'
                 +'<div class="aw-mod aw-mod-private-replay-box">'
	            + '<textarea rows="3" class="form-control" placeholder="想要对ta说点什么?" type="text" name="message"></textarea>' 
	            + '<p> <a class="btn btn-mini btn-success" href="javascript:;" onclick="ajax_post($(\'#recipient_form\'));">发送</a> '
	            + '</p> </div> </form>',
	            
// 问题详情页关注问题按钮
	    attention_question:'<a href="javascript:;" onclick="focus_question($(this), \'{{question_id}}\');"class="btn btn-normal btn-default pull-left aw-follow-question {{active}}">{{attention}}</a>',        
	    
// 问题标签
	    question_topic:'<a href=' + G_BASE_URL + '/showTopic.do?topicId={{topic_id}} data-id = {{topic_id}} class="aw-topic-name"><span>{{topic_name}}</span></a>',
	    
// 问题回答
	    question_answer:'<div class="aw-item" uninterested_count="0" force_fold="0" id="answer_list_4500">'
						+'<a class="anchor" name="answer_4500"></a>'
// <!-- 用户头像 -->
						+ '<a class="aw-user-img aw-border-radius-5 pull-right" data-id="{{user_id}}" href="' + G_BASE_URL + '/people.do?uid={{user_id}}" ><img src='+ G_BASE_URL + '{{user_img}} alt=""></a>'								
						+ '<div class="aw-mod-body clearfix">'
// 票数
						+ '<div class="aw-vote-bar pull-left"> <div class="vote-container"><em class="aw-border-radius-5 aw-vote-bar-count aw-hide-txt active">{{vote_num}}</em>'
// 赞同投票
						+ '<a class="aw-border-radius-5 " id="agree_vote" href="javascript:;" onclick="agreeVote(this,\''+ G_USER_ID + '\',\'{{answer_id}}\')"><i data-original-title="赞同回复" class="fa fa-arrow-up active" data-toggle="tooltip" title="" data-placement="right"></i><span class="vote-btn-text">赞同</span></a>'
// 反对投票
						+ '<a class="aw-border-radius-5 " id="disagree_vote" onclick="disagreeVote(this,\''+ G_USER_ID + '\',\'{{answer_id}}\')"><i data-original-title="对回复持反对意见" class="fa fa-arrow-down" data-toggle="tooltip" title="" data-placement="right"></i><span class="vote-btn-text">反对</span></a></div></div>'
						+ '<div class="pull-left aw-dynamic-topic-content"> <div class="mod-head">'
// 用户介绍
						+ '<p><a class="aw-user-name" data-id="{{user_id}}" href="{{user_url}}">{{user_nick}}</a>-<span class="aw-text-color-999">{{introduce}}</span></p>'																																						
						+ '<p class="aw-text-color-999 aw-agree-by">赞同来自: <a href="" class="aw-user-name">暂无</a></p>'
						+ '</div>'
// 回答内容
						+ '<div class="mod-body"> <div class="markitup-box">{{answer_content}}</div>'
// 添加评论
						+ '<div class="mod-footer aw-dynamic-topic-meta">'
						+ '<span class="aw-text-color-999">{{answer_time}}</span>'
						+ '<a style="color:#5CB85C" class="aw-add-comment aw-text-color-999" data-id="{{answer_id}}" data-type="answer" data-comment-count="{{comment_num}}" data-first-click="hide" href="javascript:;"><i class="fa fa-comment"></i>{{comment_text}}</a>'
						+ '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>',
//          答案评论
    answer_comment_item:'<li>'
    						+ '<a class="aw-user-name" data-id="{{user_id}}" href=' + G_BASE_URL + '/people.do?uid={{user_id}} ><img src='+ G_BASE_URL + '{{user_img}} alt=""></a>'
	                		+ '<div>'
	                			+ '<p class="clearfix">'
	                					+'<a href=' + G_BASE_URL+ '/people.do?uid={{user_id}} data-id="{{user_id}}"  class="aw-user-name author">{{user_nick}}</a>'
	                						+'• <span>{{create_time}}</span>'
	                			+'</p>'
	                			+'<p class="clearfix">{{comment}}</p>'
	                		+ '</div>'
	                	+ '</li>',
	    	
	
//    邀请回答中被邀请者显示：
	  question_invite_user: 
				'<li style="display: none;">'
					+'<a class="aw-user-img pull-left" data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
						+'<img class="img" alt="" src='+ G_BASE_URL + '{{user_img}}>'
					
					+'</a>'
					+'<div class="main">'
						+'<a class="pull-right btn btn-normal btn-default aw-active" data-value="{{user_id}}" data-id="{{user_id}}" onclick="invite_user($(this),$(this).parents(\'li\').find(\'img\').attr(\'src\'));">'
						+	'邀请'
						+'</a>'
						+'<a class="aw-user-name" data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
						+	'{{user_nick}}'
//						+	'<i class="icon-v" title="个人认证"></i>'
						+'</a>'
						+ '<p>'
						+	'在<a class="aw-topic-name" data-id="{{topic_name}}"> <span class="aw-hide-txt">{{topic_name}}</span></a>'
						+	'话题下 获得 {{vote_num}} 个赞同'
						+'</p>'	
					+'</div>'	
				+'</li>'	,
					
//				问题页面侧边栏用户显示
	question_user: '<a data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
				       +'<img class="img" alt="" src='+ G_BASE_URL + '{{user_img}}>'
				  +'</a>',
//				问题页面侧边栏关注用户：     
	question_attention_user: '<a class="aw-user-name" data-id="{{user_id}}" href=' + G_BASE_URL + '/people.do?uid={{user_id}} >{{user_nick}}</a>'
							+ '<i data-placement="bottom" title="" onclick="follow_people($(this), \'{{user_id}}\'); "data-toggle="tooltip" class="fa fa-check {{active}}" data-original-title="{{attention}}"></i>'
							,
//	问题页侧边栏问题状态
	question_status: '<ul>'
						+'<li>最新动态: <span class="aw-text-color-blue">{{last_motify}}</span></li>'
						+'<li>关注: <span class="aw-text-color-blue">{{attention_num}}</span>人</li>'
						+'<li class="aw-side-bar-user-list aw-border-radius-5" id="focus_users">'
					+'</ul>',
		
		

	                        

	        question_topic_desc : '<div class="aw-item">'
			+ '<a class="aw-topic-img aw-border-radius-5" href="{{topic_url}}" data-id="{{topic_id}}">'
			+ '<img src="{{topic_img}}" alt="" /></a>'

			+ '<div class="pull-right aw-topic-options">'
			+ '<span>{{topic_question_count}} 个问题</span> <span>{{topic_attention_count}} 个关注</span>'
			+ '</div>'

			+ '<a class="aw-topic-name" href="{{topic_url}}"'
			+ 'data-id="{{topic_id}}"><span>{{topic_name}}</span></a>'
			+ '</div>',
                        
			
//首页话题展示
			first_page_topic:'<dl><dt class="pull-left aw-border-radius-5">'
							+'<a href="{{topic_url}}"><img alt="{{topic_name}}" src="{{topic_img}}"></a></dt>'
							+'<dd class="pull-left">'
							+'<a href="{{topic_url}}" class="aw-topic-name" data-id="{{topic_id}}"><span>{{topic_name}}</span></a>'
							+'<p>该话题下有 <b>{{topic_question_count}}</b> 个问题, <b>{{topic_attention_count}}</b> 个关注</p>'
							+'</dd></dl>',

//people--用户信息展示
			     people_info:'<div class="aw-mod-head">'
							+'<img class="aw-border-radius-5 user-img" src='+ G_BASE_URL + '{{user_photo}}>'
							+'<span class="pull-right aw-user-follow-box">'
							+'<a href='+ G_BASE_URL + '/setting.do class="btn btn-mini btn-success">编辑</a></span>'
							+'<h1>{{nick_name}}</h1>'
							+'<p class="aw-text-color-999">{{user_autograph}}</p>'
							+'<p class="aw-user-flag">'
							+'<span>职位：{{user_position}} </span>' 
							+'<span>工作年限：{{user_workexperience}}</span>'
							+'<span>手机号：{{user_phonenumber}} </span>' 
							+'</p>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<div class="aw-user-center-follow-meta">'
							+'<span><i class="fa fa-thumbs-up"></i> 赞同 : <em class="aw-text-color-orange">{{user_answervotedcount}}</em></span>'
							+'</div>'
							+'</div>'
							+'<div class="aw-mod-footer">'
							+'<ul class="nav">'
							+'<li class="active"><a href="#overview" id="page_overview" data-toggle="tab">概述</a></li>'
							+'<li><a href="#questions" id="page_questions" data-toggle="tab">发问</a></li>'
							+'<li><a href="#answers" id="page_answers" data-toggle="tab">回复</a></li>'
							+'<li><a href="#actions" id="page_actions" data-toggle="tab">活动</a></li>'
							+'</ul>'
							+'</div>',	
							
//people--其他用户信息展示
			otherpeople_info:'<div class="aw-mod-head">'
							+'<img class="aw-border-radius-5 user-img" src='+ G_BASE_URL + '{{user_photo}}>'
							+'<span class="pull-right aw-user-follow-box">'
							+'<a href="javascript:;" onclick="$.dialog(\'inbox\', \'{{nick_name}}\');">发私信</a>'
							+'<a href="javascript:;" class="btn btn-mini btn-default focus {{focus}}" onclick="follow_people($(this), \'{{user_id}}\');">'
							+'{{focusTxt}}</a></span>'
							+'<h1>{{nick_name}}</h1>'
							+'<p class="aw-text-color-999">{{user_autograph}}</p>'
							+'<p class="aw-user-flag">'
							+'<span>职位：{{user_position}} </span>' 
							+'<span>工作年限：{{user_workexperience}}</span>'
							+'</p>'
							+'<p class="aw-text-color-999"></p>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<div class="aw-user-center-follow-meta">'
							+'<span><i class="fa fa-thumbs-up"></i> 赞同 : <em class="aw-text-color-orange">{{user_answervotedcount}}</em></span>'
							+'</div>'
							+'</div>'
							+'<div class="aw-mod-footer">'
							+'<ul class="nav">'
							+'<li class="active"><a href="#overview" id="page_overview" data-toggle="tab">概述</a></li>'
							+'<li><a href="#questions" id="page_questions" data-toggle="tab">发问</a></li>'
							+'<li><a href="#answers" id="page_answers" data-toggle="tab">回复</a></li>'
							+'<li><a href="#actions" id="page_actions" data-toggle="tab">活动</a></li>'
							+'</ul>'
							+'</div>',	

//people--用户关注话题展示
			people_attention_topic:'<a  class="aw-topic-name"  href=' + G_BASE_URL + '/showTopic.do?topicId={{topic_id}}><span>{{topic_name}}</span></a>',
								
//people--用户-关注人物-粉丝-展示
			people_attention_follow_user:'<a class="aw-user-name" data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
							+'<img alt="{{user_name}}" src='+ G_BASE_URL + '{{user_photo}}>'
							+'</a>',
							
//people--用户发问展示
			people_question: '<div class="aw-item">'
							+'<div class="aw-mod">'
							+'<div class="aw-mod-head">'
							+'<h4 class="aw-hide-txt">'
							+'<a href=' + G_BASE_URL + '/getQuestionById.do?questionId={{question_id}}>{{question_name}}</a>'
							+'</h4>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<a class="aw-border-radius-5 aw-user-center-count"><i class="fa fa-reply"></i>{{question_answercount}}</a>'
							+'<p class="aw-hide-txt">&nbsp;{{question_attentioncount}}个关注 •&nbsp;发问时间：{{create_time}}</p>'
							+'</div>'
							+'</div>'
							+'</div>',
							
//people--用户更多发问展示
			people_morequestion: '<div class="aw-item">'
							+'<div class="aw-mod">'
							+'<div class="aw-mod-head">'
							+'<h4 class="aw-hide-txt">'
							+'<a href=' + G_BASE_URL + '/getQuestionById.do?questionId={{question_id}}>{{question_name}}</a>'
							+'</h4>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<p class="aw-text-color-999"> &nbsp;{{question_answercount}} 个回复 &nbsp;•{{question_attentioncount}} 个关注 &nbsp;•发问时间：{{create_time}}</p>'
							+'</div>'
							+'</div>'
							+'</div>',


							
//people--用户发起活动展示
			people_activity: '<div class="aw-item">'
							+'<div class="aw-mod">'
							+'<div class="aw-mod-head">'
							+'<h4 class="aw-hide-txt">'
							+'<a href=' + G_BASE_URL + '/getActivityById.do?activity_id={{activity_id}}>{{activity_name}}</a>'
							+'</h4>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<a class="aw-border-radius-5 aw-user-center-count"><i class="fa fa-home"></i></a>'
							+'<p class="aw-hide-txt">{{activity_detail}}</p>'
							+'<p class="aw-text-color-999">&nbsp;开始时间：{{create_time}}&nbsp;&nbsp;•截止时间：{{end_time}}&nbsp;&nbsp;•报名人数{{sign_num}}</p>'
							+'</div>'
							+'</div>'
							+'</div>',
							
//people--用户回复展示
			people_answer: '<div class="aw-item">'
							+'<div class="aw-mod">'
							+'<div class="aw-mod-head">'
							+'<h4 class="aw-hide-txt">'
							+'<a href=' + G_BASE_URL + '/getQuestionById.do?questionId={{question_id}}>{{question_name}}</a>'
							+'</h4>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<a class="aw-border-radius-5 aw-user-center-count"><i class="fa fa-thumbs-up"></i>{{answer_votecount}}</a>'
							+'<p class="aw-hide-txt">{{answer_context}}</p>'
							+'<p class="aw-text-color-999">回复时间：{{answer_createtime}}</p>'
							+'</div>'
							+'</div>'
							+'</div>',
//admin--管理员话题管理表单头
		admin_topic_header:'<tr>'
	    					+'<th>话题名</th>'							
	    					+'<th>话题详细描述</th>'
	    					+'<th>发布时间</th>'
	    					+'<th>操作</th>'
	    					+'</tr>',

//admin--管理员话题管理表单
	       admin_topic_main:'<tr>'
	    				    +'<td>{{topic_name}}</td>'							
	    				    +'<td>{{topic_desc}}</td>'
	    				    +'<td>{{create_time}}</td>'
	    				    +'<td><a class="link-update" href="#">修改</a>'
	    				    +'<a class="link-del" href="#">删除</a></td>'
	    				    +'</tr>',
//		活动页发起活动
							
			'publishActivityBox' : 
					'<div class="modal fade alert-box aw-publish-box aw-activity-box">'+
						'<div class="modal-dialog">'+
							'<div class="modal-content">'+
								'<div class="modal-header">'+
									'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
									'<h3 class="modal-title" id="myModalLabel">' + _t('发起活动') + ' &nbsp;'+add_article_link+'</h3>'+
								'</div>'+
								'<div class="modal-body">'+
									'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
									'<form action="' + G_BASE_URL + '/publish/ajax/publish_activity.do" method="post" id="quick_publish" onsubmit="return false">'+
										'<div class="input-group">'+
											'<span class="input-group-addon">活动标题:</span><textarea class="form-control" name="activity_name" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
										'<div class="input-group">'+
											'<span class="input-group-addon">举办时期:</span><textarea class="form-control" name="activity_host_time" placeholder="' + _t('2015-08-10') + '" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
										'<div class="input-group">'+
											'<span class="input-group-addon">报名截止日期:</span><textarea class="form-control" name="activity_end_time"  placeholder="' + _t('2015-07-10') + '" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
										'<div class="input-group">'+
											'<span class="input-group-addon">活动地点:</span><textarea class="form-control" name="activity_address"  onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
//										'<div class="input-group">'+
//										'<span class="input-group-addon">人数限制:</span><textarea class="form-control" name="activity_people_num" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
//										'</div>'+
//										'<br/>'+
										'<div class="input-group">'+
										'<span class="input-group-addon">联系人:</span><textarea class="form-control" name="activity_host_people"  onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
										'<div class="input-group">'+
											'<span class="input-group-addon">联系方式:</span><textarea class="form-control" name="activity_phone_num"  onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
										'</div>'+
										'<br/>'+
										//'<p onclick="$(this).parents(\'form\').find(\'.aw-publish-box-supplement-content\').fadeIn().focus();$(this).hide();"><span class="aw-publish-box-supplement"><i class="aw-icon i-edit"></i>' + _t('补充说明') + ' »</span></p>'+
										'<input name="activity_detail" type="hidden" id="activity_detail_sm" >'+
										'<div class="aw-mod-head" style="padding-left:0px;height:270px">'+						
											'<textarea id="rich_text_editor_sm"  >'+
											'</textarea>'+
											'<script type="text/javascript">'+
											'function fwb_sm_save(){'+
													'$("#activity_detail_sm").val(UE.getEditor("rich_text_editor_sm").getContent());'+
												'}'+
												"UE.delEditor('rich_text_editor_sm');UE.getEditor('rich_text_editor_sm',{ toolbars:UEDITOR_CONFIG.toolbars_activity_base,initialFrameHeight:230});"+			
											'</script>'+

											'<p class="aw-text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>'+
										'</div>'+yiwenyida_str+
//													'<div class="aw-publish-title-dropdown" id="quick_publish_category_chooser">'+
//														'<p class="dropdown-toggle" data-toggle="dropdown">'+
//															'<span id="aw-topic-tags-select">' + _t('选择分类') + '</span>'+
//															'<a><i class="fa fa-chevron-down"></i></a>'+
//														'</p>'+
//													'</div>'+
//										'<div class="aw-edit-topic-box form-inline" style = "display:block;width:260px;">'+
//											'<input type="text" class="form-control" style ="width:200px !important;" name="activity_end_time" placeholder="' + _t('活动截止时间，“2015-08-10”') + '...">'+
//										'</div>'+	
										'<div class="clearfix hide" id="quick_publish_captcha">'+
											'<input type="text" class="pull-left form-control" name="seccode_verify" placeholder="' + _t('验证码') + '" onfocus="$(\'#qp_captcha\').click();" />'+
											'<img id="qp_captcha" class="pull-left" onclick="this.src = \'' +G_BASE_URL + '/account/captcha/\' + Math.floor(Math.random() * 10000);" src="" />'+
										'</div>'+
									'</form>'+
								'</div>'+
								'<div class="modal-footer">'+
									'<span class="pull-right">'+
										'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
										'<button class="btn btn-large btn-success" onclick="fwb_sm_save();ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('发起') + '</button>'+
									'</span>'+
//												'<a href="javascript:;" tabindex="-1" onclick="$(\'form#quick_publish\').attr(\'action\', \'' + G_BASE_URL + '/publish/\');document.getElementById(\'quick_publish\').submit();" class="pull-left">' + _t('高级模式') + '</a>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>',
//		修改活动：
					'editActivityBox':
							'<div class="modal fade alert-box aw-publish-box aw-activity-box">'+
								'<div class="modal-dialog">'+
									'<div class="modal-content">'+
										'<div class="modal-header">'+
											'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
											'<h3 class="modal-title" id="myModalLabel">' + _t('修改活动') + ' &nbsp;'+add_article_link+'</h3>'+
										'</div>'+
										'<div class="modal-body">'+
											'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
											'<form action="' + G_BASE_URL + '/publish/ajax/edit_activity.do" method="post" id="quick_publish" onsubmit="return false">'+
												'<textarea class="form-control hide" name="activity_id" >{{activity_id}}</textarea>' +
												'<div class="input-group">'+
													'<span class="input-group-addon">活动标题:</span><textarea class="form-control" name="activity_name" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }">{{activity_name}}</textarea>'+
												'</div>'+
												'<br/>'+
												'<div class="input-group">'+
													'<span class="input-group-addon">举办时期:</span><textarea class="form-control" name="activity_host_time" placeholder="' + _t('2015-08-10') + '" onkeydown="if (event.keyCode == 13) { return false; }" >{{activity_host_time}}</textarea>'+
												'</div>'+
												'<br/>'+
												'<div class="input-group">'+
													'<span class="input-group-addon">报名截止日期:</span><textarea class="form-control" name="activity_end_time"  onkeydown="if (event.keyCode == 13) { return false; }">{{activity_end_time}}</textarea>'+
												'</div>'+
												'<br/>'+
												'<div class="input-group">'+
													'<span class="input-group-addon">活动地点:</span><textarea class="form-control" name="activity_address" onkeydown="if (event.keyCode == 13) { return false; }">{{activity_address}}</textarea>'+
												'</div>'+
												'<br/>'+
//												'<div class="input-group">'+
//												'<span class="input-group-addon">人数限制:</span><textarea class="form-control" name="activity_people_num" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
//												'</div>'+
//												'<br/>'+
												'<div class="input-group">'+
												'<span class="input-group-addon">联系人:</span><textarea class="form-control" name="activity_host_people"  onkeydown="if (event.keyCode == 13) { return false; }">{{activity_host_people}}</textarea>'+
												'</div>'+
												'<br/>'+
												'<div class="input-group">'+
													'<span class="input-group-addon">联系方式:</span><textarea class="form-control" name="activity_phone_num"  onkeydown="if (event.keyCode == 13) { return false; }">{{activity_phone_num}}</textarea>'+
												'</div>'+
												'<br/>'+
												//'<p onclick="$(this).parents(\'form\').find(\'.aw-publish-box-supplement-content\').fadeIn().focus();$(this).hide();"><span class="aw-publish-box-supplement"><i class="aw-icon i-edit"></i>' + _t('补充说明') + ' »</span></p>'+
												'<input name="activity_detail" type="hidden" id="activity_detail_sm" >'+
												'<div class="aw-mod-head" style="padding-left:0px;height:270px">'+						
													'<textarea id="rich_text_editor_sm">{{activity_details}}'+
													'</textarea>'+
													'<script type="text/javascript">'+
													'function fwb_sm_save(){'+
															'$("#activity_detail_sm").val(UE.getEditor("rich_text_editor_sm").getContent());'+
														'}'+
														"UE.delEditor('rich_text_editor_sm');UE.getEditor('rich_text_editor_sm',{ toolbars:UEDITOR_CONFIG.toolbars_activity_base,initialFrameHeight:230});"+			
													'</script>'+

													'<p class="aw-text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>'+
												'</div>'+yiwenyida_str+
//															'<div class="aw-publish-title-dropdown" id="quick_publish_category_chooser">'+
//																'<p class="dropdown-toggle" data-toggle="dropdown">'+
//																	'<span id="aw-topic-tags-select">' + _t('选择分类') + '</span>'+
//																	'<a><i class="fa fa-chevron-down"></i></a>'+
//																'</p>'+
//															'</div>'+
//												'<div class="aw-edit-topic-box form-inline" style = "display:block;width:260px;">'+
//													'<input type="text" class="form-control" style ="width:200px !important;" name="activity_end_time" placeholder="' + _t('活动截止时间，“2015-08-10”') + '...">'+
//												'</div>'+	
												'<div class="clearfix hide" id="quick_publish_captcha">'+
													'<input type="text" class="pull-left form-control" name="seccode_verify" placeholder="' + _t('验证码') + '" onfocus="$(\'#qp_captcha\').click();" />'+
													'<img id="qp_captcha" class="pull-left" onclick="this.src = \'' +G_BASE_URL + '/account/captcha/\' + Math.floor(Math.random() * 10000);" src="" />'+
												'</div>'+
											'</form>'+
										'</div>'+
										'<div class="modal-footer">'+
											'<span class="pull-right">'+
												'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
												'<button class="btn btn-large btn-success" onclick="fwb_sm_save();ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('修改') + '</button>'+
											'</span>'+
//														'<a href="javascript:;" tabindex="-1" onclick="$(\'form#quick_publish\').attr(\'action\', \'' + G_BASE_URL + '/publish/\');document.getElementById(\'quick_publish\').submit();" class="pull-left">' + _t('高级模式') + '</a>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>',
				
					
//			活动首页每个活动
			activity_item: 
							'<div class="aw-item ">'
						   +	'<a class="aw-user-name hidden-xs" data-id="{{user_id}}" href=' + G_BASE_URL + '/people.do?uid={{user_id}} rel="nofollow"><img src='+G_BASE_URL+'{{img}} alt="{{user_id}}"></a>'
						   +	'<div class="aw-question-content">'
						   +		'<h4>'
						   +			'<a target="_blank" href='+G_BASE_URL+'/getActivityById.do?activity_id={{activity_id}}>{{activity_name}}</a>'
						   +		'</h4>'
						   +		'<p>'
						   +			'<a href=' + G_BASE_URL + '/people.do?uid={{user_id}} class="aw-user-name">{{user_nick}}</a> <span class="aw-text-color-999">{{text}}</span>'
						   +		'</p>'
						   +	'</div>'
						   +'</div>',
		// 活动详情页报名按钮
			sign_activity:'<a href="javascript:;" onclick="sign_activity($(this), \'{{activity_id}}\',\'{{user_id}}\');"class="btn btn-normal btn-default pull-left aw-follow-question {{active}}">{{sign}}</a>',        
//			活动详情页修改按钮
			edit_activity:'<a class="btn btn-normal btn-default pull-left aw-active" onclick = "edit_activity();"style="margin-left:3px;">修改</a>',
//		活动详情页报名用户
			sign_activity_user:'<span class="sign-activity-user">'
								+'<a class="aw-user-name sign-activity-user-name" data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
							       +'<img  alt="" src='+ G_BASE_URL + '{{user_img}}>'
							     +'</a>'
							     +'<a class="aw-user-name sign-activity-user-name" data-id="{{user_id}}" href=' + G_BASE_URL + '/people.do?uid={{user_id}} >{{user_nick}}</a>'
							    +'</span>' ,
							    
		    
//			专栏首页每个专栏
			article_item: 
							'<div class="aw-item ">'
						   +	'<a class="aw-user-name hidden-xs" data-id="{{user_id}}" href=' + G_BASE_URL + '/people.do?uid={{user_id}} rel="nofollow"><img src='+G_BASE_URL+'{{user_img}} alt="{{user_id}}"></a>'
						   +	'<div class="aw-question-content">'
						   +		'<h4>'
						   +			'<a target="_blank" href='+G_BASE_URL+'/getArticleById.do?article_id={{article_id}}>{{article_name}}</a>'
						   +		'</h4>'
						   +		'<p>'
						   +			'<a href=' + G_BASE_URL + '/people.do?uid={{user_id}} class="aw-user-name">{{user_nick}}</a> <span class="aw-text-color-999">{{text}}</span>'
						   +		'</p>'
						   +	'</div>'
						   +'</div>',
						   
//			专栏详情页修改按钮
			edit_article:'<a class="btn btn-normal btn-default pull-left aw-active" onclick = "edit_article();"style="margin-left:3px;width:70px;">修改</a>',
							
//			专栏评论框	
			article_comment:
							'<form action="' + G_BASE_URL + '/ajax/saveArticleComments.do"  onsubmit="return false;" method="post" id="answer_form">'
							+'<input type="hidden" name="article_id" value="{{article_id}}">'
							+'<div class="aw-mod-head">'
							+'<a href=' + G_BASE_URL + '/people.do?uid={{user_id}} class="aw-user-name"><img src='+G_BASE_URL+'{{user_img}} alt="{{user_id}}"></a>'
							+'</div>'
							+'<div class="aw-mod-body">'
							+'<textarea rows="2" name="article_context" id="comment_editor" class="form-control autosize" placeholder="写下你的评论..." '
							+'style="overflow: hidden; word-wrap: break-word; resize: none; height: 54px;"></textarea>'
							+'</div>'
							+'<div class="aw-mod-footer">'
							+'<a href="javascript:;" onclick="ajax_post($(\'#answer_form\'));" class="btn btn-large btn-success pull-right btn-submit">回复</a>'
							+'</div>'
							+'</form>',
//		    专栏评论显示
			article_comment_show:
							 '<div class="aw-item">'
							+'<!-- 用户头像 -->'
							+'<a class="aw-user-img aw-border-radius-5 pull-right" href=' + G_BASE_URL + '/people.do?uid={{user_id}}> <img src='+G_BASE_URL+'{{user_img}} alt="{{user_id}}"></a>'
							+'<!-- end 用户头像 -->'
							+'<div class="aw-mod-body clearfix">'
							+'<div class="pull-left aw-dynamic-topic-content">'
							+'<div class="mod-head">'
							+'<p><a class="aw-user-name">评论人昵称：{{user_nickname}}</a></p>'												
							+'</div>'
							+'<div class="mod-body">'
							+'<!-- 评论内容 -->'
							+'<div class="markitup-box">{{comment_context}}'
							+'<!-- end 评论内容 -->'
							+'</div>'
							+'<!-- 社交操作 -->'
							+'<div class="mod-footer aw-dynamic-topic-meta">'
							+'<span class="aw-text-color-999">评论时间：{{create_time}}</span>' 																							
							+'</div>'
							+'<!-- end 社交操作 -->'
							+'</div>'
							+'</div>'
							+'</div>'
							+'</div>',
//		     专栏页发起专栏						
					'publishArticleBox' : 
							'<div class="modal fade alert-box aw-publish-box aw-activity-box">'+
								'<div class="modal-dialog">'+
									'<div class="modal-content">'+
										'<div class="modal-header">'+
											'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
											'<h3 class="modal-title" id="myModalLabel">' + _t('发起专栏') + ' &nbsp;'+add_article_link+'</h3>'+
										'</div>'+
										'<div class="modal-body">'+
											'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
											'<form action="' + G_BASE_URL + '/publish/ajax/publish_article.do" method="post" id="quick_publish" onsubmit="return false">'+
												'<div class="input-group">'+
													'<span class="input-group-addon">文章标题:</span><textarea class="form-control" name="article_name" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }"></textarea>'+
												'</div>'+
												'<br/>'+
												'<input name="article_context" type="hidden" id="article_detail_sm" >'+
												'<div class="aw-mod-head" style="padding-left:0px;height:270px;">'+						
													'<textarea id="rich_text_editor_sm"  >'+
													'</textarea>'+
													'<script type="text/javascript">'+
													'function fwb_sm_save(){'+
															'$("#article_detail_sm").val(UE.getEditor("rich_text_editor_sm").getContent());'+
														'}'+
														"UE.delEditor('rich_text_editor_sm');UE.getEditor('rich_text_editor_sm',{ toolbars:UEDITOR_CONFIG.toolbars_article_base,initialFrameHeight:350});"+			
													'</script>'+

													'<p class="aw-text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>'+
												'</div>'+							
												'<div class="clearfix hide" id="quick_publish_captcha">'+
													'<input type="text" class="pull-left form-control" name="seccode_verify" placeholder="' + _t('验证码') + '" onfocus="$(\'#qp_captcha\').click();" />'+
													'<img id="qp_captcha" class="pull-left" onclick="this.src = \'' +G_BASE_URL + '/account/captcha/\' + Math.floor(Math.random() * 10000);" src="" />'+
												'</div>'+
											'</form>'+
										'</div>'+
										'<div class="modal-footer">'+
											'<span class="pull-right">'+
												'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
												'<button class="btn btn-large btn-success" onclick="fwb_sm_save();ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('发起') + '</button>'+
											'</span>'+
									'</div>'+
									'</div>'+
								'</div>'+
							'</div>',
									

//			修改专栏：
				'editArticleBox':
						'<div class="modal fade alert-box aw-publish-box aw-activity-box">'+
							'<div class="modal-dialog">'+
								'<div class="modal-content">'+
									'<div class="modal-header">'+
										'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
										'<h3 class="modal-title" id="myModalLabel">' + _t('修改专栏') + ' &nbsp;'+add_article_link+'</h3>'+
									'</div>'+
									'<div class="modal-body">'+
										'<div id="quick_publish_error" class="alert alert-danger hide error_message"><em></em></div>'+
										'<form action="' + G_BASE_URL + '/publish/ajax/edit_article.do" method="post" id="quick_publish" onsubmit="return false">'+
											'<textarea class="form-control hide" name="article_id" >{{article_id}}</textarea>' +
											'<div class="input-group">'+
												'<span class="input-group-addon">文章标题:</span><textarea class="form-control" name="article_name" id="quick_publish_question_activity" onkeydown="if (event.keyCode == 13) { return false; }">{{article_name}}</textarea>'+
											'</div>'+
											'<br/>'+
											'<input name="article_context" type="hidden" id="article_detail_sm" >'+
											'<div class="aw-mod-head" style="padding-left:0px;height:270px">'+						
												'<textarea id="rich_text_editor_sm">{{article_context}}'+
												'</textarea>'+
												'<script type="text/javascript">'+
												'function fwb_sm_save(){'+
														'$("#article_detail_sm").val(UE.getEditor("rich_text_editor_sm").getContent());'+
													'}'+
													"UE.delEditor('rich_text_editor_sm');UE.getEditor('rich_text_editor_sm',{ toolbars:UEDITOR_CONFIG.toolbars_article_base,initialFrameHeight:350});"+			
												'</script>'+
												'<p class="aw-text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>'+
											'</div>'+yiwenyida_str+
										'<div class="clearfix hide" id="quick_publish_captcha">'+
												'<input type="text" class="pull-left form-control" name="seccode_verify" placeholder="' + _t('验证码') + '" onfocus="$(\'#qp_captcha\').click();" />'+
												'<img id="qp_captcha" class="pull-left" onclick="this.src = \'' +G_BASE_URL + '/account/captcha/\' + Math.floor(Math.random() * 10000);" src="" />'+
											'</div>'+
										'</form>'+
									'</div>'+
									'<div class="modal-footer">'+
										'<span class="pull-right">'+
											'<a data-dismiss="modal" aria-hidden="true">' + _t('取消') + '</a>'+
											'<button class="btn btn-large btn-success" onclick="fwb_sm_save();ajax_post($(\'#quick_publish\'), _quick_publish_processer);">' + _t('修改') + '</button>'+
										'</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>',
										
						   
//用户动态页面数据模板			
            user_focus_dynamic:	   '<div class="aw-item" data-history-id="">'+'<div class="aw-item-left">'
								   +'<a class="aw-user-name aw-border-radius-5" data-id="{{user_id}}" href="{{user_url}}"><img src="{{user_img}}" title="{{user_id}}" alt="{{user_id}}" style="width:50px;height:50px"></a>'
								   +'<div class="aw-mod-body clearfix">'
								   +'<em class="aw-border-radius-5 aw-vote-count pull-left " onmouseover="insertVoteBar({element:this,agree_count:0,flag:0,user_name:{{user_id}},answer_id:{{answer_id}}})">0</em>'
                                   +'</div>'
                                   +'</div>'
                                   +'<div class="aw-item-right">'
                                   +'<div class="aw-mod-head">'
                                   +'{{title_string}} '
                                   +'{{data_time}} </span></p>	<p class="aw-text-color-999 aw-agree-by  hide">赞同来自:	</p>'
                                   +'<a class="{{if_attention}} btn btn-default btn-mini pull-right  aw-active" onclick="focus_question($(this),\'{{question_id}}\');">关注问题</a>'
                                   +'<h4><a href="{{question_url}}">{{question_title}}</a></h4>'
                                   +'</div><div class="aw-mod-body clearfix"><div class="pull-left ">'
                                   +'<div style="clear:both">'
                                   +'<div id="detail_{{data_id}}" class="markitup-box">{{data_desc_short}}				<a href="javascript:;" class="showMore hide" onclick="content_switcher($(\'#detail_{{data_id}}\'), $(\'#detail_more_{{data_id}}\'));">显示全部 »</a><br></div>'
                                   +'</div><div id="detail_more_{{data_id}}" class="hide markitup-box" style="width:650px;">{{data_desc}}			<a href="javascript:;" class="showMore" onclick="content_switcher($(\'#detail_more_{{data_id}}\'),$(\'#detail_{{data_id}}\'));"> 收起 </a>'
                                   +'</div></div></div>'
                                   +'<div class="aw-dynamic-topic-meta" style="clear:both"><a class="aw-add-comment aw-text-color-999" href="javascript:void();" data-id="6276" data-type="answer"><i class="fa fa-comment"></i>{{question_attention_count}} 人关注</a>'
                                   +'<a href="{{question_url}}" title="回答数" class="aw-text-color-999"><i class="fa fa-pencil-square"></i> {{question_answer_count}} 条回答</a>'
                                   +'<a class="hide" href="javascript:void();" title="浏览数" class="aw-text-color-999"><i class="fa fa-eye"></i> {{question_browse_count}} 人浏览</a>'
                                   +'<a data-toggle="dropdown" class="hide aw-text-color-999 dropdown-toggle" onclick="$.dialog(\'shareOut\', {item_type:\'question\', item_id:1609});"><i class="fa fa-share"></i>分享</a>'
                                   +'</div></div> </div>',	

                                   
//         话题页话题图像等显示
            topic_detail_head:'<img src=' + G_BASE_URL +'{{topic_img}} alt="创业">'
							 +'<h2 class="pull-left">{{topic_name}}</h2>'
							 +'<p class="aw-topic-detail-title-follow aw-text-color-999">'
							 +	'{{attention_user_num}} 人关注该话题<br> <a href="javascript:;"'
							 +		'onclick="focus_topic($(this), \'{{topic_id}}\');" class="btn btn-mini btn-default {{active}}">{{focus}}</a>'
							 +'</p>',
//		    话题页问题展示
			topic_detail_question:
							'<div class="aw-item active">'
								+'<a class="aw-user-name" data-id="{{user_id}}" href=' + G_BASE_URL+ '/people.do?uid={{user_id}}>'
									+'<img  alt="" src='+ G_BASE_URL + '{{user_img}}>'
								+'</a>'
								+'<div class="aw-question-content">'
								+	'<h4>'
								+		'<a target="_blank" href=' + G_BASE_URL+'/getQuestionById.do?questionId={{question_id}}>{{question_name}}</a>'
								+	'</h4>'
								+	'<p>'
								+		'<a href=' + G_BASE_URL + '/people.do?uid={{last_user_id}} class="aw-user-name">{{last_user_name}}</a> <span class="aw-text-color-999">{{text}}</span>'
								+	'</p>'
								+'</div>'
							+'</div>',

              //以下模板分别对应关注用户，关注话题，关注问题三种情况                     
              user_focus_dynamic_data_topic:'<a href="{{topic_url}}" class="aw-topic-name" data-id="{{topic_id}}">{{topic_name}}</a>',
              user_focus_dynamic_data_user:'<a href="{{user_url}}" class="aw-user-name" data-id="{{user_id}}">{{nick_name}}</a>',
              user_focus_dynamic_data_question:'<a href="{{question_url}}" class="aw-user-name" data-id="{{question_id}}">关注的问题</a>',
              //end
              //关注描述模板，用上述嵌套
              user_focus_dynamic_title:'<p>{{name_list}} {{title_string}}{{data_type}}<span class=" pull-right">'
            	  ,
              searchDropdownListActivity:     	  
            	 '<li class="question clearfix"><i class="icon-g-replay pull-left"></i><a class="aw-hide-txt pull-left" href="{{url}}">{{content}} </a><span class="pull-right aw-text-color-999">{{sign_count}} ' + _t('') + '</span><a class="aw-search-result-tags">活动</a></li>',
            	 //搜索页问题展示
              search_question:'<div class="aw-item active"><p class="aw-title">'+
            		'<a href="{{question_url}}" target="_blank">{{question_title}}'+
            		'</a></p><p class="aw-hide-text aw-text-color-666">'+
            		'<a class="aw-search-result-tags">问题</a> 发问时间:{{question_create_time}}'+'</p></div>',
              search_activity:'<div class="aw-item active">'
            	  				+'<div class="aw-question-content">'
			   					+		'<h4>'
			   					+			'<a target="_blank" href='+G_BASE_URL+'/getActivityById.do?activity_id={{activity_id}}>{{activity_name}}</a>'
			   					+		'</h4>'
			   					+		'<p>'
			   					+			'<a href=' + G_BASE_URL + '/people.do?uid={{user_id}} class="aw-user-name">{{nick_name}}</a> <span class="aw-text-color-999">{{text}}</span>'
			   					+		'</p>'
			   					+	'</div>'
			   					+'</p></div>',
			  normal_question: 			'<div class="aw-item" id="questionModule">'
				  					
			  						+'<div class="aw-question-content">'
			  						+'<h4><a target="_blank" href="{{question_url}}">{{question_name}}</a></h4>'
			  						+'<a class=" btn btn-default btn-mini pull-right " onclick="focus_question($(this),\'{{question_id}}\');">取消关注</a>'
			  						+'<p><a href="#" class="aw-user-name" data-id=""></a> <span class="aw-text-color-999">  提问者<a class="aw-user-name hidden-xs" data-id="{{user_id}}" href="{{user_url}}">{{nick_name}}</a>• {{question_focus_count}} 人关注 • {{question_answer_count}}个回复 •  {{last_modify_time}}  </span></p></div>'
			  						+'<div class="aw-state aw-state-jian"></div></div>',
			  invited_question: 	'<div class="aw-item" id="questionModule">'
					  					
				  						+'<div class="aw-question-content">'
				  						+'<h4><a target="_blank" href="{{question_url}}">{{question_name}}</a></h4>'
				  						+'<p><a href="#" class="aw-user-name" data-id=""></a> <span class="aw-text-color-999">  提问者<a class="aw-user-name hidden-xs" data-id="{{user_id}}" href="{{user_url}}">{{nick_name}}</a>• {{question_focus_count}} 人关注 • {{question_answer_count}}个回复 •  {{last_modify_time}}  </span></p></div>'
				  						+'<div class="aw-state aw-state-jian"></div></div>',
				  						
				  						
			  search_user:	'<div class="aw-item">'
								+'<a href="' + G_BASE_URL + '/people.do?uid={{user_id}}" class="aw-user-name aw-user-img" data-id="{{user_id}}"><img src="{{user_img}}" alt=""></a>'+'<p class="aw-title"><a href="' + G_BASE_URL + '/people.do?uid={{user_id}}" target="_blank">{{nick_name}}</a></p>'
								+'<p class="aw-hide-text aw-text-color-666">'
								+'<a class="aw-search-result-tags">用户</a>姓名:{{user_name}}      • 昵称:{{nick_name}}</p>'
								+'<a href="javascript:;" onclick="follow_people($(this), 2901);" class="hide btn btn-mini btn-default aw-btn-follow">取消关注</a></div>',
			   search_topic : '<div class="aw-item">'
									+ '<a class="aw-topic-name " href="{{topic_url}}" data-id="{{topic_id}}">'
									+ '<img src="{{topic_img}}" alt="" /></a>'

									+ '<div class="hide aw-search-result-tags">'
									+ '<span>{{topic_question_count}} 个问题</span> <span>{{topic_attention_count}} 个关注</span>'
									+ '</div>'

									+ '<a class="aw-topic-name" href="{{topic_url}}"'
									+ 'data-id="{{topic_id}}"><span>{{topic_name}}</span></a>'
									+ '</div>',		
									
				first_page_question :				
									'<div class="aw-item">'+
									
									'<a class="aw-user-name hidden-xs" data-id="{{user_id}}" href="javascript:;"><img src="{{user_img}}" alt="匿名用户" title="匿名用户"></a>'+
									'<div class="aw-question-content"><h4><a class="hr-question-tag {{tag_hide}}">问题</a><a target="_blank" href="{{question_url}}">{{question_name}}</a><img class="aw-state-hot {{type_hide}}" src="{{type_img}}"></h4>'+

									'<div class="pull-right hidden-xs contribute hide">'+
									'<span>贡献:</span> <a class="aw-user-name" data-id="" href="#" rel="nofollow"><img src=""'+
									'alt="@way"></a> <a class="aw-user-name" data-id="" href="#" rel="nofollow"><img src="" alt="sispher"></a>'+
									'</div><p> <span class="aw-text-color-999">{{answer_user}}{{answer_user_text}} {{focus_count}}人关注 • {{answer_count}} 个回复 • {{last_modify_time}}</span>'+
									'</p></div>'
									,
				first_page_answer_user:
					'<a href="{{user_url}}" class="aw-user-name" data-id="{{user_id}}">{{user_name}}</a>',
}


