$(document).ready(
		function() {
			
			getOtherPeopleInfomation();//获取其它用户信息
			
			getPeopleQuestion();//用户发问展示

			getPeopleAnswer();//用户回复展示

			getPeopleActivity();//用户活动展示
			
			getPeopleAttentionUser();//用户关注人物
			
			getPeopleFollowUser();//用户粉丝
			
			getPeopleAttentionTopic();//用户关注话题
			
			bp_more_load_question(G_BASE_URL + '/people/question.do',
					$('#bp_user_actions_questions_more'),
					$('#contents_user_actions_questions'),1);//用户更多发问展示
			
			bp_more_load_answer(G_BASE_URL + '/people/answer.do',
					$('#bp_user_actions_answers_more'),
					$('#contents_user_actions_answers'),1);//用户更多发问展示
			
			bp_more_load_activity(G_BASE_URL + '/people/activity.do',
					$('#bp_user_actions_activiy_more'),
					$('#contents_user_actions_activity'),1);//用户更多发问展示

			if (window.location.hash) {
				if (document.getElementById(window.location.hash.replace('#',
						''))) {
					document.getElementById(
							window.location.hash.replace('#', '')).click();
				}
			}

			$('.aw-tabs li').click(
					function() {
						$(this).addClass('active').siblings().removeClass(
								'active');

						$('#focus .aw-user-center-follow-mod').eq(
								$(this).index()).show().siblings().hide();
					});
		});