//过滤html
function removeHTMLTag(str) {
str = str.replace(/<\/?[^>]*>/g,''); //去除HTML tag
str = str.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
str=str.replace(/&nbsp;/ig,'');//去掉&nbsp;
return str;
}

//获取url中"?"符后参数
function getRequest() {
  var url = location.search; //获取url中"?"符后的字串
   var theRequest = new Object();
   if (url.indexOf("?") != -1) {
      var str = url.substr(1);
      strs = str.split("&");
      for(var i = 0; i < strs.length; i ++) {
         theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
      }
   }
   return theRequest;
}

//获取otherpeople基本信息
function getOtherPeopleInfomation() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/information.do?uid="+request["uid"], function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var userInfo = result.rsm.UserInfo;
					var isAttention = result.rsm.IsAttention;
					var uerAnswerVotedCount = result.rsm.UserAnswerVotedCount;
					var otherpeopleInfoHtml = '';
					var focus,focusTxt;
					if(isAttention == "YES"){
						focus = "";
						focusTxt = "取消关注";
					}else{
						focus = "aw-active";
						focusTxt = "关注";
					}	
					var template = Hogan.compile(
							HR_TEMPLATE.otherpeople_info).render({
								'user_photo' : userInfo.photo,
								'user_id' : userInfo.userId,
								'nick_name' : userInfo.nickName,
								'user_autograph' : userInfo.autograph,
								'user_position' : userInfo.position,
								'user_workexperience' : userInfo.workExperience,
								'focus' : focus,
								'focusTxt' : focusTxt,
								'user_answervotedcount' : uerAnswerVotedCount
					});
					otherpeopleInfoHtml += template;
					$('.aw-user-detail-box').html(otherpeopleInfoHtml);
				}
			}

		}, 'json');
	}
}

//获取people基本信息
function getPeopleInfomation() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/information.do?uid="+request["uid"], function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var userInfo = result.rsm.UserInfo;
					var uerAnswerVotedCount = result.rsm.UserAnswerVotedCount;
					var peopleInfoHtml = '';
					var template = Hogan.compile(
							HR_TEMPLATE.people_info).render({
						'user_photo' : userInfo.photo,
						'nick_name' : userInfo.nickName,
						'user_autograph' : userInfo.autograph,
						'user_position' : userInfo.position,
						'user_phonenumber' : userInfo.phoneNumber,
						'user_workexperience' : userInfo.workExperience,
						'user_answervotedcount' : uerAnswerVotedCount
					});

					peopleInfoHtml += template;
					$('.aw-user-detail-box').html(peopleInfoHtml);
				}
			}

		}, 'json');
	}
}


// 获取用户发问展示信息
function getPeopleQuestion() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/question.do?page=1&uid="+request["uid"], function(result) {
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var questionAttentionCounts = result.rsm.QuestionAttentionCounts;
					var questionAnswerCounts = result.rsm.QuestionAnswerCounts;
					var questionHtml = '';
					$.each(result.rsm.Question, function(i, item) {
						var template = Hogan.compile(
								HR_TEMPLATE.people_question).render({
							'question_id' : item.questionId,
							'question_answercount' : questionAnswerCounts[i],
							'question_attentioncount' : questionAttentionCounts[i],
							'question_name' : removeHTMLTag(item.questionName),
							'create_time' : item.createTime
						});

						questionHtml += template;
					});

					$('.aw-mod-body-question').html(questionHtml);
				}
			}

		}, 'json');
	}
}


//获取用户回复问题的展示信息
function getPeopleAnswer() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/answer.do?page=1&uid="+request["uid"], function(result) {			
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var answerHtml = '';
					$.each(result.rsm, function(i, item) {
						var question = item.question;
						var answer = item.answer;
						var template = Hogan.compile(
								HR_TEMPLATE.people_answer).render({
							'question_id' : answer.question,
							'question_name': removeHTMLTag(question.questionName),
							'answer_votecount' : answer.voteCount,
							'answer_context' : removeHTMLTag(answer.answerContext),
							'answer_createtime' : answer.createTime
						});

						answerHtml +=template;
					});
					$('.aw-mod-body-answer').html(answerHtml);
				}
			}

		}, 'json');
	}
}

//获取用户发起活动的展示信息
function getPeopleActivity() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/activity.do?page=1&uid="+request["uid"], function(result) {			
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var activitySignCounts = result.rsm.ActivitySignCounts;
					var activity = result.rsm.Activity;
					var activityHtml = '';
					$.each(activity, function(i, item) {
						var template = Hogan.compile(
								HR_TEMPLATE.people_activity).render({
						'activity_id' : item.activityId,
						'activity_name' : removeHTMLTag(item.activityName),
						'activity_detail' :removeHTMLTag(item.activityDetail),
						'create_time' : item.createTime,
						'end_time' : item.endTime,
						'sign_num' : activitySignCounts[i]
						});
						activityHtml += template;
					});

					$('.aw-mod-body-activity').html(activityHtml);
				}
			}

		}, 'json');
	}
}

//获取用户关注人物列表
function getPeopleAttentionUser() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/attentionpeople.do?page=1&uid="+request["uid"], function(result) {			
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var attentionUserNum = result.rsm.AttentionUserNum;
					var attentionUser = result.rsm.AttentionUser;
					$('.attention-user-num').text(attentionUserNum);
					var peopleAttentionUserHtml = '';
					$.each(attentionUser, function(i, item) {
						var userInfo = item.userInfo;
						var template = Hogan.compile(
								HR_TEMPLATE.people_attention_follow_user).render({
									'user_id': userInfo.userId,
									'user_name':userInfo.firstName+userInfo.lastName,
									'user_photo':userInfo.photo
						});
						peopleAttentionUserHtml += template;
					});

					$('.attention-user').html(peopleAttentionUserHtml);
				}
			}

		}, 'json');
	}
}

//获取用户粉丝列表
function getPeopleFollowUser() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/followpeople.do?page=1&uid="+request["uid"], function(result) {			
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var followUserNum = result.rsm.FollowUserNum;
					var followUser = result.rsm.FollowUser;
					$('.follow-user-num').text(followUserNum);
					var peopleFollowUser = '';
					$.each(followUser, function(i, item) {
						var userInfo = item.userInfo;
						var template = Hogan.compile(
								HR_TEMPLATE.people_attention_follow_user).render({
									'user_id': userInfo.userId,
									'user_name':userInfo.firstName+userInfo.lastName,
									'user_photo':userInfo.photo
						});
						peopleFollowUser += template;
					});

					$('.follow-user').html(peopleFollowUser);
				}
			}

		}, 'json');
	}
}

//获取用户关注话题列表
function getPeopleAttentionTopic() {
	var request = new Object();
	request = getRequest();
	if (check_user()) {
		$.get(G_BASE_URL + "/people/attentiontopic.do?page=1&uid="+request["uid"], function(result) {			
			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var atentionTopicNum = result.rsm.AtentionTopicNum;
					var attentionTopic = result.rsm.AttentionTopic;
					$('.attention-topic-num').text(atentionTopicNum);
					var peopleAttentionTopic = '';
					$.each(attentionTopic, function(i, item) {
						var topic = item.topic;
						var template = Hogan.compile(
								HR_TEMPLATE.people_attention_topic).render({
									'topic_id': topic.topicId,
									'topic_name':topic.topicName
						});
						peopleAttentionTopic += template;
					});

					$('.aw-user-list').html(peopleAttentionTopic);
				}
			}

		}, 'json');
	}
}

//获取用户更多问题的展示信息
function bp_more_load_question(url, bp_more_o_inner, target_el, start_page)
{
    if (!bp_more_o_inner.attr('id'))
    {
        return false;
    }

    _bp_more_pages[bp_more_o_inner.attr('id')] = start_page;

    _bp_more_o_inners[bp_more_o_inner.attr('id')] = bp_more_o_inner.html();

    bp_more_o_inner.unbind('click');

    bp_more_o_inner.bind('click', function ()
    {
        var _this = this;
        $(this).addClass('loading');

        $(this).find('span').html(_t('正在载入') + '...');
        
        var request = new Object();
    	request = getRequest();
        $.get(url + '?page=' + _bp_more_pages[bp_more_o_inner.attr('id')]+"&uid="+request["uid"], function (result)
        {
        	if (result.errno != 1) {
				$.alert(result.err);
			} 
        	else{
        		if (result.rsm.Question.length != 0)
        		{
        			var questionAttentionCounts = result.rsm.QuestionAttentionCounts;
					var questionAnswerCounts = result.rsm.QuestionAnswerCounts;
        			var moreQuestionHtml = '';
					$.each(result.rsm.Question, function(i, item) {
						var template = Hogan.compile(
								HR_TEMPLATE.people_morequestion).render({
							'question_id' : item.questionId,
							'question_answercount' : questionAnswerCounts[i],
							'question_attentioncount' : questionAttentionCounts[i],
							'question_name' : removeHTMLTag(item.questionName),
							'create_time' : item.createTime
						});
						moreQuestionHtml += template;
					});
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
        			{
        				target_el.html(moreQuestionHtml);
        			}
        			else
        			{
        				target_el.append(moreQuestionHtml);
        			}

        			_bp_more_pages[bp_more_o_inner.attr('id')]++;
        			
        			$(_this).html(_bp_more_o_inners[bp_more_o_inner.attr('id')]);
        		}
        		else
        		{
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
                	{
        				target_el.html('<p style="padding: 15px 0" align="center">' + _t('没有内容') + '</p>');
                	}

        			$(_this).addClass('disabled').unbind('click').bind('click', function () { return false; });
                   
        			$(_this).find('span').html(_t('没有更多了'));
        		}
        		
        		$(_this).removeClass('loading');
        	}
        });

        return false;
    });

    if (bp_more_o_inner.attr('auto-load') != 'false')
    {
        bp_more_o_inner.click();
    }
}



//获取用户更多回复的展示信息
function bp_more_load_answer(url, bp_more_o_inner, target_el, start_page)
{
    if (!bp_more_o_inner.attr('id'))
    {
        return false;
    }

    _bp_more_pages[bp_more_o_inner.attr('id')] = start_page;

    _bp_more_o_inners[bp_more_o_inner.attr('id')] = bp_more_o_inner.html();

    bp_more_o_inner.unbind('click');

    bp_more_o_inner.bind('click', function ()
    {
        var _this = this;
        $(this).addClass('loading');

        $(this).find('span').html(_t('正在载入') + '...');
        var request = new Object();
    	request = getRequest();
        $.get(url + '?page=' + _bp_more_pages[bp_more_o_inner.attr('id')]+"&uid="+request["uid"], function (result)
        {
        	if (result.errno != 1) {
				$.alert(result.err);
			} 
        	else{
        		if (result.rsm.length != 0)
        		{
        			var moreAnswerHtml = '';
					$.each(result.rsm, function(i, item) {
						var question = item.question;
						var answer = item.answer;
						var template = Hogan.compile(
								HR_TEMPLATE.people_answer).render({
							'question_id' : answer.question,
							'question_name':removeHTMLTag(question.questionName),
							'answer_votecount' : answer.voteCount,
							'answer_context' : removeHTMLTag(answer.answerContext),
							'answer_createtime' : answer.createTime
						});
						moreAnswerHtml += template;
					});
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
        			{
        				target_el.html(moreAnswerHtml);
        			}
        			else
        			{
        				target_el.append(moreAnswerHtml);
        			}
        			_bp_more_pages[bp_more_o_inner.attr('id')]++;
        			
        			$(_this).html(_bp_more_o_inners[bp_more_o_inner.attr('id')]);
        		}
        		else
        		{
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
                	{
        				target_el.html('<p style="padding: 15px 0" align="center">' + _t('没有内容') + '</p>');
                	}

        			$(_this).addClass('disabled').unbind('click').bind('click', function () { return false; });
        		
        			$(_this).find('span').html(_t('没有更多了'));
        		}
        		
        		$(_this).removeClass('loading');
        	}
        });

        return false;
    });

    if (bp_more_o_inner.attr('auto-load') != 'false')
    {
        bp_more_o_inner.click();
    }
}


//获取用户更多活动的展示信息
function bp_more_load_activity(url, bp_more_o_inner, target_el, start_page)
{
    if (!bp_more_o_inner.attr('id'))
    {
        return false;
    }

    _bp_more_pages[bp_more_o_inner.attr('id')] = start_page;

    _bp_more_o_inners[bp_more_o_inner.attr('id')] = bp_more_o_inner.html();

    bp_more_o_inner.unbind('click');

    bp_more_o_inner.bind('click', function ()
    {
        var _this = this;
        $(this).addClass('loading');

        $(this).find('span').html(_t('正在载入') + '...');
        
        var request = new Object();
    	request = getRequest();
        $.get(url + '?page=' + _bp_more_pages[bp_more_o_inner.attr('id')]+"&uid="+request["uid"], function (result)
        {
        	if (result.errno != 1) {
				$.alert(result.err);
			} 
        	else{
        		if (result.rsm.Activity.length != 0)
        		{
					var activitySignCounts = result.rsm.ActivitySignCounts;
					var activity = result.rsm.Activity;
        			var moreActivityHtml = '';
					$.each(activity, function(i, item) {
						var template = Hogan.compile(
								HR_TEMPLATE.people_activity).render({
									'activity_id' : item.activityId,
									'activity_name' : removeHTMLTag(item.activityName),
									'activity_detail' :removeHTMLTag(item.activityDetail),
									'create_time' : item.createTime,
									'end_time' : item.endTime,
									'sign_num' : activitySignCounts[i]
						});
						moreActivityHtml += template;
					});
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
        			{
        				target_el.html(moreActivityHtml);
        			}
        			else
        			{
        				target_el.append(moreActivityHtml);
        			}

        			_bp_more_pages[bp_more_o_inner.attr('id')]++;
        			
        			$(_this).html(_bp_more_o_inners[bp_more_o_inner.attr('id')]);
        		}
        		else
        		{
        			if (_bp_more_pages[bp_more_o_inner.attr('id')] == start_page && $(_this).attr('auto-load') != 'false')
                	{
        				target_el.html('<p style="padding: 15px 0" align="center">' + _t('没有内容') + '</p>');
                	}

        			$(_this).addClass('disabled').unbind('click').bind('click', function () { return false; });

        			$(_this).find('span').html(_t('没有更多了'));
        		}
        		
        		$(_this).removeClass('loading');
        	}
        });

        return false;
    });

    if (bp_more_o_inner.attr('auto-load') != 'false')
    {
        bp_more_o_inner.click();
    }
}