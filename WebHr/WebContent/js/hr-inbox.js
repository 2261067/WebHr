// 获取用户私信首页的展示信息
function getAllInboxMessage() {
	if (check_user()) {
		$.get(G_BASE_URL + "/inbox/showall.do", function(result) {

			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var inboxHtml = '';
					$.each(result.rsm, function(i, item) {
						var inbox = item.inbox;
						var from_user = item.fromUser;
						var active = '';
						if (inbox.isRead == 0) {
							active = 'active';
						}
						var interac_id = inbox.fromId;
						var interac_name = from_user.nickName;
						if (interac_id == G_USER_ID) {
							interac_id = from_user.userId;
							interac_name = from_user.nickName;
							active = '';
						}
						var template = Hogan.compile(HR_TEMPLATE.inbox_item)
								.render({
									'interac_id' : interac_id,
									'interac_name' : interac_name,
									'content' : inbox.content,
									'create_time' : inbox.createTime,
									'active' : active
								});

						inboxHtml += template;
					});

					$('.aw-mod-body').html(inboxHtml);
				}
			}

		}, 'json');
	}
}

// 获取用单个对话的详情信息
function getUserInteractionMesaage() {
	var interac_id = $.getUrlParam('interac_id');
	if (interac_id) {
		$.get(G_BASE_URL + "/inbox/getUserInteraction.do", {
			interac_id : interac_id
		}, function(result) {

			if (result.errno != 1) {
				$.alert(result.err);
			} else {
				if (result.rsm) {
					var interac_name = interac_id;

					var inboxHtml = '<ul>';
					$.each(result.rsm, function(i, item) {
						var inbox = item.inbox;
						var fromUser = item.fromUser;

						var from_name = fromUser.nickName;
						interac_name = from_name;
						var active = '';
						if (inbox.fromId == G_USER_ID) {
							from_name = '我';
							active = 'active';
						}

						var url = '#';
						if (inbox.url) {
							url = G_BASE_URL + inbox.url;
						}
						var template = Hogan.compile(
								HR_TEMPLATE.inbox_detail_item).render({
							'from_id' : inbox.fromId,
							'from_name' : from_name,
							'active' : active,
							'url' : url,
							'content' : inbox.content,
							'create_time' : inbox.createTime
						});

						inboxHtml += template;
					});
					inboxHtml += '</ul>';
					$('.aw-mod-private-replay-list').html(inboxHtml);

				}

				var headerHtml = Hogan.compile(HR_TEMPLATE.inbox_detail_header)
						.render({
							'interac_name' : interac_name,
						});
				$('.aw-mod-head').html(headerHtml);
				var sendForm = Hogan.compile(HR_TEMPLATE.inbox_send).render({
					'interac_name' : interac_name,
				});
				$('#sendform').html(sendForm);

			}

		}, 'json');
	}
}

// 删除对话
function deleteDialog(interac_id) {
	$.get(G_BASE_URL + "/inbox/delete.do", {
		interac_id : interac_id
	}, function(result) {

	});
}