$(function() {

	var accountissuccess = 0;
	var useraccountisok = 0;

	var login = {
		init : function() {
			var _this = login;
			$('#username').on('focus', _this.__userNameTipsProcess);
			$('#password').on('focus', _this.__PwdTipsProcess);

			$("#adminloginform").submit(function() {

				// 检验账户是否存在
				_this.__userNameProcess();
				if (useraccountisok == 0)
					return false;

				// 检验账户密码是否匹配
				_this.__userPwdProcess();
				if (accountissuccess == 0) {
					return false;
				}
				return true;
			});

			clearInput($);
		},

		// 用户名检测
		__userNameTipsProcess : function() {
			$('#userNameError').hide();
			$('#userNameTips').hide();
			$('#userNameOk').hide();
		},

		__userNameProcess : function() {
			var _this = login;
			var account = $("#username").val();
			if ('' == account) {
				$('#userNameTips').show();
				$('#userNameError').hide();
				$('#userNameOk').hide();
			} else {
				var path = document.location.href;
				var pos = path.lastIndexOf("/");
				path = path.substring(0, pos + 1);
				var url = path + "adminlogin/" + account + "/accountcheck.do";
				$.ajax({
					async : false,
					url : url,
					success : _this.__accountcallback
				});
			}
		},

		__accountcallback : function(data) {
			if (data.errno == 1) {
				$('#userNameTips').hide();
				$('#userNameError').hide();
				$('#userNameOk').show();

				useraccountisok = 1;
			} else {
				$('#userNameTips').hide();
				$('#userNameError').show();
				$('#userNameOk').hide();
				useraccountisok = 0;
			}
		},

		// 密码检测
		__PwdTipsProcess : function() {
			$('#passwordTips').hide();
			$('#passwordError').hide();
		},

		__userPwdProcess : function() {
			var _this = login;
			var password = $("#password").val();
			var account = $("#username").val();
			if ('' == password) {
				$('#passwordTips').show();
				$('#passwordError').hide();
			} else {
				if (useraccountisok == 1) {
					var path = document.location.href;
					var pos = path.lastIndexOf("/");
					path = path.substring(0, pos + 1);
					var url = path + "adminlogin/" + account + "/" + password
							+ "/accountpwdcheck.do";
					$.ajax({
						url : url,
						async : false,
						success : _this.__accountpwdcallback
					});
				}
			}
		},

		__accountpwdcallback : function(data) {
			if (data.errno == 1) {
				$('#passwordError').hide();
				$('#passwordTips').hide();
				accountissuccess = 1;
			}else{
				$('#passwordError').show();
				$('#passwordTips').hide();
				accountissuccess = 0;
			}
		},

	}

	login.init();

});
